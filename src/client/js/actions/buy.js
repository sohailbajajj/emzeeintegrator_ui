import axios from 'axios'

export const getForm = req => async dispatch => {
    dispatch({ type: 'GET_FORM', form: undefined })
	dispatch(getProposalFormInit())
    let form = undefined

    try {
        form = (await axios.post('/api/gpf', req)).data.proposalFormElements
    } catch (err) {
        console.log(err)
    }
	dispatch(getProposalFormEnd(form))
    dispatch({ type: 'GET_FORM', form })
}

export const SUBMIT_PROPOSAL_FORM = 'SUBMIT_PROPOSAL_FORM'
export const submitProposalForm = request => {
	return dispatch => {
		dispatch(submitProposalFormInit())

		axios.post('/api/spf', request)
		.then(resp => {
			console.log('This is the getHealthProposal response -')
			console.log(resp)
			dispatch(submitProposalFormEnd(resp.data))
		}).catch(errorResponse => {
			console.log(errorResponse.response)
			dispatch(submitProposalFormEnd(errorResponse.response))
		})
	}
}
export const SUBMIT_PROPOSAL_FORM_INIT = 'SUBMIT_PROPOSAL_FORM_INIT'
export const submitProposalFormInit = () => (
	{
		type:SUBMIT_PROPOSAL_FORM_INIT,
	}
)
export const SUBMIT_PROPOSAL_FORM_END = 'SUBMIT_PROPOSAL_FORM_END'
export const submitProposalFormEnd = response => (
	{
		type:SUBMIT_PROPOSAL_FORM_END,
		response
	}
)
export const SET_CASCADING_OPTIONS = 'SET_CASCADING_OPTIONS'
export const setCascadingOptions = payload => (
	{
		type: SET_CASCADING_OPTIONS,
		payload
	}
)
export const GET_PROPOSAL_FORM_INIT = 'GET_PROPOSAL_FORM_INIT'
const getProposalFormInit = () => (
	{
		type: GET_PROPOSAL_FORM_INIT
	}
)
export const GET_PROPOSAL_FORM_END = 'GET_PROPOSAL_FORM_END'
const getProposalFormEnd = fields => (
	{
		type: GET_PROPOSAL_FORM_END,
		fields
	}
)
export const GET_VALIDATIONS = 'GET_VALIDATIONS'
export const CREATE_PROPOSAL_FORM = 'CREATE_PROPOSAL_FORM'
export const UPDATE_PROPOSAL_FORM = 'UPDATE_PROPOSAL_FORM'
export const SET_DISCOUNTED_PREMIUM = 'SET_DISCOUNTED_PREMIUM'
export const setDiscountedPremium = payload => (
	{
		type: SET_DISCOUNTED_PREMIUM,
		payload
	}
)
export const SET_PG_URL = 'SET_PG_URL'
export const setPgUrl = url => (
	{
		type: SET_PG_URL,
		payload: url
	}
)