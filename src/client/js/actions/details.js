import axios from 'axios'

export const resetDetails = () => (
	{
		type: 'RESET_DETAILS'
	}
)

export const editQuestion = question => (
	{
		type: 'EDIT_QUESTION',
		question
	}
)

export const getCities = async() => {
	let cities = []
	
	try {
		cities = (await axios.get('/api/getCities?cityName=')).data
	} catch(err) { 
		cities = []
	}
	
	return {
		type: 'GET_CITIES',
		cities 
	}
}

export const toggleDisease = member => (
	{
		type: 'TOGGLE_DISEASE',
		member
	}
)

export const toggleDiseaseForChild = child => (
	{
		type: 'TOGGLE_DISEASE_FOR_CHILD',
		child
	}
)

export const removeDiseasesChild = child => (
	{
		type: 'REMOVE_DISEASED_CHILD',
		child
	}
)

export const CHANGE_POLICY_GROUP = 'CHANGE_POLICY_GROUP'
export const changePolicyGroup = (group) => (
	{
		type: CHANGE_POLICY_GROUP,
		payload: group
	}
)
export const CHANGE_POLICY_DURATION = 'CHANGE_POLICY_DURATION'
export const changePolicyDuration = (duration) => (
	{
		type: CHANGE_POLICY_DURATION,
		payload: duration
	}
)

export const GET_QUOTES = 'GET_QUOTES'
export const getQuotes = (request) => (
	{
		type: GET_QUOTES,
		payload: request
	}
)

export const GET_QUOTES_CHILDREN = 'GET_QUOTES_CHILDREN'
export const getQuotesParents = (request) => (
	{
		type: GET_QUOTES_PARENTS,
		payload: request
	}
)

export const GET_QUOTES_PARENTS = 'GET_QUOTES_PARENTS'
export const getQuotesChildren = (request) => (
	{
		type: GET_QUOTES_CHILDREN,
		payload: request
	}
)

export const GOT_QUOTE = 'GOT_QUOTE'
export const gotQuote = (quote) => (
	{
		type: GOT_QUOTE,
		payload: quote
	}
)

export const GOT_QUOTE_PARENTS = 'GOT_QUOTE_PARENTS'
export const gotQuoteParents = (quote) => (
	{
		type: GOT_QUOTE_PARENTS,
		payload: quote
	}
)

export const GOT_QUOTE_CHILDREN = 'GOT_QUOTE_CHILDREN'
export const gotQuoteChildren = (quote) => (
	{
		type: GOT_QUOTE_CHILDREN,
		payload: quote
	}
)
export const BUY_POLICY = 'BUY_POLICY'
export const buyPolicy = policy => {
	return (dispatch, getState) => {
		if (!getState().health.policies.selectedPolicy ||
				(getState().health.policies.selectedPolicy.id != policy.id)
			) {
			dispatch(buyPolicyInit(policy))

			let request = { 
				verticalID: "1", 
				insurerID: policy.insurerId, 
				productID: policy.productId, 
				postalCode: getState().form.UserDetailsForm.values.own_pincode.value,
				cityName: getState().form.UserDetailsForm.values.own_pincode.city,
				stateName: getState().form.UserDetailsForm.values.own_pincode.state
			}

			if (getState().health.policies.selectedGroup.name == 'parents') {
				request = {
					...request,
					postalCode: getState().form.UserDetailsForm.values.parents_pincode.value,
					cityName: getState().form.UserDetailsForm.values.parents_pincode.city,
					stateName: getState().form.UserDetailsForm.values.parents_pincode.state
				}
			}

			return dispatch(getProposalForm(request))
		}
	}
}
export const UPDATE_GROUPS = 'UPDATE_GROUPS'
export const updateGroups = groups => (
	{
		type: UPDATE_GROUPS,
		payload: groups
	}
)