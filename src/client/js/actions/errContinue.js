export const addError = error => (
	{
		type: 'ADD_ERROR',
		error
	}
)

export const removeError = error => (
	{
		type: 'REMOVE_ERROR',
		error
	}
)

export const toggleContinue = _continue => (
	{
		type: 'TOGGLE_CONTINUE',
		..._continue
	}
)