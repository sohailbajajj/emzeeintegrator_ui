import { removeDiseasesChild } from './details'

export const resetHousehold = () => (
	{
		type: 'RESET_HOUSEHOLD'
	}
)

export const addMember = member => (
	{
		type: 'ADD_MEMBER',
		member
	}
)

export const editMember = member => (
	{
		type: 'EDIT_MEMBER',
		member
	}
)

export const addChild = child => (
	{
		type: 'ADD_CHILD',
		child
	}
)

export const editChild = child => (
	{
		type: 'EDIT_CHILD',
		child
	}
)

const _removeChild = child => (
	{
		type: 'REMOVE_CHILD',
		child
	}
)

export const removeChild = child => {
	return (dispatch, getState) => {
		dispatch(_removeChild(child))
		return dispatch(removeDiseasesChild(child))
	}
}