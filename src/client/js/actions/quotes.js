import axios from 'axios'

export const resetUser = () => (
	{
		type: 'RESET_USER'
	}
)

export const initUser = user => (
	{
		type: 'INIT_USER',
		user
	}
)

export const editFilter = filter => (
	{
		type: 'EDIT_FILTER',
		filter
	}
)

export const resetFilters = () => (
	{
		type: 'RESET_FILTERS'
	}
)

export const getInsurersList = async() => {
	let insurers = []

	try {
		insurers = (await axios.get('/api/ghil?id=1')).data.payload
	} catch(err) {
		insurers = []
	}
	
	return {
		type: 'GET_HEALTH_INSURERS_LIST',
		insurers
	}
}

export const toggleInsurer = insurer => (
	{
		type: 'TOGGLE_INSURER',
		insurer
	}
)

export const toggleGroup = group => (
	{
		type: 'TOGGLE_GROUP',
		group
	}
)

export const toggleCombo = group => (
	{
		type: 'TOGGLE_COMBO',
		group
	}
)

export const resetQuotes = (group = undefined) => (
	{
		type: 'RESET_QUOTES',
		group
	}
)

export const getQuotes = req => (
	{
		type: 'GET_QUOTES',
		...req
	}
)

export const gotQuote = quote => (
	{
		type: 'GOT_QUOTE',
		...quote
	}
)

export const getQuoteDetails = async(req) => {
	let details = undefined

	try {
		details = await axios.post('/api/gqd?productId='+req.productId, req.payload)
	} catch(err) { console.log(err) }
	
	return {
		type: 'GET_QUOTE_DETAILS',
		details: details && details.data || null
	}
}

export const editCart = cart => (
	{
		type: 'EDIT_CART',
		cart
	}
)