import React, { Component, Fragment } from 'react'
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import promise from 'redux-promise'
import thunk from 'redux-thunk'
import sse from './utils/sse'
import rootReducer from './reducers'
import { ThemeProvider } from 'styled-components'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { throttle } from 'lodash'
import axios from 'axios'

import { theme } from './utils/theme'
import { loadState, saveState } from './utils/persist'
import Nav from './containers/nav'
import NavRequirements from './containers/navRequirements'
import Household from './containers/household'
import Details from './containers/details'
import GetQuotes from './containers/getQuotes'
import Quotes from './containers/quotes'
import Buy from './containers/buy'
import QuoteDetails from './containers/quoteDetailsMobile'
import Footer from './components/footer'
import { addMember, resetHousehold } from './actions/household'
import { resetDetails } from './actions/details'
import { initUser, resetUser, resetQuotes } from './actions/quotes'

const store = createStore(rootReducer, window.appState || loadState(), applyMiddleware(sse,thunk, promise))

store.subscribe(throttle(() => saveState(store.getState()), 1000))

class App extends Component {

	state = {
		isMobile: false
	}

	componentWillMount() {

		if (window.appState) {
			sessionStorage.setItem('gender', JSON.stringify(window.appState.quotes.user.gender))
			axios.get('/api/getCities?cityName=').then(cities => {
				let city = cities.data.find(city => city.includes(parseInt(window.appState.quotes.user.pincode)))
				sessionStorage.setItem('city', JSON.stringify({ city: city[2], state: city[1], pincode: city[0] }))
			})
			return
		}

		let local = {
			pincode: JSON.parse(sessionStorage.getItem('city')) && JSON.parse(sessionStorage.getItem('city')).pincode,
			gender: JSON.parse(sessionStorage.getItem('gender'))
		}

		let _store = {
			pincode: store.getState().quotes.user.pincode || undefined,
			gender: store.getState().quotes.user.gender || undefined,
		}

		if (!local.pincode || !local.gender) window.location = '/'
		else this.addMember(local.gender)

		if (_store.gender && (_store.gender != local.gender)) {
			store.dispatch(resetHousehold())
			store.dispatch(resetDetails())
			store.dispatch(resetUser())
			store.dispatch(resetQuotes())
			this.addMember(local.gender)
		}

		if (_store.pincode && (_store.pincode != local.pincode)) {
			store.dispatch(resetQuotes())
		}

		store.dispatch(initUser({
			pincode: local.pincode,
			gender: local.gender
		}))

		if (sessionStorage.getItem('email') && sessionStorage.getItem('number') && sessionStorage.getItem('name')) {
			store.dispatch(initUser({
				email: sessionStorage.getItem('email'),
				number: sessionStorage.getItem('number'),
				name: sessionStorage.getItem('name')
			}))
		}
	}

	componentDidMount() {
		this.checkIfMobile()
		window.addEventListener('resize', this.checkIfMobile)
	}

	addMember = gender => {
		store.dispatch(addMember({
			gender,
			name: 'you',
			init: true,
			type: gender == 'male' ? 'husband' : 'wife'
		}))

		store.dispatch(addMember({
			gender: gender == 'male' ? 'female' : 'male',
			name: 'spouse',
			init: true,
			type: gender == 'male' ? 'wife' : 'husband',
		}))

	}

	isRequirements = path => {
		const is = stage => path.includes(stage)

		if (!is('buy') && (is('household') || (is('details') && !is('quotes')) || is('getquotes'))) {
			return true
		}
		return false
	}

	isQuoteDetailsMobile = path => {
		const is = stage => path.includes(stage)
		if (is('details') && is('quotes/details')) return true
	}

	isQuotes = path => {
		const is = stage => path.includes(stage)
		if (is('quotes') && !is('getquotes') && !is('details')) {
			return true
		} else return false
	}

	checkIfMobile = () => {
		this.setState({
			isMobile: window.innerWidth < 651 ? true : false
		})
	}

	render() {
		return (
			<Provider store={store}>
				<Router>
					<ThemeProvider theme={theme}>
						<Fragment>
							<Route render={({ location, history }) =>
								<Fragment>
									<Nav isRequirements={this.isRequirements(location.pathname)} history={history} isMobile={this.state.isMobile}
										isPurchase={location.pathname.includes('buy')} isQuotes={this.isQuotes(location.pathname)}
									/>
								</Fragment>
							} />
							<Route render={({ location }) => 
								<TransitionGroup component={null}>
									{this.isRequirements(location.pathname) &&
										<CSSTransition key={'kmdk'} classNames="fade-nav" timeout={{ enter: 500, exit: 300 }}>
											<NavRequirements location={location} isMobile={this.state.isMobile}/>
										</CSSTransition>
									}
								</TransitionGroup>
							} />
							<Route render={({ location }) =>
								<Fragment>
									<Switch>
										<Redirect exact from='/health' to='/health/household' />
										<Redirect exact from='/health/buy' to='/health/buy/details' />
									</Switch>
									<TransitionGroup component="main" 
										className={`${this.isRequirements(location.pathname) ? 'requirements' : ''}${this.isQuotes(location.pathname) ? 'quotes' : ''}${this.isQuoteDetailsMobile(location.pathname) ? 'quote-details-mobile' : ''}`}
									>
										<CSSTransition key={!location.pathname.includes('buy') ? location.key : 'same'} 
											classNames="fade" timeout={{ enter: 500, exit: 300 }}>
											<Switch location={location}>
												<Route exact path="/health/household" render={props => <Household {...props} isMobile={this.state.isMobile} />}/>} />
												<Route exact path="/health/details" render={props => <Details {...props} isMobile={this.state.isMobile}/>} />
												<Route exact path="/health/getquotes" render={props => <GetQuotes {...props} isMobile={this.state.isMobile}/>} />
												<Route exact path="/health/quotes" render={props => <Quotes {...props} isMobile={this.state.isMobile}/>} />
												<Route exact path="/health/quotes/details" render={props => <QuoteDetails {...props} isMobile={this.state.isMobile}/>} />
												<Route path="/health/buy" render={props => <Buy {...props} isMobile={this.state.isMobile} />} />
											</Switch>
										</CSSTransition>
									</TransitionGroup>
								</Fragment>
							} />
							<Footer />
						</Fragment>
					</ThemeProvider>
				</Router>
			</Provider>
		)
	}
}

export default hot(module)(App)