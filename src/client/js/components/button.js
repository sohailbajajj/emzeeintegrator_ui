import React from 'react'
import styled from 'styled-components'

let Button = styled.button`
	background: ${props => !props.outline ? props.theme.green : '#FFF'};
	border-radius: 5px;
	color:${props => props.theme.purple};
	border:${props => !props.outline ? 'none' : `solid 1px ${props.theme.purple}`};
	outline:none;
	box-sizing:border-box;
	height:${props => props.big ? '3rem' : '2.5rem'};
	width:${props => props.big ? '25rem' : '10rem'};
	font-size:${props => props.big ? '1.3rem' : '1.1rem'};
	font-family:${props => props.big && !props.medium ? 'regular' : 'medium'};
	white-space: nowrap;
	position:relative;
	cursor:pointer;
	transition:all 300ms ease;
	user-select:none;

	>label {
		user-select:none;
		position:absolute;
		top:0;
		line-height:${props => props.big ? '3rem' : '2.5rem'};
		color:#58b58d;

		&.continue {
			right:1rem;
		}

		&.back {
			left:1rem;
			line-height:${props => props.big ? '2.8rem' : '2.5rem'};
			color: ${props => props.theme.purple};
		}
	}

	&.continue {
		a {
			width:100%;
			height:100%;
			display:block;
			position:absolute;
			left:0;
			top:0;
			opacity:0;
		}
	}

	.error {
		display:block;
		width:100%;
		height:2rem;
		padding: 0.6rem 0;
		padding-top: 0.8rem;
		background-color:${props => props.theme.tooltip};
		position:absolute;
		bottom: auto;
		top: -3.9rem;
		border-radius:3px;
		color:#fff;
		text-align:center;
		font-size:0.9rem;
		line-height:1.1rem;
		transition:opacity 100ms ease;
		opacity:0;
		pointer-events: none;
		left: 0;

		label {
			${props => props.theme.absoluteCenter()}
			width: calc(100% - 2rem);
			text-align:center;
		}
	}

	&::before {
		content:'';
		width: 0; 
		height: 0; 
		border-left: 0.5rem solid transparent;
		border-right: 0.5rem solid transparent;
		border-top: 0.5rem solid ${props => props.theme.tooltip};
		position:absolute;
		left:0;
		right:0;
		margin:0 auto;
		top: -0.7rem;
		transition:opacity 100ms ease;
		opacity:0;
		pointer-events: none;
	}

	&.disable {
		background: ${props => props.theme.disable}; 
		color:${props => props.theme.copyLighter};

		>label {
			color:${props => props.theme.copyLighter};
		}

		&.error:hover {
			.error, &::before {
				opacity:1;
			}
		}

		a {
			pointer-events:none;
		}
	}

	>a {
		position:absolute;
		left:0;
		right:0;
		width:100%;
		height:100%;
	}

	@media (max-width: 650px) {
		height: 4rem;
		font-family: medium;
		font-size: 18px;
		line-height: 3.9rem;

		&.continue {
			width:100%;
			max-width: 25rem;
		}

		.continue {
			line-height: 4rem;
		}

		.back {
			line-height: 3.7rem !important;
		}

		.error {
			font-size:12px;
		}
	}

	@media (max-width: 450px) {
		max-width:100% !important;
	}
`

export default (props) => <Button {...props}>
	{props.children}
	{props.label}
	{props.continue && <label className="continue">&rarr;</label>}
	{props.back && <label className="back">&larr;</label>}
	<span className="error"><label>{props.error}</label></span>
</Button>