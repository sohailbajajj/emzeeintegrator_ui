import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form';
//import { Values } from "redux-form-website-template";
import { getInput } from '../forms/formFields'
import { getValidations } from '../forms/formValidations'
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'

export default class Address extends Component {
    componentDidMount() {
		
	}
	handleSubmit(values) { }
    render() {
        return (
            <Fragment>
                <div className="head" ref={_form => this._form = _form}>
                    <h3>Contact information</h3>
                    <p>STEP 2 of 5</p>
                 
                    <div>
                        <AddressForm fields={this.props.form} onSubmit={this.handleSubmit} forms={this.props.forms} insurer={this.props.insurer} initialValues={{
							[this.props.insurer]: {
								mobileNumber: this.props.UserDetailsForm.values.number,
								emailId: this.props.UserDetailsForm.values.email,
								pincode: this.props.UserDetailsForm.values.pincode
							}
						}} setCascadingOptions={this.props.setCascadingOptions} />
                    </div>
                </div>
            </Fragment>
        )
    }
}
class AddressForm extends Component {
    componentDidMount() {
		if (this.props.forms.AddressForm && !isEmpty(this.props.forms.AddressForm.values)) {
			this.props.handleSubmit()
		}

		if (this.props.fields) {
			this.setReadOnlyFields(this.props)
		}
    }
    componentWillReceiveProps(next) {
		if (next.forms.AddressForm && !isEmpty(next.forms.AddressForm.values)) {
			next.handleSubmit()
		}

		if (next.fields && !isEqual(next.fields, this.props.fields)) {
			this.setReadOnlyFields(next)
		}
    }
    setReadOnlyFields = props => {
		props.fields.map(field => {
			if (field.typeOfControl.toLowerCase() == 'readonly' || field.typeOfControl.toLowerCase() == 'hidden') {
				props.change(`${props.insurer}.${field.name}`, field.code || this.getSelectionValue(field))
			}
		})
	}
    getSelectionValue = field => (
		this.props.initialValues[this.props.insurer][field.name]
		||
		field.selection
		||
		'Error'
	)
    render() {
        const { handleSubmit } = this.props
        return (
            this.props.fields.map((field, index) => (
                <Field name={`${this.props.insurer}.${field.name}`} label={field.value}
                    component={getInput(field.typeOfControl)['input']}
                    type={getInput(field.typeOfControl)['type']}
                    handleSubmit={handleSubmit} key={field + index}
                    options={field.options} selection={this.getSelectionValue(field)}
                    validate={(field.selection && (field.selection != 'NA')) ? [] : getValidations(field.validationId)}
                    validations={(field.selection && (field.selection != 'NA')) ? [] : getValidations(field.validationId)} values={this.props.forms.AddressForm && this.props.forms.AddressForm.values}
                    insurer={this.props.insurer} index={index}
                    setCascadingOptions={this.props.setCascadingOptions}
                />
            ))
        )
    }
}
AddressForm = reduxForm({
	form: 'AddressForm',
	destroyOnUnmount: false,
	enableReinitialize: true,
	keepDirtyOnReinitialize: true
})(AddressForm)