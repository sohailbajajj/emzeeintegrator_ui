import React, { Component, Fragment } from 'react'
//import { Provider } from "react-redux";
import { Field, reduxForm } from 'redux-form';
//import { Values } from "redux-form-website-template";
//import store from "../react-redux-form/store";
//import showResults from "../react-redux-form/showResults";
import { getInput, fixDate } from '../forms/formFields'
import { getValidations, adharPanMatch } from '../forms/formValidations'
import capitalize from 'lodash/capitalize'
import isEmpty from 'lodash/isEmpty'

const afterStyle = {
    clear: 'both'
};
export default class Buyer extends Component {
    constructor(props) {
        super(props)
        this.members = this.props.members.map(
            member => `${member.type == 'child' ? member.name : member.type}`
        )
    }
    componentDidMount() {
		
    }
    getName = name => {
		switch (name) {
			case 'husband':
				if (this.props.gender == 'male') return 'you'
				else return name
			case 'wife':
				if (this.props.gender == 'female') return 'you'
				else return name
			default:
				return name
		}
    }
    getProposerAge = () => {
		let proposer = this.members.filter(member => this.getName(member) == 'you')[0]
		if (proposer) {
			if(proposer == this.props.members[0].type) {
                return this.props.members[0].age.value
            }
		}
	}
    handleSubmit(values) { }
    render() {
        return (
            <Fragment>
                <div className="head" ref={_form => this._form = _form}>
                    <h3>Policy Buyer's Details</h3>
                    <p>STEP 1 of 5</p>
                    <div className="row">
                        <h4 className="purple-small-text">Proposer Form</h4>
                        <ProposerForm fields={this.props.form.proposer} policy={this.props.policy} policies={this.props.policies} insurer={this.props.insurer} age={this.getProposerAge()} onSubmit={this.handleSubmit} initialValues={{
                            [this.props.insurer]: {
                                firstName: this.props.UserDetailsForm.values.name.split(' ')[0],
                                lastName: this.props.getNames,
                                salutation: this.props.gender == 'male' ? 'Mr.' : 'Mrs.',
                                gender: capitalize(this.props.gender)
                            }
                        }} forms={this.props.forms} />
                    </div>
                    <div style={afterStyle}></div>
                    <div className="row">
                        <h4 className="purple-small-text">Nominee Form</h4>
                        <NomineeForm fields={this.props.form.nominee} insurer={this.props.insurer} onSubmit={this.handleSubmit} forms={this.props.forms} />
                    </div>
                </div>
            </Fragment>
        )
    }
}
class ProposerForm extends Component {
    componentDidMount() {
		if (this.props.forms.ProposerForm && !isEmpty(this.props.forms.ProposerForm.values)) {
			this.props.handleSubmit()
		}
    }
    componentWillReceiveProps(next) {
        if (next.forms.ProposerForm && !isEmpty(next.forms.ProposerForm.values)) {
			next.handleSubmit()
		}
    }
    getValidations = (field, insurer) => {
		if (field.name.toLowerCase().includes('gst') && insurer.toLowerCase().includes('star')) {
			return [...getValidations(field.validationId), adharPanMatch(insurer)]
		}
		return field.selection ? [] : getValidations(field.validationId)
	}
    render() {
        const { handleSubmit } = this.props
        return (
            this.props.fields.map((field, index) => (
                <div key={field + index}>
                    {
                        !(
							(this.props.policy.insurerId == 5) && 
							(field.name == 'question') &&
							(this.props.policies.selectedGroup.name == 'children')
                        ) &&
                        <Field
                        name={
							field.typeOfControl == 'DropDownBoxHeight' ?
								`${this.props.insurer}` :
								`${this.props.insurer}.${field.name}`
                        }
                        component={getInput(field.typeOfControl)['input']} type={getInput(field.typeOfControl)['type']} label={field.value} handleSubmit={handleSubmit} key={field + index} options={field.options} age={this.props.age} range={true} feet={field.feet || undefined} inch={field.inch || undefined} long={field.value.length > 18 ? true : false} validate={this.getValidations(field, this.props.insurer)} validations={field.selection ? [] : getValidations(field.validationId)} normalize={fixDate(field.name)} radiovalues={this.props.forms && this.props.forms[this.props.form] && this.props.forms[this.props.form] && this.props.forms[this.props.form].values && this.props.forms[this.props.form].values[this.props.insurer] && this.props.forms[this.props.form].values[this.props.insurer].question} selectedYear={this.props.forms.ProposerForm && this.props.forms.ProposerForm.values && this.props.forms.ProposerForm.values[this.props.insurer] && this.props.forms.ProposerForm.values.ProposerForm && this.props.forms.ProposerForm.values.ProposerForm.DOB && this.props.forms.ProposerForm.values.ProposerForm.DOB.Year} selectedMonth={this.props.forms.ProposerForm && this.props.forms.ProposerForm.values && this.props.forms.ProposerForm.values[this.props.insurer] && this.props.forms.ProposerForm.values.ProposerForm && this.props.forms.ProposerForm.values.ProposerForm.DOB && this.props.forms.ProposerForm.values.ProposerForm.DOB.Month} formNameForDOB={this.props && this.props.form}
                        />
                    }
                </div>
            ))
        )
    }
}

ProposerForm = reduxForm({
	form: 'ProposerForm',
	destroyOnUnmount: false,
	enableReinitialize: true,
	keepDirtyOnReinitialize: true
})(ProposerForm)

class NomineeForm extends Component {
    componentDidMount() {
		if (this.props.forms.NomineeForm && !isEmpty(this.props.forms.NomineeForm.values)) {
			this.props.handleSubmit()
		}
    }
    componentWillReceiveProps(next) {
        if (next.forms.NomineeForm && !isEmpty(next.forms.NomineeForm.values)) {
			next.handleSubmit()
		}
    }
    render() {
        const { handleSubmit } = this.props
        return (
            this.props.fields.map((field, index) => (
                <div key={field + index}>
                    <Field 
                        name={`${this.props.insurer}.${field.name}`} component={getInput(field.typeOfControl)['input']}	type={getInput(field.typeOfControl)['type']} label={field.value} handleSubmit={handleSubmit} key={field + index} options={field.options} selection={field.selection || null} range={true} feet={field.feet || undefined} inch={field.inch || undefined} validate={field.selection ? [] : getValidations(field.validationId)} validations={field.selection ? [] : getValidations(field.validationId)} normalize={fixDate(field.name)} selectedYear={this.props.forms.NomineeForm && this.props.forms.NomineeForm.values && this.props.forms.NomineeForm.values[this.props.insurer] && this.props.forms.NomineeForm.values.NomineeForm && this.props.forms.NomineeForm.values.NomineeForm.DOB && this.props.forms.NomineeForm.values.NomineeForm.DOB.Year} selectedMonth={this.props.forms.NomineeForm && this.props.forms.NomineeForm.values && this.props.forms.NomineeForm.values[this.props.insurer] && this.props.forms.NomineeForm.values.NomineeForm && this.props.forms.NomineeForm.values.NomineeForm.DOB && this.props.forms.NomineeForm.values.NomineeForm.DOB.Month} formNameForDOB={this.props && this.props.form}
					/>
                </div>
            ))
        )
    }
}

NomineeForm = reduxForm({
	form: 'NomineeForm',
	destroyOnUnmount: false
})(NomineeForm)