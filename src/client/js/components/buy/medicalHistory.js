import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form';
//import { Values } from "redux-form-website-template";
import { renderFieldRadioMedicalHistoryNew } from '../forms/formFields'
import isEmpty from 'lodash/isEmpty'

export default class MedicalHistory extends Component {
    componentDidMount() {
		
    }
    handleSubmit(values) { }
    getInitialValues = () => {
		let init = {
			common: {}
		}

		if (this.props.policies.selectedPolicy.insurerId == 4) init.extra = {}

		this.props.fields.medicalHistory.forEach(question => {
			if (question.selectMembers && question.name) {
				init.common[`${question.name}`] = 'No'
			}

			else if (!question.selectMembers && !question.typeOfControl && question.name) {
				init.common[`${question.name}`] = 'No'
			}
        })
		return init
	}
    render() {
        return (
            <Fragment>
                <div className="head" ref={_form => this._form = _form}>
                    <h3>MedicalHistory</h3>
                    <p>STEP 4 of 5</p>
                    
                    <div>
                        <MedicalHistoryForm members={this.props.members} onSubmit={this.handleSubmit} fieldsToRender={this.props.form} initialValues={this.getInitialValues()} forms={this.props.forms} policies={this.props.policies} gender={this.props.gender} />
                    </div>
                </div>
            </Fragment>
        )
    }
}
class MedicalHistoryForm extends Component {
    componentDidMount() {
        if (this.props.forms.MedicalHistoryForm && !isEmpty(this.props.forms.MedicalHistoryForm.values)) {
			this.props.handleSubmit()
		}
    }
    componentWillReceiveProps(next) {
        if (next.forms.MedicalHistoryForm && !isEmpty(next.forms.MedicalHistoryForm.values)) {
			next.handleSubmit()
		}
    }
    render() {
        const { handleSubmit } = this.props
        let selectedValues = this.props.forms.MedicalHistoryForm && this.props.forms.MedicalHistoryForm.values ? this.props.forms.MedicalHistoryForm.values : null
        return (
            this.props.fieldsToRender.map((question, index) => (
                question.value &&
                <Field name={`${question.name}`} key={index + question.id}
                    component={renderFieldRadioMedicalHistoryNew} type="radio"
                    handleSubmit={handleSubmit} index={index + 1} members={this.props.members}
                    values={selectedValues} options={question.options || null}
                    gender={this.props.gender} selectMembers={question.selectMembers || null}
                    label={question.value} questions={question.questions || null}
                    typeOfControl={question.typeOfControl}
                    validations={question.validationId}
                    insurerId={this.props.policies.selectedPolicy.insurerId}
                    form={this.props.form.MedicalHistoryForm} change={this.props.change} formNameForDOB={this.props && this.props.form}
                />
            ))
        )
    }
}
MedicalHistoryForm = reduxForm({
	form: 'MedicalHistoryForm',
	destroyOnUnmount: false,
	forceUnregisterOnUnmount: true,
	// enableReinitialize: true
})(MedicalHistoryForm)
