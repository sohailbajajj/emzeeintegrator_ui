import React, { Component, Fragment } from 'react'
import { Field, reduxForm } from 'redux-form';
//import { Values } from "redux-form-website-template";
import { getInput, fixDate } from '../forms/formFields'
import { getValidations } from '../forms/formValidations'
import isEmpty from 'lodash/isEmpty'

export default class Members extends Component {
    constructor(props) {
        super(props)
        this.members = this.props.members.map(
            //member => `${member.type}`
            //member => `${member.counttype}${member.name ? `-${member.name}` : ''}`
            member => `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`
        )
    }
    componentDidMount() {
        
    }
    handleSubmit() { }
    getName = name => {
		switch (name) {
			case 'husband':
				if (this.props.gender == 'male') return 'you'
				else return name
			case 'wife':
				if (this.props.gender == 'female') return 'you'
				else return name
			default:
				return name
		}
    }
    getInitialValues = () => {
        let proposer = this.members.filter(member => this.getName(member) == 'you')[0]
        if (proposer) {
            return {
                [this.props.insurer]: {
                    [proposer]: {
                        firstName: this.props.UserDetailsForm.values.name.split(' ')[0],
                        lastName: this.props.getNames,//saurav values getting from props because if user changes the value on 1st form / ProposerForm so it should be reflect on second form. Currently the value has been not reflecting. And also please check this on "old project"
                        dateOfBirth: this.props.forms.ProposerForm.values[this.props.insurer].dateOfBirth || undefined,
                        occupation: this.props.forms.ProposerForm.values[this.props.insurer].occupation || undefined,
                        maritalStatus: this.props.forms.ProposerForm.values[this.props.insurer].maritalStatus || undefined,

                    }
                },
                [proposer]: {
                    ['DOB']: {
                        Year: this.props.forms.ProposerForm.values.ProposerForm && this.props.forms.ProposerForm.values.ProposerForm.DOB.Year || undefined,
                        Month: this.props.forms.ProposerForm.values.ProposerForm && this.props.forms.ProposerForm.values.ProposerForm.DOB.Month || undefined,
                        Day: this.props.forms.ProposerForm.values.ProposerForm && this.props.forms.ProposerForm.values.ProposerForm.DOB.Day || undefined
                    }
                }
            }
        }
	}
    render() {
        return (
            <Fragment>
                <div className="head" ref={_form => this._form = _form}>
                    <h3>Insured Members</h3>
                    <p>STEP 2 of 5</p>
                    
                    <div>
                        <MembersForm onSubmit={this.handleSubmit} getName={this.getName} members={this.members} fields={this.props.form} insurer={this.props.insurer} actualMembers={this.props.members} initialValues={this.getInitialValues()} forms={this.props.forms} />
                    </div>
                </div>
            </Fragment>
        )
    }
}

class MembersForm extends Component {
    componentDidMount() {
		if (this.props.forms.MembersForm && !isEmpty(this.props.forms.MembersForm.values)) {
			this.props.handleSubmit()
		}
    }
    componentWillReceiveProps(next) {
        if (next.forms.MembersForm && !isEmpty(next.forms.MembersForm.values)) {
			next.handleSubmit()
		}
    }
    getOptions = (field, member, insurer) => {
		if (!field.options) return

		let isUniversal = insurer.toLowerCase().includes('universal')
		let isOccupation = field.name.toLowerCase().includes('occupation')
		let isChild = member.toLowerCase().includes('child')
		let isStudent = option => option.name.toLowerCase().includes('student')

		if (isUniversal && isOccupation && isChild) {
			return field.options.filter(isStudent)
		}

		return field.options
	}
    render() {
        const { handleSubmit, members, fields, insurer } = this.props
        return (
            members.map((member, index) => (
                <div key={member + index} className="container-member">
                    <h4 className="member-name">
                        {
                            this.props.getName(member).includes('-') ?
                                this.props.getName(member).split('-')[1] :
                                this.props.getName(member)
                        }
                        &nbsp; (<span className="digit">{this.props.actualMembers[index].age.value} Years</span>)
                    </h4>
                    {
                        fields.map(field => (
                            field.name &&
                            <Field name={
                                field.typeOfControl == 'DropDownBoxHeight' ?
                                    `${this.props.insurer}.${member}` :
                                    `${this.props.insurer}.${member}.${field.name}`
                                }
                                key={member + index + field.name}
                                component={getInput(field.typeOfControl, field.name)['input']}
                                type={getInput(field.typeOfControl)['type']} label={field.value}
                                handleSubmit={handleSubmit} options={this.getOptions(field, member, insurer)}
                                feet={field.feet || undefined} inch={field.inch || undefined}
                                age={this.props.actualMembers[index].age.value} range={true}
                                validate={field.selection ? [] : getValidations(field.validationId)}
                                validations={field.selection ? [] : getValidations(field.validationId)}
                                normalize={fixDate(field.name)} selectedYear={this.props.forms.MembersForm && this.props.forms.MembersForm.values && this.props.forms.MembersForm.values[this.props.insurer] && this.props.forms.MembersForm.values[member] && this.props.forms.MembersForm.values[member].DOB && this.props.forms.MembersForm.values[member].DOB.Year} selectedMonth={this.props.forms.MembersForm && this.props.forms.MembersForm.values && this.props.forms.MembersForm.values[this.props.insurer] && this.props.forms.MembersForm.values[member] && this.props.forms.MembersForm.values[member].DOB && this.props.forms.MembersForm.values[member].DOB.Month} formNameForDOB={`${member}`}
                            />
                        ))
                    }
                </div>
            ))       
        )
    }
}

MembersForm = reduxForm({
	form: 'MembersForm',
	destroyOnUnmount: false,
	enableReinitialize: true,
	keepDirtyOnReinitialize: true
})(MembersForm)