import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import Rupee from '../rupee'

const _Nav = styled.nav`
    width: 100%;
    background-color: #fcfdfd;
    box-shadow: 0 1px 2px 0 rgba(101, 116, 124, 0.1);
    height: 4.5rem;
    ${props => props.theme.fullWidth()} 
    .linkEnableStages a.disable { pointer-events: none; }
    .wrapper >div {
        float: left;
        height: 4.5rem;
        line-height: 4.7rem;
        margin-right: 5rem;
        color: #96a9b3;
        letter-spacing: 0.1rem;
        font-size: 0.8rem;
        position: relative;
        padding-left: 1.8rem;

        a {
            opacity:0;
            width:100%;
            height:100%;
            position:absolute;
            top:0;
            left:0;
            cursor:pointer;
        }

        &::before {
            content: '✓';
            width: 1rem;
            height: 1rem;
            line-height:1rem;
            text-indent:0.15rem;
            border-radius: 50%;
            background: #fff;
            border: solid 1px #96a9b3;
            color:transparent;
            ${props => props.theme.verticalCenter()} left:0;
        }

        &::after {
            content: '';
            width: 3rem;
            height: 1px;
            background: #96a9b3;
            ${props => props.theme.verticalCenter()} right: -4rem;
        }

        &:last-child {
            &::after {
                display: none;
            }
        }

        &.active {
            color: ${props => props.theme.blue}

            &::before {
                border-color: ${props => props.theme.blue}
            }
        }

        &.done {
            &::before {
                border-color: ${props => props.theme.blue}
                color: #FFF;
                background: ${props => props.theme.blue}
            }
        }
    }
`

export default class Nav extends Component {
    getComboMembers = (getGroup = false, getCombo = false) => {
        let { selected: { combo, group }, groups } = this.props.quotes.filters.members
        let selectedGroup = groups.find(_group => _group.id == group)
        let selectedCombo = selectedGroup.combos.find(_combo => _combo.id == combo[group])
        if (getGroup) return selectedGroup
        if (getCombo) return selectedCombo
        return selectedCombo.name
    }
    render() {
        let { active, completedStages, enabledStages } = this.props
        return (
            <_Nav>
                <div className="visible-xs mobilePolicyDetails">
                {
                    this.props && this.props.location && this.props.location.pathname && !this.props.location.pathname.includes("health/buy/postPayment") && this.props.quotes.cart.quote &&
                    <div className="container-policy-details">
                        <div className="container-policy"> 
                            <div className="policy">
                              <div className="secRow">
                                <div className="col-xs-6">
                                  <p>{this.props.quotes.cart.product.productName}</p>
                                </div>
                                <div className="col-xs-4 pull-right text-right">
                                  <img className="responsive_logo" src={this.props.quotes.cart.product.logo} />
                                </div>                               
                              </div>
                            </div>

                            <p className="border up">Insuring {this.getComboMembers()}.</p>
                            <p className="border">Sum Insured 
                                <span>
                                    <Rupee />
                                    {parseInt(this.props.quotes.cart.sum).toLocaleString('en-IN')}
                                </span>
                            </p>
                            <p className="border total">
                                Total Payable
                                <span>
                                    <Rupee />
                                    {this.props.quotes.cart.duration.value}
                                </span>
                            </p>
                        </div>
                    </div>
                }
                </div>
                <div className="wrapper linkEnableStages">
                    <div className={`${active == 'details' ? 'active' : ''} ${completedStages.details == true ? 'done' : ''}`}>
                        POLICY BUYER
                        <NavLink to="/health/buy/details" className={`${enabledStages.details || active == 'details' ? '' : 'disable'}`} />
                    </div>
                    <div className={`${active == 'members' ? 'active' : ''} ${completedStages.members == true ? 'done' : ''}`}>
                        INSURED MEMBERS
                        <NavLink to="/health/buy/members" className={`${enabledStages.members || active == 'members' ? '' : 'disable'}`} />
                    </div>
                    <div className={`${active == 'address' ? 'active' : ''} ${completedStages.address == true ? 'done' : ''}`}>
                        ADDRESS
                        <NavLink to="/health/buy/address" className={`${enabledStages.address || active == 'address' ? '' : 'disable'}`} />
                    </div>
                    <div className={`${active == 'medicalHistory' ? 'active' : ''} ${completedStages.medicalHistory == true ? 'done' : ''}`}>
                        MEDICAL HISTORY
                        <NavLink to="/health/buy/medicalHistory" className={`${enabledStages.medicalHistory || active == 'medicalHistory' ? '' : 'disable'}`} />
                    </div>
                    <div className={`${active == 'review' ? 'active' : ''} ${completedStages.review == true ? 'done' : ''}`}>
                        REVIEW &amp; PAY
                        <NavLink to="/health/buy/review" className={`${completedStages.review || active == 'review' ? '' : 'disable'}`} />
                    </div>
                </div>
            </_Nav>
        )
    }
}
