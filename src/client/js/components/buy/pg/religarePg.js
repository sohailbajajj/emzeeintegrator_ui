import React from 'react'

const Pg = ({premium, proposalResponseId, firstName, email, returnUrl, handleClick, activityId }) => (
	<form action='https://faveo.religarehealthinsurance.com/portalui/PortalPayment.run' name='PAYMENTFORM' method='post'>
		<input type='hidden' name='proposalNum' value={ proposalResponseId } />
		<input type='hidden' name='returnURL' value={ returnUrl }/>	
		<input type="submit" className={`btn-view-summary enable`} value="Buy Now" title="Submit" onClick={event => handleClick(event, activityId)} />
    </form>
)

export default Pg