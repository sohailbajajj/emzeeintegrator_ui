import React from 'react'

const Pg = ({premium, quoteId, firstName, email, returnUrl, handleClick, activityId }) => (
	<form id="PostForm" name="PostForm" action="https://www.royalsundaram.in/web/dtc/paymentgateway" method="post">
		<input type="hidden" name="reqType" id="reqType" value="JSON" />
		<input type="hidden" name="process" value="paymentOption" />
		<input type="hidden" name="apikey" id="apikey" value="310ZQmv/bYJMYrWQ1iYa7s43084=" />
		<input type="hidden" name="agentId" id="agentId" value="AG023760" />
		<input type="hidden" name="premium" value={ premium } />
		<input type="hidden" name="quoteId" id="quoteId" value={ quoteId } />
		<input type="hidden" name="version_no" id="version_no" value="123456" />
		<input type="hidden" name="strFirstName" id="strFirstName" value={ firstName } />
		<input type="hidden" name="strEmail" id="strEmail" value={ email } />
		<input type="hidden" name="isQuickRenew" id="isQuickRenew" value="" />
		<input type="hidden" name="crossSellProduct" id="crossSellProduct" value="" />
		<input type="hidden" name="crossSellQuoteid" id="crossSellQuoteid" value="" />
		<input type="hidden" name="returnUrl" id="returnUrl" value={ returnUrl } />
		<input type="hidden" value="privatePassengerCar" name="vehicleSubLine" id="vehicleSubLine" />
		<input type="hidden" value="" name="elc_value" id="elc_value" />
		<input type="hidden" value="" name="nonelc_value" id="nonelc_value" />
		<input type="hidden" value="billDesk" name="paymentType" />
		<input className={`btn-view-summary enable`} value="Submit" title="Submit" type="submit"  onClick={event => handleClick(event, activityId)} />
	</form>
)

export default Pg