import React, { Component, Fragment } from 'react'
import MemberIcon from '../common/memberIcon'
import { Link } from 'react-router-dom'
import PerfectScrollbar from 'react-perfect-scrollbar'
import values from 'lodash/values'
import close from '../../../images/close.png'
import Notifier from '../common/notifier'
import isEqual from 'lodash/isEqual'
import axios from 'axios'
import ModalShare from '../common/modalShare'
import { loadState } from '../../utils/persist'
import RSAPg from './pg/rsaPg'
import ReligarePg from './pg/religarePg'

const RenderField = ({ label, value, long }) => (
	<div className={`container-field field-read-only input-render-field ${long ? 'long' : ''}`}>
		<label>{label}:</label>
		<div className="value">{value || <span>&mdash;</span>}</div>
	</div>
)

const RenderMemberSelection = ({ members, values, gender }) => (
	<div className="container-disease even">
		<div className="wrapper-level-1">
			<p className="disease-text"><span className="text">Members</span></p>
			<div className="container-checks">
				{members.map((member, index) => (
					<div key={1 + index} className="container-check">
						<div className="container-field">
							<input type="checkbox" disabled checked={values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`] == 'Yes'} />
							<MemberIcon gender={gender} member={`${member.type == 'child' ? member.name : member.type}`} key={index} />
						</div>
					</div>
				))}
			</div>
		</div>
	</div>
)
class RenderDiseases extends Component {
	getSelectedMember(values, member) {
		if (values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`] && values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`].selected) {
			return true
		}
		else return false
	}

	toggleAdditionalInfo(vals) {
		return values(vals).map(val => val.selected ? true : false).includes(true)
	}

	render() {
		const { questions, members, values, root, gender } = this.props
		return (
			<div className="container-disease">
				<div className={`container-additional-info quesAnsDisease`}>
					{members.map((member, indexInner) => (
						<div className="wrapper-additional-info" key={`${1 + indexInner}additionalInfo`}>
							<MemberIcon gender={gender} member={`${member.type == 'child' ? member.name : member.type}`} key={indexInner} />
							{questions.map((question, index) => (
								<div className={`wrapper-field`} key={question.name + indexInner}>
									{
										<div className={`container-field dark ${!values && root[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`] && 'padding-solo'}`}>
											<div className="wrapper-field" key={`outer${index}${question.name}`}>
												<p className="descriptor">{question.value}</p>
											</div>
											{
												(values && root[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`] == 'Yes') &&
												<div className="value">
													{values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`][question.name]}
												</div>
											}
											{
												(!values && root[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`]) &&
												<div className="value">
													{root[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`]}
												</div>
											}
										</div>
									}
								</div>
							))}
						</div>
					))}
					{/*
						values &&
						<div className="container-descriptors">
							{
								questions.map((question, index) => (
									<div className="wrapper-field" key={`outer${index}${question.name}`}>
										<p className="descriptor">{question.value}</p>
										<div className="container-field"><input /></div>
									</div>
								))
							}
						</div>
						*/}
				</div>
			</div>
		)
	}
}
const getMemberValues = (values, questions) => {
	let _values = { ...values }
	delete _values.common
	let members = Object.keys(_values)
	let slectedMembers = {}

	if (typeof (questions) == 'string') {
		members.forEach(member => {
			if (_values[member][questions]) {
				slectedMembers[member] = _values[member][questions]
			}
		})
	}

	if (Array.isArray(questions)) {
		questions = questions.map(question => question.name)
		members.forEach(member => {
			questions.forEach(question => {
				if (_values[member][question]) {
					if (!slectedMembers[member]) slectedMembers[member] = {}
					slectedMembers[member][question] = _values[member][question]
				}
			})
		})
	}

	return slectedMembers
}
class RenderReligareNested extends Component {

	innerQuestionEnable = (values, question) => {
		let _values = { ...values }
		delete _values.common
		let members = Object.keys(_values)
		let enable = []
		members.forEach(member => {
			enable.push(_values[member][question])
		})
		return enable.includes('Yes')
	}

	render() {
		const { question, values, members } = this.props
		return (
			<div className="renderDiseasesNew" >
				{
					question.questions.map((_question, index) => {
						if (this.innerQuestionEnable(values, _question.name)) {
							return (
								<div key={_question.name + index} className={`container-disease ${index % 2 == 0 ? 'even' : 'odd'}`}>
									<div className="wrapper-level-1 wrapper-table">
										<header>
											<p className="disease-text">
												<span className="dash">-</span>
												<span className="text">{_question.value}</span>
											</p>
										</header>
									</div>
									<div className={`container-additional-info`}>
										{members.map((member, indexInner) => (
											<div className="wrapper-additional-info" key={`${_question.name + indexInner}additionalInfo`}>
												{
													(values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`] &&
														values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`][_question.name] &&
														values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`][_question.name] == 'Yes') ?
														_question.questions.map(questionInner => (
															<div key={_question.name + questionInner.name + indexInner}
																className={`wrapper-field`}
															>
																<div className="value dark">
																	{
																		(values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`] &&
																			values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`][questionInner.name]) ?
																			values[`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`][questionInner.name] :
																			<span>&mdash;</span>
																	}
																</div>
															</div>
														))

														:

														<div className="wrapper-field">
															<div className="value dark">
																<span>&mdash;</span>
															</div>
														</div>
												}
											</div>
										))}
										<div className="container-descriptors">
											{
												_question.questions.map((questionInner, indexQuestions) => (
													<div className="wrapper-field" key={`outer${indexQuestions}${question.name}`}>
														<p className="descriptor">{questionInner.value == 'Enter Details:' ? 'Since:' : questionInner.value}</p>
													</div>
												))
											}
										</div>
									</div>
								</div>
							)
						}
					})
				}
			</div>
		)
	}
}
const RenderStarSocialSecurity = ({ members, question, values }) => (
	<div className="container-disease">
		<div className={`container-additional-info`}>
			<div className="wrapper-additional-info">
				{question.questions.map((_question, index) => {
					return (
						<div className={`wrapper-field`} key={_question.name + index}>
							<div className={`container-field dark`}>
								<div className="value">
									{values.common[_question.name] || 'No'}
								</div>
							</div>
						</div>
					)
				})}
			</div>
			{
				values &&
				<div className="container-descriptors">
					{
						question.questions.map((_question, index) => {
							return (
								<div className="wrapper-field" key={`outer${index}${_question.name}`}>
									<p className="descriptor">{_question.value}</p>
									<div className="container-field"><input /></div>
								</div>
							)
						})
					}
				</div>
			}
		</div>
	</div>
)
const innerQuestionEnable = (values, question) => {
	let _values = { ...values }
	delete _values.common
	let members = Object.keys(_values)
	let enable = []
	members.forEach(member => {
		_values[member][question] && enable.push(_values[member][question])
	})
	return enable.length > 0
}
const RenderFieldRadioMedicalHistory = ({ question, values, index, gender, members }) => (
	<div className={`container-field field-radio-medicalHistory medicalHistory-review`}>
		<div className="container-question-main">
			<label className="container-question">
				<span className="question">Q<span className="digit">&nbsp;{index}.</span></span>
				<span className="question-text">{question.value}</span>
			</label>
			{
				!question.typeOfControl &&
				<div className="value">{values.common[question.name]}</div>
			}
			{
				(question.typeOfControl && (Object.keys(getMemberValues(values, question.name)).length == 0)) &&
				<div className="value">&mdash;</div>
			}
		</div>
		{
			(
				((question.questions || question.selectMembers) && values.common[question.name] == 'Yes')
				||
				(!question.selectMembers && ((!question.typeOfControl && values.common[question.name] == 'Yes') || question.typeOfControl))
				||
				(question.selectMembers && question.typeOfControl && !question.questions)
				||
				(!question.selectMembers && question.questions && !question.typeOfControl)
			) &&
			<div className={`container-member-details review ${question.typeOfControl && (Object.keys(getMemberValues(values, question.name)).length == 0) ? 'hide' : ''}`}>
				{
					!(
						(values.common[question.name] == 'No') &&
						(!question.selectMembers && question.questions && !question.typeOfControl)
					) &&
					<div className="padding" />
				}
				{/*
					!(!question.selectMembers && question.questions && !question.typeOfControl) &&
					!(question.selectMembers && question.typeOfControl && !question.questions && !innerQuestionEnable(values, question.name)) &&
					<div className="container-members">
						<div className="wrapper-members">
							{
								members.map((member, index) => {
									let _member = `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`
									if (values[_member] && !(values[_member][question.name] == 'No')) {
										return (
											<MemberIcon gender={gender} member={`${member.type == 'child' ? member.name : member.type}`} key={index} />
										)
									}
								})
							}
						</div>
					</div>
				*/}
				<div className={`container-questions-inner ${!question.selectMembers && question.questions && !question.typeOfControl && 'no-margin'} review-container-answers`}>
					{
						((question.selectMembers && values.common[question.name] == 'Yes') || (!question.selectMembers && !question.typeOfControl)) &&
						!(question.questions && question.selectMembers) &&
						!(!question.selectMembers && question.questions && !question.typeOfControl) &&
						!(!question.selectMembers && !question.questions && !question.typeOfControl) &&
						<RenderMemberSelection members={members} values={getMemberValues(values, question.name)} gender={gender} />
					}
					{
						question.questions && (values.common[question.name] == 'Yes') &&
						!(!question.selectMembers && question.questions && !question.typeOfControl) &&
						Array.isArray(question.questions) &&
						!(question.questions.filter(_question => _question.questions).length > 0) &&
						<RenderDiseases members={members} questions={question.questions}
							values={getMemberValues(values, question.questions)}
							root={getMemberValues(values, question.name)} gender={gender}
						/>
					}
					{
						question.typeOfControl && !question.selectMembers &&
						<RenderDiseases members={members} questions={[question]}
							root={getMemberValues(values, question.name)} gender={gender}
						/>
					}
					{
						question.questions && (values.common[question.name] == 'Yes') &&
						Array.isArray(question.questions) &&
						(question.questions.filter(_question => _question.questions).length > 0) &&
						<RenderReligareNested members={members} question={question} values={values} />
					}
					{
						(values.common[question.name] == 'Yes') &&
						!question.selectMembers && question.questions && !question.typeOfControl &&
						<RenderStarSocialSecurity members={members} question={question} values={values} />
					}
					{
						question.selectMembers && question.typeOfControl && !question.questions &&
						innerQuestionEnable(values, question.name) &&
						<RenderDiseases members={members} questions={[question]}
							root={getMemberValues(values, question.name)} gender={gender}
						/>
					}
				</div>
			</div>
		}
	</div>
)
export default class Review extends Component {
    constructor(props) {
		super(props)
		this.members = this.props.members.map(member => `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`)
		this.state = {
			enablePopup: undefined,
			popups: undefined,
			enablePopupDeclaration: false,
			enableModalShare: false,
			notifier: {
				enable: false,
				success: undefined
			}
		}
    }
    static defaultProps = {
		notifier: {
			success: 'Proposal form successfully delivered to the specified email address(es).',
			error: 'Oops! Looks like something went wrong. Please try again in some time.'
		}
    }
    componentDidMount() {
		this._form && this._form.classList.remove('disable')
		this._form && setTimeout(() => this._form.classList.remove('hide'))
		console.log('This is the getHealthProposal request -')
		console.log(this.genSubmitRequest())
		this.props.submitProposalForm(this.genSubmitRequest())
    }
    componentWillReceiveProps(next) {
		if (next.proposalForm.submitResponse && !isEqual(next.proposalForm.submitResponse, this.props.proposalForm.submitResponse)) {
			if (next.proposalForm.submitResponse.success && next.proposalForm.submitResponse.paymentUrl) {
				this.getPaymentToken(next.proposalForm.submitResponse.proposalResponseId, next.proposalForm.submitResponse.insurerId)
			}
		}

		if (next.proposalForm.submitResponse != undefined &&
			next.proposalForm.submitResponse.popUp && 
			next.proposalForm.submitResponse.popUp.length > 0 && 
			this.state.enablePopup == undefined
		) {
			setTimeout(() => {
				this.setState({
					enablePopup: 0,
					popups: next.proposalForm.submitResponse.popUp
				})
			}, 1000)
		}
		if (next.proposalForm.submitResponse != undefined &&
			next.proposalForm.submitResponse.data && 
			next.proposalForm.submitResponse.data.message && next.proposalForm.submitResponse.status == 400
		) {
			setTimeout(() => {
				this.setState({
					enablePopup: 0,
					badRequestPopup: next.proposalForm.submitResponse.data
				})
			}, 1000)
		}
	}
	onBuyButtonClick(event, activityId) {
		event && event.preventDefault()
		let form = event.currentTarget.parentElement.tagName == 'FORM' ? event.currentTarget.parentElement : undefined
		let link = event.currentTarget.href || undefined
		let endpoint = `/api/obbc?activityId=${activityId}`
		//let endpoint = `http://zipcover.in:8093/emzee/onBuyButtonClick?activityId=${activityId}`

		let submit = () => {
			form && form.submit()
			link && (window.location = link)
		}

		axios.get(endpoint)
			.then(resp => submit())
			.catch(err => submit())
	}
	isEmpty = (obj) => {
		for(var key in obj) {
			if(obj.hasOwnProperty(key))
				return false;
		}
		return true;
	}
	genSubmitRequest = () => {
		let insurer = this.props.insurer
		let policy = this.props.policy
		let proposer = { ...this.props.forms.ProposerForm.values[insurer] }
		let nominee = { ...this.props.forms.NomineeForm.values[insurer] }
		let contact = { ...this.props.forms.AddressForm.values[insurer] }
		let members = { ...this.props.forms.MembersForm.values[insurer] }
		let medicalHistory = JSON.parse(JSON.stringify(this.props.forms.MedicalHistoryForm.values))
		let addons = [];
		let getAddons = { ...this.props.addons }
		if(!this.isEmpty(getAddons)) {
			Object.keys(getAddons).forEach(function(key) {
				addons.push(getAddons[key].addonsId)
			})
		}
		let memberNames = Object.keys(members)

		let policyDetails = {
			//quoteId: policy.quoteId,
			quoteId: "",
			netPremium: policy.premium[policy.premium.selected].discount.premium || policy.premium[policy.premium.selected].premium,
			//totalPremium: 1,
			totalPremium: policy.premium[policy.premium.selected].discount.totalPremium || policy.premium[policy.premium.selected].totalPremium,
			sumInsured: parseInt(policy.sumInsured).toString(),
			policyDuration: this.getDuration(policy.premium.selected),
			serviceTax: policy.premium[policy.premium.selected].discount.serviceTax || policy.premium[policy.premium.selected].serviceTax,
			insurerId: policy.insurerId,
			productId: policy.productId,
			schemeId: policy.schemeId
		}

		let deleteNestedQuestions = (question, member) => {
			if (question && question.questions) {
				question.questions.forEach(_q => {
					if (!_q.common) {
						medicalHistory[member][_q.name] && delete medicalHistory[member][_q.name]
						if (_q.questions) deleteNestedQuestions(_q, member)
					}
				})
			}
		}

		memberNames.forEach(member => {
			if (medicalHistory[member]) {
				Object.keys(medicalHistory.common).forEach(question => {
					if (medicalHistory.common[question] == 'No') {
						medicalHistory[member][question] && delete medicalHistory[member][question]
						let _question = this.props.fields.medicalHistory.filter(field => field.name == question)[0]
						deleteNestedQuestions(_question, member)
					}
				})
				let questionsObject = { ...medicalHistory.common, ...medicalHistory[member] }
				let questionArray = []
				let questions = Object.keys(questionsObject)
				questions.forEach(question => questionArray.push({ name: question, value: questionsObject[question] }))
				medicalHistory[member] = questionArray
			}
			else {
				let questionArray = []
				let questionsObject = medicalHistory.common
				Object.keys(questionsObject).forEach(question => questionArray.push({ name: question, value: questionsObject[question] }))
				medicalHistory[member] = questionArray
			}
		})

		memberNames.forEach(member => {
			members[member].relation = this.getRelation(member)
			members[member].gender = ((member == 'husband') || (member == 'father') || (member.includes('son')) ? 'male' : 'female')
			members[member].medicalHistory = medicalHistory[member]
		})

		let getMasterPincode = () => {
			if (this.props.policies.selectedGroup.name == 'parents') {
				return this.props.UserDetailsForm.parentsCity[0]
			}
			else {
				return this.props.UserDetailsForm.values.pincode
			}
		}

		let request = {
			proposer: {
				proposer,
				nominee
			},
			insured: values(members),
			contact,
			...policyDetails,
			postPopUp: false,
			masterPincode: parseInt(getMasterPincode()),
			activityId: policy.activityId,
			addOns: addons
			//activityId: 123
		}

		return request

	}
	getRelation = member => {
		if (member.includes('son')) return 'son'
		if (member.includes('daughter')) return 'daughter'

		switch (member) {
			case 'husband':
				if (this.props.gender == 'male') {
					return 'proposer'
				}
				return 'spouse'
			case 'wife':
				if (this.props.gender == 'female') {
					return 'proposer'
				}
				return 'spouse'
			case 'father':
				return 'father'
			case 'mother':
				return 'mother'
		}
	}
	getDuration = duration => {
		switch (duration) {
			case 'firstYear':
				return 1
				break
			case 'secondYear':
				return 2
				break
			case 'thirdYear':
				return 3
				break
			default:
				return 1
		}
	}
	getName(name) {
		switch (name) {
			case 'husband':
				if (this.props.gender == 'male') return 'you'
				else return name
				break
			case 'wife':
				if (this.props.gender == 'female') return 'you'
				else return name
				break
			default:
				return name
		}
	}
	getValues = (vals, elements) => {
		let final = []

		elements.forEach(element => {
			if (element.typeOfControl == "DropDownBox" || element.typeOfControl == "CascadeDropDownBox") {
				if (element.options && (element.options.length > 0)) {
					if (element.options[0].value) {
						let option = element.options.filter(option => option.value == vals[element.name])[0]
						if (option) {
							final.push({
								key: element.value,
								value: option.name
							})
						}
					}

					if (!element.options[0].value) {
						final.push({
							key: element.value,
							value: vals[element.name]
						})
					}
				}
			}
			else if (element.typeOfControl == "DropDownBoxHeight") {
				final.push({
					key: 'height',
					value: `${vals.feet} ${vals.inch}`
				})
			}
			else if (element.code) {
				final.push({
					key: element.value,
					value: element.selection
				})
			}
			else {
				final.push({
					key: element.value,
					value: vals[element.name]
				})
			}
		})

		return final
	}
	handlePopupAction = (action, index) => {
		if (!action || (action == 'STOP')) {
			this.setState({ enablePopup: 9999 })
			setTimeout(() => { this.setState({ enablePopup: index+1 }) }, 1000)
		}
		if (action == 'OKCANCEL') {
			console.log('This is the getHealthProposal request -')
			console.log({ ...this.genSubmitRequest(), postPopUp: true })
			this.props.submitProposalForm({ ...this.genSubmitRequest(), postPopUp: true })
			this.handlePopupAction(undefined, index)
		}

		if (action == 'DISCOUNT') {
			this.props.setDiscountedPremium(this.props.proposalForm.submitResponse.totalPremium)
			this.handlePopupAction(undefined, index)
		}

		if (action == 'cancel') {
			this.props.history.push('/health/quotes')
		}
	}
	getPaymentToken = (referenceId, insurerId) => {
		axios.get(`/api/getPaymentToken?referenceId=${referenceId}&insurerId=${insurerId}`)
			.then(resp => {
				if (this.props.proposalForm.submitResponse.paymentUrl.includes('{paymentToken}')) {
					this.props.setPgUrl(this.props.proposalForm.submitResponse.paymentUrl.replace('{paymentToken}', resp.data))
				}
				else {
					this.props.setPgUrl(this.props.proposalForm.submitResponse.paymentUrl.replace('token', resp.data))
				}
			})
			.catch(error => {
				console.log('This is the getPaymentToken error -')
				console.log(error)
			})
	}
	toggleModalShare = enable => {
		this.setState({
			enableModalShare: enable
		})
	}

	shareProposal = email => {
		let request = {
			"activityId": (this.props.proposalForm.submitResponse && this.props.proposalForm.submitResponse.activityId) || "75",
			"activityName": "shareProposal",
			"mailInfo": {
				"to": email.to,
				"cc": email.cc
			},
			"appState": loadState()
		}
		axios.post('/api/share', request)
			.then(resp => this.setState({
				notifier: {
					enable: true,
					success: true
				}
			}))
			.catch(err => this.setState({
				notifier: {
					enable: true,
					success: false
				}
			}))
	}

	toggleNotifier = status => {
		this.setState({
			notifier: {
				...this.state.notifier,
				enable: status
			}
		})
	}
    render() {
		const { forms, members, gender } = this.props;
		
		const checkPayment = this.props.proposalForm && (this.props.proposalForm.pgUrl || this.props.proposalForm.submitResponse) && this.props.proposalForm.submitResponse.success == true;
		
		const successFalse = (this.props.proposalForm && this.props.proposalForm.submitResponse && this.props.proposalForm.submitResponse.data && this.props.proposalForm.submitResponse.data.status == 400 && this.props.proposalForm.submitResponse.data.statusText == "Bad Request") || (this.props.proposalForm && this.props.proposalForm.submitResponse && this.props.proposalForm.submitResponse.data && this.props.proposalForm.submitResponse.data.success == false);

        return (
            <Fragment>
                <div className="head" ref={_form => this._form = _form}>
			 		<h3>Full Review</h3>
                    <p>STEP 5 of 5</p>                   
					<div className="container-header header-main review-header">
						<span>REVIEW</span>
						<div className="button-share" onClick={() => this.toggleModalShare(true)} >Share Proposal</div>
					</div>
                    <div className="container-header all-form-detail">
                        <span>Proposer</span>
                        <Link className="edit review-edit-btn" to={'/health/buy/details'}>Edit</Link>
					
                    </div>
                        {
						forms.ProposerForm && forms.ProposerForm.submitSucceeded &&
						<div className="reviewForms">
							{
								this.getValues(forms.ProposerForm.values[this.props.insurer], this.props.fields.proposer.proposer).map((field, index) =>
									<RenderField key={field.key + index} label={field.key} value={field.value}
											long={field.key.length > 18 ? true : false}
									/>
								)
							}
						</div>
                        }
                    <div className="container-header all-form-detail">
                        <span>Nominee</span>
                        <Link className="edit review-edit-btn" to={'/health/buy/details'}>Edit</Link>
                    </div>
                    {
						forms.NomineeForm && forms.NomineeForm.submitSucceeded &&
						<div className="reviewForms">
							{
                            this.getValues(forms.NomineeForm.values[this.props.insurer], this.props.fields.proposer.nominee).map((field, index) =>
                                    <RenderField key={field.key + index} label={field.key} value={field.value} />
							)
							}
						</div>
                    }
                    <div className="container-header all-form-detail">
                        <span>Insured Members</span>
                        <Link className="edit review-edit-btn" to={'/health/buy/members'}>Edit</Link>
                    </div>
                    {
						forms.MembersForm && forms.MembersForm.submitSucceeded &&
						<div className="reviewForms">
							{
                            this.members.map((member, index) => (
                                <div key={member + index} className="container-fields-member">
                                    <p className="member-name">
                                        {
                                            this.getName(member).includes('-') ?
                                                this.getName(member).split('-')[1] :
                                                this.getName(member)
                                        }
                                    </p>
                                    {
                                        this.getValues(forms.MembersForm.values[this.props.insurer][member], this.props.fields.insured).map((field, index) =>
                                            <RenderField key={field.key + index} label={field.key} value={field.value} />
                                        )
                                    }
                                </div>
							))
							}
						</div>
                    }
                    <div className="container-header all-form-detail">
                        <span>Address Info</span>
                        <Link className="edit review-edit-btn" to={'/health/buy/address'}>Edit</Link>
                    </div>
                    {
						forms.AddressForm && forms.AddressForm.submitSucceeded &&
						<div className="reviewForms">
							{
                            this.getValues(forms.AddressForm.values[this.props.insurer], this.props.fields.contact).map((field, index) => (
                                <RenderField key={field.key + index} label={field.key} value={field.value} />
							))   
							}
						</div>                    
                    }
                    <div className="container-header all-form-detail">
                        <span>Medical History</span>
                        <Link className="edit review-edit-btn" to={'/health/buy/medicalHistory'}>Edit</Link>
                    </div>
                    {
						forms.MedicalHistoryForm && forms.MedicalHistoryForm.submitSucceeded &&
						<div className="reviewForms">
							{
                            this.props.fields.medicalHistory.map((question, index) => (
                                <RenderFieldRadioMedicalHistory key={question.name + index} index={index + 1}
                                    values={forms.MedicalHistoryForm.values} question={question}
                                    gender={gender} members={members}
                                />
							))
							}
						</div>
                    }
                    <div className="container-header all-form-detail">
                        <span>Terms &amp; Conditions</span>
                    </div>
						<div className="PaymentStatus">
							{
								!checkPayment && !successFalse && <div className="checkPayment">
									Please wait...checking payment...
								</div>
							}

							{
								successFalse && <div className="successFalse">
									Some error occur in proposal...please contact our administrator
								</div>
							}

							{
								checkPayment && <div className="successPayment">
									Now you can accept terms and conditions to purchase policy.
								</div>
							}
						</div>
					<p className="container-tnc">
						<input type="checkbox" onChange={event => this.props.acceptTnc(event.currentTarget.checked)} disabled={`${checkPayment ? "" : "disabled"}`} />
						<span>
							I hereby declare that all information provided above is true,
							and I accept all Terms &amp; Conditions of&nbsp;
							<a href="/legal" target="_blank">Zipcover</a>
							{
								this.props.proposalForm.fields.declarations &&
								this.props.proposalForm.fields.declarations.declarationsData &&
								<label>
									&nbsp;&amp; the <a href="javascript:void(0)" onClick={() => this.setState({ enablePopupDeclaration: true })}className="color_blue">Insurer</a>.
								</label>
							}
							{
								!this.props.proposalForm.fields.declarations &&
								<label>.</label>
							}
						</span>
					</p>

					{/*Buy now for RSA & Religare*/}
					{
						this.props.proposalForm.submitResponse &&
						this.props.proposalForm.submitResponse.success &&
						!this.props.proposalForm.submitResponse.paymentUrl &&
						this.props.tnc &&
						<div>
							{
								this.props.proposalForm.submitResponse.insurerId == 3 &&
								<RSAPg premium={this.props.proposalForm.submitResponse.totalPremium}
									quoteId={this.props.proposalForm.submitResponse.proposalResponseId}
									firstName={this.props.proposalForm.submitResponse.firstName}
									email={this.props.proposalForm.submitResponse.email}
									returnUrl={this.props.proposalForm.submitResponse.returnUrl}
									activityId={this.props.policy.activityId}
									handleClick={this.onBuyButtonClick}
								/>
							}
							{
								this.props.proposalForm.submitResponse.insurerId == 7 &&
								<ReligarePg premium={this.props.proposalForm.submitResponse.totalPremium}
									proposalResponseId={this.props.proposalForm.submitResponse.proposalResponseId}
									firstName={this.props.proposalForm.submitResponse.firstName}
									email={this.props.proposalForm.submitResponse.email}
									returnUrl={this.props.proposalForm.submitResponse.returnUrl}
									activityId={this.props.policy.activityId}
									handleClick={this.onBuyButtonClick}
								/>
							}
						</div>
					}

					{/*Popups*/}
					{
						this.props.proposalForm.submitResponse &&
						this.state.popups &&
						this.state.popups.map((popup, index) => (
							<div className={` container-popup ${(index == this.state.enablePopup) && 'active'}`} key={popup.popupMessage+index}>
								<div className="popup">
									<img src={close} className="close"
										onClick={() => this.handlePopupAction(undefined, index)}
									/>
									<h3>Kindly note</h3>
									<p>
										{
											popup.popupMessage
										}
									</p>
									<div className="container-buttons">
										<div className="button" onClick={() => this.handlePopupAction(popup.action, index)}>OK</div>
										{
											!(popup.action == "DISCOUNT") &&
											<div className="button" onClick={() => this.handlePopupAction('cancel')} >BACK TO QUOTES</div>
										}
									</div>
								</div>
							</div>
						))
					}
					{ /* 400 bad request error message popup*/ }
					{
						this.props.proposalForm.submitResponse &&
						this.state.badRequestPopup &&
						<div className={`container-popup ${(0 == this.state.enablePopup) && 'active'}`}>
								<div className="popup">
									<img src={close} className="close"
										onClick={() => this.handlePopupAction(undefined, 0)}
									/>
									<h3>Kindly note</h3>
									<p>
										{
											this.state.badRequestPopup.message
										}
									</p>
									<div className="container-buttons">
										<div className="button" onClick={() => this.handlePopupAction(undefined, 0)}>OK</div>
										<div className="button" onClick={() => this.handlePopupAction('cancel')} >BACK TO QUOTES</div>
									</div>
								</div>
							</div>
					}

					{/*Buy now for Star, Apollo & Universal Sompo */}
					{
						this.props.proposalForm.submitResponse &&
						this.props.proposalForm.submitResponse.paymentUrl &&
						this.props.proposalForm.pgUrl &&
						this.props.tnc &&
						<a className="btn-view-summary enable" href={this.props.proposalForm.pgUrl} onClick={event => this.onBuyButtonClick(event, this.props.policy.activityId)}>Buy now</a>
					}

					{/* Declaration Popup */}
					{
						this.props.proposalForm.fields.declarations &&
						this.props.proposalForm.fields.declarations.declarationsData &&
						<div className={`container-popup reviewPagePopup popup-declaration ${this.state.enablePopupDeclaration && 'active'}`}>
							<div className="popup">
							<div className="fixedTitleClose">
								<img src={close} className="close"
									onClick={() => this.setState({ enablePopupDeclaration: false })}
								/>
								<h3>Insurer's Terms and Conditions</h3>
							</div>
								<div className="wrapper-content">
									<PerfectScrollbar>
										<p>
											{
												this.props.proposalForm.fields.declarations &&
												this.props.proposalForm.fields.declarations.declarationsData
											}
										</p>
									</PerfectScrollbar>
								</div>
							 </div>							
						</div>
					}

					<ModalShare active={this.state.enableModalShare} toggleModal={this.toggleModalShare}
						type={'Proposal'} self={this.props.UserDetailsForm.values.email}
						share={this.shareProposal}
					/>

					<Notifier active={this.state.notifier.enable} success={this.state.notifier.success}
						toggleNotifier={this.toggleNotifier} successMsg={this.props.notifier.success}
						errorMsg={this.props.notifier.error}
					/>
                </div>
            </Fragment>
        )
    }
}