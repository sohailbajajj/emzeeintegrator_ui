import React from 'react'
import styled from 'styled-components'

import { stopBubbling } from '../utils/helpers'

const Checkbox = styled.span`
	width: 20px;
	height: 20px;
	display: block;
	float: left;
	border-radius: 4px;
	cursor: pointer;
	position: relative;
	background:${props => props.theme.border};
	${props => props.theme.verticalCenter()};
	font-size: 15px;
	line-height: 20px;
	color:#FFF;
	text-align:center;
	user-select:none;
	
	.unchecked {
		position: absolute;
		width: 18px;
		height: 18px;
		border-radius: 4px;
		background-color: #FFF;
		top: 1px;
		left: 1px;
		opacity:1;
	}

	&.circular {
		border-radius: 50%;
		width: 22px;
		height: 22px;
		line-height: 22px;

		.unchecked {
			border-radius: 50%;
			width: 20px;
			height: 20px;
		}
	}
	
	&.checked {
		background:${props => props.theme.blue};

		.unchecked {
			opacity:0;
		}
	}
`

const handleClick = (event, callback) => {
	stopBubbling(event)
	callback && callback()
}

export default props => (
	<Checkbox className={`check ${props.checked && 'checked'} ${props.circular && 'circular'}`} 
		onClick={e => handleClick(e, props.toggleCheck)}
	>
		&#10003;
			<span className="unchecked" />
	</Checkbox>
)