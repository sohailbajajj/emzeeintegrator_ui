import React from 'react'

const MemberIcon = ({ member, age = '', gender, proposalForm }) => (
    <div className="container-member">
        <div className={ `wrapper-member member-${member} ${member == 'wife' || member == 'mother' || member == 'daughter' ? 'member-female' : 'member-male'}` }>
            <p className="member-name">
                <span>
                    { member == 'wife' && gender == 'female' ? 'you' :
                        member == 'husband' && gender == 'male' ? 'you' :
                        member
                    }
					{
						proposalForm ? <span className="digit">&nbsp;{`(${age} Years)`}</span> : ''	
					}
                </span>
            </p>
        </div>
    </div>
)

export default MemberIcon