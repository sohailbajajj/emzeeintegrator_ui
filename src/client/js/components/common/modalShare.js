import React, { Component } from 'react'

import close from '../../../images/landing/purple-cross.png'

class ModalShare extends Component {
	state = {
		error: false,
		recievers: {
			self: {
				id: this.props.self,
				selected: true
			},
			alternate: {
				id: '',
				selected: false
			}
		}
	}

	validateAndSetEmail = email => {
		let error = email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)
		
		this.setState({ 
			error, 
			recievers: {
				...this.state.recievers,
				alternate: {
					id: (!error && email) || '',
					selected: !error && email
				}
			}
		})
	}

	toggleRecievers = reciever => {
		this.setState({
			recievers: {
				...this.state.recievers,
				[reciever]: {
					...this.state.recievers[reciever],
					selected: !this.state.recievers[reciever].id ? false : !this.state.recievers[reciever].selected
				}
			}
		})
	}

	share = () => {
		this.props.share({
			to: (this.state.recievers.self.selected && this.state.recievers.self.id) || '',
			cc: (this.state.recievers.alternate && this.state.recievers.alternate.id) || ''
		})
	}

	render() {
		return (
			<div className={`REVIEW_PAGE_SHARE_PROP Review-popup action-overlay ${this.props.active ? 'active' : ''}`}>
				<div className={`action-share ${this.props.active ? 'active' : ''}`}>
					<img className="close" src={close} onClick={() => this.props.toggleModal(false)} />
					<h6 className="head"><span>Share {this.props.type}</span></h6>
					<p className="desc">
						Send {this.props.type} to the following address(es):
					</p>
					<div className="container-addresses">
						<div className="container-address">
							<div className="selector"
								onClick={() => this.toggleRecievers('self')}
							>
								<span className={this.state.recievers.self.selected ? 'selected' : ''} />
							</div>
							<span className="id">{this.state.recievers.self.id}</span>
						</div>
						<br />
						<div className="container-address">
							<div className="selector"
								onClick={() => this.toggleRecievers('alternate')}
							>
								<span className={this.state.recievers.alternate.selected ? 'selected' : ''} />
							</div>
							<input className="id" placeholder="Alternate Email ID" type="email"
								onChange={event => this.validateAndSetEmail(event.target.value)}
							/>
							<span className={`error ${this.state.error ? 'active' : ''}`}><span>Invalid ID</span></span>
						</div>
					</div>
					<div className={`continue send ${this.state.error || (!this.state.recievers.self.selected && !this.state.recievers.alternate.selected) ? 'disable' : ''}`}
						onClick={event => { this.props.toggleModal(false, event); this.share() }}
					>
						<a><label>send</label><span><label /></span></a>
					</div>
				</div>
			</div>
		)
	}
}

export default ModalShare