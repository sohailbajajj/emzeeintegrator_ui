import React, { Component } from 'react'
import errorIcon from '../../../images/error.png'
import successIcon from '../../../images/success.png'
import close from '../../../images/close.png'

class Notifier extends Component {
	componentDidMount() {
		document.addEventListener('keydown', this.toggle)
	}

	componentWillUnmount() {
		document.removeEventListener('keydown', this.toggle)
	}

	toggle = e => {
		e.keyCode == 27 && this.props.toggleNotifier(false)
	}
	
	render() {
		let { active, success, toggleNotifier, successMsg, errorMsg } = this.props
		return (
			<div className={ `container-notifications ${active ? 'active' : ''}` }>
			<img className="close" src={ close } onClick={ () => toggleNotifier(false) } />
			<div className="container-icon">
				<img className={ `error ${!success ? 'active' : ''}` } src={ errorIcon } />
				<img className={ `success ${success ? 'active' : ''}` } src={ successIcon } />
			</div>
			<p className={ `msg ${success ? 'active' : ''}` }>{successMsg}</p>
	
			<p className={ `msg ${!success ? 'active' : ''}` }>{errorMsg}</p>
		</div>
		)
	}
}

export default Notifier