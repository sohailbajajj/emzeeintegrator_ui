import React from 'react'
import styled from 'styled-components'

import twitter from '../../images/twitter.png'
import facebook from '../../images/facebook.png'
import linkedin from '../../images/linkedin.png'

const _Footer = styled.footer`
    margin-bottom:2.5rem;
    ${props => props.theme.fullWidth()}

    .links {
        width: 40%;
        float:left;
        margin-top: 4rem;
        padding-bottom: 3rem;

        .col {
            width: 50%;
            float:left;

            label {
                color:#070c4e;
                font-size:12px;
                line-height:18px;
                font-family:medium;
                display:block;
                margin-bottom: 16px;
            }

            a {
                display:block;
                font-size:12px;
                line-height:18px;
                color:#65747c;
                margin-bottom: 6px;			

                &.high {
                    line-height: 23px;
                }

                &.low {
                    margin-bottom: 3px;
                }

                &.social {
                    width: 24px;
                    height: 24px;
                    margin-right:13px;
                    float:left;
                    cursor:pointer;
                    @include background-image();

                    &.tw {
                        background-image: url(${twitter});
                    }

                    &.fb {
                        background-image: url(${facebook});
                    }

                    &.in {
                        background-image: url(${linkedin});
                    }
                }
            }
        }
    }

    .irda {
        color:#96a9b3;
        font-size:11px;
        line-height: 20px;
        border-top:solid 1px #f2f7f9;
        border-bottom:solid 1px #f2f7f9;
        width:100%;
        float:left;
        padding: 1.3rem 0;
    }

    .copy {
        color:#96a9b3;
        font-size:11px;
        line-height: 20px;
        width:100%;
        float:left;
        margin-top:1.3rem;
    }

    @media (max-width: 1000px) {
        .links {
            width:100%;
            
            .col {
                width:50%;
            }

            &:first-child {
                border:none;
            }

            &:nth-child(2) {
                margin-top: -1rem;
            }
        }
    }
`

export default () => {
    return (
        <_Footer className="full-width">
            <div className="wrapper">
                <div className="links">
                    <div className="col">
                        <label>Insurance</label>
                        <a href="">Get Insured</a>
                        <a href="">Business Solutions</a>
                        <a href="">Claims</a>
                        <a href="">Login</a>
                    </div>
                    <div className="col">
                        <label>Company</label>
                        <a href="">About us</a>
                        <a href="">Blog</a>
                        <a href="">Careers</a>
                    </div>
                </div>
                <div className="links">
                    <div className="col">
                        <label>Get in Touch</label>
                        <a href="mailto:info@zipcover.in" target="_blank">
                            info@zipcover.in
                        </a>
                        <a className="low" href="tel:+917840040042" target="_blank">
                            +91 78400-40042
                        </a>
                        <a className="high" href="">
                            Suite 501, Success Tower,<br />
                            Sector 65, Golf Course<br />
                            Extension Road, Gurgaon
                        </a>
                    </div>
                    <div className="col">
                        <label>Get in Touch</label>
                        <a href="https://twitter.com/ZipCover_In" target="_blank" className="social tw" />
                        <a href="https://www.facebook.com/ZipCover.Insurance/" target="_blank" className="social fb" />
                        <a href="https://www.linkedin.com/company/13224248/" target="_blank" className="social in" />
                    </div>
                </div>
                <div className="irda">
                    IRDA Direct Broker License No: IRDA/DB641/16, Valid Till: 26/04/2019, Principal Officer: Suhail P Bajaj, CIN: U45200DL2007PTC160312<br />
                    Disclaimer: Insurance is the subject matter of solicitation.
                </div>
                <div className="copy">&copy; 2018. Zipcover is powered by Instapolicy Insurance Broking Pvt Ltd.</div>
            </div>
        </_Footer>
    )
}
