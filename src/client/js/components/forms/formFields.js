import React, { Component } from 'react'
import { Field, FieldArray } from 'redux-form'
import uuid from 'node-uuid'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import '../../../styles/datepicker.scss'
import PerfectScrollbar from 'react-perfect-scrollbar'
import values from 'lodash/values'
import axios from 'axios'
import { YearPicker, MonthPicker, DayPicker } from 'react-dropdown-date';
import MemberIcon from '../common/memberIcon'
import loader from '../../../images/loader.gif'
import { loadState } from '../../utils/persist'
import { getValidations } from './formValidations'

export const renderField = ({ input, handleSubmit, long, label, type, meta: { touched, error } }) => (
	<div className={`container-field ${long ? 'long' : ''} input-render-field common-render-field ${input.value ? 'selected' : ''}`}>
		<label><span>{label}</span></label>
		<div>
			<input { ...input } type={type}
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			/>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderFieldDOB = ({ input, handleSubmit, long, label, type, meta: { touched, error } }) => (
	<div className={`container-field ${long ? 'long' : ''} input-render-field common-render-field ${input.value ? 'selected' : ''}`}>
		<label><span>{label}</span></label>
		<div>
			<input { ...input } type={type}
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			/>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderFieldTextArea = ({ input, handleSubmit, long, label, type, meta: { touched, error } }) => (
	<div className={`container-field field-text-area`}>
		<label>{label}:</label>
		<div>
			<textarea { ...input } placeholder={label} type={type}
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			/>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderFieldRadio = ({ input, handleSubmit, label, long, type, validate, value, meta: { touched, error, warning } }) => (
	<div className={`container-field field-radio radio-${input.name} ${long ? 'long' : ''}`}>
		<label>{label}</label>
		<div>
			<label className='container-radio'>
				<Field name={input.name} component="input" type="radio" value={input.value[0]} validate={[required]}
					onChange={event => {
						input.onChange(event)
						setTimeout(handleSubmit)
					}}
				/>
				<span>{input.value[0]}</span>
			</label>
			<label className='container-radio'>
				<Field name={input.name} component="input" type="radio" value={input.value[1]} validate={[required]}
					onChange={event => {
						input.onChange(event)
						setTimeout(handleSubmit)
					}}
				/>
				<span>{input.value[1]}</span>
			</label>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderFieldSelect = ({ input, handleSubmit, label, options, type, meta: { touched, error, warning } }) => (
	<div className={`container-field field-select`}>
		<label>{label}</label>
		<div>
			<select { ...input }
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			>
				<option />
				{options.map((option, index) => <option key={option + index} value={option}>{option}</option>)}
			</select>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderHeightSelect = ({ input, handleSubmit, options, opname = null, meta: { touched, error, warning } }) => (
	<div className={`container-select ${opname}`}>
		<select { ...input }
			onChange={event => {
				input.onChange(event)
				setTimeout(handleSubmit)
			}}
		>
			<option value="">{opname}</option>
			{
				options &&
				options.map((option, index) => <option key={option + index} value={option}>{option}</option>)
			}
			{
				!options &&
				<option key={uuid.v1()} value={'Error'}>Error</option>
			}
		</select>
		{touched && error && <span className="error">{error}</span>}
	</div>
)

export const renderFieldHeightSelect = ({ input, handleSubmit, label, options, type, meta: { touched, error, warning } }) => (
	<div className={`container-field field-heightSelect`}>
		<label>{label}:</label>
		<div>
			<Field name={`${input.name}Feet`} component={renderHeightSelect} options={options[0]}
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			/>
			<Field name={`${input.name}Inches`} component={renderHeightSelect} options={options[1]}
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			/>
		</div>
	</div>
)

const getMaxDate = age => {
	if (age > 0) {
		return moment().subtract(age, 'years')
	}
	else if (age == 0) {
		return moment().subtract(3, 'months')
	}
	else {
		return moment().subtract(18, 'years')
	}
}

const getMaxNumber = age => {
	if (age > 0) {
		return moment().year() - age
	}
	else if (age == 0) {
		//return moment().month() - 3
		return moment().year()
	}
	else {
		return moment().year() - 18
	}
}

export const renderYearSelect = ({ input, handleSubmit, meta: { touched, error, warning }, minNumber, maxNumber }) => (
	<div className={`col-sm-3 col-xs-3 pull-right`}>
	<div className={`field-select customDropDown ${input.value ? 'selected' : ''}`}>
		<label><span>Year</span></label>
		<YearPicker { ...input }
            start={minNumber}
			end={maxNumber} 
			onChange={event => {
				input.onChange(event)
				setTimeout(handleSubmit)
			}}
		/>
		{touched && error && <span className="error">{error}</span>}
	</div>
	</div>
)

export const renderMonthSelect = ({ input, handleSubmit, meta: { touched, error, warning } }) => (
	<div className={`col-sm-3 col-xs-3 pLR5`}>
	<div className={`field-select customDropDown ${input.value ? 'selected' : ''}`}>
	<label><span>Month</span></label>
		<MonthPicker { ...input }
			//end={} 
			onChange={event => {
				input.onChange(event)
				setTimeout(handleSubmit)
			}}
		/>
		{touched && error && <span className="error">{error}</span>}
	</div>
	</div>
)

export const renderDaySelect = ({ input, handleSubmit, meta: { touched, error, warning }, selectedYear = null, selectedMonth = null }) => (
	<div className={`col-sm-3 col-xs-3`}>
	<div className={`field-select customDropDown ${input.value ? 'selected' : ''}`}>
	<label className={`dobLabel ${input.value ? 'dobLabelDisplayNone' : ''}`}><span>Day</span></label>
		<DayPicker { ...input }
			year={selectedYear}
			month={selectedMonth}
			onChange={event => {
				input.onChange(event)
				setTimeout(handleSubmit)
			}}
		/>
		{touched && error && <span className="error">{error}</span>}
	</div>
	</div>
)

export const renderDropdownDate = ({ input, range, last, handleSubmit, age, label, meta: { touched, error }, validations = null, selectedYear = null, selectedMonth = null, formNameForDOB = null }) => (
	<div className={`input-render-field borderBtmNone`}>
		<label className={`dateLabel`}><span>{label}</span></label>
		<div className={`secRow selectDateFields`}>
			<Field name={`${formNameForDOB}.DOB.Day`} component={renderDaySelect} selectedYear={selectedYear} selectedMonth={selectedMonth} validate={[required]} />
			
			<Field name={`${formNameForDOB}.DOB.Month`} component={renderMonthSelect} validate={[required]}  />

			<Field name={`${formNameForDOB}.DOB.Year`} component={renderYearSelect} minNumber={range ? !(age >= 0) ? moment().year() - 100 : moment().year() - (age + 1) : undefined} maxNumber={range ? getMaxNumber(age) : moment().year() } validate={[required]} />

			<div className={`hide`}>
			  <Field name={`${input.name}`} type="hidden" component={renderFieldDOB} validate={validations} />
		    </div>
		</div>
	</div>
)

export const renderDatePicker = ({ input, range, last, handleSubmit, age, label, meta: { touched, error } }) => (
	<div className={`container-field field-datepicker input-render-field`}>
		<label>{label}:</label>
		<div>
			<DatePicker { ...input } dropdownMode="select"
				dateFormat="DD/MM/YYYY"
				showYearDropdown showMonthDropdown fixedHeight
				popoverTargetAttachment="bottom left"
				popoverTargetOffset={last ? "10px -50px" : '10px 0px'}
				selected={input.value ? moment(input.value, 'DD/MM/YYYY') : null}
				minDate={range ? !(age >= 0) ? moment().subtract(100, 'years') : moment().subtract((age + 1), 'years') : undefined}
				maxDate={range ? getMaxDate(age) : moment() }
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
				onClickOutside={() => { }}
				onBlur={() => { }}
			/>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderFieldCheck = ({ input, name, handleSubmit, type, meta: { touched, error } }) => (
	<div className="container-field">
		<div>
			<input { ...input } type={type}
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			/>
		</div>
	</div>
)

export const renderFieldTextarea = ({ input, name, handleSubmit, label, type, meta: { touched, error } }) => (
	<div className="container-field">
		<textarea { ...input }
			onChange={event => {
				input.onChange(event)
				setTimeout(handleSubmit)
			}}
		/>
	</div>
)

export const renderFieldNoPlaceholder = ({ input, name, handleSubmit, label, type, meta: { touched, error } }) => (
	<div className="container-field">
		<label>{label}:</label>
		<div>
			<input { ...input } type={type}
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			/>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

class renderMemberSelection extends Component {

	componentDidMount() {
		this.props.fields.removeAll()
		this.props.fields.push({})
	}

	render() {
		const { fields, handleSubmit, meta: { touched, error } } = this.props
		return (
			<div>
				{fields.map((disease, index) =>
					<div key={disease + index} className="container-disease even">
						<div className="wrapper-level-1">
							<p className="disease-text"><span className="text">Select members</span></p>
							<div className="container-checks">
								{this.props.members.map((member, indexInner) => (
									<div key={disease + member.type + indexInner} className="container-check">
										<Field name={`${disease}.${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}.selected`} component={renderFieldCheck} type="checkbox" handleSubmit={handleSubmit} />
									</div>
								))}
							</div>
						</div>
					</div>
				)}
			</div>
		)
	}
}

class renderDiseases extends Component {

	componentDidMount() {
		if (this.props.fields.length != this.props.diseases.length) {
			this.props.fields.removeAll()
			this.props.diseases.forEach(disease => this.props.fields.push({ name: disease.name }))
		}
	}

	getComponent(field) {
		switch (field) {
			case 'date':
				return renderDropdownDate
				break
			case 'textarea':
				return renderFieldTextarea
				break
			case 'select':
				return renderFieldSelect
				break
			case 'input':
				return renderFieldNoPlaceholder
				break
		}
	}

	toggleAdditionalInfo(vals) {
		vals = values(vals).map(val => val.selected ? true : false)
		return vals.includes(true)
	}

	render() {
		const { fields, handleSubmit, meta: { touched, error } } = this.props
		return (
			<div>
				{fields.map((disease, index) =>
					<div key={disease + index} className={`container-disease ${index % 2 == 0 ? 'even' : 'odd'}`}>
						<div className="wrapper-level-1">
							<p className="disease-text"><span className="dash">-</span><span className="text">{this.props.diseases[index].text}</span></p>
							<div className="container-checks">
								{this.props.members.map((member, indexInner) => (
									<div key={disease + member.type + indexInner} className={`container-check`}>
										<Field name={`${disease}.${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}.selected`} component={renderFieldCheck} type="checkbox" handleSubmit={handleSubmit} />
									</div>
								))}
							</div>
						</div>

						<div className={`container-additional-info ${this.toggleAdditionalInfo(this.props.selectedValues[index]) ? '' : 'hide'}`}>
							{this.props.members.map((member, indexInner) => (
								<div className="wrapper-additional-info" key={`${disease + member.type + indexInner}additionalInfo`}>
									{this.props.diseases[index].questions && this.props.diseases[index].questions.map((question, indexQuestions) => (
										<div key={disease + member.type + indexInner + question.name}
											className={`wrapper-field ${this.props.selectedValues[index][`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`] && this.props.selectedValues[index][`${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`]['selected'] ? '' : 'hide'}`}
										>
											<Field name={`${disease}.${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}.${question.name}`}
												component={this.getComponent(question.type)}
												options={question.options || null}
												label={question.text} handleSubmit={handleSubmit}
											/>
										</div>
									))}
								</div>
							))}

							<div className="container-descriptors">
								{this.props.diseases[index].questions && this.props.diseases[index].questions.map((question, indexQuestions) => (
									<div className="wrapper-field" key={`outer${indexQuestions}${question.name}`}>
										<p className="descriptor">{question.text}</p>
										<Field name={question.name}
											component={this.getComponent(question.type)} options={question.options || null} label={question.text} handleSubmit={null}
										/>
									</div>
								))}
							</div>

						</div>
					</div>
				)}
			</div>
		)
	}
}



export const getInput = type => {
	switch (type) {
		case 'TextBox':
			return {
				input: renderField,
				type: 'text'
			}
		case 'DropDownBox':
			return {
				input: renderFieldSelectNew,
				type: 'select'
			}
		case 'RadioButton':
			return {
				input: renderFieldRadioNew,
				type: 'radio'
			}
		case 'DatePicker':
			return {
				input: renderDropdownDate,
				type: 'date'
			}
		case 'ReadOnly':
			return {
				input: renderFieldReadOnly,
				type: 'text'
			}
		case 'Readonly':
			return {
				input: renderFieldReadOnly,
				type: 'text'
			}
		case 'AutoTextBox':
			return {
				input: renderField,
				type: 'text'
			}
		case 'DropDownBoxHeight':
			return {
				input: renderFieldHeightSelectNew,
				type: 'select'
			}
		case 'DateMM/YYYY':
			return {
				input: renderDateMMYYYY,
				type: 'text'
			}
		case 'MultiLineTextBox':
			return {
				input: renderFieldTextArea,
				type: 'text'
			}
		case 'CascadeDropDownBox':
			return {
				input: renderFieldSelectCacading,
				type: 'select'
			}
		case 'Hidden':
			return {
				input: renderFieldReadOnlyHidden,
				type: 'text'
			}

		default:
			return {
				input: renderField,
				type: 'text'
			}
	}
}


export class renderFieldSelectCacading extends Component {
	state = {
		options: [],
		loading: false
	}

	componentDidMount() {
		this.getLocation(this.props)
	}

	componentWillReceiveProps(next) {
		let oldProps = this.props.values && this.props.values[this.props.insurer]

		let oldPin = oldProps && oldProps.pincode
		let oldTown = oldProps && oldProps.town

		let pin = next.values && next.values[next.insurer].pincode
		let town = next.values && next.values[next.insurer].town

		let isTown = next.input.name.includes('town')
		let isArea = next.input.name.includes('area')

		if (isTown && (oldPin != pin)) {
			this.reset()
			this.getLocation(next)
		}

		if (isArea && ((oldPin != pin) || (oldTown != town))) {
			this.reset()
			this.getLocation(next)
		}
	}

	reset = () => {
		this.props.input.onChange('')
	}

	getLocation = props => {
		if (props.values && props.values[props.insurer]) {
			if (props.input.name.includes('town')) {
				if (props.values[props.insurer].pincode) {
					this.setState({ loading: true })
					axios.get(`/api/getLocation?pincode=${props.values[props.insurer].pincode}`)
						.then(resp => {
							this.props.setCascadingOptions({
								form: 'contact',
								index: this.props.index,
								options: this.transformOptions(resp.data.city, 'town')
							})
							this.setState({ loading: false })
						})
				}
			}

			if (props.input.name.includes('area')) {
				if (props.values[props.insurer].pincode && props.values[props.insurer].town) {
					this.setState({ loading: true })
					axios.get(`/api/getLocation?pincode=${props.values[props.insurer].pincode}&cityId=${props.values[props.insurer].town}`)
						.then(resp => {
							this.props.setCascadingOptions({
								form: 'contact',
								index: this.props.index,
								options: this.transformOptions(resp.data.area, 'area')
							})
							this.setState({ loading: false })
						})
				}
			}
		}
	}

	transformOptions = (options, type) => {
		if (!options) return []

		if (type == 'town') {
			return options.map(city => { return { name: city.city_name, value: city.city_id } })
		}

		if (type == 'area') {
			return options.map(area => { return { name: area.areaName, value: area.areaID } })
		}
	}

	render() {
		let { input, handleSubmit, label, options, type, meta: { touched, error, warning } } = this.props

		return (
			<div className={`container-field field-select input-render-field field-select-cascading ${this.state.loading && 'loading'} ${input.value ? 'selected' : ''}`}>
				<label>{label}:</label>
				{ <img src={loader} /> }
				<div>
					<select { ...input }
						onChange={event => {
							input.onChange(event)
							setTimeout(handleSubmit)
						}}
					>
						<option />
						{
							this.props.options && (this.props.options.length > 0) &&
							this.props.options.map(option =>
								<option key={uuid.v1()} value={option.value}>{option.name}</option>
							)
						}
					</select>
					{touched && error && <span className="error">{error}</span>}
				</div>
			</div>
		)
	}
}

export const renderFieldRadioMedicalHistory = ({ input, selectMembers, questions, selectedValue, selectedValueInner, gender, members, handleSubmit, label, index, type, meta: { touched, error, warning } }) => (
	<div className={`container-field field-radio-medicalHistory radio-${input.name}`}>
		<div className="container-question-main">
			<label className="container-question">
				<span className="question">Q<span className="digit">&nbsp;{index}.</span></span>
				<span className="question-text">{label}</span>
			</label>
			<div className='wrapper-radio'>
				<label className='container-radio'>
					<Field name={input.name} component="input" type="radio" value={input.value[0]}
						onChange={event => {
							input.onChange(event)
							setTimeout(handleSubmit)
						}}
					/>
					<span>{input.value[0]}</span>
				</label>
				<label className='container-radio'>
					<Field name={input.name} component="input" type="radio" value={input.value[1]}
						onChange={event => {
							input.onChange(event)
							setTimeout(handleSubmit)
						}}
					/>
					<span>{input.value[1]}</span>
				</label>
				{touched && error && <span className="error">{error}</span>}
			</div>
		</div>
		{(questions || selectMembers) &&
			<div className={`container-member-details ${selectMembers ? 'single-detail' : ''} ${selectedValue == 'yes' ? 'show' : ''}`}>
				<div className="padding" />
				<div className="container-members">
					<div className="wrapper-members">
						{members.map((member, index) => <MemberIcon gender={gender} member={`${member.type == 'child' ? member.name : member.type}`} key={index} proposalForm={true} age={member.age} />)}
					</div>
				</div>
				<div className="container-questions-inner">
					{
						(selectMembers && !questions) &&
						<FieldArray name={`${input.name}Inner`} component={renderMemberSelection} members={members} handleSubmit={handleSubmit} />
					}
					{
						(!selectMembers && questions) &&
						<PerfectScrollbar>
							<FieldArray name={`${input.name}Inner`} component={renderDiseases} diseases={questions} members={members} selectedValues={selectedValueInner} handleSubmit={handleSubmit} />
						</PerfectScrollbar>
					}
				</div>
			</div>
		}
	</div>
)

export const renderFieldCheckNew = ({ input, label, extraClass, name, handleSubmit, type, meta: { touched, error } }) => (
	<div className={`container-field ${extraClass||''}`} >
		{label && <label>{label}:</label>}
		<input { ...input } type={type} checked={input.value == 'Yes'}
			onChange={event => {
				input.onChange(event)
				setTimeout(handleSubmit)
			}} id={input.name} className={`renderFieldCheckboxClass`}
		/>
		<label htmlFor={input.name}></label>
	</div>
)

export const renderDateMMYYYY = ({ input, handleSubmit, long, label, type, meta: { touched, error } }) => (
	<div className={`container-field ${long ? 'long' : ''}`}>
		<label>{label}:</label>
		<div>
			<input { ...input } placeholder={'MM/YYYY'} type={'text'}
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			/>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderFieldSelectNew = ({ input, handleSubmit, label, options, type, meta: { touched, error, warning } }) => (
	<div className={`container-field field-select input-render-field common-render-field  ${input.value ? 'selected' : ''}`}>
		<label><span>{label}</span></label>
		<div>
			<select { ...input }
				onChange={event => {
					input.onChange(event)
					setTimeout(handleSubmit)
				}}
			>
				<option />
				{
					(!options || options.length == 0) &&
					<option key={uuid.v1()} value={'Error'}>Error</option>
				}
				{
					options && (options.length > 0) && options[0].name && options[0].value &&
					options.map(option => <option key={uuid.v1()} value={option.value}>{option.name}</option>)
				}
				{
					options && (options.length > 0) && !options[0].name && !options[0].value &&
					options.map(option => <option key={uuid.v1()} value={option}>{option}</option>)
				}
			</select>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderFieldRadioNew = ({ radiovalues = null, input, handleSubmit, long, label, type, meta: { touched, error, warning } }) => (
	<div className={`container-field field-radio radio-new radio-${input.name} ${long ? 'long' : ''}`}>
		<label className="labelRadioBtn">{label}</label>
		<div className={`RadioInnerClass`}>
			<label className={`container-radio ${radiovalues == 'Yes' ? 'active' : ''}`}>
				<Field name={input.name} component="input" type="radio" value={'Yes'}
					validate={[required]}
					onChange={event => {
						input.onChange(event)
						setTimeout(handleSubmit)
					}}
				/>
				<span>Yes</span>
			</label>
			<label className={`container-radio ${radiovalues == 'No' ? 'active' : ''}`}>
				<Field name={input.name} component="input" type="radio" value={'No'}
					validate={[required]}
					onChange={event => {
						input.onChange(event)
						setTimeout(handleSubmit)
					}}
				/>
				<span>No</span>
			</label>
			{touched && error && <span className="error">{error}</span>}
		</div>
	</div>
)

export const renderFieldReadOnly = ({ input, selection, name, handleSubmit, label, type, meta: { touched, error } }) => (
	<div className="container-field field-read-only input-render-field">
		<label><span>{label}</span></label>
		<div className="value" >{selection}</div>
		{touched && error && <span className="error">{error}</span>}
	</div>
)

export const renderFieldReadOnlyHidden = ({ input, selection, name, handleSubmit, label, type, meta: { touched, error } }) => (
	<div className="container-field field-read-only field-hidden hidden_field_element">
		<label>{label}:</label>
		<div className="value" >{selection}</div>
		{touched && error && <span className="error">{error}</span>}
	</div>
)

export const renderFieldHeightSelectNew = ({ input, handleSubmit, validations, label, meta: { touched, error, warning }, feet, inch }) => (
	<div className={`container-field field-heightSelect input-render-field`}>
		<label><span>{label}</span></label>
		<div>
			<Field name={`${input.name}.feet`} component={renderHeightSelect} options={feet} opname="Feet" validate={validations} />
			<Field name={`${input.name}.inch`} component={renderHeightSelect} options={inch} validate={validations} opname="Inch" />
		</div>
	</div>
)

const renderFieldRadioMedicalHistoryWrapper = ({ input, handleSubmit, validations, meta: { touched, error }, values = null, valueKeys = null }) => (
	<div className="medicalHistoryBlueRadioButtons">
		<div className='wrapper-radio'>
			<label className={`container-radio ${values && values.common[valueKeys] == 'Yes' ? 'active' : ''}`}>
				<Field name={input.name} component="input" type="radio" value={'Yes'}
					onChange={event => {
						input.onChange(event)
						setTimeout(handleSubmit)
					}}
					validate={validations}
				/>
				<span>Yes</span>
			</label>
			<label className={`container-radio ${values && values.common[valueKeys] == 'No' ? 'active' : ''}`}>
				<Field name={input.name} component="input" type="radio" value={'No'}
					onChange={event => {
						input.onChange(event)
						setTimeout(handleSubmit)
					}}
					validate={validations}
				/>
				<span>No</span>
			</label>
		</div>
		{touched && error && <span className="error">{error}</span>}
	</div>
)

class renderDiseasesNew extends Component {

	getComponent = field => getInput(field)

	getMemberName = member => `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`

	toggleAdditionalInfo(vals) {
		vals = values(vals).map(val => val.selected ? true : false)
		return vals.includes(true)
	}

	_toggleAdditionalInfo = (question, all) => {
		let members = { ...all }
		members.common && delete members.common
		let isMemberSelected = values(members).filter(member => member[question] && member[question] == 'Yes').length > 0
		return isMemberSelected
	}

	render() {
		const { selectedValues, input: { name }, questions, members, handleSubmit, meta: { touched, error }, gender = null } = this.props
		return (
			<div className="renderDiseasesNew" >
				{questions.map((question, index) => (
					<div key={question.name + index} className={`container-disease ${index % 2 == 0 ? 'even' : 'odd'}`}>
						<div className="wrapper-level-1 wrapper-table">
							<header>
								<p className="disease-text">
									<span className="dash">-</span>
									<span className="text">{question.value}</span>
								</p>
							</header>
							<div className="container-checks">
								{
									members.map((member, memberIndex) => (
										<div key={question.name + memberIndex} className={`container-check`}>
											<Field component={renderFieldCheckNew} type="checkbox"
												name={`${this.getMemberName(member)}.${question.name}`}
												handleSubmit={handleSubmit} normalize={toYesNo}
											/>
											<MemberIcon gender={gender} member={`${member.type == 'child' ? member.name : member.type}`} key={memberIndex} proposalForm={true} age={member.age.value} />
										</div>
									))
								}
							</div>
						</div>
						{
							this._toggleAdditionalInfo(question.name, selectedValues) &&
							<div className={`container-additional-info add_info`}>
								{members.map((member, indexInner) => (
									<div className="wrapper-additional-info" key={`${question.name + indexInner}additionalInfo`}>
										{
											question.questions.map(questionInner => (
												selectedValues[this.getMemberName(member)] && selectedValues[this.getMemberName(member)] &&
												selectedValues[this.getMemberName(member)][question.name] &&
												selectedValues[this.getMemberName(member)][question.name] == 'Yes' &&
												<div key={question.name + questionInner.name + indexInner}
													className={`wrapper-field`}
												>
													<Field name={`${this.getMemberName(member)}.${questionInner.name}`}
														component={this.getComponent(questionInner.typeOfControl)['input']}
														type={this.getComponent(questionInner.typeOfControl)['type']}
														options={questionInner.options || null}
														last={indexInner == (this.props.members.length - 1) ? true : false}
														label={questionInner.value} handleSubmit={handleSubmit}
														validate={questionInner.typeOfControl == 'DateMM/YYYY' ? [...getValidations(questionInner.validationId), validateMMYYYY] : getValidations(questionInner.validationId)}
													/>
												</div>
											))
										}
									</div>
								))}

								{/*<div className="container-descriptors">
									{
										question.questions.map((questionInner, indexQuestions) => (
											<div className="wrapper-field" key={`outer${indexQuestions}${question.name}`}>
												<p className="descriptor">{questionInner.value}</p>
												<Field name={questionInner.name} component={this.getComponent(question.typeOfControl)['input']}
													handleSubmit={null}
												/>
											</div>
										))
									}
								</div>*/}
							</div>
						}
					</div>
				))}
			</div>
		)
	}
}

class renderDiseasesTypeB extends Component {

	componentDidMount() {
		this.props.members.map(member => {
			let hasValue = this.props.values[this.getMemberName(member)] && this.props.values[this.getMemberName(member)][this.props.input.name]
			this.props.change(`${this.getMemberName(member)}.${this.props.input.name}`, hasValue || 'No')
		})
	}

	getComponent = field => getInput(field)

	toggleAdditionalInfo(vals, question) {
		return !requireMemberSelect(question)(null, vals) && true
	}

	getMemberName = member => `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`

	render() {
		const { input: { name }, handleSubmit, meta: { touched, error }, gender = null } = this.props
		return (
			<div className="renderDiseasesTypeB">
				<div className={`container-disease even`}>
					<div className="wrapper-level-1">
						<p className="disease-text">
							<span className="text">Select Members</span>
						</p>
						<div className="container-checks">
							{
								this.props.members.map((member, memberIndex) => (
									<div key={name + member.type + memberIndex} className={`container-check`}>
										<Field component={renderFieldCheckNew} type="checkbox" handleSubmit={handleSubmit}
											name={`${this.getMemberName(member)}.${name}`} normalize={toYesNo}
										/>
										<MemberIcon gender={gender} member={`${member.type == 'child' ? member.name : member.type}`} key={memberIndex} proposalForm={true} age={member.age.value} />
										{
											this.toggleAdditionalInfo(this.props.values, name) &&
											<div className={`container-additional-info ${this.toggleAdditionalInfo(this.props.values, name) ? '' : 'hide'}`}>
												{
													//this.props.members.map((member, indexInner) => (
														<div className="wrapper-additional-info" key={`${name + member.type + memberIndex}additionalInfo`}>
															{
																this.props.values && this.props.values[this.getMemberName(member)] &&
																(this.props.values[this.getMemberName(member)][name] == 'Yes') &&
																this.props.questions &&
																this.props.questions.map((question) => (
																	<div key={name + member.type + memberIndex + question.name} className={`wrapper-field`} >
																		<Field name={`${this.getMemberName(member)}.${question.name}`}
																			component={getInput(question.typeOfControl)['input']}
																			type={getInput(question.typeOfControl)['type']}
																			label={question.value} options={question.options || null}
																			last={memberIndex == (this.props.members.length - 1) ? true : false}
																			handleSubmit={handleSubmit}
																			validate={getValidations(question.validationId)}
																			normalize={fixDate(question.typeOfControl)} formNameForDOB={`${this.props.formNameForDOB}.${this.getMemberName(member)}`} 
																			selectedYear={ this.props.values && this.props.values[this.props.formNameForDOB] && this.props.values[this.props.formNameForDOB][this.getMemberName(member)] && this.props.values[this.props.formNameForDOB][this.getMemberName(member)].DOB && this.props.values[this.props.formNameForDOB][this.getMemberName(member)].DOB.Year } 
																			selectedMonth={ this.props.values && this.props.values[this.props.formNameForDOB] && this.props.values[this.props.formNameForDOB][this.getMemberName(member)] && this.props.values[this.props.formNameForDOB][this.getMemberName(member)].DOB && this.props.values[this.props.formNameForDOB][this.getMemberName(member)].DOB.Month }
																		/>
																	</div>
																))
															}
														</div>
													//))
												}

												{/*<div className="container-descriptors">
													{
														this.props.questions && this.props.questions.map((question, indexQuestions) => (
															<div className="wrapper-field" key={`outer${indexQuestions}${question.id.toString()}`}>
																<p className="descriptor">{question.value}</p>
																<Field name={question.id.toString()}
																	component={getInput(question.typeOfControl)['input']}
																	type={getInput(question.typeOfControl)['type']}
																	label={question.value} options={question.options || null}
																	handleSubmit={handleSubmit}
																/>
															</div>
														))
													}
												</div>*/}

											</div>
										}
									</div>
								))
							}
						</div>
					</div>

					

				</div>
			</div>
		)
	}
}

class renderDiseasesTypeC extends Component {

	getComponent = field => getInput(field)

	getMemberName = member => `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`

	render() {
		const { input: { name }, handleSubmit, validations, typeOfControl, options, gender = null } = this.props
		return (
			<div className="renderDiseasesTypeC">
				<div className={`container-disease even`}>
					<div className={`container-additional-info no-checks`}>
						{
							this.props.members.map((member, index) => (
								<div className="wrapper-additional-info" key={`${name + index}additionalInfo`}>
									<div className={`wrapper-field`} >
										<Field name={`${this.getMemberName(member)}.${name}`}
											component={getInput(typeOfControl)['input']}
											type={getInput(typeOfControl)['type']}
											options={options || null}
											handleSubmit={handleSubmit}
											last={index == (this.props.members.length - 1) ? true : false}
											validate={validations}
										/>
										<MemberIcon gender={gender} member={`${member.type == 'child' ? member.name : member.type}`} key={index} proposalForm={true} age={member.age.value} />
									</div>
								</div>
							))
						}
					</div>
				</div>
			</div>
		)
	}
}

class renderMemberSelectionNew extends Component {

	componentDidMount() {
		this.props.members.map(member => {
			let hasValue = this.props.values[this.getMemberName(member)] && this.props.values[this.getMemberName(member)][this.props.input.name]
			this.props.change(`${this.getMemberName(member)}.${this.props.input.name}`, hasValue || 'No')
		})
	}

	getMemberName = member => `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`

	render() {
		const { input: { name }, handleSubmit, meta: { touched, error }, gender = null } = this.props
		return (
			<div className="container-disease even renderMemberSelectionNew">
				<div className="wrapper-level-1 members-select">
					<p className="disease-text"><span className="text">Select members</span></p>
					<div className="container-checks">
						{
							this.props.members.map((member, indexInner) => (
								<div key={name + indexInner} className="container-check">
									<Field name={`${this.getMemberName(member)}.${name}`}
										component={renderFieldCheckNew} type="checkbox"
										handleSubmit={handleSubmit}
										normalize={toYesNo}
									/>
									<MemberIcon gender={gender} member={`${member.type == 'child' ? member.name : member.type}`} key={indexInner} proposalForm={true} age={member.age.value} />
								</div>
							))
						}
					</div>
				</div>
			</div>
		)
	}
}

class RenderSocialSecurity extends Component {
	render() {
		return (
			<div className="container-member-details single-detail show social-security">
				{
					this.props.meta.error &&
					<label className="error">{this.props.meta.error}</label>
				}
				<div className="container-questions-inner">
					<div className="container-disease even">
						<div className="container-additional-info ">
							<div className="wrapper-additional-info">
								{
									this.props.questions.map((question, index) => (
										<div key={question.name + index} className={`wrapper-field`} >
											<Field name={`common.${question.name}`} component={renderFieldCheckNew} 
												type="checkbox" normalize={toYesNo} handleSubmit={this.props.handleSubmit}
												label={question.value} extraClass="field-social-security-checkbox"
												validate={[]}
											/>
										</div>
									))
								}
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export class renderFieldRadioMedicalHistoryNew extends Component {
	constructor(props) {
        super(props)
        this.members = this.props.members.map(
            member => `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`
        )
    }
	getValidations = () => {
		let questions = this.props.questions
		let selectMembers = this.props.selectMembers
		let typeOfControl = this.props.typeOfControl

		if (
			(selectMembers && !questions) ||
			(!selectMembers && !questions && !typeOfControl) ||
			(selectMembers && questions && Array.isArray(questions) && !(questions.filter(question => question.questions).length > 0))
		) {
			return [requireMemberSelect(this.props.input.name)]
		}

		else if (
			(selectMembers && questions) &&
			Array.isArray(questions) &&
			(questions.filter(question => question.questions).length > 0)
		) {
			return [requireDiseaseSelect(this.props.questions, this.props.input.name)]
		}
		else return []
	}

	render() {
		let { input, selectMembers, validations, typeOfControl, questions, selection, options, values, gender, members, handleSubmit, label, index, type, meta: { touched, error, warning } } = this.props
		return (
			<div className={`container-field field-radio-medicalHistory radio-${input.name} ${selectMembers && !questions && 'member-select'}`}>
				<div className="container-question-main">
					<label className="container-question">
						<span className="question">Q<span className="digit">&nbsp;{index}.</span></span>
						<span className="question-text">{label}</span>
					</label>
					{/* Root yes or no */}
					{
						((selectMembers && !typeOfControl) || !typeOfControl) &&
						<Field name={`common.${input.name}`}
							component={renderFieldRadioMedicalHistoryWrapper}
							handleSubmit={handleSubmit}
							validations={this.getValidations()} values={values} valueKeys={input.name}
						/>
					}
				</div>
				{
					(values && values.common[input.name] == 'Yes') && (questions || selectMembers) && !(!selectMembers && questions) &&
					<div className={`container-member-details ${selectMembers && (!questions || !(Array.isArray(questions) && questions.filter(question => question.questions).length > 0)) ? 'single-detail' : ''} ${values && values.common[input.name] == 'Yes' ? 'show' : ''}`}>
						<div className="container-questions-inner">
							{/* renderMemberSelectionNew */}
							{
								selectMembers && !questions && !typeOfControl &&
								<Field name={`${input.name}`} component={renderMemberSelectionNew}
									members={members} handleSubmit={handleSubmit}
									validate={[requireMemberSelect(input.name)]}
									change={this.props.change} values={values} gender={gender}
								/>
							}
							{
								selectMembers && questions &&
								<PerfectScrollbar option={{ wheelPropagation: true }}>
									{/* renderDiseasesTypeB */}
									{
										// If the question is an object
										!Array.isArray(questions) &&
										<Field name={`${input.name}`} component={renderDiseasesTypeB}
											questions={[questions]} members={members} values={values}
											handleSubmit={handleSubmit} change={this.props.change} gender={gender} formNameForDOB={this.props.formNameForDOB}
										/>
									}
									{/* renderDiseasesTypeB */}
									{
										// No nested questions - display each question
										Array.isArray(questions) &&
										!(questions.filter(question => question.questions).length > 0) &&
										<Field name={`${input.name}`} component={renderDiseasesTypeB}
											questions={questions} members={members}  change={this.props.change}
											values={values} handleSubmit={handleSubmit} gender={gender} formNameForDOB={this.props.formNameForDOB}
										/>
									}
									{/* renderDiseasesNew */}
									{
										// Nested questions - display each question enclosed in check box
										Array.isArray(questions) &&
										(questions.filter(question => question.questions).length > 0) &&
										<Field name={`${input.name}`} component={renderDiseasesNew}
											questions={questions} members={members} selectedValues={values}
											handleSubmit={handleSubmit} gender={gender}
										/>
									}
								</PerfectScrollbar>
							}
						</div>
					</div>
				}
				{/*For RSA last question*/}
				{
					selectMembers && !questions && typeOfControl &&
					<div className={`container-member-details show single-detail`}>
						<div className="container-questions-inner">
							<Field name={`${input.name}`} component={renderDiseasesTypeC}
								members={members} selectedValues={values && values.members}
								handleSubmit={handleSubmit} typeOfControl={typeOfControl}
								validations={getValidations(validations) || []}
								options={options || null} gender={gender}
							/>
						</div>
					</div>
				}
				{
					!selectMembers && !questions &&
					<div className={`container-member-details single-detail ${(typeOfControl || (!typeOfControl && (values && values.common[input.name] == 'Yes')) ? 'show' : '')}`}>
						<div className="container-questions-inner">
							{/* renderDiseasesTypeC */}
							{
								typeOfControl &&
								<Field name={`${input.name}`} component={renderDiseasesTypeC}
									members={members} selectedValues={values && values.members}
									handleSubmit={handleSubmit} typeOfControl={typeOfControl}
									validations={getValidations(validations) || []}
									options={options || null} gender={gender}
								/>
							}
							{/* renderMemberSelectionNew */}
							{
								!typeOfControl && (values && values.common[input.name] == 'Yes') &&
								<Field name={`${input.name}`} component={renderMemberSelectionNew}
									members={members} handleSubmit={handleSubmit}
									validate={[requireMemberSelect(input.name)]}
									change={this.props.change} values={values} gender={gender}
								/>
							}
						</div>
					</div>
				}
				{/*For Star social security question*/}
				{
					(values && values.common[input.name] == 'Yes') &&
					!selectMembers && questions && !typeOfControl &&
					<Field name={input.name} component={RenderSocialSecurity} questions={questions} 
						handleSubmit={handleSubmit} validate={[requireSocialStatusSelect(input.name, questions)]}
					/>
				}
			</div>
		)
	}
}


//Validations
const required = value => value ? undefined : 'Required'

const requireMemberSelect = question => (value, all) => {
	if (all.common[question] == 'Yes') {
		let members = { ...all }
		members.common && delete members.common
		let anyMemberSelected = values(members).filter(member => member[question] && member[question] == 'Yes').length > 0
		return anyMemberSelected ? undefined : 'Select member'
	}
	else {
		return undefined
	}
}

const requireDiseaseSelect = (questions, rootQuestion) => (value, all) => {
	questions = questions.map(question => question.name)
	let members = { ...all }
	members.common && delete members.common
	let memberNames = Object.keys(members).map(member => {
		// Get all questions which are answered 'Yes'
		let yes = Object.keys(members[member]).filter(_question => members[member][_question] == 'Yes')
		// Filter out all root level questions.
		yes = yes.filter(_yes => questions.includes(_yes))

		return yes.length > 0
	}).includes(true)

	return memberNames || (all.common[rootQuestion] == 'No') ? undefined : 'Select a member'
}

const requireSocialStatusSelect = (rootQuestion, questions) => (value, all) => {
	let children = questions.map(_q => _q.name)
	let childrenExist = children.filter(child => all.common[child] && (all.common[child] == 'Yes')).length > 0
	if (all.common[rootQuestion] && (all.common[rootQuestion] == 'Yes')) {
		if (!childrenExist) return 'Select an option'
	}
	return undefined
}

const validateMMYYYY = value => {
	if (value && !/^((0[1-9]|10|11|12))(-|\/)([0-9]{4})$/.test(value)) return 'Invalid date'
	else {
		let current = moment(new Date()).format("MM/YYYY").split('/')
		let entered = value.split('/')
		if (entered[1] > current[1]) return "Can't be a future date"
		else if ((entered[1] == current[1]) && (entered[0] > current[0])) return "Can't be a future date"
		else return undefined
	}
}

//Normalizations
const toBool = value => {
	if (value == 'true') return true
	else if (value == 'false') return false
}

const toYesNo = value => {
	if (value) return 'Yes'
	else if (!value) return 'No'
}

export const fixDate = name => value => {
	if (name.toLowerCase().includes('date')) {
		if (!moment(value, 'DD/MM/YYYY').isValid()) return ''
		return moment(value, 'DD/MM/YYYY').format('DD/MM/YYYY')
	}
	else return value
}

