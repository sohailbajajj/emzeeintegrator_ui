import moment from 'moment'

export const validateMedicalHistoryForm = values => {
	const errors = {}

	if (!values.preexistingDiseases) {
		errors.preexistingDiseases = 'Required'
	}
	if (!values.recentIllnesses) {
		errors.recentIllnesses = 'Required'
	}
	if (!values.previousClaim) {
		errors.previousClaim = 'Required'
	}
	if (!values.proposal) {
		errors.proposal = 'Required'
	}
	if (!values.preCover) {
		errors.preCover = 'Required'
	}

	return errors
}

const required = value => value ? undefined : 'Required'

const number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined

const mobile = value => value && !isNaN(Number(value)) && Number(value).length == 10 
	&& ((value.charAt(0) == 7) || (value.charAt(0) == 8) || (value.charAt(0) == 9)) ?
	undefined : 'Invalid mobile number'

const email = value => value && 
	!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
  	'Invalid email address' : undefined

const string = value => value && !/^[a-zA-Z()]+$/.test(value) ? 'Must only have alphabets' : undefined

const mobile789 = value => value && !/^[789]\d{9}$/.test(value) ? 'Invalid mobile number' : undefined

const gst = value => value && !/^(([0-2]{1}[0-9]|3[0-5]{1})([a-zA-z]{3}[pfchatgljPFCHALTGLJ]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1})([a-zA-Z0-9]{1})[zZ]([a-zA-Z0-9]{1}))$/.test(value) ? 'Invalid GST number' : undefined

const spaceQuote = value => value && 
	!/^[A-Za-z']+( [A-Za-z']+)*$/.test(value) ?
	'Invalid' : undefined

const max = max => value => value && parseInt(value) > max ? 
	`Must be at most ${max}` : undefined

const min = min => value => value && parseInt(value) < min ? 
	`Must be atleast ${min}` : undefined

const range = (min, max) => value => value && !((parseInt(value) >= min) && (parseInt(value) <= max)) ? 
  	`Must be between ${min} and ${max}` : undefined

const age18 = value => moment().diff(moment(value,'DD/MM/YYY'), 'years') >= 18 ? undefined : `Must be atleast 18`

const year034 = range(0,34)

const cigarettes10 = max(10)

const bloodSuagr = range(70,99)

const bloodPressureSys = range(90,129)

const bloodPressureDia = range(60,79)

const cholesterol = range(150,199)

const tobacco = max(7)

const peg = max(9)

const beer = max(10)

const wine = max(6)

const alphaNumericSingleQuote = value => value && !/^([a-zA-Z0-9']{1,30})$/.test(value) ? 'Must only be alpha-numeric. No other special characters, including space, are allowed.' : undefined

const pan = value => value && 
!/^([a-zA-z]{3}[pfchatgljPFCHALTGLJ]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1})$/.test(value) ?
  'Invalid pan number' : undefined

const maxChars = max => value => value && value.toString().length > max ? 
	`Must be at most ${max} characters` : undefined

const maxDigitsWeight = value => value && !/^((2[0]{1,2})|(1[0-9]{1,2})|([0-9]{1,2}))$/.test(value) ? 'Invalid number' : undefined

const adhar = value => value && !/^[0-9]{1,12}$/.test(value) ? 'Invalid Adhar number' : undefined
											
export const adharPanMatch = insurer => (value, all) => {
	let isGSTINInvalid = () => gst(value)
	let panDefined = all && all[insurer] && all[insurer].panNumber
	let isPanInvalid = () => pan(panDefined)

	if (!panDefined || (panDefined && isPanInvalid())) return undefined
	else {
		if (value && !isGSTINInvalid() && value.toLowerCase().includes(panDefined.toLowerCase())) {
			return undefined
		}
		else return 'GST PAN mismatch'
	}
}

const validationMap = validation => {
	switch (validation) {
	case 1:
		return string
	case 2:
		return number
	case 4:
		return mobile789
	case 5:
		return email
	case 6:
		return spaceQuote
	case 7:
		return () => undefined
	case 8:
		return () => undefined
	case 9:
		return age18
	case 10:
		return year034
	case 11:
		return required
	case 12:
		return cigarettes10
	case 13:
		return bloodSuagr
	case 14:
		return bloodPressureSys
	case 15:
		return bloodPressureDia
	case 16:
		return cholesterol
	case 17:
		return () => undefined
	case 18:
		return tobacco
	case 19:
		return peg
	case 20:
		return beer
	case 21:
		return wine
	case 22:
		return () => undefined
	case 23:
		return () => undefined
	case 24:
		return () => undefined
	case 25:
		return pan
	case 26:
		return maxDigitsWeight
	case 27:
		return maxChars(150)
	case 28:
		return maxChars(80)
	case 29:
		return maxChars(6)
	case 30:
		return maxChars(10)
	case 33:
		return gst
	case 34:
		return adhar
	case 35:
		return alphaNumericSingleQuote
	case 36:
		return maxChars(30)
	default:
		return () => undefined
	}
}

export const getValidations = validationIds => {
	if (!validationIds) return []
	
	let validations = []

	if (Array.isArray(validationIds)) {
		validationIds.forEach(validation => {
			validations.push(validationMap(validation))
		})
	}
	else {
		validations.push(validationMap(validationIds))
	}
	
	return validations
}