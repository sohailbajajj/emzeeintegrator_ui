import React from 'react'
import styled from 'styled-components'

const HeaderDesc = styled.div`
	h1 {
		text-align:center;
		color:${props => props.details ? props.theme.copyLighter : props.theme.copyDark};
		font-size:${props => props.quotes ? '1.3rem' : '1.5rem'};
		font-family: ${props => props.quotes ? 'medium' : 'regular'};
	}

	p {
		color:${props => props.theme.copyLighter};
		font-size:${props => props.quotes ? '1.15rem' : '0.9rem'};
		text-align:center;
		margin-top:${props => props.quotes ? '0.7rem' : '0.5rem'};
	}

	h1, p {
		>label {
			position:relative;
			display:inline-block;

			span:not(.city) {
				text-transform:capitalize;
			}

			&.active {
				span {
					color: ${props => props.theme.purple};
	
					>label {
						text-transform:none;
					}
				}
	
				&::after {
					content: '';
					position:absolute;
					height:2px;
					width:100%;
					background:${props => props.theme.green};
					left:0;
					bottom:-0.4rem;
				}
			}
		}
	}

	@media (max-width: 1200px) {
		h1 {
			font-size: 20px;
			line-height: 30px;
		}

		p {
			font-size: 14px;
			line-height:19px;
		}
	}

	@media (max-width: 650px) {
		p {
			font-size:16px;
			line-height: 24px;
			
			label::after {
				display:none;
			}
		}
	}
`

export default props => <HeaderDesc {...props}>
	<h1>{props.head}</h1>
	<p>{props.desc}</p>
</HeaderDesc>