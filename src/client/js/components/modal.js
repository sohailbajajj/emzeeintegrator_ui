import React, { Component } from 'react'
import styled from 'styled-components'
import PerfectScrollbar from '@opuscapita/react-perfect-scrollbar'

import close from '../../images/close.png'
import closeWhite from '../../images/close-white.png'

import { enableScroll, disableScroll, stopBubbling } from '../utils/helpers'

const _Modal = styled.section`
	position:fixed;
	top:0;
	left:0;
	background: transparent;
	width:0px;
	height:0px;
	overflow:hidden;
	opacity:0;
	z-index:-20;
	pointer-events: none;
	
	.underlay {
		width:100%;
		height:100%;
		position:absolute;
		left:0;
		top:0;
		z-index:1;
		background-color: #4b5960;
		opacity:0;
		transition: opacity 300ms ease;
		transition-delay:150ms;
	}

	.container-modal {
		background: #FFF;
		border-radius: 8px;
		${props => props.theme.absoluteCenter()};
		z-index:2;
		box-shadow: ${props => props.theme.shadow};
		padding: 1.3rem 1.5rem;
		box-sizing:border-box;
		margin-top:5px;
		opacity:0;
		transition: all 300ms ease-in;
		transition-delay:0;

		.close {
			width: 0.9rem;
			padding: 0.8rem;
			display: block;
			position: absolute;
			top: 0.7rem;
			right: 0.8rem;
			cursor: pointer;
			transition: width 300ms ease-out;

			:hover {
				width:1rem;
			}

			&.white {
				background:${props => props.theme.purple}
				width: 0.7rem !important;
				padding: 0.7rem;
				top:0;
				right: -2.2rem;
			}
		}

		h4 {
			font-family:medium;
			color: ${props => props.theme.copyDark}
			font-size:1.1rem;
		}
	}

	&.activate {
		opacity: 1;
		width:100%;
		height:100%;
		overflow:visible;
		pointer-events:all;
		z-index:20;
	}

	&.active {
		.underlay {
			opacity:0.85;
			transition-delay:0 !important;
		}

		.container-modal {
			margin-top:0;
			opacity:1;
			transition-delay:150ms;
		}
	}

	.ps__rail-y {
		z-index:1;
	}

	@media (max-width: 650px) {
		.container-modal {
			min-width:0 !important;
			
			h4 {
				font-size:16px;
			}

			.close {
                width: 14px !important;
            }
		}
	}
`

export default class Modal extends Component {
    state = { classes: '' }

    componentDidMount() {
        window.addEventListener('keydown', this.checkEscape)
    }

    componentWillUnmount() {
        window.removeEventListener('click', this.checkEscape)
		window.removeEventListener('scroll', disableScroll)
		document.body.removeEventListener('touchmove', stopBubbling, false)
    }

    componentDidUpdate(_props) {
        _props.active != this.props.active && this.toggle(this.props.active)
    }

    checkEscape = event => {
        event.which == 27 && this.toggle(false)
    }

    toggle = active => {
        if (active) {
            this.setState({ classes: 'activate active' })
			window.addEventListener('scroll', disableScroll)
			document.body.addEventListener('touchmove', stopBubbling, false)
        } else {
            const close = () => setTimeout(() => this.setState({ classes: '' }), 500)
            this.setState({ classes: 'activate' }, close)
            this.props.onClose && this.props.onClose()
			window.removeEventListener('scroll', disableScroll)
			document.body.removeEventListener('touchmove', stopBubbling, false)
            enableScroll()
        }
    }

    render() {
        return (
			<_Modal className={`modal ${this.props.className} ${this.state.classes}`} 
				onClick={!this.props.bubble ? stopBubbling : () => ''} 
			>
                <div className={`underlay`} />
				<PerfectScrollbar>
					<div className={`container-modal`}>
						<h4>{this.props.head}</h4>
						{
							!this.props.closeWhite && 
							<img className="close" src={close} onClick={() => this.toggle(false)} />
						}
						{
							this.props.closeWhite && 
							<img className="close white" src={closeWhite} onClick={() => this.toggle(false)} />
						}
						{this.props.children}
					</div>
				</PerfectScrollbar>
            </_Modal>
        )
    }
}
