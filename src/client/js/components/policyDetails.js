import React from 'react'
import CountUp from 'react-countup'

const PolicyDetails = ({ policy, PostPayment, PaymentSuccess, proposalForm }) => (
    <div className="nav-policy-details" >
		{
			policy &&
			<div className="wrapper">
				<div className="logo-name">
					<div className="wrapper">
						<img src={ policy.logo } />
						<div>{ policy.productName }</div>
					</div>
				</div>
				<div className="duration">
					<div className="wrapper">
						<label>duration</label>
						{
							policy.premium.selected ? 
							getDuration(policy.premium.selected)
							:
							getDuration('firstYear')
						}
					</div>
				</div>
				<div className="sum">
					<div className="wrapper">
						<label>SUM INSURED</label>
						{ formatSum(parseInt(policy.sumInsured)) }
					</div>
				</div>
				<div className="premium">
					<div className="wrapper">
						<label>PREMIUM/year</label>
						<span className="digit">
							<i className="fa fa-inr" aria-hidden="true" />&nbsp;
							{
								<CountUp start={parseInt(policy.premium[policy.premium.selected || 'firstYear'].totalPremium)} 
									end={getEndValue(proposalForm,policy)} 
									useEasing={true} duration={3} useGrouping={true} separator=","
								/>
							}
						</span>
					</div>
				</div>
				{
					PostPayment &&
					<div className="amount">
						<div className="wrapper">
							<label>{PaymentSuccess ? 'Paid' : 'To be paid'}</label>
							<span className="digit">
								₹&nbsp;
								{
									parseInt(policy.premium[policy.premium.selected || 'firstYear'].totalPremium).toLocaleString('en-IN')
								}
							</span>
						</div>
					</div>
				}
			</div>
		}
	</div>
)

export default PolicyDetails

const getDuration = duration => {
	switch(duration) {
		case 'firstYear':
			return <span><span className="digit">1</span> year</span>
		case 'secondYear':
			return <span><span className="digit">2</span> years</span>
		case 'thirdYear':
			return <span><span className="digit">3</span> years</span>
	}
}

const formatSum = (val, isMobile) => {
	if (val >= 10000000) val = `${val/10000000 } Crore`
	else if (val >= 100000) val = `${val/100000 } Lacs`
	else if (val >= 1000) val = `${val/1000 } K`
	return <span><i className="fa fa-inr" aria-hidden="true" /> <span className="digit">{val.split(' ')[0]}</span> {val.split(' ')[1]}</span>
}

const getEndValue = (proposalForm, policy) => {
	if (proposalForm && (proposalForm.submitResponse || proposalForm.discountedPremium)) {
		if (proposalForm.discountedPremium) {
			return proposalForm.discountedPremium
		}

		if (proposalForm.submitResponse) {
			if (proposalForm.submitResponse.popUp) {
				let isDiscounted = proposalForm.submitResponse.popUp.filter(popup => popup.action == 'DISCOUNT').length > 0
				if (!isDiscounted && proposalForm.submitResponse.totalPremium) {
					return proposalForm.submitResponse.totalPremium
				}
				else return parseInt(policy.premium[policy.premium.selected || 'firstYear'].totalPremium)
			}
			else return proposalForm.submitResponse.totalPremium
		}
	}
	else {
		return parseInt(policy.premium[policy.premium.selected || 'firstYear'].totalPremium)
	}
}