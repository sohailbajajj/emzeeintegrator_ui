import React, { Component } from 'react'

import success from '../../images/payment_success.png'
import failure from '../../images/payment_failure.png'
import RSAPg from './buy/pg/rsaPg'
import ReligarePg from './buy/pg/religarePg'
import Results from './results'
import PolicyDetails from './policyDetails'

export default class PostPayment extends Component {

    /* state = {
		enableNotification: false
	} */

    componentDidMount() {
        //this.updateProposal()
        /* setTimeout(() => {
			this.activateNotification(this.props)
		}, 1500) */
    }

    /* activateNotification = props => {
        this.setState({
			enableNotification: (props.location && props.location.query && props.location.query.response && props.location.query.response.toLowerCase() == 'success') && (Object.keys(props && props.members).length > 0)
		})
	} */

    render() {
        const locQuery = this.props.location && this.props.location.query;
        const status = locQuery && locQuery.response && locQuery.response.toLowerCase() == 'success' ? 'SUCCESSFUL' : 'FAILED';
        const transId = locQuery && locQuery.tid;
        return (
            <div className={`container-stage stage-postPayment`}>
                <div className="wrapper-main">
                    <h1 className={status}>
                        TRANSACTION { status }
                    </h1>
                    <h6>
                        Transaction ID : { transId }
                    </h6>
                    {
                        locQuery && locQuery.quoteId && <h6>
                            Proposal No. : { locQuery.quoteId }
                        </h6>
                    }

                    {
                        locQuery && locQuery.policyNum && <h6>
                            Policy No. : { locQuery.policyNum }
                        </h6>
                    }
                    <div className="msg">
                        {
                            locQuery && locQuery.response && locQuery.response.toLowerCase() == 'success' && <p>
                                Thank You for buying a policy with us! We will send a confirmation once the policy is issued. Your policy details are mentioned below.
                            </p>
                        }

                        {
                            locQuery && locQuery.response && !(locQuery.response.toLowerCase() == 'success') && <p>
                                Whoops! Seems like there was an error while paying. <br />
								To place the order again please review the policy details as mentioned below and continue to checkout.
                            </p>
                        }
                    </div>

                    <div className="container-purchase-details">
                        
                        <Results {...this.props} enable={true} PostPayment={true} family = { this.props.family } />
                        
                        <PolicyDetails policy = { this.props.policy } PostPayment = {true} PaymentSuccess = { locQuery && locQuery.response && locQuery.response.toLowerCase() == 'success' } />

                        {
                            locQuery && locQuery.response && !(locQuery.response.toLowerCase() == 'success') && this.props.proposalForm && this.props.proposalForm.submitResponse && <div className="button-redirect">
                                {
                                    this.props.proposalForm.submitResponse && this.props.proposalForm.submitResponse.insurerId == 3 && <RSAPg premium = { this.props.proposalForm.submitResponse.totalPremium } quoteId = { this.props.proposalForm.submitResponse.proposalResponseId } firstName = { this.props.proposalForm.submitResponse.firstName } email = { this.props.proposalForm.submitResponse.email } returnUrl = { this.props.proposalForm.submitResponse.returnUrl } />
                                }

                                {
                                    this.props.proposalForm.submitResponse && this.props.proposalForm.submitResponse.insurerId == 7 && <ReligarePg premium = { this.props.proposalForm.submitResponse.totalPremium } proposalResponseId = { this.props.proposalForm.submitResponse.proposalResponseId } firstName = { this.props.proposalForm.submitResponse.firstName } email = { this.props.proposalForm.submitResponse.email } returnUrl = { this.props.proposalForm.submitResponse.returnUrl } />
                                }
                                Retry Payment&nbsp;&nbsp;>
                            </div>
                        }
                    </div>

                    <div className="container-banner">
                        <img src={locQuery && locQuery.response && locQuery.response.toLowerCase() == 'success' ? success : failure} />
                    </div>
                </div>
            </div>
        );
    }
}