import React, { Component, Fragment } from 'react'
import styled from 'styled-components'

import Button from '../button'
import Modal from '../modal'
import Select from '../select'
import Rupee from '../rupee'

import remove from '../../../images/remove.png'

const _Compare = styled.div`
    width: 58.9rem;
    background: #FFF;
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    margin: 0 auto;
    border-radius: 6px 6px 0 0;
    box-shadow: 0 -6px 12px 0 rgba(101, 116, 124, 0.15);
    border: solid 1px #d9e6ec;
    bottom:-5px;
    opacity:0;
    transition:all 300ms ease;
    pointer-events:none;

    .container-quote-name {
        width: 13rem;
        height: 4rem;
        border-radius: 5px;
        box-shadow: 0 1px 2px 0 rgba(101, 116, 124, 0.2);
        background-color: #FFF;
        border: solid 1px #d9e6ec;
        display:inline-block;
        margin: 1.3rem;
        margin-right: 0;
        margin-bottom: 1.1rem;
        float:left;
        position:relative;
        transition:all 100ms ease;

        span {
            color:${props => props.theme.copyLighter}
            font-size:0.9rem;
            ${props => props.theme.verticalCenter()}
            left: 1.1rem;
            line-height: 1.2rem;
        }

        p {
            font-family:medium;
            color:${props => props.theme.purple}
            font-size: 0.95rem;
            line-height: 1.2rem;
            ${props => props.theme.verticalCenter()}
            left: 1.1rem;
            max-height: 2.3rem;
            overflow: hidden;
            width: 87%;
        }

        img {
            position: absolute;
            top: 7px;
            right: 7px;
            cursor: pointer;
            width: 0.95rem;
        }

        &.added {
            background-color: #f9fbfc;
        }
    }

    button {
        width:6rem;
        background: ${props => props.theme.blue}
        color:#FFF;
        font-family:regular;
        text-transform: capitalize;
        font-size: 1rem;
        float:left;
        margin: 1.3rem;
        margin-right: 0;
        margin-top: 2.2rem;

        &:last-child {
            margin-left: 1rem;
        }
    }

    &.active {
        bottom:0;
        opacity:1;
        pointer-events:all;
    }
`

const _Comparisons = styled(Modal)`
    .underlay {
        background-color: #4b5960;
    }

    .container-modal {
        width: calc(20rem + ${props => 20 * props.compare}rem + 3rem);
        transform: none;
        min-height: 15rem;
        top: calc(3rem + 5px);
        margin-bottom: 4rem;

        .close.white {
            top:1.5rem;
        }

        .wrapper {
            float:left;

            .container {
                width:20rem;
                float:left;
                position:relative;
                min-height:1px;
            }
        }

        .head {
            width:100%;
            float:left;

            .wrapper {
                .container {
                    img {
                        width: 10.5rem;
                    }
                    
                    h6 {
                        font-size:1.15rem;
                        line-height: 1.7rem;
                        color:${props => props.theme.tooltip}
                        width: 95%;
                    }
                }

                &.buy {
                    margin-top:1.3rem;
                    margin-bottom: 0.8rem;
                }
            }
        }

        .section {
            width:100%;
            float:left;
            font-size:1.7rem;
            line-height: 4.9rem;
            color:${props => props.theme.tooltip}
            margin-top: 1.5rem;

            &.top {
                border-top: solid 1px #f1f6f8;
            }
        }

        .features {
            .wrapper {
                height:4rem;
                border-top: solid 1px #f1f6f8;

                .container {
                    min-height:4rem;
                    position:relative;

                    b {
                        color:${props => props.theme.copyLighter}
                        font-weight: normal;
                        display: block;
                        line-height: 1.5rem;
                        width: 85%;
                        margin: 1.26rem 0;

                        &.premium {
                            color:${props => props.theme.purple}
                            font-family: medium;
                            font-size:1.4rem;

                            .icon-rupee {
                                height: 1.07rem;

                                svg {
                                    top: 55%;
                                }
                                
                                .icon {
                                    stroke:${props => props.theme.purple};
                                    stroke-width: 110;
                                }
                            }
                        }
                    }

                    label {
                        color:${props => props.theme.tooltip}
                        display: block;
                        line-height: 1.5rem;
                        width: 85%;
                        margin: 1.26rem 0;

                        &.na {
                            color:${props => props.theme.copyLighter}
                        }
                    }

                    .select {
                        float:left;
                        height: calc(4rem - 2px);
                        width: 10.5rem;
        
                        label {
                            font-size:1.4rem;
                            line-height: 1.5rem;
                            white-space:nowrap;
                            margin: 1.26rem 0;
                            color:${props => props.theme.purple}
                        }
                    }
                }
            }
        }
    }

    &.active {
        .underlay {
            opacity: 0.85;
            transition-delay: 0 !important;
        }

        .container-modal {
            top: 3rem;
        }
    }
`

export class Compare extends Component {
    state = { compare: false }

    onClose = () => {
        this.setState({ compare: false })
        this.props.toggleCompare()
    }

    getSumOptions = product => {
        let payloads = this.props.details[product.productId] && this.props.details[product.productId].payloads || product.payloads
        return payloads.map(policy => ({
            label: this.props.formatSum(policy.sumInsured).label,
            value: policy.id
        }))
    }

    handleSelect = product => {
        return quote => this.props.changeSum(product.productId, quote.value)
    }

    getQuote = product => {
        let quoteId = this.props.selectedSums[product.productId]
        let payloads = this.props.details[product.productId] && this.props.details[product.productId].payloads || product.payloads
         
        if (quoteId) return payloads.find(quote => quote.id == quoteId)
         else return product.payloads[0]
    }

    render() {
        
        return (
            <Fragment>
                <_Compare className={`comparisons ${this.props.active ? 'active' : ''}`}>
                    <div className={`container-quote-name ${this.props.compare[0] ? 'added' : ''}`}>
                        {!this.props.compare[0] && (
                            <span>
                                Add a plan to<br />compare
                            </span>
                        )}
                        {this.props.compare[0] && (
                            <p>{this.props.compare[0].insurer} {this.props.compare[0].productName}</p>
                        )}
                        <img src={remove} onClick={() => this.props.toggleCompare(this.props.compare[0])} />
                    </div>
                    <div className={`container-quote-name ${this.props.compare[1] ? 'added' : ''}`}>
                        {!this.props.compare[1] && (
                            <span>
                                Add a plan to<br />compare
                            </span>
                        )}
                        {this.props.compare[1] && (
                            <p>{this.props.compare[1].insurer} {this.props.compare[1].productName}</p>
                        )}
                        <img src={remove} onClick={() => this.props.toggleCompare(this.props.compare[1])} />
                    </div>
                    <div className={`container-quote-name ${this.props.compare[2] ? 'added' : ''}`}>
                        {!this.props.compare[2] && (
                            <span>
                                Add a plan to<br />compare
                            </span>
                        )}
                        {this.props.compare[2] && (
                            <p>{this.props.compare[2].insurer} {this.props.compare[2].productName}</p>
                        )}
                        <img src={remove} onClick={() => this.props.toggleCompare(this.props.compare[2])} />
                    </div>
                    <Button label="cancel" onClick={() => this.props.toggleCompare()} />
                    <Button label="compare" className={!this.props.compare[1] ? 'disable' : ''} onClick={() => this.setState({ compare: true })} />
                </_Compare>
                {this.props.compare[0] && (
                    <_Comparisons active={this.state.compare} onClose={this.onClose} closeWhite compare={this.props.compare.length} bubble>
                        <div className="head">
                            <div className="wrapper">
                                <div className="container" />
                                {this.props.compare.map((product, index) => (
                                    <div className="container" key={product.productName + index}>
                                        <img src={product.logo} />
                                        <h6>{product.productName}</h6>
                                    </div>
                                ))}
                            </div>
                            <div className="wrapper buy">
                                <div className="container" />
                                {this.props.compare.map((product, index) => (
                                    <div className="container" key={product.productName + index}>
                                        <Button label="Buy Policy" />
                                    </div>
                                ))}
                            </div>
                        </div>
                        
                        <div className="section top">Highlights</div>
                        <div className="wrapper features">
                            <div className="wrapper">
                                <div className="container">
                                    <b>Sum Insured</b>
                                </div>
                                {this.props.compare.map((product, index) => {
                                    let policy = this.getQuote(product)
                                    return (
                                        <div className="container" key={product.productName + product.sumInsured + index}>
                                            <Select sum value={this.props.formatSum(policy.sumInsured)} onlyOne={product.payloads.length == 1}
                                                options={this.getSumOptions(product)} handleSelect={this.handleSelect(product)}
                                            />
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="wrapper">
                                <div className="container">
                                    <b>Policy Premium (with taxes)</b>
                                </div>
                                {this.props.compare.map((product, index) => {
                                    let policy = this.getQuote(product)
                                    return (
                                        <div className="container" key={'premium'+product.productName + product.sumInsured + index}>
                                            <b className="premium"><Rupee /> {this.props.transformPremium(policy.premium)[0].value}</b>
                                        </div>
                                    )
                                })}
                            </div>
                            {this.props.compare[0].payloads[0].features.top.map((feature, index) => (
                                <div className="wrapper" key={feature.name + index}>
                                    <div className="container">
                                        <b>{feature.name}</b>
                                    </div>
                                    {this.props.compare.map((product, _index) => {
                                        let policy = this.getQuote(product)
                                        return (
                                            <div className="container" key={product.productId + feature.name + index + _index}>
                                                <label className={!policy.features.top[index].available ? 'na' : ''}>{policy.features.top[index].value}</label>
                                            </div>
                                        )
                                    })}
                                </div>
                            ))}
                        </div>

                        <div className="section">Key Features</div>
                        <div className="wrapper features">
                            {this.props.compare[0].payloads[0].features.primary.map((feature, index) => (
                                <div className="wrapper" key={feature.name + index}>
                                    <div className="container">
                                        <b>{feature.name}</b>
                                    </div>
                                    {this.props.compare.map((product, _index) => {
                                        let policy = this.getQuote(product)
                                        return (
                                            <div className="container" key={product.productId + feature.name + index + _index}>
                                                <label className={!policy.features.primary[index].available ? 'na' : ''}>{policy.features.primary[index].value}</label>
                                            </div>
                                        )
                                    })}
                                </div>
                            ))}
                        </div>

                        <div className="section">Other benefits</div>
                        <div className="wrapper features">
                            {this.props.compare[0].payloads[0].features.secondary.map((feature, index) => (
                                <div className="wrapper" key={feature.name + index}>
                                    <div className="container">
                                        <b>{feature.name}</b>
                                    </div>
                                    {this.props.compare.map((product, _index) => {
                                        let policy = this.getQuote(product)
                                        return (
                                            <div className="container" key={product.productId + feature.name + index + _index}>
                                                <label className={!policy.features.secondary[index].available ? 'na' : ''}>{policy.features.secondary[index].value}</label>
                                            </div>
                                        )
                                    })}
                                </div>
                            ))}
                        </div>
                    </_Comparisons>
                )}
            </Fragment>
        )
    }
}

export default Compare
