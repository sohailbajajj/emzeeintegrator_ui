import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import PerfectScrollbar from '@opuscapita/react-perfect-scrollbar'
import { NavLink } from 'react-router-dom'
import { isEqual } from 'lodash'

import Modal from '../modal'
import Select from '../select'
import Check from '../checkbox'
import Button from '../button'
import Rupee from '../rupee'
import Spinner from '../spinner'

import location from '../../../images/location.png'

const _Details = styled(Modal)`
    padding-bottom: 5rem;

    .underlay {
        background-color: #f1f6f8;
    }

    .container-modal {
        background: #fcfdfd;
        position: relative;
        top: 5px;
        transform: none;
        padding: 0;
        width: calc(100% - 20rem);
        max-width: 1200px;
        margin: 0 auto;
        height: 100%;
        border-radius: 0;

        .head {
            position: relative;
            background:#FFF;
            margin-right: 36%;
            box-sizing: border-box;
            border-right:solid 1px #eaf2f5;

            h3 {
                font-size: 1.4rem;
                line-height: 2rem;
                font-family: medium;
                width: calc(100% - 8rem);
                padding: 1.6rem 1.5rem;
                padding-bottom: 1.8rem;
            }

            img {
                ${props => props.theme.verticalCenter()} width:8rem;
                right: 0;
            }
        }

        .wrapper {
            display:block;
        }

        .container-details {
            background: #fff;
            float: left;
            width: 64%;
            padding: 0 1.5rem;
            box-sizing: border-box;
            border-right:solid 1px #eaf2f5;
            min-height: 100%;

            .container-sum {
                display:none;
                border-top: solid 1px #f2f7f9;
                height:56px;
                background: #FFF;
                width:calc(100% + 3rem);
                margin-left:-1.5rem;
    
                >label {
                    float:left;
                    margin-left:3rem;
                    line-height:56px;
                    font-size: 18px;
                    color:${props => props.theme.blue}
                    
    
                    .icon-rupee {
                        height: 14px;
                        margin-right: 8px;
    
                        svg {
                            top: 55%;
                        }
                        
                        .icon {
                            stroke:${props => props.theme.blue};
                        }
                    }
                }
    
                .select {
                    height:56px;
                    width: 12rem;
                    margin-left:1.5rem;
                    float:left;
    
                    label {
                        line-height:56px !important;
                        font-size: 18px !important;
                        color:#232c37;
                    }
                }
            }

            .menu {
                background: #fcfdfd;
                width: 100%;
                padding: 0 1.5rem;
                margin-left: -1.5rem;
                height: calc(4rem - 2px);
                box-shadow: 0 1px 3px 0 rgba(101, 116, 124, 0.1);
                border-top: solid 1px #f1f6f8;

                ul {
                    float:left;
                    list-style:none;

                    li {
                        float:left;
                        line-height: 4rem;
                        margin-right:2rem;
                        color:${props => props.theme.copyLight}
                        position:relative;
                        cursor:pointer;

                        &.active {
                            color:${props => props.theme.blue} !important;
                            
                            &::after {
                                background-color:${props => props.theme.blue} !important;
                            }
                        }

                        &::after {
                            content:'';
                            width:100%;
                            height:2px;
                            background-color:transparent;
                            position:absolute;
                            left:0;
                            bottom:1rem;
                        }

                        &:hover {
                            color:${props => props.theme.blue}
                        }
                    }
                }

                a {
                    float:right;
                    line-height:4rem;
                    color:${props => props.theme.purple}
                    position:relative;
                    width:10rem;
                    
                    &::before {
                        content:'';
                        position:absolute;
                        top:0;
                        left: -1.5rem;
                        height:1.5rem;
                        width:1px;
                        background:#f1f6f8;
                        ${props => props.theme.verticalCenter()}
                    }

                    &::after {
                        content:'↓';
                        color:#FFF;
                        position:absolute;
                        ${props => props.theme.verticalCenter()}
                        background-color:${props => props.theme.purple}
                        width:1.4rem;
                        height:1.4rem;
                        line-height: 1.4rem;
                        font-size:0.9rem;
                        font-weight:900;
                        text-indent:0.3rem;
                        border-radius:50%;
                        right:0;
                    }
                }
            }

            .container-copy {
                width:100%;
                float: left;
                position:relative;

                >div {
                    width:100%;
                    float: left;
                    padding-bottom: 3rem;
                    opacity:0;
                    transition:all 300ms ease-out;
                    top:3px;
                    left:0;
                    position:absolute;
                    pointer-events:none;
                    display:none;

                    &.hospitals {
                        padding-top:1rem;
                    }

                    h3 {
                        color:${props => props.theme.tooltip}
                        font-size: 1.7rem;
                        margin-top: 2.5rem;
                        margin-bottom: 1.5rem;
                        float:left;
                    }
    
                    .container {
                        float: left;
                    }
    
                    .feature {
                        width:100%;
                        float:left;
                        margin-bottom:1rem;
    
                        p {
                            width:50%;
                            float:left;
                            line-height: 1.4rem;
                            box-sizing:border-box;
    
                            &.name {
                                color:${props => props.theme.copyLighter}
                                text-indent:1.5rem;
                                position:relative;
    
                                &::before {
                                    content:'';
                                    width:8px;
                                    height:8px;
                                    border-radius:50%;
                                    background:#ff9595;
                                    ${props => props.theme.verticalCenter()}
                                    left:0;
                                }
    
                                &.available {
                                    &::before {
                                        background:#0ace8e;
                                    }
                                }
                            }
    
                            &.value {
                                color:${props => props.theme.tooltip}
                                padding-right:1rem;
    
                                &.not-available {
                                    color:${props => props.theme.copyLighter}
                                }
                            }
                        }
                    }
    
                    p {
                        float:left;
                        color:${props => props.theme.copyLight}
                        width:90%;
                        line-height:1.4rem;
    
                        &.wp-value {
                            margin-bottom:1.5rem;
                        }
                    }
    
                    h6 {
                        float:left;
                        width:100%;
                        font-family:medium;
                        font-size:1rem;
                        margin-top:3rem;
                        margin-bottom:0.7rem;
                    }

                    .hospital {
                        border-bottom:solid 1px #f1f6f8;
                        width:100%;
                        float:left;
                        min-height:5rem;
                        position:relative;
    
                        span {
                            font-family:medium;
                            width: 20%;
                            max-width: 20%;
                            float:left;
                            font-size:0.95rem;
                            line-height:1.4rem;
                            color:${props => props.theme.tooltip}
                            margin: 1rem 0;
                        }
    
                        p {
                            width: 60%;
                            max-width: 60%;
                            float:right;
                            font-size:0.95rem;
                            line-height:1.4rem;
                            color:${props => props.theme.copyLight}
                            margin: 1rem 0;
                            margin-right: 13%;
                        }
    
                        a {
                            display:block;
                            width:1.2rem;
                            ${props => props.theme.verticalCenter()}
                            right:0;
    
                            img {
                                display:block;
                                width:100%;
                            }
                        }
                    }

                    &.hospitals {
                        h3 {
                            font-size: 1.3rem;
                            color:${props => props.theme.copyLighter}
                        }
                    }

                    &.active {
                        display:block;
                    }

                    &.activate {
                        position:relative;
                        top:0;
                        opacity:1;
                        transition:all 300ms ease-in;
                        pointer-events:all;
                    }
                }
            }
        }

        .container-options {
            float: right;
            width: 36%;
            min-height: 100%;
            padding: 0 1.5rem;
            box-sizing: border-box;
            position:relative;

            .container {
                width:100%;
                background:#FFF;
                margin-bottom:1rem;
                border-radius: 6px;
                box-shadow: 0 1px 2px 0 rgba(101, 116, 124, 0.1);
                border: solid 1px #eaecef;
                min-height:4rem;
                box-sizing: border-box;

                >label {
                    line-height:calc(4rem - 2px);
                    font-size: 1.2rem;
                    padding-left:1.2rem;
                    color:${props => props.theme.copyLight}
                }

                >h6 {
                    color:${props => props.theme.copyLighter}
                    text-align:center;
                    border-bottom:solid 1px #f1f6f8;
                    font-family:medium;
                    font-size: 0.8rem;
                    line-height: 3rem;
                    letter-spacing: 1px;
                }

                >b {
                    line-height: calc(4rem - 2px);
                    color:${props => props.theme.blue};
                    font-size:1.2rem;
                    float:right;
                    margin-right:1.2rem;

                    .icon-rupee {
                        height: 1rem;
                        
                        .icon {
                            stroke:${props => props.theme.blue};
                        }
                    }
                }

                .select {
                    float:right;
                    height: calc(4rem - 2px);
                    margin-right:1.2rem;
                    width: 9rem;
    
                    label {
                        line-height: calc(4rem - 2px) !important;
                        font-size: 1.2rem !important;
                        color:${props => props.theme.tooltip}
                    }
                }

                .premium {
                    border-bottom: solid 1px #f1f6f8;
                    position:relative;
                    width:100%;
                    height:5rem;
                    cursor:pointer;

                    .duration {
                        font-family:medium;
                        line-height:4rem;
                        color:${props => props.theme.copyDark}
                        position:absolute;
                        left:4rem;
                        pointer-events:none;

                        span {
                            position:absolute;
                            left:0;
                            top:1.4rem;
                            color:#97a0aa !important;
                            font-size:0.85rem;
                            font-family:regular;
                        }
                    }

                    .value {
                        font-size:1.1rem;
                        line-height:4rem;
                        font-family:medium;
                        color:${props => props.theme.tooltip}
                        position:absolute;
                        right: 1.3rem;
                        pointer-events:none;

                        .icon-rupee {
                            height: 0.85rem;
                            
                            .icon {
                                stroke:${props => props.theme.tooltip};
                            }
                        }
                    }

                    .check {
                        left: 1.2rem;
                        pointer-events:none;
                    }

                    &.selected {
                        .duration {
                            color:${props => props.theme.blue}
                        }
                    }

                    &:last-child {
                        border:none;
                    }
                }

                .addon {
                    border-bottom: solid 1px #f1f6f8;
                    position:relative;
                    cursor:pointer;

                    p {
                        font-size: 13px;
                        line-height: 19px;
                        color:${props => props.theme.tooltip}
                        width:calc(100% - 5.3rem);
                        padding: 1.2rem 0;
                        padding-left:4rem;
                        cursor:pointer;
                        user-select:none;

                        .icon-rupee {
                            height: 11px;
                            margin-left: 3px;
                            margin-right: 8px;

                            svg {
                                top: 62%;

                                .icon {
                                    stroke: ${props => props.theme.tooltip}
                                }
                            }
                        }

                        label {
                            white-space:nowrap;
                        }
                    }

                    .check {
                        left: 1.2rem;
                        pointer-events:none;
                    }

                    &:last-child {
                        border:none;
                    }

                    &.isCompulsory {
                        cursor: default;
                        .checked {
                            background: ${props => props.theme.green}
                            cursor: default;
                        }

                        p {
                            cursor: default;
                        }
                    }
                }
            }

            button {
                width:100%;
                height:4rem;
                line-height:calc(4rem - 2px);
                font-size: 1.4rem;
                box-shadow: 0 4px 6px 0 rgba(0, 255, 200, 0.2);

                .continue {
                    line-height:calc(4rem - 2px);
                }
            }

            .note {
                color:${props => props.theme.copyLighter}
                font-size:0.85rem;
                line-height:1.4rem;
                text-align:center;
                margin-top:1rem;
                padding: 0 1rem;
            }

            .icon-spinner {
                width: 3rem;
                position:absolute;
                left:0;
                right:0;
                margin:0 auto;
                top: calc(39vh - 3rem);
                opacity:0;
                pointer-events:none;

                &.active {
                    opacity:1;
                }
            }

            .wrapper-options {
                margin-top:10px;
                opacity:0;
                transition: all 300ms ease;

                &.active {
                    margin-top:0;
                    opacity:1;
                }
            }
        }

        button.continue {
            position:fixed;
            bottom:-10px;
            width:100%;
            left:0;
            margin:0;
            border-radius:0;
            max-width: 100% !important;
            height: 4rem;
            font-family: medium;
            font-size: 18px;
            line-height: 3.9rem;
            opacity:0;
            transition: all 300ms ease;
            pointer-events:none;
            display:none;

            &.active {
                bottom:0;
                opacity:1;
                pointer-events:all;
            }

        }

        .ps {
            .ps__rail-y {
                opacity:0;
            }

            &.ps--scrolling-y {
                .ps__rail-y {
                    opacity:0.6;
                }
            }
        }
    }

    &.active {
        .underlay {
            opacity: 0.97;
            transition-delay: 0 !important;
        }

        .container-modal {
            top: 0;
        }
    }

    button.backMobile {
        background: ${props => props.theme.blue};
        color:#FFF;
        font-family:regular;
        font-size:13px;
        height:2rem;
        line-height: 1.9rem;
        width:5.5rem;
		position:fixed !important;
        top: 1.15rem;
        right:10px;
        z-index:21;
        display:none;
	}

    @media (min-width:951px) and (max-width:1200px) {
        .container-modal {
            width: calc(100% - 60px);
        }
    }

    @media (max-width:950px) {
        .container-modal {
            width:100%;
            border-top: solid 3.5rem ${props => props.theme.purple};

            .close, .close.white {
                width: 1.1rem !important;
                top: -3rem;
                right: 0.85rem;
            }

            .head {
                margin-right:0;
                box-shadow: 0 1px 2px 0 rgba(101, 116, 124, 0.1);
                z-index: 1;

                img {
                    right: 1.5rem;
                }
            }

            .container-details {
                width:100%;
                padding-bottom:3rem;

                .container-sum {
                    display:block;
                }

                .menu {
                    ul li, a {
                        font-size:13px;
                    }
                }

                .container-copy {
                    >div {

                        p {
                            font-size:13px;
                            line-height: 17px;
                        }

                        h6 {
                            font-size:15px;
                        }

                        .hospital {
                            span, p {
                                font-size:13px;
                                line-height: 17px;
                                max-width: 100%;
                            }

                            p {
                                width: 55%;
                            }

                            span {
                                width: 25%;
                            }
                        }
                    }
                }
            }

            .container-options {
                width:100%;
                background:#fcfdfd;
                padding-top: 1.3rem;

                .note {
                    margin-bottom: 3rem;
                    font-size: 12px;
                    line-height: 17px;
                    padding: 0;
                }

                .container {
                    >h6 {
                        font-size: 12px;
                        line-height: 38px;
                    }

                    .premium {
                        .duration, >span {
                            font-size:14px !important;

                            span {
                                font-size:11px;
                            }
                        }
                    }
                }
            }

            .container-details, .container-options {
                min-height:0 !important;
                max-height:0;
                overflow:hidden;
                margin-top:10px;
                opacity:0;
                transition:opacity 300ms ease, margin-top 300ms ease, max-height 1s ease;
                position: absolute;
                top: 0;

                &.active {
                    max-height:2000px;
                    overflow: visible;
                    margin-top:0;
                    opacity:1;
                    transition:opacity 300ms ease, margin-top 300ms ease, max-height 0s ease;
                }
            }

            button.continue {
                display:block;
            }
        }

        button.backMobile {
            display:block;
            right:20px;
        }
    }

    @media (max-width:500px) {
        .container-modal {
            background:#FFF;
            
            .head {
                h3 {
                    width: calc(100% - 11rem);
                }
            }

            .container-details {
                
                .container-sum {
                    border-bottom: solid 1px #f2f7f9;
                    height: 48px !important;
    
                    >label {
                        float:right;
                        margin-left:0;
                        margin-right:1.5rem;
                    }
    
                    .select {
                        height: 48px !important;
                        width: 10.5rem;
    
                        label {
                            font-size:16px !important;
                            line-height: 48px !important;
                        }
                    }
    
                    label {
                        font-size: 16px;
    
                        .icon-rupee {
                            height: 12px;
                        }
                    }
                }

                .container-copy {
                    >div {
                        .feature {
                            p.name {
                                width:100%;
                            }

                            p.value {
                                width:100%;
                                padding-left: 1.5rem;
                                box-sizing:border-box;
                                margin-top: 3px;
                            }
                        }

                        .hospital {
                            span, p {
                                width:100% !important;
                                margin-right:0 !important;
                                width: calc(100% - 3rem) !important;
                            }

                            span {
                                margin-bottom:0 !important;
                            }

                            p {
                                margin-top:3px !important;
                                float: left !important;
                            
                            }
                        }
                    }
                }
            }

            .menu {
                margin-top:42px;
                position:relative;

                a {
                    position:absolute !important;
                    left:1.5rem;
                    top: -47px;
                    width: 10.5rem !important;
                }
            }
        }
    }

    @media (max-width: 400px) {
        button.backMobile {
            right:10px;
        }
    }
`

export default class Details extends Component {
    state = { active: 'features', mobile: 'details' }

    componentDidMount() {
        this.toggleCopy()
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.active && this.props.active != prevProps.active) {
            this.setState({ active: 'features'})
            this.toggleCopy()
        }
        if (this.state.mobile != prevState.mobile) {
            let wrapper = document.querySelector('.wrapper-inner')
            if (wrapper) {
                wrapper.scrollIntoView(true)
                wrapper.scrollTop = 0
            }
        }
        if (this.state.active != prevState.active) this.toggleCopy(this.state.active)
        
        //Add compulsary addons
        if (this.props.active && (this.props.quote.addOnsCount > 0) && this.props.details[this.props.active.productId]) {
            if (!this.props.cart.addons[0]) {
                let product = this.props.details[this.props.active.productId]
                let quote = product.payloads.find(_quote => _quote.sumInsured == this.props.quote.sumInsured)
                let compulsory = quote.addOnsList.filter(addon => addon.isCompulsory)
                compulsory.forEach(addon => this.props.toggleAddon(addon))
            }
        }
    }

    toggleCopy = (item = 'features') => {
        if (!this.copy) return
        let all = this.copy.children
        let container = this.copy.querySelector(`.${item}`)

        Array.from(all).forEach(container => {
            if (container.classList.contains(item)) return
            container.classList.remove('activate')
            setTimeout(() => container.classList.remove('active'), 300)
        })

        container.classList.add('active')
        setTimeout(() => container.classList.add('activate'), 100)
    }

    getSavings = year => {
        let premium = this.props.quote.premium
        if (!premium[year].discount || (premium[year].discount && (!premium[year].discount.discountPercentage || parseFloat(premium[year].discount.discountPercentage) == 0))) {
            return <span>No savings</span>
        } else return <span>You save Rs. {Math.round(parseFloat(premium[year].premium) - parseFloat(premium[year].discount.premium))}</span>
    }

    getHeadHeight = () => (this.head ? window.getComputedStyle(this.head, null).height : 0)

    getSumOptions = () => {
        let product = this.props.details[this.props.product.productId] || this.props.product
        return product.payloads.map(policy => ({
            label: this.props.formatSum(policy.sumInsured).label,
            value: policy.id
        }))
    }

    getAddonPrice = addon => {
        if (addon.isFlat) {
            return addon.premium
        } else {
            let premium = parseInt(this.props.cart.duration.value.replace(/,/g, ''))
            return Math.round((premium*addon.premium)/100)
        }
    }

    getNetPremium = () => {
        if (this.props.cart.addons[0]) {
            let addons = this.props.cart.addons.reduce((net, addon) => {
                net = net + parseInt(this.getAddonPrice(addon))
                return net
            }, 0)
            return (parseInt(this.props.cart.duration.value.replace(/,/g, '')) + addons).toLocaleString('en-IN')
        }
        else return this.props.cart.duration.value
    }

    handleSelect = quote => {
        let product = this.props.details[this.props.product.productId] || this.props.product
        let selected = product.payloads.find(policy => policy.id == quote.value)
        this.props.cartQuote({ product, quote: selected })
        this.props.changeSum(product.productId, quote.value)
        this.props.selectDuration(this.props.transformPremium(selected.premium)[0])
        this.props.selectSum(selected.sumInsured)
        selected.addOnsList[0] && selected.addOnsList.forEach(addon => {
            addon.isCompulsory && this.props.toggleAddon(addon)
        })
    }

    onBack = () => {
        if (this.state.mobile == 'options') {
            this.setState({ mobile: 'details' })
        } else {
            this.props.cartQuote()
            this.props.history.push('/health/quotes')
        }
    }

    renderInternals = () => {
        let quote = this.props.quote
        let premium = quote && this.props.transformPremium(quote.premium)
        let details = this.props.details && this.props.product && this.props.details[this.props.product.productId]
        details = details && details.payloads.find(_quote => _quote.sumInsured == quote.sumInsured)

        return (
            <Fragment>
                <div className={`wrapper ${this.state.mobile == 'details' && 'details'}`}>
                    <div className="wrapper-inner">
                        <div className="head" ref={head => (this.head = head)}>
                            <h3>{this.props.product.productName}</h3>
                            <img src={this.props.product.logo} />
                        </div>
                        <div className="wrapper-positioning">
                            <div className={`container-details ${this.state.mobile == 'details' && 'active'}`} 
                                style={{ minHeight: `calc(100% - ${this.getHeadHeight()})` }}>
                                <div className="container-sum">
                                    <Select sum options={this.getSumOptions()} value={this.props.formatSum(quote.sumInsured)} 
                                        handleSelect={this.handleSelect} onlyOne={this.getSumOptions().length < 2} 
                                        isMobile={this.props.isMobile}
                                    />
                                    <label><Rupee /> {premium[0].value} / year</label>
                                </div>
                                <div className="menu">
                                    <ul>
                                        <li className={this.state.active == 'features' ? 'active' : ''} onClick={() => this.setState({ active: 'features' })}>
                                            Features
                                        </li>
                                        <li className={this.state.active == 'hospitals' ? 'active' : ''} onClick={() => this.setState({ active: 'hospitals' })}>
                                            Cashless Hospitals
                                        </li>
                                        <li className={this.state.active == 'faqs' ? 'active' : ''} onClick={() => this.setState({ active: 'faqs' })}>
                                            FAQs
                                        </li>
                                    </ul>
                                    <a href={this.props.product.termsAndConditions} target="_blank">Policy Wordings</a>
                                </div>
                                <div className="container-copy" ref={copy => (this.copy = copy)}>
                                    <div className={`features`}>
                                        {quote.features.primary[0] && (
                                            <Fragment>
                                                <h3>Key features</h3>
                                                <div className="container">
                                                    {quote.features.primary.map((feature, index) => (
                                                        <div className="feature" key={feature.name + index}>
                                                            <p className={`name ${feature.available && 'available'}`}>{feature.name}</p>
                                                            <p className={`value ${!feature.available && 'not-available'}`}>{feature.value}</p>
                                                        </div>
                                                    ))}
                                                </div>
                                            </Fragment>
                                        )}
                                        {quote.features.secondary[0] && (
                                            <Fragment>
                                                <h3>Other benefits</h3>
                                                <div className="container">
                                                    {quote.features.secondary.map((feature, index) => (
                                                        <div className="feature" key={feature.name + index}>
                                                            <p className={`name ${feature.available && 'available'}`}>{feature.name}</p>
                                                            <p className={`value ${!feature.available && 'not-available'}`}>{feature.value}</p>
                                                        </div>
                                                    ))}
                                                </div>
                                            </Fragment>
                                        )}
                                        {(quote.features.exclusions[0] || quote.features.waitingPeriod[0]) && (
                                            <Fragment>
                                                <h3>What's not included</h3>
                                                <p>Health plans cover all conditions. But, some specific conditions are covered after a certain period or never covered.</p>
                                            </Fragment>
                                        )}
                                        {quote.features.exclusions[0] && (
                                            <Fragment>
                                                <h6>Exclusions</h6>
                                                <p>{quote.features.exclusions[0].value}</p>
                                            </Fragment>
                                        )}
                                        {quote.features.waitingPeriod[0] && (
                                            <Fragment>
                                                <h6>Waiting period</h6>
                                                {quote.features.waitingPeriod.map((wp, index) => (
                                                    <Fragment key={index + name}>
                                                        <p>{wp.name}</p>
                                                        <p className="wp-value">{wp.value}</p>
                                                    </Fragment>
                                                ))}
                                            </Fragment>
                                        )}
                                    </div>
                                    <div className={`hospitals`}>
                                        {!this.props.loadingDetails &&
                                            this.props.details[this.props.product.productId].cashlessHospitals.map((hospital, index) => (
                                                <div className="hospital" key={hospital.name + index}>
                                                    <span className="name">{hospital.name}</span>
                                                    <p className="address">
                                                        {hospital.address1} {hospital.address2}
                                                    </p>
                                                    <a>
                                                        <img src={location} />
                                                    </a>
                                                </div>
                                            ))}
                                        {!this.props.loadingDetails && !this.props.details[this.props.product.productId].cashlessHospitals[0] && <h3>No cashless hospitals found for this product in your city.</h3>}
                                    </div>
                                    <div className={`faqs`}>
                                        <h1>FAQs</h1>
                                    </div>
                                </div>
                            </div>
                            <div className={`container-options ${this.state.mobile == 'options' && 'active'}`}
                                style={{ minHeight: `calc(100% - ${this.getHeadHeight()})` }}>
                                <Spinner className={this.props.loadingDetails ? 'active' : ''} />
                                <div className={`wrapper-options ${!this.props.loadingDetails ? 'active' : ''}`}>
                                    <div className="container">
                                        <label>Sum Insured</label>
                                        <Select sum options={this.getSumOptions()} value={this.props.formatSum(quote.sumInsured)} 
                                            handleSelect={this.handleSelect} onlyOne={this.getSumOptions().length < 2} 
                                            isMobile={this.props.isMobile}
                                        />
                                    </div>

                                    <div className="container">
                                        <h6>PREMIUM DURATION</h6>
                                        <div className={`premium ${isEqual(this.props.duration, premium[0]) ? 'selected' : ''}`} onClick={() => this.props.selectDuration(premium[0])}>
                                            <Check circular checked={isEqual(this.props.duration, premium[0])} />
                                            <p className="duration">
                                                1 Year's Premium
                                                {this.getSavings('firstYear')}
                                            </p>
                                            <span className="value">
                                                <Rupee /> {premium[0].value}
                                            </span>
                                        </div>
                                        {premium[1] && (
                                            <div className={`premium ${isEqual(this.props.duration, premium[1]) ? 'selected' : ''}`} onClick={() => this.props.selectDuration(premium[1])}>
                                                <Check circular checked={isEqual(this.props.duration, premium[1])} />
                                                <p className="duration">
                                                    2 Years' Premium
                                                    {this.getSavings('secondYear')}
                                                </p>
                                                <span className="value">
                                                    <Rupee /> {premium[1].value}
                                                </span>
                                            </div>
                                        )}
                                        {premium[2] && (
                                            <div className={`premium  ${isEqual(this.props.duration, premium[2]) ? 'selected' : ''}`} onClick={() => this.props.selectDuration(premium[2])}>
                                                <Check circular checked={isEqual(this.props.duration, premium[2])} />
                                                <p className="duration">
                                                    3 Years' Premium
                                                    {this.getSavings('thirdYear')}
                                                </p>
                                                <span className="value">
                                                    <Rupee /> {premium[2].value}
                                                </span>
                                            </div>
                                        )}
                                    </div>

                                    {
                                        details && details.addOnsList[0] &&
                                        <div className="container">
                                            <h6>ADDONS</h6>
                                            {
                                                details.addOnsList.map((addon, index) =>
                                                    <div key={addon.addonsId+index}
                                                        className={`addon ${addon.isCompulsory && 'isCompulsory'}`} 
                                                        onClick={() => this.props.toggleAddon(addon)}
                                                    >
                                                        <Check checked={this.props.cart.addons.includes(addon)} />
                                                        <p>
                                                            {addon.comAddOns.addonsDesc} for <label><Rupee /> {this.getAddonPrice(addon)}</label>
                                                        </p>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    }

                                    <div className="container">
                                        <label>Premium</label>
                                        <b>
                                            <Rupee /> {this.getNetPremium()}
                                        </b>
                                    </div>
                                    <NavLink to="/health/buy">
                                        <Button continue label={window.innerWidth > 950 ? 'Buy Policy' : 'Buy Now'} />
                                    </NavLink>

                                    <p className="note">You will be taken to proposal form in next step. No payment needed at the moment.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Button className={`backMobile`} label='< Back' onClick={() => this.onBack()} />
                <Button continue label="Buy Policy" className={`continue ${this.state.mobile == 'details' && 'active'}`} 
                    onClick={() => this.setState({ mobile: 'options'})}
                />
            </Fragment>
        )
    }

    render() {
        return (
            <Fragment>
                {
                    !this.props.renderMobile &&
                    <_Details className={'quote-details'} closeWhite bubble {...this.props}>
                        {
                            this.props.quote &&
                            <PerfectScrollbar>{this.renderInternals()}</PerfectScrollbar>
                        }
                    </_Details>
                }

                {
                    this.props.renderMobile && 
                    this.props.quote && this.renderInternals()
                }
            </Fragment>
        )
    }
}
