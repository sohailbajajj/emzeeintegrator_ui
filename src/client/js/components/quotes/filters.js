import React, { Component, Fragment } from 'react'
import styled from 'styled-components'

import { enableScroll, disableScroll, stopBubbling } from '../../utils/helpers'
import Select from '../select'
import Checkbox from '../checkbox'
import Button from '../button'
import Modal from '../modal'

import filter from '../../../images/filter.png'
import filterActive from '../../../images/filter-active.png'

const _Filters = styled.div`
    ${props => props.theme.fullWidth()}
    width:100%;
    height:4rem;
    background:#FFF;
    box-shadow: 0 6px 12px 0 rgba(150, 169, 179, 0.1);
    opacity:0;
    margin-top:-10px;
    margin-bottom: 10px;
    transition:all 0ms ease;

    &.init {
        transition:all 300ms ease;
        opacity:1;
        margin-top:0px;
        margin-bottom:0;
    }

    .select {
        margin-top: 1rem;
        margin-right: 0.8rem;

        &:nth-last-child(2) {
            float:right;
            margin-right:0;
            box-shadow: 0 1px 2px 0 rgba(82, 103, 255, 0.2);
        }

        &.suminsured, &.members {
            width: auto;
            
            ul {
                width:100%;
                
                li {
                    text-align:left;
                    text-indent: 1.3rem;
                }
            }
        }

        &.members {
            min-width: 11.5rem;

            label {
                text-transform: capitalize;
            }
        }

        &.more {
            ul {
                >div {
                    &:last-child {
                        position:relative; 
                        &::after {
                            content:'';
                            position:absolute;
                            width:calc(100% - 30px);
                            height:1px;
                            background:#eaf2f5;
                            top:0;
                            left:15px;
                        }
                    }
                }
            }
        }
    }

    .overlay {
        width: 100%;
        height: calc(100% - 8.5rem);
        background-color: #4b5960;
        position: fixed;
        left: 0;
        top: 8.5rem;
        z-index: 1;
        pointer-events:none;
        opacity:0;
        transition:all 300ms ease;

        &.active {
            pointer-events:all;
            opacity: 0.85;
        }
    }

    button.filter {
        font-family:regular;
        height:2rem;
        margin-top: 1rem;
        float:right;
        background:#FFF;
        border:solid 1px #eaf2f5;
        color:${props => props.theme.copyLight}
        text-align: left;
        text-indent: 0.5rem;
        width: 7.7rem;
        font-size: 14px;
        line-height: 14px;
			
        img {
            ${props => props.theme.verticalCenter()}
            width:0.9rem;
            right:1rem;

            &.active {
                opacity:0;
            }
        }

        &.active {
            background:${props => props.theme.blue}
            color:#FFF;
            width: 9.4rem;
            font-size:13px;

            img {
                opacity:0;
                
                &.active {
                    opacity:1;
                }
            }
        }
	}

    @media (max-width: 1021px) {
        .select {
            &:nth-last-child(2) {
                float:left;
            }

            &.members {
                margin-right:0;

                &.open {
                    ul {
                        top: 26px !important;
                    }
                }
            }
        }
    }
`

const _FiltersMobile = styled(Modal)`
    .underlay {
        background-color: #f5f8fa;
    }
    
    .container-modal {
        top:0;
        transform:none;
        border-radius:0;
        box-shadow: 0 2px 4px 0 rgba(101, 116, 124, 0.15);
        padding-bottom: 8rem;

        img.close {
            display:none;
        }

        h4 {
            background: ${props => props.theme.purple}
            color:#FFF;
            text-transform:uppercase;
            text-align:center;
            font-family:regular;
            font-size: 14px;
            letter-spacing: 0.5px;
            line-height:44px;
            width:calc(100% + 3rem);
            margin-left:-1.5rem;
            margin-top:-1.3rem;
        }

        h6 {
            font-family: medium;
            font-size: 16px;
            color: #1b272f;
        }

        .filter-toggle {
            padding: 1.3rem 0;
            width:100%;
            position:relative;
            border-top: solid 1px #eaf2f5;

            &.restore {
                border-bottom: solid 1px #eaf2f5;
            }

            p {
                font-size:12px;
                line-height:17px;
                width: calc(100% - 6rem);
                margin-top: 6px;
            }

            .toggle {
                font-size: 16px;
                right: 5px;
                top: 17px;
            }
        }

        .filter-sum {
            padding-bottom: 17px;
            padding-top: 23px;
            box-sizing: border-box;

            .select {
                border-radius: 5px;
                box-shadow: 0 1px 2px 0 rgba(82, 103, 255, 0.2);
                border: solid 1px rgba(82, 103, 255, 0.5);
                width:100%;
                padding: 0 1.5rem;

                .pointer {
                    right: 1.5rem;
                }

                ul {
                    li {
                        text-align: left;
                        text-indent: 1.5rem;
                    }
                }
            }
        }

        .filter-insurers {
            padding-top:15px;

            h6 {
                padding-bottom:17px;
            }

            .insurers {
                list-style-type:none;
                
                li {
                    text-align:left !important;
                    text-indent:3rem !important;
                    position:relative;
                    font-size: 13px;
                    color: #65747c;
                    height: 2.5rem;
                    line-height: 2.5rem;
            
                    .check {
                        width: 16px;
                        height: 16px;
                        line-height: 16px;
                        font-size: 13px;
                        left: 0rem;
                        text-indent:0;
                        pointer-events:none;
            
                        .unchecked {
                            width: 14px;
                            height: 14px;
                        }
                    }
            
                    &:last-child {
                        border-bottom:none;
                    }
                }
            }
        }

        .container-actions {
            position:fixed;
            bottom:0;
            left:0;
            width:100%;
            background:#FFF;
            box-shadow: 0 -2px 8px 0 rgba(150, 169, 179, 0.1);
            height:64px;

            button {
                height: 36px;
                margin-left: 20px;
                width: calc(50% - 30px);
                margin-top: 14px;
                font-size: 14px;

                &:first-child {
                    box-shadow: 0 1px 2px 0 rgba(82, 103, 255, 0.2);
                    font-family:regular;
                    line-height: 32px;
                    border: solid 1px rgba(82, 103, 255, 0.5);
                    color:${props => props.theme.blue}
                }

                &:last-child {
                    box-shadow: 0 1px 2px 0 rgba(0, 255, 200, 0.2);
                    line-height: 35px;
                }
            }
        }
    }
`

export default class Filters extends Component {
    static defaultProps = {
        filtersInit: {
            sum: false,
            insurers: false,
            nocopay: false,
            roomrent: false,
            maternity: false,
            restore: false,
            more: false,
            members: false,
            mobile: false
        },
        sumRanges: [
            {
                label: 'Upto 3 lacs',
                value: {
                    low: 0,
                    high: 300000
                }
            },
            {
                label: '3 to 6 lacs',
                value: {
                    low: 300000,
                    high: 600000
                }
            },
            {
                label: '6 to 10 lacs',
                value: {
                    low: 600000,
                    high: 1000000
                }
            },
            {
                label: 'above 10 lacs',
                value: {
                    low: 1000000,
                    high: 90000000
                }
            },
        ]
    }

    state = {
        init: false,
        filters: this.props.filtersInit
    }

    componentDidMount() {
        this.setState({ init: true })
        window.innerWidth > 1021 && window.addEventListener('click', this.closeAll)
        window.innerWidth < 1021 && window.addEventListener('click', () => this.closeFilter('members'))
    }

    componentWillUnmount() {
        this.setState({ init: false })
        window.innerWidth > 1021 && window.removeEventListener('click', this.closeAll)
        window.innerWidth < 1021 && window.removeEventListener('click', () => this.closeFilter('members'))
    }

    toggleFilter = (event, type) => {
        stopBubbling(event)
        this.props.editFilter({
            type,
            selected: !this.props.filters[type].selected
        })
    }

    toggleOpen = filter => {
        this.setState({
            filters: {
                ...this.props.filtersInit,
                [filter]: !this.state.filters[filter]
            }
        })
    }

    closeFilter = filter => {
        this.setState({
            filters: {
                ...this.props.filtersInit,
                [filter]: false
            }
        })
    }

    closeAll = () => this.setState({ filters: this.props.filtersInit })

    toggleInsurer = (event, insurer) => {
         stopBubbling(event)
        this.props.toggleInsurer(insurer)
    }

    changeSumRange = range => {
        this.props.editFilter({
            type: 'sum',
            selected: range
        })
        window.innerWidth > 1021 && this.closeAll()
    }

    isFilterOpen = () => Object.keys(this.state.filters).filter(filter => this.state.filters[filter]).length > 0

    getMemberCombos = (onlyGroup = false) => {
        let members = this.props.filters.members
        let group = undefined

        if (!onlyGroup && window.innerWidth < 1021) {
            group = members.groups.reduce((group, current) => {
                let combos = current.combos.map(combo => ({ ...combo, groupId: current.id }))
                group = { combos: [ ...group.combos, ...combos ] }
                return group
            }, { combos: [] })
        } else {
            group = members.groups.reduce((group, current) => {
                if (current.id == members.selected.group) {
                    group = { ...group, ...current }
                }
                return group
            }, {})
        }
        
        if (onlyGroup) return group
        
        return group.combos.map(combo => {
            return {
                label: combo.name,
                value: !combo.groupId ? combo.id : {
                    combo: combo.id,
                    group: combo.groupId
                }
            }
        })
    }

    getComboMembers = (getId = false) => {
        let selected = this.props.filters.members.selected
        let group = this.getMemberCombos(true)
        let combo = group.combos.reduce((combo, current) => {
            if (current.id == selected.combo[group.id]) {
                combo = { ...combo, ...current }
            }
            return combo
        }, {})
        if (getId) return combo.id 
        return combo.name
    }

    toggleCombo = combo => {

        if (!combo.value.group) {
            this.props.toggleCombo({
                id: this.props.filters.members.selected.group,
                combo: combo.value
            })
        } else {
            this.props.toggleGroup(combo.value.group)
            this.props.toggleCombo({
                id: combo.value.group,
                combo: combo.value.combo
            })
        }
        this.setState({ filters: this.props.filtersInit })
    }
    
    getActiveCount = () => {
        let filters = { ...this.props.filters }
        delete filters.sum; delete filters.members
        let selected = Object.keys(filters).map(filter => {
            if (typeof(this.props.filters[filter].selected) == 'object') {
                return this.props.filters[filter].selected.length > 0
            } else return this.props.filters[filter].selected
        }).filter(filter => filter).length

        if (selected > 0) return ` (${selected})`
        
        return ``
    }

    render() {
        return (
            <_Filters className={this.state.init ? 'init' : ''}>
                <div className="wrapper">
                    {window.innerWidth > 1021 && (
                        <Fragment>
                            <Select sum isFilter options={this.props.sumRanges} 
                                value={this.props.filters.sum.selected && { 
                                    label: `Sum Insured ${this.props.filters.sum.selected.label}`, 
                                    selected: this.props.filters.sum.selected.value
                                }}
                                default={!this.props.filters.sum.selected && this.props.sumRanges[1]} 
                                handleSelect={this.changeSumRange} className={'suminsured applied'}
                                open={this.state.filters.sum} onClick={() => this.toggleOpen('sum')}
                            />
                            
                            <Select sum isFilter placeholder={'Insurers'} className={this.props.filters.insurers.selected.length && 'applied'} 
                                open={this.state.filters.insurers} onClick={() => this.toggleOpen('insurers')}>
                                <InsurersList insurers={this.props.filters.insurers} toggle={this.toggleInsurer} />
                            </Select>
        
                            <Select sum isFilter placeholder={'No Copay'} className={this.props.filters.nocopay.selected && 'applied'}
                                open={this.state.filters.nocopay} onClick={() => this.toggleOpen('nocopay')}>
                                <ToggleFilter head="No Copay" handleClick={e => this.toggleFilter(e,'nocopay')}
                                    selected={this.props.filters.nocopay.selected} onClick={e => stopBubbling(e)}
                                />
                            </Select>
        
                            <Select sum isFilter placeholder={'No Room rent limit'} className={this.props.filters.roomrent.selected && 'applied'}
                                open={this.state.filters.roomrent} onClick={() => this.toggleOpen('roomrent')}>
                                <ToggleFilter head="No Room rent limit" handleClick={e => this.toggleFilter(e,'roomrent')}
                                    selected={this.props.filters.roomrent.selected} onClick={e => stopBubbling(e)}
                                />
                            </Select>
        
                            <Select sum isFilter placeholder={'More filters'} open={this.state.filters.more} onClick={() => this.toggleOpen('more')}
                                className={`more ${(this.props.filters.maternity.selected || this.props.filters.restore.selected) && 'applied'}`}
                            >
                                <ToggleFilter head="Maternity benefits" handleClick={e => this.toggleFilter(e,'maternity')}
                                    selected={this.props.filters.maternity.selected} onClick={e => stopBubbling(e)}
                                />
                                <ToggleFilter head="Restore benefits" handleClick={e => this.toggleFilter(e,'restore')}
                                    selected={this.props.filters.restore.selected} onClick={e => stopBubbling(e)}
                                />
                            </Select>
                        </Fragment>
                    )}

                    {window.innerWidth < 1021 && (
                        <Fragment>
                            <Button className={`filter ${this.getActiveCount() && 'active'}`} label={`Filters${this.getActiveCount()}`}
                                onClick={e => {stopBubbling(e);this.toggleOpen('mobile')}}
                            >
                                <img src={filter} />
                                <img src={filterActive} className="active"/>
                            </Button>
                            
                            <_FiltersMobile bubble active={this.state.filters.mobile} head="Filter Plans"
                                onClose={() => this.closeAll()}
                            >
                                <div className="filter-sum">
                                    <h6>Sum Insured</h6>
                                    <Select sum options={this.props.sumRanges} 
                                        value={this.props.filters.sum.selected && { 
                                            label: `Sum Insured ${this.props.filters.sum.selected.label}`, 
                                            selected: this.props.filters.sum.selected.value
                                        }}
                                        default={!this.props.filters.sum.selected && this.props.sumRanges[1]} 
                                        handleSelect={this.changeSumRange} isMobile={window.innerWidth < 650}
                                    />
                                </div>
                                <ToggleFilter head="No Copay" handleClick={e => this.toggleFilter(e,'nocopay')}
                                    selected={this.props.filters.nocopay.selected}
                                />
                                <ToggleFilter head="No Room rent limit" handleClick={e => this.toggleFilter(e,'roomrent')}
                                    selected={this.props.filters.roomrent.selected}
                                />
                                <ToggleFilter head="Maternity benefits" handleClick={e => this.toggleFilter(e,'maternity')}
                                    selected={this.props.filters.maternity.selected}
                                />
                                <ToggleFilter head="Restore benefits" handleClick={e => this.toggleFilter(e,'restore')}
                                    selected={this.props.filters.restore.selected} className="restore"
                                />
                                 <div className="filter-insurers">
                                    <h6>Insurers</h6>
                                    <InsurersList insurers={this.props.filters.insurers} toggle={this.toggleInsurer} />
                                </div>
                                <div className="container-actions">
                                    <Button outline blue label="Clear Filters" onClick={() => this.props.resetFilters()}/>
                                    <Button label="Done" onClick={() => this.closeAll()} />
                                </div>
                            </_FiltersMobile>
                        </Fragment>
                    )}

                    <Select sum isFilter blue options={this.getMemberCombos()}
                        open={this.state.filters.members} onClick={() => this.toggleOpen('members')}
                        className={'members'} handleSelect={this.toggleCombo}
                        value={{ label: `Covering ${this.getComboMembers()}`, selected: this.getComboMembers(true)}}
                        isMobile={window.innerWidth < 650}
                    />
                    {this.isFilterOpen() && disableScroll()}
                    {!this.isFilterOpen() && enableScroll()}
                    {window.innerWidth > 1021 && <div className={`overlay ${this.isFilterOpen() && 'active'}`} />}
                </div>
            </_Filters>
        )
    }
}

const ToggleFilter = props => (
    <_ToggleFilter {...props} className={`filter-toggle ${props.className}`}>
        <h6>{props.head}</h6>
        <p>
            Room Rent Limit is applied by Insurance Cos 
            on the the choice of Hospital Room you can opt for. 
            The lesser the cappings the better.
        </p>
        <Toggle onClick={props.handleClick}  selected={props.selected}/>
    </_ToggleFilter>
)

const _ToggleFilter = styled.div`
    width: 20rem;
    padding: 1.3rem 1.2rem;
    box-sizing: border-box;

    h6 {
        width: calc(100% - 3rem);
        font-family:medium;
        font-size: 1.07rem;
        color: ${props => props.theme.tooltip};
        text-align:left;
        text-indent: 0;
    }

    p {
        margin-top: 0.3rem;
        width: calc(100% - 3rem);
        color: ${props => props.theme.copyLight};
        font-size: 0.85rem;
        line-height: 1.25rem;
        text-align:left;
        text-indent: 0;
    }

    .toggle {
        position: absolute;
        right: 1rem;
        top: 1rem;    
    }
`

const Toggle = props => (
    <_Toggle {...props} className={`toggle ${props.selected && 'selected'}`}>
        <div className={`switch ${props.selected && 'selected'}`}>
            {props.selected && <span>&#10003;</span>}
            {!props.selected && <span>&times;</span>}
        </div>
    </_Toggle>
)

const _Toggle = styled.div`
    width:2.8em;
    height:2em;
    border-radius: 1em;
    border: solid 1px ${props => props.theme.copyLighter};
    background:${props => props.theme.copyLighter};
    position:relative;
    cursor:pointer;
    transition: all 200ms ease;

    &.selected {
        border: solid 1px ${props => props.theme.blue};
        background:${props => props.theme.blue};
    }

    .switch {
        width:2em;
        height:2em;
        cursor:pointer;
        border-radius:50%;
        position:absolute;
        top: -1px;
        left: -2px;
        background:#FFF;
        color:${props => props.theme.copyLighter};
        border: solid 1px ${props => props.theme.copyLighter};
        text-indent: 0;
        user-select:none;
        transition: all 200ms ease;

        span {
            font-size: 1.3em;
            line-height: 1.6em;
            text-align:center;
            display:block;
            width: 95%;
        }

        &.selected {
            left:calc(100% - 2em + 2px);
            color:${props => props.theme.blue};
            border: solid 1px ${props => props.theme.blue};
        }
    
    }
`

const _InsurersList  = styled.div`
    width:20rem;

    li {
        text-align:left !important;
        text-indent:1.3rem !important;
        position:relative;

        .check {
            width: 18px;
            height: 18px;
            line-height: 18px;
            font-size: 14px;
            right: 1rem;
            text-indent:0;
            pointer-events:none;

            .unchecked {
                width: 16px;
                height: 16px;
            }
        }

        &:last-child {
            border-bottom:none;
        }
    }
`

const InsurersList = props => (
    <_InsurersList className="insurers">
        {props.insurers.list && props.insurers.list.map(insurer => 
            <li key={insurer.insurerId} onClick={e => props.toggle(e, insurer.insurerId)}>
                {insurer.insurerName}
                <Checkbox checked={props.insurers.selected.includes(insurer.insurerId)} />
            </li>
        )}
    </_InsurersList>
)