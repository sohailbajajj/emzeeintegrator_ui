import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import { CSSTransition, TransitionGroup } from 'react-transition-group'

import Select from '../select'
import Button from '../button'
import Rupee from '../rupee'

import location from '../../../images/location.png'
import document from '../../../images/document.png'

const _Quote = styled.div`
    width: calc((100% / 3) - 1rem);
    height: auto;
    background: #fff;
    float: left;
    margin-right: 1.5rem;
    margin-top: 1.5rem;
    box-shadow: ${props => props.theme.shadow};
    border-radius: 7px;
    border: solid 1px ${props => props.theme.border};
    box-sizing: border-box;
    padding: 1.4rem;

    .icon-rupee {
        .icon {
            stroke:green;
        }
    }

    &.right {
        margin-right: 0;
    }

    &:nth-child(3n+1), &:nth-child(3n+2) {
        margin-right: 1.5rem !important;
    }

    &:nth-child(3n) {
        margin-right: 0 !important;
    }

    .head {
        position: relative;
        height: 3rem;
        margin-bottom: 1.5rem;

        h6 {
            ${props => props.theme.verticalCenter()} width: 12.5rem;
            font-size: 1.15rem;
            line-height: 1.6rem;
            color: ${props => props.theme.tooltip};
        }

        img {
            ${props => props.theme.verticalCenter()} width: 9rem;
            right: 1.5rem;
        }
    }

    .sum-switch {
        height: 3.2rem;
        border-top: solid 1px ${props => props.theme.border};
        border-bottom: solid 1px ${props => props.theme.border};
        position: relative;

        .select {
            ${props => props.theme.verticalCenter()} width: 9.5rem;
        }

        > label {
            width: calc(100% - 9.5rem);
            box-sizing: border-box;
            padding-right: 1.3rem;
            float: right;
            line-height: 3.2rem;
            color: ${props => props.theme.blue};
            font-size: 1.2rem;
            white-space: nowrap;
            text-align: left;
            text-indent: 3rem;

            .icon-rupee {
                .icon {
                    stroke:${props => props.theme.blue};
                }
            }
        }
    }

    .features {
        margin-top: 1rem;
        border-bottom: solid 1px ${props => props.theme.border};
        padding-bottom: 1rem;

        > div {
            height: 2rem;
            line-height: 2rem;
            font-size: 0.9rem;

            label {
                display: block;
                float: left;
                white-space: nowrap;

                &:first-child {
                    width: 9.5rem;
                    color: ${props => props.theme.copyLight};
                }
                &:last-child {
                    width: calc(100% - 9.5rem);
                    text-indent: 3rem;
                    color: ${props => props.theme.tooltip};
                }
            }
        }
    }

    .tags {
        float: left;
        width: 100%;
        margin-top: 1rem;
        border-bottom: solid 1px ${props => props.theme.border};
        padding-bottom: 1rem;

        > span {
            color: #858135;
            background: #fffddc;
            margin-right: 0.5rem;
            padding: 0.5rem 0.7rem;
            font-size: 0.9rem;
            display: block;
            float: left;
            border-radius: 4px;
            text-transform: capitalize;
        }
    }

    .additional {
        width: 100%;
        height: 4rem;
        line-height: 4rem;
        border-bottom: solid 1px ${props => props.theme.border};
        float: left;

        > div {
            width: 50%;
            float: left;
            position: relative;
            box-sizing: border-box;
            color: ${props => props.theme.copyLight};
            font-size: 0.9rem;
            text-indent: 1.4rem;
            white-space: nowrap;

            img {
                ${props => props.theme.verticalCenter()} top: 48%;
                width: 1.1rem;
                left: 0;
            }

            &:last-child {
                padding-left: 1rem;

                img {
                    width: 1rem;
                    left: 1rem;
                }
            }
        }
    }

    button {
        margin-top: 1.3rem;

        &.getQuotes, &.compare {
            border-color: ${props => props.theme.blue};
            color: ${props => props.theme.blue};
            font-family: regular;
            box-shadow: 0 1px 2px 0 rgba(82, 103, 255, 0.2);

            &.disable {
                color: #b7ccd8;
                border-color: #f1f6f8;
                pointer-events:none;
            }
        }

        &.getQuotes {
            width: 100%;
        }

        &.compare, &.details {
            width: calc((100% / 2) - 0.75rem);
            line-height: 2.35rem;
        }

        &.details {
            float: right;
            box-shadow: 0 1px 2px 0 rgba(0, 255, 200, 0.2);
            line-height: 2.45rem;
        }

        &.mobile {
            width:100%;
            background:${props => props.theme.green}
            border-color:${props => props.theme.green}
        }
    }

    @media (min-width: 651px) and (max-width: 1020px) {
        width: calc(50% - 0.8rem);

        &:nth-child(2n) {
            margin-right: 0 !important;
        }

        &:nth-child(2n-1) {
            margin-right: 1.5rem !important;
        }
    }

    @media (max-width: 650px) {
        margin-right: 0 !important;
        width:100%;
        margin-top: 25px;

        &:first-child {
            margin-top: 5px;
        }

        &:nth-child(3n+1), &:nth-child(3n+2) {
            margin-right: 0 !important;
        }

        button {
            height: 3rem;
            line-height:2.8rem;
            font-size:14px;
        }

        .features {
            float:left;
            width:100%;
            margin-top: 13px;
            padding-bottom: 13px;
            
            div {
                float:left;
                width:100%;
                height:auto;
                line-height: 30px;

                label {
                    font-size: 13px;
                    float:left;

                    &:first-child {
                        width:40%;
                    }

                    &:last-child {
                        width:60%;
                    }
                }
            }
        }

        .sum-switch {
            label {
                font-size:16px !important;
                white-space:nowrap;
            }
            .select {
                width:40%;
            }

            >label {
                width:60%;
            }
        }

        .head {
            h6 {
                font-size:15px;
                line-height:20px;
                width:55%;
            }

            img {
                width:10rem;
            }
        }

        .tags {
            margin:0;
            padding:0.5rem 0;

            span {
                font-size: 12px;
                margin: 0.5rem 0;
                margin-right:0.5rem;
            }
        }

        .additional {
            div {
                font-size: 12px;
            }
        }
    }

    @media (max-width: 400px) {
        width:calc(100% - 20px);
        margin-left:10px;

        .head {
            img {
                right:0;
            }

            &.auto {
                min-height:3rem;
                height:auto;

                h6 {
                    position:relative;
                    transform:none;
                    top:0;
                }
            }
        }

        .sum-switch {

            >label {
                width:60%;
                text-align:right;
                padding-right: 5px;
            }
        }

        .features {
            margin-top: 17px;
            padding-bottom: 3px;
            
            div {
                label {
                    white-space: normal;
                    line-height: 18px;
                    margin-bottom: 15px;

                    &:last-child {
                        text-indent: 0 !important;
                        padding-left: 20px;
                        box-sizing: border-box;
                    }
                }
            }
        }

        .additional {
            > div {
                :last-child {
                    padding-right:5px;
                    box-sizing:border-box;
                    text-align:right;

                    img {
                        left:auto;
                        right:69px;
                    }
                }
            }
        }
    }
`

const _Quotes = styled.div`
    margin-top: 2.5rem;
    margin-bottom: 10rem;
    width: 100%;
    float: left;

    @media (max-width: 400px) {
        width: calc(100% + 20px);
        margin-left: -10px;
    }
`

class Quote extends Component {

    getSumOptions = () => {
        return this.props.product.payloads.map(policy => ({
            label: this.props.formatSum(policy.sumInsured).label,
            value: policy.id
        }))
    }

    handleSelect = quote => {
        if (!this.props.changeSum) return
        this.props.changeSum(this.props.product.productId, quote.value)
    }

    render() {
        let quote = this.props.product.payloads.find(quotes => quotes.id == this.props.quote) || this.props.product.payloads[0]
        let premium = this.props.transformPremium(quote.premium)
        let sum = this.props.formatSum(quote.sumInsured)
        let tags = Object.keys(quote.searchFilter).filter(filter => quote.searchFilter[filter])

        return (
            <_Quote {...this.props} className={`quote ${this.props.className}`}>
                <div className={`head ${this.props.isMobile && this.props.product.productName.length > 15 && 'auto'}`}>
                    <h6>{this.props.product.productName}</h6>
                    <img src={this.props.product.logo} />
                </div>
                <div className="sum-switch">
                    <Select sum options={this.getSumOptions()} value={sum} 
                        handleSelect={this.handleSelect} onlyOne={this.props.product.payloads.length == 1 || !this.props.changeSum}
                        isMobile={this.props.isMobile}
                    />
                    <label><Rupee /> {premium[0].value} / year</label>
                </div>
                <div className="features">
                    {quote.features.top.map(feature => (
                        <div key={feature.name}>
                            <label>{feature.name}</label>
                            <label>{feature.value}</label>
                        </div>
                    ))}
                </div>
                {
                    !this.props.isMobile && 
                    <div className="tags">
                        { tags.slice(0, 2).map(tag => <span key={tag}>{tag.replace(/([a-z])([A-Z])/g, '$1 $2')}</span>) }
                        { tags.length > 2 && !this.props.isMobile && <span>{tags.length - 2} more</span>}
                        { tags.length > 2 && this.props.isMobile && <span>+ {tags.length - 2}</span>}
                        {!tags[0] && <span>No special features</span>}
                    </div>
                }
                <div className="additional">
                    <div>
                        <img src={location} />
                        {this.props.product.cashlessHospitalCount} Cashless hospitals
                    </div>
                    <div>
                        <img src={document} />
                        {quote.addOnsCount} Add-ons {!this.props.isMobile ? 'available' : ''}
                    </div>
                </div>
                {this.props.isGetQuotes && 
                    <Button purple outline label="View Details" className={!this.props.isGetQuotes ? 'getQuotes' : 'mobile'} 
                        onClick={() => this.props.onSelect({ product: this.props.product, quote, duration: premium[0], sum: quote.sumInsured })} 
                    />
                }
                {!this.props.isGetQuotes && (
                    <Fragment>
                        <Button className={`compare ${this.props.disableCompare ? 'disable' : ''}`} purple outline label="Add to compare" 
                            onClick={() => this.props.toggleCompare(this.props.product)}
                        />
                        <Button className="details" label="View Details" 
                            onClick={() => this.props.onSelect({ product: this.props.product, quote, duration: premium[0], sum: quote.sumInsured })} 
                        />
                    </Fragment>
                )}
            </_Quote>
        )
    }
}

export default class Quotes extends Component {
    getStyle = index => {
        if (index % 3 == 0) return 'left'
        if (index % 3 == 1) return 'center'
        if (index % 3 == 2) return 'right'
    }

    render() {
        return (
            <_Quotes className="quotes-list">
                <TransitionGroup>
                    {this.props.products.map((product, index) => (
                        <CSSTransition key={product.productId} classNames="slide-quote" timeout={{ enter: 500, exit: 300 }}>
                            <Quote className={this.getStyle(index)} product={product}
                                onSelect={this.props.onSelect} 
                                isGetQuotes={this.props.isGetQuotes}
                                formatSum={this.props.formatSum}
                                transformPremium={this.props.transformPremium}
                                toggleCompare={this.props.toggleCompare}
                                disableCompare={this.props.disableCompare}
                                changeSum={this.props.changeSum}
                                quote={(this.props.selectedSums && this.props.selectedSums[product.productId]) || product.payloads[0].id}
                                isMobile={this.props.isMobile}
                            />
                        </CSSTransition>
                    ))}
                </TransitionGroup>
            </_Quotes>
        )
    }
}
