import React, { Component } from 'react'
import styled from 'styled-components'

import Modal from '../modal'
import Check from '../checkbox'
import Button from '../button'

import { notEmail } from '../../utils/validations'

const _Share = styled(Modal)`
    .container-modal {
        width: 30rem;

        h4 {
            font-size: 1.2rem;
        }

        h6 {
            color: ${props => props.theme.copyLight}
            font-size:13px;
            line-height: 17px;
            margin-top: 2rem;
        }

        .container-email {
            width:100%;
            height:2rem;
            position:relative;
            margin-top:1.5rem;

            input, label {
                line-height:2rem;
                padding-left: 3rem;
                font-family:medium;
                font-size:1.1rem;
                color: ${props => props.theme.tooltip}
            }

            input {
                 box-sizing:border-box;
                 border:none;
                 border-bottom:solid 1px ${props => props.theme.border}
                 float:right;
                 width:calc(100% - 5rem);
                 padding:0;
                 margin-right:2rem;
                 outline:none;

                 &::placeholder {
                    color: ${props => props.theme.copyLighter}
                    font-family:regular;
                 }
            }

            span.error {
                color:${props => props.theme.red}
                font-size:0.9rem;
                width:100%;
                position:absolute;
                left:3rem;
                top:2.4rem;
                opacity:0;
            }

            .check {
                .unchecked {
                }
            }

            &.error {
                span.error {
                    opacity:1;
                }
            }
        }

        button {
            margin-top:3rem;

            &.disable {
                pointer-events: none;
            }
        }

        @media (max-width: 650px) {
            width:calc(100% - 20px);

            button {
                height: 3rem;
                line-height: 3rem;
                font-size: 16px;

                label {
                    line-height: 3rem;
                }
            }
        }
    }
`

export default class Share extends Component {
    state= {
        email: undefined,
        error: false
    }

    handleInput = e => {
        let value = e.currentTarget.value
        this.setState({ error: value && notEmail(value)})
        if (!notEmail(value)) this.props.toggleEmail(value)
        else this.props.toggleEmail(undefined)
    }

    isDisabled = () => {
        if (!this.props.share.user.selected && !this.props.share.additional.selected) return true
        if (this.props.share.additional.selected && !this.props.share.additional.email) return true
    }

    render() {

        return (
            <_Share head="Send quotes by email" active={this.props.share.active} onClose={this.props.onClose}>
                <h6>Based on your requirements, send the top quotes to your email. Or share them with them someone else to review.</h6>
                
                <div className="container-email">
                    <Check checked={this.props.share.user.selected} toggleCheck={() => this.props.toggleUser('user')}/>
                    <label>{this.props.userEmail}</label>
                </div>

                <div className={`container-email ${this.state.error && 'error'}`}>
                    <Check checked={this.props.share.additional.selected} 
                        toggleCheck={() => this.props.toggleUser('additional')} 
                    />
                    <input placeholder="Add another email" onInput={e => this.handleInput(e)}/>
                    <span className="error">Please enter a valid email address</span>
                </div>
                <Button continue label="Send" className={this.isDisabled() && 'disable'} onClick={() => this.props.shareQuotes()}/>
            </_Share>
        )
    }
}
