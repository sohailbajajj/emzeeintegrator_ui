import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { getInput } from './formFields'

const SimpleForm = props => {
  const { handleSubmit, pristine, reset, submitting } = props;
  return (
    props.fields.map((field, index) => (
      <div key={index}>
            {
              //console.log(field)
            }
            <Field
              name={field.name}
              type={getInput(field.typeOfControl)['type']}
              component={getInput(field.typeOfControl)['input']}
              label={field.value}
              options={field.options}
              feet={field.feet || undefined} inch={field.inch || undefined}
              selection={field.selection || null}
              key={field + index}
            />
      </div>
    ))
  );
};

export default reduxForm({
  form: 'simple', // a unique identifier for this form
})(SimpleForm);
