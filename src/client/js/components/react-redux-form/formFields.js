import React from 'react'
import { Field } from 'redux-form';

export const renderField = ({ input, label, type, meta: { touched, error } }) => (
    <div>
      <label>{label}</label>
      <div>
        <input {...input} placeholder={label} type={type} />
        {touched && error && <span>{error}</span>}
      </div>
    </div>
  )
 export const renderSelector = ({ input, label, options, meta: { touched, error } }) => (
    <div>
        <div><label>{label}</label></div>
        <select {...input}>
        <option value=""></option>
        {
            options && (options.length > 0) && options[0].name && options[0].value &&
            options.map(option => <option key={option.value + option.name} value={option.value}>{option.name}</option>)
        }
        {
            options && (options.length > 0) && !options[0].name && !options[0].value &&
            options.map(option => <option key={option} value={option}>{option}</option>)
        }
        </select>
      {touched && error && <span>{error}</span>}
    </div>
  )

  export const renderFieldHeightSelectNew = ({ input, label, feet, inch,  meta: { touched, error } }) => (
    <div>
        <div><label>{label}</label></div>
        <div>
			<Field name={`${input.name}.feet`} component={renderHeightSelect} options={feet} />
			<Field name={`${input.name}.inch`} component={renderHeightSelect} options={inch} />
		</div>
    </div>
  )

  export const renderHeightSelect = ({ input, options, meta: { touched, error } }) => (
    <div>
        <select {...input}>
        <option value=""></option>
        {
            options.map((op, val) => (
                <option value={op} key={op + val}>{op}</option>
            ))
        }
        </select>
      {touched && error && <span>{error}</span>}
    </div>
  )

  export const renderFieldReadOnly = ({ input, label, selection, type, meta: { touched, error } }) => (
    <div>
        <label>{label}</label>
        <div className="value" >{selection}</div>
        {touched && error && <span>{error}</span>}
    </div>
  )

export const getInput = type => {
	switch (type) {
		case 'TextBox':
            return {
                type: 'text',
                input: renderField
            }
        case 'DropDownBox':
            return {
                type: 'select',
                input: renderSelector
            }
        case 'RadioButton':
            return {
               type: 'radio',
               input: 'input'
            } 
        case 'DatePicker':
            return {
               type: 'date',
               input: 'input'
            } 
        case 'ReadOnly':
            return {
                type: 'text',
                input: renderFieldReadOnly
            }
        case 'Readonly':
            return {
                type: 'text',
                input: renderFieldReadOnly
            }
        case 'AutoTextBox':
            return {
                type: 'text',
                input: 'input'
            }   
        case 'DropDownBoxHeight':
            return {
                type: 'select',
                input: renderFieldHeightSelectNew
            }
        case 'DateMM/YYYY':
            return {
                type: 'text',
                input: 'input'
            }
        case 'MultiLineTextBox':
            return {
                type: 'text',
                input: 'input'
            }
        case 'CascadeDropDownBox':
            return {
                type: 'select',
                input: 'select'
            }
        case 'Hidden':
            return {
                type: 'text',
                input: 'input'
            }
		default:
			return {
                type: 'text',
                input: 'input'
            }
	}
}