import React, { Component } from 'react'
import uuid from 'node-uuid'
import { Link } from 'react-router-dom'

class Results extends Component {

    constructor(props) {
        super(props)
        this.sortGroups = this.sortGroups.bind(this) 
		this.membersText = this.membersText.bind(this)
		this.renderGroup = this.renderGroup.bind(this)
        this.renderGroups = this.renderGroups.bind(this)
        //this.resetProposalForm = this.resetProposalForm.bind(this)
        this.state = {}
    }

    componentDidMount() {

    }

    componentWillReceiveProps(next) {

    }

    sortGroups(family) {
        //console.log(family)
        let familyRevised = {};
        family && family.children && Object.keys(family.children).forEach(function(key) {
            var ageValue = family.children[key].age && family.children[key].age.value;
            var selValue = family.children[key] && family.children[key].selected;
            var typeValue = family.children[key] && family.children[key].name;
            familyRevised[family.children[key].counttype] = { age: ageValue, selected: selValue, type: typeValue };
        })
        family && Object.keys(family).forEach(function(key) {
            if(key && key != 'children') {
                var ageValue = family[key] && family[key].age && family[key].age.value;
                var selValue = family[key] && family[key].selected;
                var typeValue = family[key] && family[key].name;
                familyRevised[key] = { age: ageValue, selected: selValue, type: typeValue };
            }
        })
        family = familyRevised;
        let members = Object.keys(family).filter(member => family[member].selected)
        let parents = []
		let children = []
        members.filter(member => member == 'father' || member == 'mother').forEach(member => parents.push({...family[member], member}))
        members.filter(member => member != 'father' && member != 'mother').forEach(member => children.push({...family[member], member}))

        let groups = { parents, children }

		for (let group in groups) {
			groups[group].length > 0 ? null : delete groups[group]
		}
		return groups
    }

    membersText(group, members) {
		let gender = this.props.gender
		let text = ''
		members = members.map(member => member.member)
		let totalMembers = members.length

		if (group == 'parents') {
			if (members.includes('father') && members.includes('mother')) {
				text = 'your parents'
			}
			if (members.includes('father') && !members.includes('mother')) {
				text = 'your father'
			}
			if (!members.includes('father') && members.includes('mother')) {
				text = 'your mother'
			}
		}
		if (group == 'children') {
			const you = (gender == 'female' && members.includes('wife')) || (gender == 'male' && members.includes('husband'))
			const spouse = (gender == 'female' && members.includes('husband')) || (gender == 'male' && members.includes('wife'))
			const children = members.includes('child1') || members.includes('child2') || members.includes('child3') || members.includes('child4')

			if (you) {
				if (!spouse && !children) {
					text = 'you'
				}
				else if (spouse && !children) {
					if (gender == 'male') {
						text = 'you & your wife'
					}
					else if (gender == 'female') {
						text = 'you & your husband'
					}
				}
				else if (!spouse && children) {
					if (totalMembers > 2) {
						text = 'you & your children'
					}
					else if (totalMembers <= 2) {
						text = 'you & your child'
					}
				}
				else if (spouse && children) {
					if (totalMembers > 3) {
						if (gender == 'male') {
							text = 'you, your wife & your children'
						}
						else if (gender == 'female') {
							text = 'you, your husband & your children'
						}
					}
					else if (totalMembers <= 3) {
						if (gender == 'male') {
							text = 'you, your wife & your child'
						}
						else if (gender == 'female') {
							text = 'you, your husband & your child'
						}
					}
				}
			}
			else if (!you) {
				if (spouse && !children) {
					if (gender == 'male') {
						text = 'your wife'
					}
					else if (gender == 'female') {
						text = 'your husband'
					}
				}
				else if (!spouse && children) {
					if (totalMembers > 1) {
						text = 'your children'
					}
					else if (totalMembers <= 1) {
						text = 'your child'
					}
				}
				else if (spouse && children) {
					if (totalMembers > 2) {
						if (gender == 'male') {
							text = 'your wife & your children'
						}
						else if (gender == 'female') {
							text = 'your husband & your children'
						}
					}
					else if (totalMembers <= 2) {
						if (gender == 'male') {
							text = 'your wife & your child'
						}
						else if (gender == 'female') {
							text = 'your husband & your child'
						}
					}
				}
			}
		}

		return text
	}

    handleGroupClick = (group, members, stop) => {
		if (!stop) {
            this.props.resetProposerForm()
            this.props.resetNomineeForm()
            this.props.resetMembersForm()
            this.props.resetAddressForm()
            this.props.resetMedicalHistoryForm()
			//this.resetProposalForm(group)
			//this.props.changePolicyGroup({ name:group, members })
		}
	}

    renderGroup(group, members) {
        return (
            <div key={ uuid.v1() } className={ `
			container-group group-${group} 
			${this.props.policies.selectedGroup.name == group ? 'selected' : ''}
			${!(this.props.policies.selectedGroup.name == group) && this.props.location.pathname.includes('buy') ? 'disable' : ''}
			` }>
                <div className="wrapper-members" style={{ width: `${this.props.PostPayment ? members.length > 1 && `${((members.length - 1)*3.8)+3.5}em` : 'auto'}`}}>
					{
						members.map(member => (
						<div key={ uuid.v1() }
							className={ `
								icon-member member-${member.member}
								${member.member == 'father' || member.member == 'husband' ? 'member-male' : ''}
								${member.member == 'mother' || member.member == 'wife' ? 'member-female' : ''}
								${member.member.includes('child') && (member.type == 'son' ? 'member-male member-son' : 'member-female member-daughter')}
							` }
						/>
						))
                    }
				</div>
				<div className="group-text">
					<span className="members">
						<div>
							{this.props.isMobile ? 'For' : (this.props.PostPayment && !this.props.ContinueBuying) ? 'This plan covers' : 'plans covering'} 
							<span> {this.membersText(group, members)}</span>.
						</div>
					</span>
				</div>
				{    
					<div className="button-redirect">
						<Link to="/health/quotes" onClick={ () => this.handleGroupClick(group, members, false) }>
							Buy Now&nbsp;&nbsp;>
						</Link>
					</div>
				}
            </div>
        )
    }

    renderGroups() {
        let groups = this.sortGroups(this.props.family)
        return (
			<div className="container-groups">
				{
					Object.keys(groups).map(group => this.renderGroup(group, groups[group]))
				}
			</div>
		)
	}

    render() {
        return (
            <div className={ `nav-results`}>
                { this.renderGroups() }
            </div>
        )
    }

}

export default Results