import React from 'react'
import styled from 'styled-components'

const _Rupee = styled.span`
    height:1rem;
    display: inline-block;
    margin-right: 0.7rem;
    position:relative;

    svg {
        ${props => props.theme.verticalCenter()}
        top:57%;
        left:0;
        width: auto;
        height:100%;
    }
`

const Rupee = () => {
    return (
        <_Rupee className="icon-rupee">
            <svg width="0" height="0" viewBox="0 0 572 865" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs />
                <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g className="icon" transform="translate(0.000000, 40.000000)" stroke="#000000" strokeWidth="100">
                        <g id="Group">
                            <path d="M412,795.4 L46,454.9 L46,426.3 L154.7,426.3 C271.8,426.3 367.5,330.5 367.5,213.5 C367.5,96.4 271.7,0.7 154.7,0.7 L0.7,0.7" id="Shape" />
                            <path d="M0.7,0.6 L571.3,0.6" id="Shape" />
                            <path d="M0.7,213.5 L571.3,213.5" id="Shape" />
                        </g>
                    </g>
                </g>
            </svg>
        </_Rupee>
    )
}

export default Rupee
