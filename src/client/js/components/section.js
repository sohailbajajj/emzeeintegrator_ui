import React from 'react'
import styled from 'styled-components'

const _Section = props => (
	<section {...props}>
		{props.nowrapper}
		<div className="wrapper">
			{props.children}
		</div>
	</section>
)

const Section = styled(_Section)`
	width:100%;
	height:100%;
	position:relative;
	background:#FFF;
	${props => props.theme.fullWidth()}
`

export const RequirementsSection = Section.extend`
	padding-top:3.5rem !important;
	height:calc(100% - 3.5rem);
	background:${props => props.theme.requirementsBg};

	button.continue {
		margin: 0 auto;
		display: table;
		margin-top: 5rem;
	}

	@media (max-width: 650px) {
		padding-left:0;
		padding-right:0;
		margin-left:0;
		
		button.continue {
			margin-top: 3rem;
			margin-bottom: calc(3rem + 7px);
		}
	}
`

export default Section