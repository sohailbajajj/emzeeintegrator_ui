import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import { isEqual } from 'lodash'

import PerfectScrollbar from '@opuscapita/react-perfect-scrollbar'
import { stopBubbling } from '../utils/helpers'

import chevron from '../../images/down.png'
import chevron_white from '../../images/down_white.png'

import Spinner from './spinner'

const _Select = styled.div`
    width: 8rem;
    height: 3.3rem;
    display: inline-block;
    position: relative;
    cursor: pointer;
    transition: background 300ms ease, border-color 300ms ease;
    transition-delay: ${props => (props.isFilter ? '0' : '300ms')};
    pointer-events: ${props => (props.onlyOne || props.loading ? 'none' : 'all')};

    label {
        line-height: 3.3rem;
        color: ${props => props.theme.copyLighter};
        transition: color 300ms ease;
        transition-delay: ${props => (props.isFilter ? '0' : '300ms')};
        cursor: pointer;
        user-select: none;
    }

    .pointer {
        display: ${props => (props.onlyOne || props.loading ? 'none' : 'block')};
        width: 0;
        height: 0;
        border-left: 0.35rem solid transparent;
        border-right: 0.35rem solid transparent;
        border-top: 0.35rem solid ${props => props.theme.copyLighter};
        ${props => props.theme.verticalCenter()};
        right: ${props => (props.sum ? '0' : '1rem')};
    }

    .icon-spinner {
        ${props => props.theme.verticalCenter()} right: 0rem;
        width: 1.6rem;
    }

    ul {
        width: calc(100% + 1px);
        height: 15.5rem;
        overflow: hidden;
        position: absolute;
        transition: all 300ms ease;
        list-style-type: none;
        box-shadow: ${props => props.theme.shadow};
        background: #fff;
        border: solid 1px ${props => props.theme.border};
        border-top: solid 2px $purple;
        box-sizing: border-box;
        border-radius: 6px;
        opacity: 0;
        pointer-events: none;

        li {
            line-height: 2.5rem;
            text-indent: 1.3rem;
            color: ${props => props.theme.copyLighter};
            border-bottom: solid 1px ${props => props.theme.border};
            transition: all 100ms ease;
            cursor: pointer;
            text-transform: capitalize;

            &:hover {
                background-color: ${props => props.theme.ddHover};
                color: ${props => props.theme.blue};
            }

            &.hide {
                display: none;
            }

            &.active {
                color: ${props => props.theme.blue};
            }
        }
    }

    select {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 5;
        opacity: 0;
    }

    &.open {
        ul {
            opacity: 1;
            pointer-events: all;
        }
    }

    :not(.add) {
        ul {
            top: 20px;
        }

        &.open {
            ul {
                top: 10px;
            }
        }
    }

    &.onTop {
        z-index: 2 !important;
    }

    .error {
        display: block;
        width: 100%;
        height: 2rem;
        line-height: 2rem;
        background-color: ${props => props.theme.tooltip};
        position: absolute;
        bottom: -2.7rem;
        border-radius: 3px;
        color: #fff;
        text-align: center;
        font-size: 0.8rem;
        transition: opacity 100ms ease;
        opacity: 0;
        pointer-events: none;
        left: 0;
	}
	
	&.virgin {
		position: static !important;
		transform:none !important;
		margin-right: calc(50vw - 8rem - 20px);
		float: right;

		.pointer {
			right: calc(50vw - 9.23rem) !important;
		}

		select {
			width: calc(100% - 6rem);
		}
	}

    &::before {
        content: '';
        width: 0;
        height: 0;
        border-left: 0.5rem solid transparent;
        border-right: 0.5rem solid transparent;
        border-bottom: 0.5rem solid ${props => props.theme.tooltip};
        position: absolute;
        left: 0;
        right: 0;
        margin: 0 auto;
        bottom: -0.8rem;
        transition: opacity 100ms ease;
        opacity: 0;
        pointer-events: none;
    }

    &.disable {
        background: ${props => props.theme.disable};
        border-color: ${props => props.theme.disable};
        transition-delay: 00ms;

        label {
            color: ${props => props.theme.copyLighter};
            transition-delay: 00ms;
        }

        &:hover {
            .error,
            &::before {
                opacity: 1;
            }
        }
    }

    @media (max-width: 1200px) {
        ${props =>
            !props.isFilter &&
            `
			height: 36px;
			
			label {
				font-size:14px !important;
				line-height: 35px !important;
			}
			
			ul {
				top: 42px !important;

				li {
					font-size: 14px;
					line-height: 35px;
				}
			}
	
			&.open {
				ul {
					top: 32px !important;
				}
			}
		`};
    }
`

const Add = _Select.extend`
	width:${props => (!props.isFilter ? '10rem' : 'auto')};
	border: ${props => (!props.sum || props.blue ? `solid 1px ${props.theme.blue}` : props.isFilter ? `solid 1px #eaf2f5` : 'none')};
	text-align: center;
	height: ${props => (!props.isFilter ? '2.5rem' : '2rem')};
	border-radius:4px;
	box-sizing:border-box;
	background:#FFF;
	text-indent: ${props => (props.isFilter ? '0.8rem' : '0')};
	padding-right: ${props => (props.isFilter ? '2.3rem' : '0')};

	label {
		line-height: ${props => (!props.isFilter ? '2.5rem' : '1.9rem')};
		font-family:regular;
		font-size:${props => (!props.sum || props.isFilter ? '1rem' : '1.2rem')};
		color:${props => (!props.sum || props.blue ? props.theme.blue : props.isFilter ? props.theme.copyLight : '#232c37')};
		text-align: ${props => (!props.sum ? 'center' : 'left')};
		display: block;
		float: left;
		width:100%;
	}

	ul {
		width:${props => (props.isFilter ? 'auto' : 'calc(100% + 2px)')};
		height:auto;
		left:-1px;
		top: calc(2.5rem + 9px);

		li {
			text-indent: 0;
			text-align: center;
		}
	}

	.chevron {
		width: 0.95rem;
		position:absolute;
		right:0.5rem;
		top: 0.5rem;
		user-select:none;
		
		&.white {
			transform: rotate(180deg);
			opacity:0;
		}
	}

	&.open {
		ul {
			top: calc(2.5rem - ${props => (props.isFilter ? '3px' : '1px')});
		}

		.chevron {
			transform: rotate(180deg);
			top: 0.35rem;

			&.white {
				transform: rotate(0deg);
			}
		}
	}

	&.applied {
		background: ${props => props.theme.blue};
		
		label {
			color:#FFF;
		}

		.chevron {
			opacity:0;
			
			&.white {
				opacity:1;
			}
		}
	}

	@media (max-width: 1200px) {
		${props => !props.isFilter && 'width: 12rem;'}

		&.open {
			ul {
				top: 35px !important;
			}
		}
	}

`

export default class Select extends Component {
    state = {
        applied: false,
        timeout: undefined
    }

    componentDidMount() {
        if (!this.props.isFilter) {
            window.addEventListener('click', this.close)
            window.addEventListener('touchstart', this.close)
        }
        this.props.default && this.props.handleSelect(this.props.default)
        this.props.open ? this.open() : this.close()
    }

    componentWillUnmount() {
        if (!this.props.isFilter) {
            window.removeEventListener('click', this.close)
            window.removeEventListener('touchstart', this.close)
        }
    }

    componentDidUpdate(prevProps) {
        if (!isEqual(this.props.open, prevProps.open) || !isEqual(this.props.className, prevProps.className) || !isEqual(this.props.disable, prevProps.disable)) {
            this.props.open ? this.open() : this.close()
        }
    }

    close = () => {
        if (this.select) {
            this.select.classList.remove('open')
            this.setState({
                timeout: setTimeout(() => this.select && this.select.classList.remove('onTop'), 300)
            })
        }
    }

    open = () => {
        if (this.select) {
            clearTimeout(this.state.timeout)
            this.select.classList.add('open')
            this.select.classList.add('onTop')
        }
    }

    toggle = select => {
        if (select.classList.contains('open')) {
            if (!this.props.isFilter) {
                select.classList.remove('open')
                this.setState({
                    timeout: setTimeout(() => select.classList.remove('onTop'), 300)
                })
            }
        } else {
            if (!this.props.isFilter) {
                clearTimeout(this.state.timeout)
                select.classList.add('open')
                select.classList.add('onTop')
            }
            this.closeOtherSelectes(select)
        }
    }

    closeOtherSelectes = select => {
        let otherSelects = document.querySelectorAll('.select')
        otherSelects &&
            otherSelects.forEach(_select => {
                if (_select != select) {
                    _select.classList.remove('open')
                    _select.classList.remove('onTop')
                }
            })
    }

    handleClick = event => {
        stopBubbling(event)
        this.props.onClick && this.props.onClick()
        this.toggle(event.currentTarget)
    }

    handleSelect = (event, option, mobile = false) => {
		stopBubbling(event)
		if (!option) return
		
		if (!mobile) {
			this.props.handleSelect(option)
			this.toggle(event.currentTarget.parentElement.parentElement)
		} else {
			clearTimeout(this.state.timeout)
			this.setState({ timeout: setTimeout(() => this.props.handleSelect(JSON.parse(option)), 500)})
		}
    }

    renderInternals = () => (
        <Fragment>
            {!this.props.loading && (
                <label>
                    {this.props.add && `${this.props.placeholder} +`}
                    {(this.props.select || this.props.sum) && ((this.props.value && this.props.value.label) || this.props.placeholder)}
                </label>
            )}
            {this.props.loading && <Spinner />}
            {(this.props.select || this.props.sum) && !this.props.isFilter && <span className="pointer" />}
            {this.props.isFilter && (
                <Fragment>
                    <img className="chevron" src={chevron} />
                    <img className="chevron white" src={chevron_white} />
                </Fragment>
			)}
			
            {!this.props.disable && this.props.options && !this.props.isMobile && (
				<ul>
					{this.props.select && (
						<PerfectScrollbar option={{ maxScrollbarLength: 40 }}>
							{this.props.options.map((option, index) => (
								<li value={option.value} key={option.value + index} onClick={e => this.handleSelect(e, option)} className={this.props.value && isEqual(this.props.value.selected, option.value) ? 'active' : ''}>
									{option.label}
								</li>
							))}
						</PerfectScrollbar>
					)}
					{(this.props.add || this.props.sum) &&
						this.props.options.map((option, index) => (
							<li value={option.value} key={option.value + index} onClick={e => this.handleSelect(e, option)} className={this.props.value && isEqual(this.props.value.selected, option.value) ? 'active' : ''}>
								{option.label}
							</li>
						))}
				</ul>
			)}

            {!this.props.disable && this.props.options &&
				this.props.isMobile && (
				<select onChange={e => this.handleSelect(e, e.currentTarget.value, true)} 
                    onBlur={e => this.handleSelect(e, e.currentTarget.value, true)}
                    value=""
				>
					<option label="" value="" style={{ display: 'none' }} />
					{this.props.options.map((option, index) => (
						<option key={option.value + index} value={JSON.stringify(option)}>
							{option.label}
						</option>
					))}
				</select>
			)}
		<span className="error">{this.props.error}</span>
        </Fragment>
    )

    render() {
        if (this.props.select) {
            return (
                <_Select {...this.props} innerRef={select => (this.select = select)} onClick={this.handleClick} className={`select ${this.props.className} ${this.props.disable && 'disable'}`}>
                    {this.renderInternals()}
                </_Select>
            )
        }
        if (this.props.add || this.props.sum) {
            return (
                <Add {...this.props} innerRef={select => (this.select = select)} onClick={this.handleClick} className={`select add ${this.props.className} ${this.props.disable && 'disable'}`}>
                    {this.renderInternals()}
                    {!this.props.options && <ul>{this.props.children}</ul>}
                </Add>
            )
        }
    }
}
