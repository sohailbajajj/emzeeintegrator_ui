import React from 'react'
import styled from 'styled-components'

const _Spinner = styled.span`
    width:1.8rem;
    height:auto;
    display:block;
    transition:all 300ms ease;
    opacity:1;

    @keyframes rotator {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(270deg);
        }
    }

    @keyframes dash {
        0% {
            stroke-dashoffset: 187px;
        }
        50% {
            stroke-dashoffset: ${187/4}px;
            transform: rotate(135deg);
        }
        100% {
            stroke-dashoffset: 187px;
            transform: rotate(450deg);
        }
    }

    .spinner {
        animation: rotator 1.4s linear infinite;
        width:100%;
        height:100%;
        overflow:visible;
    }

    .path {
        stroke-dasharray: 187;
        stroke-dashoffset: 0;
        stroke: ${props => props.color || props.theme.blue};
        transform-origin: center;
        animation: dash 1.4s ease-in-out infinite;
    }
`

const Spinner = (props) => {
    return (
        <_Spinner className={`icon-spinner ${props.className}`}>
            <svg className="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                <circle className="path" fill="none" strokeWidth="6" strokeLinecap="round" cx="33" cy="33" r="30"></circle>
            </svg>
        </_Spinner>
    )
}

export default Spinner
