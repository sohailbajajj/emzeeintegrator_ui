import React, { Component } from 'react'
import styled from 'styled-components'

const _YesNo = styled.div`
	button {
		outline:none;
		border:solid 1px ${props => props.theme.blue};
		color:${props => props.theme.blue};
		text-align:center;
		font-family:medium;
		border-radius:5px;
		height: 2.2rem;
		width: 4rem;
		line-height: 2rem;
		font-size:0.9rem;
		padding: 0;
		cursor:pointer;
		box-shadow: ${props => props.theme.shadow};
		background:#FFF;
		transition:all 300ms ease;
		
		&:first-child {
			margin-right:1rem;
		}

		&:hover, &.selected {
			color:#FFF;
			background: ${props => props.theme.blue};
		}
	}
`

export default class YesNo extends Component {
	state = { hovering: false }

	render() {
		return (
			<_YesNo className="yesNo" >
				<button className={`${this.props.selected && !this.state.hovering && 'selected'} ${this.state.hovering && 'hovering'}`} 
					onMouseEnter={() => this.setState({ hovering: true })}
					onMouseLeave={() => this.setState({ hovering: false })}
					onClick={() => this.props.handleClick(true)}
				>
					Yes
				</button>
				<button className={`${!this.props.selected && !this.state.hovering && 'selected'} ${this.state.hovering && 'hovering'}`}
					onMouseEnter={() => this.setState({ hovering: true })}
					onMouseLeave={() => this.setState({ hovering: false })}
					onClick={() => this.props.handleClick(false)}
				>
					No
				</button>
			</_YesNo>
		)
	}
}