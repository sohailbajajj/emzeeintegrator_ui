import React, { Component } from 'react'
import { Link, Route, Switch } from "react-router-dom";
import { connect } from 'react-redux'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import styled from 'styled-components'
import isEmpty from 'lodash/isEmpty'
import queryString from 'query-string'
import { destroy } from 'redux-form'

import Section from '../components/section'
import Button from '../components/button'
import Rupee from '../components/rupee'
import Nav from '../components/buy/nav'
import Buyer from '../components/buy/buyer'
import Members from '../components/buy/members'
import Address from '../components/buy/address'
import MedicalHistory from '../components/buy/medicalHistory'
import Review from '../components/buy/review'
import PostPayment from '../components/postPayment'
import isEqual from 'lodash/isEqual'
import { GENDER_SELECTION, FAMILY_SELECTION, DISEASE_DECLARATION, ENTER_DETAILS, POLICY_RESULTS, POLICY_FORM } from '../components/constants'
import { submitProposalForm, setCascadingOptions, setDiscountedPremium, setPgUrl } from '../actions/buy'

import GENDER_SELECTION_inactive from '../../images/nav/1.png'
import GENDER_SELECTION_active from '../../images/nav/1.1.png'
import FAMILY_SELECTION_inactive from '../../images/nav/2.png'
import FAMILY_SELECTION_active from '../../images/nav/2.1.png'
import DISEASE_DECLARATION_inactive from '../../images/nav/3.png'
import DISEASE_DECLARATION_active from '../../images/nav/3.1.png'
import ENTER_DETAILS_active from '../../images/nav/4.1.png'
import ENTER_DETAILS_inactive from '../../images/nav/4.png'

const _Buy = styled(Section)`
    >.wrapper {
        margin-top:3rem !important;
        height:calc(100% - 7.5rem);
    }
    a.buy_continue_btn.disable { pointer-events: none; }
    form {
        width:65%;
        float:left;
        height:100%;

        .head {
            h3 {
                color:${props => props.theme.copyLighter}
                font-size:1.6rem;
            }
    
            p {
                font-family:medium;
                font-size:0.9rem;
                color:${props => props.theme.tooltip}
                margin-top: 0.8rem;
            }
        }
    }

    .container-policy-details {
        width:35%;
        float:left;
        height:100%;
        position:relative;

        .container-policy {
            width:300px;
            padding:1.5rem;
            box-sizing:border-box;
            border-radius: 6px;
            box-shadow: 0 1px 2px 0 rgba(101, 116, 124, 0.1);
            border: solid 1px #eaecef;
            ${props => props.theme.absoluteCenter()}
            top: calc(50% - 4.5rem);
            left:auto;
            right:0;

            h6 {
                color: ${props => props.theme.copyLighter}
                font-size:0.8rem;
                letter-spacing: 0.05rem;
                margin-bottom: 0.8rem;
                text-transform:uppercase;
            }

            p {
                color: ${props => props.theme.copyLighter}
                font-size:0.95rem;
                margin-bottom: 0.3rem;

                &.number {
                    margin-top: 0.4rem;
                }

                &.border {
                    border-bottom: solid 1px #f1f6f8;
                    color: ${props => props.theme.copyLight}
                    line-height:3rem;
                    margin:0;

                    &.total {
                        border:none;
                    }
                }

                &.up {
                    text-transform:capitalize;
                }

                span {
                    float:right;
                    color:#1b272f;
                    position:relative;

                    .icon-rupee {
                        ${props => props.theme.verticalCenter()}
                        left: -0.8rem;
                        height: 0.77rem;
    
                        svg {
                            top: 54%;
                        }
    
                        .icon {
                            stroke:#1b272f;
                        }
                    }
                }

                &.total {
                    span {
                        color: ${props => props.theme.purple}
                        font-family:medium;

                        .icon-rupee .icon {
                            stroke:${props => props.theme.purple}
                        }
                    }
                }
            }

            .policy {
                border-top: solid 1px #f1f6f8;
                border-bottom: solid 1px #f1f6f8;
                margin-top: 2rem;
                padding: 1rem 0;
                padding-top: 1.2rem;

                h6 {
                    margin:0;
                }

                img {
                    width:10rem;
                    margin-top: -0.4rem;
                }

                p {
                    margin-top: -0.5rem;
                }
            }
        }
    }

    .container-nav {
        width:100%;
        background:#FFF;
        position:fixed;
        bottom:0;
        left:0;
        height:6rem;
        box-shadow: 0 -4px 8px 0 rgba(101, 116, 124, 0.05), 0 -2px 4px 0 rgba(212, 221, 232, 0.25);

        .wrapper {
            ${props => props.theme.applyGutter()}
            height:100%;
            position:relative;
        }

        .note {
            color:${props => props.theme.copyLight}
            font-size:12px;
            ${props => props.theme.verticalCenter()}
            top: calc(50% - 0.2rem);

            br {
                display:block;
                line-height:1.4rem;
            }
        }

        .container-buttons {
            ${props => props.theme.verticalCenter()}
            right:0;

            button:first-child {
                box-shadow: 0 1px 2px 0 rgba(82, 103, 255, 0.2);
                border: solid 1px rgba(82, 103, 255, 0.5);
                font-family: regular;
            }

            button:last-child {
                margin-left:1.5rem;
                width: 17rem;
            }
        }
    }
`

class Buy extends Component {
    constructor(props) {
        super(props)
        this.names = this.props.UserDetailsForm.values.name.split(' ')
        this.members = this.props.members.map(
            member => `${member.counttype ? `${member.counttype}${member.name ? `-${member.name}` : ''}` : `${member.type}`}`
        )
        /* */
        if(this.props && this.props.location && !this.props.location.query && this.props.location.query == undefined) {
            this.props.location.query = {}
        }
        this.props.location.query = queryString.parse(this.props && this.props.location && this.props.location.search);
        /* */
    }
    static defaultProps = {
		formStages: {
			details: {
				linkBack: '/health/quotes',
				linkNext: '/health/buy/members',
				next: 'members'
			},
			members: {
				linkBack: '/health/buy/details',
				linkNext: '/health/buy/address',
				back: 'details',
				next: 'address'
            },
            address: {
				back: 'members',
				next: 'medicalHistory',
				linkBack: '/health/buy/members',
				linkNext: '/health/buy/medicalHistory',
			},
			medicalHistory: {
				back: 'address',
				next: 'review',
				linkBack: '/health/buy/address',
				linkNext: '/health/buy/review',
            },
            review: {
				back: 'medicalHistory',
				//linkNext: '/payment',
				linkNext: '',
				linkBack: '/health/buy/medicalHistory'
            },
            postPayment: {
                back: '',
				linkNext: '',
				linkBack: ''
            }
        },
        /* These stages use will that time if client wants back and continue button on every pages. At that time this "stages" JSON need to be use on previous pages and set "stages" in to props not here. */
        stages: {
            [GENDER_SELECTION]: {
                current: GENDER_SELECTION,
                description: 'Who are you?',
                link: '/',
                next: '/health/household',
                active: GENDER_SELECTION_active,
                inactive: GENDER_SELECTION_inactive
            },
            [FAMILY_SELECTION]: {
                current: FAMILY_SELECTION,
                description: 'Select family members',
                link: '/health/household',
                next: '/health/details',
                prev: '/',
                active: FAMILY_SELECTION_active,
                inactive: FAMILY_SELECTION_inactive
            },
            [DISEASE_DECLARATION]: {
                current: DISEASE_DECLARATION,
                description: 'Declare diseases',
                link: '/health/details',
                next: '/health/getquotes',
                prev: '/health/household',
                active: DISEASE_DECLARATION_active,
                inactive: DISEASE_DECLARATION_inactive
            },
            [ENTER_DETAILS]: {
                current: ENTER_DETAILS,
                description: 'Enter details',
                link: '/health/getquotes',
                next: '/health/quotes',
                prev: '/health/details',
                active: ENTER_DETAILS_active,
                inactive: ENTER_DETAILS_inactive
            },
            [POLICY_RESULTS]: {
                current: POLICY_RESULTS,
                description: 'Policy results',
                link: '/health/quotes',
                next: '/health/buy/details',
                prev: '/health/getquotes'
            },
            [POLICY_FORM]: {
                current: POLICY_FORM,
                description: 'Policy form',
                link: '/health/buy/details'
            }
        }
        /* End of stages */
	}

	state = {
		currentStage: 'details',
		enableNext: false,
		completedStages: {
			details: false,
			members: false,
			address: false,
			medicalHistory: false,
			review: false
		},
		enabledStages: {
			details: false,
			members: false,
			address: false,
			medicalHistory: false,
			review: false
		},
		acceptTnc: false
	}
    getStage = () => this.props.location.pathname.split('/').slice(-1).pop()

    getComboMembers = (getGroup = false, getCombo = false) => {
        let { selected: { combo, group }, groups } = this.props.quotes.filters.members
        let selectedGroup = groups.find(_group => _group.id == group)
        let selectedCombo = selectedGroup.combos.find(_combo => _combo.id == combo[group])
        if (getGroup) return selectedGroup
        if (getCombo) return selectedCombo
        return selectedCombo.name
    }
    scrollToTop = () => {
		let docBody = document.body
		let focElem = document.documentElement
		setTimeout(() => docBody.scrollTop = focElem.scrollTop = 0, 1000)
	}
    componentWillMount() {
        this.changeStage()
    }
    componentDidMount() {
        this.scrollToTop()
        this.toggleNext(this.props)
        this.markCompletedStages()
        this.checkEnabledStages(this.props)
        this.changeFormStage(this.getCurrentForm(this.props.location.pathname))
    }
    componentWillReceiveProps(next) {
        //console.log(next)
        if(next.forms.ProposerForm) {
            if(next.forms.ProposerForm && next.forms.ProposerForm.values && next.forms.ProposerForm.values[this.props.insurer] && next.forms.ProposerForm.values.ProposerForm && next.forms.ProposerForm.values.ProposerForm.DOB) {
                let dob = next.forms.ProposerForm.values.ProposerForm.DOB
                if(dob && dob.Day && dob.Month && dob.Year) {
                    let day = (dob.Day < 10) ? ("0" + dob.Day) : dob.Day
                    let month = parseInt(dob.Month) + 1
                    let finalMonth = (month < 10) ? ("0" + month) : month
                    next.forms.ProposerForm.values[this.props.insurer].dateOfBirth = day + "/" + finalMonth + "/" + dob.Year
                }
            }
        }
        if(next.forms.NomineeForm) {
            if(next.forms.NomineeForm && next.forms.NomineeForm.values && next.forms.NomineeForm.values[this.props.insurer] && next.forms.NomineeForm.values.NomineeForm && next.forms.NomineeForm.values.NomineeForm.DOB) {
                let dob = next.forms.NomineeForm.values.NomineeForm.DOB
                if(dob && dob.Day && dob.Month && dob.Year) {
                    let day = (dob.Day < 10) ? ("0" + dob.Day) : dob.Day
                    let month = parseInt(dob.Month) + 1
                    let finalMonth = (month < 10) ? ("0" + month) : month
                    next.forms.NomineeForm.values[this.props.insurer].dateOfBirth = day + "/" + finalMonth + "/" + dob.Year
                }
            }
        }
        Object.keys(next.members).forEach(member => {
            let memberType = next.members[member].type == 'child' ? next.members[member].counttype+'-'+next.members[member].name : next.members[member].type
            if(next.forms.MembersForm) {
                if(next.forms.MembersForm && next.forms.MembersForm.values && next.forms.MembersForm.values[this.props.insurer] && next.forms.MembersForm.values[memberType] && next.forms.MembersForm.values[memberType].DOB && next.forms.MembersForm.values[this.props.insurer][memberType]) {
                    let dob = next.forms.MembersForm.values[memberType].DOB
                    if(dob && dob.Day && dob.Month && dob.Year) {
                        let day = (dob.Day < 10) ? ("0" + dob.Day) : dob.Day
                        let month = parseInt(dob.Month) + 1
                        let finalMonth = (month < 10) ? ("0" + month) : month
                        next.forms.MembersForm.values[this.props.insurer][memberType].dateOfBirth = day + "/" + finalMonth + "/" + dob.Year
                    }
                }
            }
            if(next.forms.MedicalHistoryForm) {
                if(next.forms.MedicalHistoryForm && next.forms.MedicalHistoryForm.values && next.forms.MedicalHistoryForm.values[memberType] && next.forms.MedicalHistoryForm.values.MedicalHistoryForm && next.forms.MedicalHistoryForm.values.MedicalHistoryForm[memberType] && next.forms.MedicalHistoryForm.values.MedicalHistoryForm[memberType].DOB) {
                    let dob = next.forms.MedicalHistoryForm.values.MedicalHistoryForm[memberType].DOB
                    if(dob && dob.Day && dob.Month && dob.Year) {
                        let day = (dob.Day < 10) ? ("0" + dob.Day) : dob.Day
                        let month = parseInt(dob.Month) + 1
                        let finalMonth = (month < 10) ? ("0" + month) : month
                        next.forms.MedicalHistoryForm.values[memberType].Q3 = day + "/" + finalMonth + "/" + dob.Year
                    }
                }
            }
        })
        this.toggleNext(next) 
        this.markCompletedStages()
        this.checkEnabledStages(this.props)
        if (!isEqual(this.props.location.pathname, next.location.pathname)) {
			this.changeFormStage(this.getCurrentForm(next.location.pathname))
        }
        /* conver pincode to integer */
        if(next.forms && next.forms.AddressForm && next.forms.AddressForm.values && next.forms.AddressForm.values[this.props.insurer] && next.forms.AddressForm.values[this.props.insurer].pincode) {
            next.forms.AddressForm.values[this.props.insurer].pincode = parseInt(next.forms.AddressForm.values[this.props.insurer].pincode)
        }
        /* */
        
        if (!isEqual(this.props.location.pathname, next.location.pathname)) {
			this.animateScrollTop(300)
        }
    }
    getProposerType = () => {
        let proposer = this.members.filter(member => this.getProposerName(member) == 'you')[0]
        return proposer
    }
    getProposerName = name => {
		switch (name) {
			case 'husband':
				if (this.props.gender == 'male') return 'you'
				else return name
			case 'wife':
				if (this.props.gender == 'female') return 'you'
				else return name
			default:
				return name
		}
    }
    getCurrentForm = path => {
		return path.split('/').slice(-1).pop()
    }
    changeStage = () => {
        if (this.props.gender) {
            if(this.props.members[0].init) {
                
            } else {
                this.props.history.push(this.props.stages[FAMILY_SELECTION].link)
            }
        } else {
            //this.props.history.push(this.props.stages[GENDER_SELECTION].link)
            window.location = '/'
        }
    }
    changeFormStage = stage => {
		if (stage) this.setState({ currentStage: stage })
		this.toggleNext(this.props, stage)
		this.checkEnabledStages(this.props)
	}
    toggleNext = (props, currentStage = null) => {
		let stage = currentStage || this.state.currentStage
		let enable = this.checkStage(props, stage)
		this.setState({ enableNext: enable })
    }
    checkStage = (props, stage) => {
		const conditions = {
			details: props.forms.ProposerForm && props.forms.NomineeForm && props.forms.ProposerForm.submitSucceeded && props.forms.NomineeForm.submitSucceeded,
			members: props.forms.MembersForm && props.forms.MembersForm.submitSucceeded,
			address: props.forms.AddressForm && props.forms.AddressForm.submitSucceeded,
			medicalHistory: props.forms.MedicalHistoryForm && props.forms.MedicalHistoryForm.submitSucceeded
		}

		switch (stage) {
			case 'details':
				return conditions.details ? true : false
				break
			case 'members':
				return conditions.details && conditions.members ? true : false
				break
			case 'address':
				return conditions.details && conditions.members && conditions.address ? true : false
				break
			case 'medicalHistory':
				return conditions.details && conditions.members && conditions.address && conditions.medicalHistory ? true : false
				break
			case 'review':
				return conditions.details && conditions.members && conditions.address && conditions.medicalHistory ? true : false
				break
			default:
				return false
				break
		}
    }

    markCompletedStages = (stage = null) => {
		if (stage) {
			this.setState({ completedStages: { ...this.state.completedStages, [stage]: this.checkStage(this.props, stage) } })
			return
		}

		let completedStages = {}

		Object.keys(this.props.formStages).forEach(stage => {
			completedStages[stage] = this.checkStage(this.props, stage)
		})

		this.setState({
			completedStages: {
				...this.state.completedStages,
				...completedStages
			}
		})
    }
    
    checkEnabledStages = (props) => {

		const conditions = {
			details: props.forms.ProposerForm && props.forms.NomineeForm && props.forms.ProposerForm.submitSucceeded && props.forms.NomineeForm.submitSucceeded,
			members: props.forms.MembersForm && props.forms.MembersForm.submitSucceeded,
			address: props.forms.AddressForm && props.forms.AddressForm.submitSucceeded,
			medicalHistory: props.forms.MedicalHistoryForm && props.forms.MedicalHistoryForm.submitSucceeded
		}

		let checkStage = (stage) => {
			switch (stage) {
				case 'details':
					return conditions.details ? true : false
					break
				case 'members':
					return this.checkStage(this.props, 'details') && conditions.members ? true : false
					break
				case 'address':
					return this.checkStage(this.props, 'members') && conditions.address ? true : false
					break
				case 'medicalHistory':
					return this.checkStage(this.props, 'address') && conditions.medicalHistory ? true : false
					break
				default:
					return false
					break
			}
		}

		let enabledStages = {}

		Object.keys(this.props.formStages).forEach(stage => {
			enabledStages[stage] = checkStage(stage)
		})

		this.setState({
			enabledStages: {
				...this.state.enabledStages,
				...enabledStages
			}
        })
	}

    acceptTnc = accept => this.setState({ acceptTnc: accept })

    getNames = () => {
        let names = this.names
        let return_names = ''
        Object.keys(names).forEach(function(key) {
            return_names += key > 0 && !isEmpty(names[key]) ? (key > 1 ? ' '+names[key]: names[key]) : ''
        })
        if(return_names)
            return return_names
        else
            return ''
    }
    getAddonPrice = addon => {
        if (addon.isFlat) {
            return addon.premium
        } else {
            let premium = parseInt(this.props.quotes.cart.duration.value.replace(/,/g, ''))
            return Math.round((premium*addon.premium)/100)
        }
    }
    getNetPremium = () => {
        if (this.props.quotes.cart.addons[0]) {
            let addons = this.props.quotes.cart.addons.reduce((net, addon) => {
                net = net + parseInt(this.getAddonPrice(addon))
                return net
            }, 0)
            return (parseInt(this.props.quotes.cart.duration.value.replace(/,/g, '')) + addons).toLocaleString('en-IN')
        } else {
            return this.props.quotes.cart.duration.value
        }
    }

    animateScrollTop = (speed = 300) => {
        let stepTime = 20
      let docBody = document.body
      let focElem = document.documentElement

      var scrollAnimationStep = function (initPos, stepAmount) {
          let newPos = initPos - stepAmount > 0 ? initPos - stepAmount : 0

          docBody.scrollTop = focElem.scrollTop = newPos

          newPos && setTimeout(function () {
              scrollAnimationStep(newPos, stepAmount)
          }, stepTime)
      }

      let scrollTopAnimated = function (speed) {
          let topOffset = docBody.scrollTop || focElem.scrollTop
          let stepAmount = topOffset

          speed && (stepAmount = (topOffset * stepTime)/speed)

          scrollAnimationStep(topOffset, stepAmount)
      }

      scrollTopAnimated(speed)
    }

    render() {
        const postPaymentPage = this.props && this.props.location && this.props.location.pathname && this.props.location.pathname.includes("health/buy/postPayment");
        return (
            <_Buy className={`buy-policy ${!postPaymentPage ? "notPostPaymentPage" : ""}`} nowrapper={ !postPaymentPage ? <Nav quotes={this.props.quotes} active={this.getStage()} completedStages={this.state.completedStages} enabledStages={this.state.enabledStages} location = {this.props.location} /> : ""}>
                <TransitionGroup className={`customerDetailsForms ${postPaymentPage ? "postPaymentPage" : ""}`}>
                    <CSSTransition key={this.props.location.pathname} classNames="fade" timeout={{ enter: 500, exit: 300 }}>
                        <Switch location={this.props.location}>
                            <Route path="/health/buy/details" render={props => 
                                <Buyer {...props} isMobile={this.props.isMobile} 
                                    form={this.props.form && this.props.form.proposer}
                                    policy={this.props.policy} policies={this.props.policies} insurer={this.props.insurer} age={this.props.age} gender={this.props.gender} members={this.props.members} UserDetailsForm={this.props.UserDetailsForm} forms={this.props.forms} getNames={this.getNames()}
                                />
                            } />
                            <Route path="/health/buy/members" render={props => 
                                <Members {...props} isMobile={this.props.isMobile} 
                                    form={this.props.form && this.props.form.insured}
                                    insurer={this.props.insurer} gender={this.props.gender} members={this.props.members} UserDetailsForm={this.props.UserDetailsForm} getNames={this.getNames()} forms={this.props.forms}
                                />
                            } />
                            <Route path="/health/buy/address" render={props => 
                                /*<Address {...props} isMobile={this.props.isMobile} 
                                    form={this.props.form && this.props.form.contact} forms={this.props.forms} insurer={this.props.insurer} UserDetailsForm={this.props.UserDetailsForm} setCascadingOptions={this.props.setCascadingOptions}
                                />*/
                                <Address {...props} isMobile={this.props.isMobile} 
                                    form = { this.props && this.props.proposalForm && this.props.proposalForm.fields && this.props.proposalForm.fields.contact } forms={this.props.forms} insurer={this.props.insurer} UserDetailsForm={this.props.UserDetailsForm} setCascadingOptions={this.props.setCascadingOptions}
                                />
                            } />
                            <Route path="/health/buy/medicalHistory" render={props => 
                                <MedicalHistory {...props} isMobile={this.props.isMobile} 
                                    form={this.props.form && this.props.form.medicalHistory} policies={this.props.policies} forms={this.props.forms} fields={this.props.form && this.props.form} members={this.props.members} gender={this.props.gender}
                                />
                            } />
                            <Route path="/health/buy/review" render={props => 
                                <Review {...props} isMobile={this.props.isMobile} 
                                    form={this.props.form && this.props.form.medicalHistory} forms={this.props.forms} fields={this.props.form && this.props.form} members={this.props.members} gender={this.props.gender} insurer={this.props.insurer} acceptTnc={this.acceptTnc} proposalForm={this.props.proposalForm} submitProposalForm={this.props.submitProposalForm} policy={this.props.policy} policies={this.props.policies} UserDetailsForm={this.props.UserDetailsForm} setDiscountedPremium={this.props.setDiscountedPremium}
                                    tnc={this.state.acceptTnc} setPgUrl={this.props.setPgUrl} addons={this.props.quotes.cart && this.props.quotes.cart.addons}
                                />
                            } />
                            <Route path="/health/buy/postPayment" render={props => 
                                <PostPayment {...props} proposalForm = { this.props.proposalForm } policy = { this.props.policy } members = { this.props.members } family = { this.props.household } resetProposerForm = { this.props.resetProposerForm } resetNomineeForm = { this.props.resetNomineeForm } resetMembersForm = { this.props.resetMembersForm } resetAddressForm = { this.props.resetAddressForm } resetMedicalHistoryForm = { this.props.resetMedicalHistoryForm } policies = { this.props.policies } gender = { this.props.gender } />
                            } />
                        </Switch>
                    </CSSTransition>
                </TransitionGroup>
                {
                    !postPaymentPage && this.props.quotes.cart.quote &&
                    <div className="hidden-xs container-policy-details">
                        <div className="container-policy">
                            <h6>YOU</h6>
                            <p>{this.props.quotes.user.email}</p>
                            <p className="number">{this.props.quotes.user.number}</p>
                            <p>{this.props.quotes.user.name}</p>

                            <div className="policy">
                                <h6>selected policy</h6>
                                <img src={this.props.quotes.cart.product.logo} />
                                <p>{this.props.quotes.cart.product.productName}</p>
                            </div>

                            <p className="border up">Insuring {this.getComboMembers()}.</p>
                            <p className="border">Sum Insured 
                                <span>
                                    <Rupee />
                                    {parseInt(this.props.quotes.cart.sum).toLocaleString('en-IN')}
                                </span>
                            </p>
                            <p className="border total">
                                Total Payable
                                <span>
                                    <Rupee />
                                    {this.getNetPremium()}
                                </span>
                            </p>
                        </div>
                    </div>
                }
                {
                    !postPaymentPage && <div className="container-nav">
                        <div className="wrapper">
                            <div className="note">
                                The policy period will start immediately from the date this transaction gets processed.
                                <br />
                                Any questions? Call us on Toll Free number: <a href="tel:7840040042" target="_blank">7840040042</a>
                            </div>
                            <div className="container-buttons">
                                <Link to={this.props && this.props.formStages && this.props.formStages[this.state.currentStage] && this.props.formStages[this.state.currentStage].linkBack ? this.props.formStages[this.state.currentStage].linkBack : ''} className={`${this.props && this.props.formStages && this.props.formStages[this.state.currentStage] && this.props.formStages[this.state.currentStage].linkBack ? '' : 'disable'} forms_back_btn`}	onClick={() => this.changeFormStage(this.props.formStages[this.state.currentStage].back)}>
                                    Back
                                    <label className="backbtn-arrow">←</label>
                                </Link>
                                    {
                                        /*
                                        <Button label="Back" back outline onClick={() => this.props.history.goBack()}/>
                                        */
                                    }
                                {/*<span className="continue disable-review">*/}
                                    <Link to={this.props && this.props.formStages[this.state.currentStage].linkNext ? this.props.formStages[this.state.currentStage].linkNext : ''} className={`buy_continue_btn ${this.props && this.props.formStages && this.props.formStages[this.state.currentStage].linkNext ? (this.state.enableNext ? ((this.state.currentStage == 'review' &&
                                                (
                                                    !this.state.acceptTnc ||
                                                    !this.props.proposalForm.submitResponse ||
                                                    (this.props.proposalForm.submitResponse.paymentUrl && !this.props.proposalForm.pgUrl)
                                                )
                                            ) ? 'disable disable-review' : '') : 'disable') : 'disable'}`} onClick={() => this.changeFormStage(this.props.formStages[this.state.currentStage].next)}>
                                        {this.state.currentStage != 'review' ? 'Continue' : 'Buy Now'}
                                        <label className="continuebtn-arrow">→</label>
                                    </Link>
                                {/*</span>*/}
                                {
                                    /*
                                    <Button label="Continue" continue />
                                    */
                                }
                            </div>
                        </div>
                    </div>
                }
            </_Buy>
        )
    }
}

const mapStateToProps = state => {
    //console.log(state)
    let { quotes, details, buy: { form }, proposalForm, household } = state
    let forms = state.form
    let quote = quotes.cart.quote
    if(quote == "" || quote == undefined || quote == null) {
        window.location = "/health/quotes";
        return {}
    }
    let product = quotes.cart.product
    Object.keys(quote).forEach(function(key) {
        product[key] = quote[key]
    })
    let policy = product
    policy.premium['selected'] = quotes.cart.duration.selected
    //policy['activityId'] = policy.trackingId
    let policies = {selectedGroup: {name: state.quotes.filters.members.groups[0].type}, selectedPolicy: {insurer: state.quotes.cart.product.insurer, insurerId: state.quotes.cart.product.insurerId}}
    let insurer = `${policies.selectedPolicy.insurer.split(' ')[0]}${policies.selectedPolicy.insurerId}`
    let age = state.quotes.filters.members.members[0].age.value
    let gender = state.quotes.filters.members.members[0].gender
    //let members = state.quotes.filters.members.members
    let selectedGroupID = state.quotes.filters.members.selected.group
    let group = state.quotes.filters.members.groups
    let members = null
    Object.keys(group).forEach(function(key) {
        if(group[key].id == selectedGroupID) {
            members = group[key].members
            return
        }
    })
    let ComboID = state.quotes.filters.members.selected.combo[selectedGroupID]
    let QuotesLength = state.quotes.quotes[selectedGroupID][ComboID].length
    policy['activityId'] = state.quotes.quotes[selectedGroupID][ComboID][QuotesLength - 1]['trackingId']
    policy['trackingId'] = state.quotes.quotes[selectedGroupID][ComboID][QuotesLength - 1]['trackingId']
    let UserDetailsForm = { values: state.quotes.user, parentsCity: details.selectCity.selected && details.selectCity.selection }
    return { quotes, details, form, forms, policy, policies, insurer, age, gender, members, UserDetailsForm, proposalForm, household }
}

const mapDispatchToProps = dispatch => {
    return {
        submitProposalForm: request => dispatch(submitProposalForm(request)),
        setCascadingOptions: request => dispatch(setCascadingOptions(request)),
        setDiscountedPremium: request => dispatch(setDiscountedPremium(request)),
        setPgUrl: request => dispatch(setPgUrl(request)),
        resetProposerForm: () => dispatch(destroy('ProposerForm')),
		resetNomineeForm: () => dispatch(destroy('NomineeForm')),
		resetMembersForm: () => dispatch(destroy('MembersForm')),
		resetAddressForm: () => dispatch(destroy('AddressForm')),
		resetMedicalHistoryForm: () => dispatch(destroy('MedicalHistoryForm')),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Buy)
