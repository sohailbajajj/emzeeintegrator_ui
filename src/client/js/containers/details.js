import React, { Component, Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { values, omit, isEmpty, isEqual, pick } from 'lodash'
import { destroy } from 'redux-form'

import PerfectScrollbar from '@opuscapita/react-perfect-scrollbar'

import search from '../../images/search.png'
import chevron from '../../images/chevron.png'

import { editQuestion, getCities, toggleDisease, toggleDiseaseForChild } from '../actions/details'
import { toggleContinue } from '../actions/errContinue'

import { RequirementsSection } from '../components/section'
import HeaderDesc from '../components/headerDesc'
import Button from '../components/button'
import YesNo from '../components/yesNo'
import Modal from '../components/modal'
import Checkbox from '../components/checkbox'

const _Details = RequirementsSection.extend`
    .container-questions {
        .question {
            &:first-child {
                border-bottom: none;
                margin-top: 9vh;
                border-top-left-radius: 6px;
                border-top-right-radius: 6px;
            }

            &:last-child {
                border-bottom-left-radius: 6px;
                border-bottom-right-radius: 6px;
                border-bottom: solid 1px ${props => props.theme.border} !important;
            }
        }
    }
`

const _Question = styled.div`
    width: 50vw;
    height: 6.5rem;
    margin: 0 auto;
    background: #fff;
    border: solid 1px ${props => props.theme.border};
    min-width: 45rem;
    position: relative;
    transition: height 300ms ease;

    .container-question {
        ${props => props.theme.verticalCenter()} left: 1.7rem;
        height: 2.9rem;
        transition: height 300ms ease;

        > label {
            display: block;
            color: ${props => props.theme.tooltip};
            font-size: 1.2rem;
        }

        > span {
            display: block;
            color: ${props => props.theme.copyLighter};
            margin-top: 0.4rem;
            font-size: 0.9rem;
        }

        .city {
            color: ${props => props.theme.blue};
            margin-top: calc(0.9rem + 5px);
            opacity: 0;
            user-select: none;
            transition: all 200ms ease;

            label {
                font-family: medium;
                text-transform: capitalize;
                font-size: 1.2rem;
                position: relative;

                &::after {
                    content: '';
                    position: absolute;
                    width: 100%;
                    height: 2px;
                    background: ${props => props.theme.green};
                    left: 0;
                    bottom: -0.3rem;
                }
            }

            span {
                cursor: pointer;
                font-size: 1.1rem;
            }
        }
    }

    .yesNo {
        ${props => props.theme.verticalCenter()};
        right: 1.7rem;
    }

    &.selectCity {
        &.selected {
            height: 8rem;

            .container-question {
                height: 5rem;

                .city {
                    opacity: 1;
                    margin-top: 0.9rem;
                    transition: all 300ms ease;
                }
            }
        }
    }

    @media (max-width: 650px) {
        width:100%;
        min-width:0;
        box-sizing:border-box;
        height:auto !important;

        .container-question {
            position:relative;
            transform:none;
            top:0;
            right:0;
            left:0;
            padding:1.7rem;
            height:auto !important;

            > label {
                font-size:16px;
                line-height: 19px;
                margin-top: 0.5rem;
            }

            > span {
                font-size:12px;
                line-height:17px;
            }

            .city {
                margin-top: 1.1rem !important;

                label {
                    font-size: 16px;
                }
            }
        }

        .yesNo {
            position:relative;
            transform:none;
            top:0;
            right:0;
            padding:1.7rem;
            padding-top: 0;

            button {
                height: 2.5rem;
                width: 4.5rem;
                font-size:14px;
            }
        }

        &.selectCity {
            margin-top: 3.5rem !important;
        }
    }
`

const _ModalSelectCity = styled(Modal)`
    .container-modal {
        width: 24rem;
    }

    .search {
        position: relative;
        width: 100%;
        height: 2.5rem;
        border: solid 1px ${props => props.theme.border};
        margin-top: 2rem;
        border-radius: 3px;
        overflow: hidden;
        box-shadow: 0px 0px 12px -6px rgba(134, 134, 134, 0.75);

        input {
            border: none;
            outline: none;
            width: 100%;
            height: 100%;
            padding: 0;
            text-align: center;
            text-transform: capitalize;
            font-family: regular;
            color: ${props => props.theme.purple};

            &::placeholder {
                color: ${props => props.theme.copyLighter};
                text-transform: none;
            }
        }

        img {
            ${props => props.theme.verticalCenter()} width: 1.2rem;
            left: 0.9rem;
        }
    }

    .cities {
        margin-top: 1.3rem;
        height: 21.5rem;
        width: calc(100% + 13px);
        overflow: hidden;

        .city {
            display: block;
            width: calc(100% - 13px);
            border-bottom: solid 1px ${props => props.theme.border};
            min-height: 3rem;
            font-size: 1rem;
            color: ${props => props.theme.blue};
            cursor: pointer;
            position: relative;
            cursor: pointer;
            transition: font-size 100ms ease;

            span {
                font-size: 1rem;
                line-height: 1.4rem;
                padding: 0.82rem 0;
                display:block;
            }

            img {
                ${props => props.theme.verticalCenter()} width: 1.2rem;
                right: 2px;
                transition: all 100ms ease-out;
            }

            &:hover {
                font-size: 1.1rem;

                img {
                    width: 1.3rem;
                    right: 0;
                }
            }
        }

        .ps {
            .ps__rail-y {
                opacity: 0;
            }

            &.ps--scrolling-y {
                .ps__rail-y {
                    opacity: 0.6;
                }
            }
        }
    }

    .note {
        font-size: 0.85rem;
        color: ${props => props.theme.copyLighter};
        line-height: 1.1rem;
        margin-top: 1rem;
    }

    @media (max-width: 700px) {
        .container-modal {
            width:calc(100% - 40px);
            height:calc(100% - 40px);
            margin-bottom:20px;
            top: 20px;
            transform: none;

            .search {
                height:3.5rem;

                input {
                    font-size:14px;
                }

                img {
                    width: 17px;
                }
            }

            .cities {
                height: calc(100% - 14rem);
                margin-bottom: 2rem;

                .city {
                    span {
                        font-size:14px;
                        line-height: 20px;
                        padding: 12px 0;
                    }
                }
            }

            .note {
                font-size:12px;
                line-height:18px;
            }
        }

        .ps__rail-x {
            display:none;
        }

        .ps__rail-y {
            right:0;
            left:auto !important;
        }
    }
`

const _ModalSelectDiseases = styled(Modal)`
	.container-modal {
		min-width: 60rem;
		width: 75vw;
		max-width: 80rem;
	}

	.head-members {
		width:100%;
		height:3rem;
		background:#f9fbfc;
		border-radius: 6px;
		margin-top: 1.5rem;
			
		label {
			display:inline-block;
			color:${props => props.theme.purple}
			text-transform: capitalize;
			font-family:medium;
			line-height:3rem;
			font-size:1rem;
			width: 8rem;
			float:left;
			text-align:center;
		}
	}

	.table-diseases {
		>div {
			height:3.5rem;
			line-height:3.5rem;
			border-bottom:solid 1px ${props => props.theme.border};
			position:relative;

			label {
				color:${props => props.theme.copyLight}
				line-height:3.5rem;
				position:absolute;
				left:0;
				top:0;
			}
				
			.container-check {
				width: 8rem;
				float:left;
				height:100%;
				margin:0 auto;
				display:inline-block;
				position:relative;

				.check {
					${props => props.theme.absoluteCenter()}
                }
                
                >label {
                    display:none;
                    pointer-events:none;
                }
			}
		}
	}

	.note {
		font-size: 0.85rem;
		color: ${props => props.theme.copyLighter};
		line-height: 1.1rem;
		margin-top: 1.4rem;

		span {
			color: ${props => props.theme.copyLight};
		}
	}

	.buttons {
		margin-top: 1.5rem;

		button {
			margin-top:0;
			float:left;
			width:calc(50% - 0.7rem);

			&:not(.disable) {
				&:last-child {
					box-shadow: 0 4px 6px 0 rgba(0, 255, 200, 0.2);
				}
			}

			&.disable {
				&:last-child {
					box-shadow: 0px 0px 12px -6px rgba(134,134,134,0.75);
					font-family:regular;
				}
			}

			&:last-child {
				float:right;
			}

			&:first-child {
				border:solid 1px rgba(82, 103, 255, 0.5);
				box-shadow: 0 1px 2px 0 rgba(82, 103, 255, 0.2);
			}
		}
    }
    
    @media (max-width: 700px) {

        .container-modal {
            position:relative;
            top: 25px;
            transform:none;
            width:calc(100% - 30px);
            margin-bottom:50px;
            float:left;
            margin-left:15px;
        }
        
        h4 {
            font-size:16px !important;
            line-height:28px;
            padding-bottom:12px;
        }

        .close {
            width: 14px !important;
            top: 12px !important; 
        }

        .head-members {
            display:none;
        }

        .table-diseases {
            float:left;
            width:100%;

            .disease {
                border-bottom:none;
                height:auto;
                width:100%;
                float:left;
                padding-bottom: 16px;

                &::before {
                    content:'';
                    position:absolute;
                    left:-1.5rem;
                    top:0;
                    width:calc(100% + 3rem);
                    height:1px;
                    background:${props => props.theme.border};
                }

                >label {
                    display:block;
                    width:100%;
                    position:relative;
                    font-size:14px;
                    padding-bottom: 2px;
                    padding-top: 7px;
                }

                .container-check {
                    height: 30px;
                    margin:0 !important;
                    width:50% !important;
                    display:block;

                    .check {
                        right: auto;
                    }

                    >label {
                        display:block;
                        line-height:30px;
                        text-transform:capitalize;
                        color:${props => props.theme.tooltip};
                        font-size:12px;
                        margin-left: 2.6rem;
                        white-space: nowrap;
                    }
                }

                &:last-child {
                    &::after {
                        content:'';
                        position:absolute;
                        left:-1.5rem;
                        bottom:0;
                        width:calc(100% + 3rem);
                        height:1px;
                        background:${props => props.theme.border};
                    }
                }
            }
        }

        .note {
            font-size:12px;
            line-height:18px;
            float:left;
            width:100%;
            margin-top:16px;
        }

        .buttons {
            width:100%;
            float:left;

            button {
                width:100%;

                &:first-child {
                    font-family:regular;
                    margin-bottom: 15px;

                    .back {
                        display:none;
                    }
                }

                &:last-child {
                    margin-bottom: 6px;
                }
            }
        }

        .ps__rail-x {
            display:none;
        }

        .ps__rail-y {
            right:0;
            left:auto !important;
        }
    }
`

class ModalSelectCity extends Component {
    static defaultProps = {
        cities: [[110001, 'Delhi', 'Delhi'], [400001, 'Maharashtra', 'Mumbai'], [560002, 'Karnataka', 'Bangalore'], [122001, 'NCR', 'Gurgaon'], [500001, 'Telangana', 'Hyderabad'], [411001, 'Maharashtra', 'Pune'], [600001, 'Tamilnadu', 'Chennai']]
    }

    state = { cities: [], search: '' }

    componentDidMount() {
        this.getCities()
    }

    componentDidUpdate(_props, _state, snapshot) {
        this.state.search != _state.search && this.handleSearch()
    }

    componentWillReceiveProps(_props) {
        !isEqual(this.props.allCities, _props.allCities) && this.getCities(undefined, _props)
    }

    getCities = (city, props = this.props) => {
        let defaultCities = props.cities.map(city => city[2])
        let allCities = props.allCities.filter(city => !defaultCities.includes(city[2]))
        if (!city) {
            this.setState({ cities: [...props.cities, ...allCities] })
        } else {
            let cities = [...props.cities, ...allCities].filter(_city => {
                let match = _city[2].slice(0, city.length).toLowerCase()
                return match === city.toLowerCase()
            })
            this.setState({ cities })
        }
    }

    handleSearch = event => (this.state.search ? this.getCities(this.state.search) : this.getCities())

    onClose = () => {
        this.props.onClose()
        this.setState({ search: '' })
    }

    render() {
        return (
            <_ModalSelectCity head="Select parent’s city" active={this.props.active} onClose={() => this.onClose()}>
                <div className="search">
                    <img src={search} />
                    <input placeholder="Search city by name" value={this.state.search} onInput={event => this.setState({ search: event.currentTarget.value })} />
                </div>
                <div className="cities">
                    <PerfectScrollbar option={{ minScrollbarLength: 20 }}>
                        {this.state.cities.map(city => (
                            <label key={city[2] + city[1]} className="city" onClick={this.props.selectCity(city)}>
                                <span>{city[2]} ({city[1]})</span>
                                <img src={chevron} />
                            </label>
                        ))}
                    </PerfectScrollbar>
                </div>
                <div className="note">City help us find plans with best local hospital coverage.</div>
            </_ModalSelectCity>
        )
    }
}

class ModalSelectDiseases extends Component {
    getWidth = position => {
        let members = this.props.members.length

        return {
            width: `calc((100% - ${members > 4 ? '10rem' : '20rem'})/${members})`,
            marginLeft: position == 0 ? (members > 4 ? '10rem' : '20rem') : '0'
        }
    }

    checkIfSelected = (member, disease) => {
        let { type, id } = member
        if (type != 'child') {
            return this.props.selection[type].includes(disease)
        } else {
            let child = this.props.selection.children[id]
            return child && child.includes(disease)
        }
    }

    ageIfChildren = member => {
        let children = this.props.members.filter(member => member.type == 'child').length > 1
        if (member.type == 'child' && children) return `(${member.age.value || '3 - 12 Mo.'})`
        else return ''
    }

    render() {
        return (
            <_ModalSelectDiseases head="Select applicable diseases" active={this.props.active} onClose={this.props.onClose}>
                <div className="head-members" ref={head => (this.head = head)}>
                    {this.props.members.map((member, index) => (
                        <label key={member.name + member.id} style={this.getWidth(index)}>
                            {member.name} {this.ageIfChildren(member)}
                        </label>
                    ))}
                </div>
                <div className="table-diseases">
                    {this.props.diseases.map(disease => (
                        <div key={disease} className="disease">
                            <label>{disease}</label>
                            {this.props.members.map((member, index) => (
                                <div className="container-check" style={this.getWidth(index)} key={member.name + member.id}>
                                    <Checkbox checked={this.checkIfSelected(member, disease)} toggleCheck={this.props.toggleDisease({ type: member.type, disease, id: member.id })} />
                                    <label>{member.name} {member.type == 'child' && `(${member.age.label})`}</label>
                                </div>
                            ))}
                        </div>
                    ))}
                </div>
                <div className="note">
                    <span>Why we need this information?</span> This helps us find plans that with best features for pre existing conditions. All health information is private and only used to generate better recommendations.
                </div>
                <div className="buttons">
                    <Button className={`continue`} purple big back outline label="No Diseases To Declare" onClick={this.props.noDiseasesToDeclare} />
                    <NavLink to="/health/getquotes">
                        <Button className={`continue ${!this.props.enableGetQuotes && 'disable error'}`} purple medium big continue label="Get Quotes" error={"Please select a disease, or click 'No Diseases To Declare'"} />
                    </NavLink>
                </div>
            </_ModalSelectDiseases>
        )
    }
}

const Question = props => (
    <_Question {...props}>
        <div className={`container-question`}>
            <label>{props.question}</label>
            <span>{props.desc}</span>
            {props.type == 'selectCity' &&
                props.selection && (
                    <div className="city">
                        <label>{props.selection[2]}</label>
                        &nbsp;&nbsp;
                        <span onClick={props.changeCity}>(change)</span>
                    </div>
                )}
        </div>
        <YesNo selected={props.selected} handleClick={props.toggleYesNo} />
    </_Question>
)

class Details extends Component {
    state = { selectCity: false, selectDiseases: false }

    componentWillMount() {
        !this.props.errContinue.continue.household && (window.location = '/health/household')
    }

    componentDidMount() {
        this.initQuestions()

        this.props.toggleContinue({
            stage: 'details',
            enable: true
        })

        this.props.details.selectDiseases.selected && this.props.editQuestion({
            type: 'selectDiseases',
            selected: this.isAnyDiseasesSelected()
        })
    }

    componentWillReceiveProps(_props) {
        if (!isEqual(_props.details.selectDiseases,this.props.details.selectDiseases) || !isEqual(_props.details.selectCity,this.props.details.selectCity)) {
            this.props.resetProposerForm()
			this.props.resetNomineeForm()
			this.props.resetMembersForm()
			this.props.resetAddressForm()
			this.props.resetMedicalHistoryForm()
        }
    }

    componentDidUpdate(_props, _state) {
        if (!isEqual(_props.details.selectDiseases.selection, this.props.details.selectDiseases.selection)) {
            !this.isAnyDiseasesSelected() &&
                this.props.editQuestion({
                    type: 'selectDiseases',
                    selected: false
                })
        }
    }

    selectCity = city => event => {
        if (!city) return

        this.props.editQuestion({
            type: 'selectCity',
            selection: city
        })

        this.setState({ selectCity: false })
    }

    isCitySelected = (props = this.props) => {
        let { selected, selection } = props.details.selectCity
        selected &&
            !selection &&
            this.props.editQuestion({
                type: 'selectCity',
                selected: false
            })
        this.setState({ selectCity: false })
    }

    initQuestions = () => {
        
        this.props.editQuestion({
            type: 'selectCity',
            init: !isEmpty(this.orderMembers().filter(member => ['father', 'mother'].includes(member.type)))
        })

        this.props.getCities()

        this.props.editQuestion({
            type: 'selectDiseases',
            init: true
        })
    }

    getHead = () => {
        let members = this.orderMembers()
        
        const punctuate = index => {
            if (index == members.length - 1) return ''
            if (index == members.length - 2) return <span> &amp; </span>
            return ', '
        }

        let text = members.map((member, index) => {
            return (
                <span key={member.name + index}>
                    {member.name}({member.age.value || '3 - 12 Mo.'})<label>{punctuate(index)}</label>
                </span>
            )
        })

        return (
            <Fragment>
                Insuring <label className="active">{text}</label>
            </Fragment>
        )
    }

    orderMembers = (props = this.props) => {
        let members = [...values(omit(props.household, 'children')), ...props.household.children].filter(member => member.selected && member.age)
        let userPosition = members.findIndex(member => member.name == 'you')
        let user = members.splice(userPosition, 1)[0]
        members.unshift(user)
        return members
    }

    toggleYesNo = type => selected => {
        this.props.editQuestion({
            type,
            selected
        })

        if (type == 'selectCity') {
            let { selection } = this.props.details.selectCity
            this.setState({ selectCity: selected && !selection })
        }

        if (type == 'selectDiseases') {
            this.setState({ selectDiseases: selected })
        }
    }

    isAnyDiseasesSelected = () => {
        let selected = this.orderMembers().reduce(
            (members, member) => {
                !member.id && members.adults.push(member.type)
                member.id && members.children.push(member.id)
                return members
            },
            { adults: [], children: [] }
        )

        let adults = pick(omit(this.props.details.selectDiseases.selection, 'children'), selected.adults)
        let children = pick(this.props.details.selectDiseases.selection.children, selected.children)

        let anyAdultSelected = values(adults).filter(diseases => diseases.length > 0).length > 0

        let anyChildSelected = values(children).filter(diseases => diseases.length > 0).length > 0

        return anyAdultSelected || anyChildSelected
    }

    toggleDisease = member => () => {
        let { type, disease, id } = member

        type != 'child' && this.props.toggleDisease({ type, disease })
        type == 'child' && this.props.toggleDiseaseForChild({ id, disease })
    }

    noDiseasesToDeclare = () => {
        this.setState({ selectDiseases: false })
        this.props.editQuestion({
            type: 'selectDiseases',
            selected: false
        })
    }

    render() {
        return (
            <_Details>
                <HeaderDesc details head={this.getHead()} />

                <div className="container-questions">
                    {
                        values(this.props.details).map(question => 
                            question.init && 
                            <Question key={question.label + question.desc} question={question.label} 
                                className={`question ${question.type} ${question.selected && question.selection && 'selected'}`} 
                                desc={question.desc} selected={question.selected} toggleYesNo={this.toggleYesNo(question.type)} 
                                selection={question.selection} type={question.type} 
                                changeCity={() => this.setState({ selectCity: true })} 
                            />)
                    }
                </div>

                {this.props.details.selectCity.init && <ModalSelectCity allCities={this.props.details.selectCity.cities} onClose={this.isCitySelected} selectCity={this.selectCity} active={this.state.selectCity} />}

                <ModalSelectDiseases diseases={this.props.details.selectDiseases.diseases} members={this.orderMembers()} selection={this.props.details.selectDiseases.selection} toggleDisease={this.toggleDisease} active={this.state.selectDiseases} onClose={() => this.setState({ selectDiseases: false })} enableGetQuotes={this.isAnyDiseasesSelected()} noDiseasesToDeclare={this.noDiseasesToDeclare} />
                <NavLink to="/health/getquotes">
                    <Button className={`continue`} big continue label="Continue" />
                </NavLink>
            </_Details>
        )
    }
}

const mapStateToProps = state => {
    let { household, errContinue, details } = state
    return { household, errContinue, details }
}

const mapDispatchToProps = dispatch => {
    return {
        editQuestion: question => dispatch(editQuestion(question)),
        getCities: city => dispatch(getCities(city)),
        toggleDisease: member => dispatch(toggleDisease(member)),
        toggleDiseaseForChild: child => dispatch(toggleDiseaseForChild(child)),
        toggleContinue: _continue => dispatch(toggleContinue(_continue)),
        resetProposerForm: () => dispatch(destroy('ProposerForm')),
		resetNomineeForm: () => dispatch(destroy('NomineeForm')),
		resetMembersForm: () => dispatch(destroy('MembersForm')),
		resetAddressForm: () => dispatch(destroy('AddressForm')),
		resetMedicalHistoryForm: () => dispatch(destroy('MedicalHistoryForm')),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Details)
