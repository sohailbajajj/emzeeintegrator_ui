import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { values, omit, isEqual } from 'lodash'
import shortid from 'shortid'

import { generateQuotesRequest } from '../utils/helpers'

import { RequirementsSection } from '../components/section'
import HeaderDesc from '../components/headerDesc'
import Button from '../components/button'
import Modal from '../components/modal'
import Quotes from '../components/quotes/quotes'
import Spinner from '../components/spinner'

import { initUser, getInsurersList } from '../actions/quotes'
import { toggleContinue } from '../actions/errContinue'
import { editFilter, toggleGroup, toggleCombo, getQuotes, resetQuotes } from '../actions/quotes'

import { notName as notString, notEmail, notMobile } from '../utils/validations'

const _GetQuotes = RequirementsSection.extend`
    padding-bottom: 4rem !important;

    .quotes-list {
        margin-bottom: 4rem !important;
    }

    .note {
        text-align: center;
        color: ${props => props.theme.copyLighter};
        font-size: 0.9rem;
    }

    button.continue {
        margin-top: 1.5rem;
        height: 4rem;
        width: 37rem;
        font-size: 1.55rem;

        label {
            line-height: 4rem;
        }
    }

    .loader {
        text-align: center;
        margin: 8rem 0;
        font-size: 3rem;
        color: ${props => props.theme.blue};
    }

    @media (max-width: 650px) {
        .wrapper {
            >button.continue {
                width:100%;
                max-width:100%;
                position:fixed;
                z-index: 2;
                bottom:0;
                left:0;
                margin:0;
                border-radius:0;
            }
    
            >.note {
                font-size: 13px;
                line-height: 19px;
                position: fixed;
                bottom: 4rem;
                left: 0;
                margin: 0;
                padding: 1rem;
                width:100%;
                z-index: 2;

                &::after {
                    content:'';
                    background: #fcfdfd;
                    opacity:0.85;
                    position:absolute;
                    z-index:-1;
                    left:0;
                    top:0;
                    width:100%;
                    height:100%;
                }
            }
        }
    }
`

const _ModalGetQuotes = styled(Modal)`
    .container-modal {
        width: 27rem;

        h4 {
            color: ${props => props.theme.purple};
            font-family: regular;
            font-size: 1.25rem;
            line-height: 1.8rem;
            margin-top: 1.1rem;
        }

        > img {
            padding: 0.3rem;
            padding-right: 0.2rem;
        }

        .note {
            font-size: 0.8rem;
            color: ${props => props.theme.copyLighter};
            line-height: 1.1rem;
            margin-top: 1rem;
            text-align: left;

            a {
                color: ${props => props.theme.copyLight};
                cursor: pointer;
            }

            &.first {
                margin-bottom: 0.7rem;
            }
        }

        .container-input {
            position: relative;
            height:5rem;

            input {
                width: 100%;
                border: none;
                outline: none;
                border-bottom: solid 1px ${props => props.theme.border};
                height: calc(2rem + 2px);
                padding:0;
                border-radius:0;
                padding-top: 2.5rem;
                font-family: regular;
                font-size: 1.1rem;
                color: ${props => props.theme.tooltip};
                transition: all 100ms ease;

                &::placeholder {
                    color: ${props => props.theme.copyLighter};
                    font-family: regular;
                    font-size: 1.1rem;
                }

                &:focus {
                    border-color: ${props => props.theme.blue} !important;

                    & + span {
                        opacity: 0;
                    }

                    & + span + label {
                        top: 1.5rem;
                        color: ${props => props.theme.copyLight};
                        font-size: 0.8rem;
                    }
                }
            }

            span {
                position: absolute;
                color: ${props => props.theme.red};
                font-size: 0.8rem;
                left: 0;
                bottom: -1.04rem;
                z-index: 1;
                opacity: 0;
            }

            &.error {
                input {
                    border-color: ${props => props.theme.red};
                }

                span {
                    opacity: 1;
                }
            }

            label {
                position: absolute;
                left: 0;
                color: ${props => props.theme.copyLighter};
                font-family: regular;
                font-size: 1.1rem;
                pointer-events: none;
                top: 2.9rem;
                transition: all 300ms ease;
            }

            &.active {
                label {
                    top: 1.5rem;
                    color: ${props => props.theme.copyLight};
                    font-size: 0.8rem;
                }
            }
        }

        button {
            margin-top: 2.3rem;
            margin-bottom: 1.6rem;
            height: 3.5rem;
            width: 100%;
            font-size: 1.4rem;
            font-family: regular;
            box-shadow: 0 4px 6px 0 rgba(0, 255, 200, 0.2);
            position:relative;

            label.continue {
                line-height: 3.5rem;
            }

            .icon-spinner {
                ${props => props.theme.absoluteCenter()}
                opacity:0;
                transition:opacity 300ms ease;
            }

            &.loading {
                color:transparent;
                
                label.continue {
                    opacity:0;
                }

                .icon-spinner {
                    opacity:1;
                }
            }
        }
    }

    &.active {
        .underlay {
            background-color: #4b5960;
            opacity: 0.85;
        }
    }

    @media (max-width: 650px) {
        .container-modal {
            width:calc(100% - 40px);
            position:relative;
            top:25px;
            transform:none;

            h4 {
                font-size:18px;
                line-height: 23px;

                br {
                    display:none;
                }
            }

            .note {
                font-size: 11px;
                line-height:16px;

                &:first {
                    margin-bottom: 0;
                }
            }

            button {
                height: 4rem;
                font-size: 18px;

                .continue {
                    line-height: 4rem !important;
                }
            }

            .container-input {

                label {
                    font-size:16px;
                    top: 2.8rem;
                }

                input {
                    font-size:16px;
                }

                span {
                    font-size: 11px;
                    bottom: -1.2rem;
                }

                &.active {
                    label {
                        font-size:11px;
                        top: 1.4rem;
                    }
                }
            }
        }
    }

    @media (max-width: 400px) {
        .container-modal {
            width:calc(100% - 30px);
        }
    }
`

class ModalGetQuotes extends Component {
    state = {
        name: {
            error: false,
            value: ''
        },
        number: {
            error: false,
            value: ''
        },
        email: {
            error: false,
            value: ''
        },
        loading: false
    }

    componentDidMount() {
        if (this.props.userData.email) {
            this.setState({ email: { ...this.state.email, value: this.props.userData.email } })
        }

        if (this.props.userData.number) {
            this.setState({ number: { ...this.state.number, value: this.props.userData.number } })
        }

        if (this.props.userData.name) {
            this.setState({ name: { ...this.state.name, value: this.props.userData.name } })
        }
    }

    validate = input => () => {
        if (input == 'name') {
            this.setState({
                name: {
                    ...this.state.name,
                    error: notString(this.state.name.value)
                }
            })
        }

        if (input == 'number') {
            this.setState({
                number: {
                    ...this.state.number,
                    error: notMobile(this.state.number.value)
                }
            })
        }

        if (input == 'email') {
            this.setState({
                email: {
                    ...this.state.email,
                    error: notEmail(this.state.email.value)
                }
            })
        }

        if (!input) {
            let name = notString(this.state.name.value)
            let number = notMobile(this.state.number.value)
            let email = notEmail(this.state.email.value)

            this.setState({
                name: {
                    ...this.state.name,
                    error: name
                },
                number: {
                    ...this.state.number,
                    error: number
                },
                email: {
                    ...this.state.email,
                    error: email
                }
            })

            return !name && !number && !email
        }
    }

    handleInput = (event, type) => {
        this.setState({
            [type]: {
                ...this.state[type],
                value: event.currentTarget.value
            }
        })
    }

    viewQuotes = () => {
        if (this.validate()()) {
            this.setState({ loading: true })
            sessionStorage.setItem('email', this.state.email.value)
            sessionStorage.setItem('number', this.state.number.value)
            setTimeout(() => {
                this.props.initUser({
                    name: this.state.name.value,
                    number: this.state.number.value,
                    email: this.state.email.value,
                    init: false
                })
                this.props.history.push('/health/quotes')
            },500)
        }
    }

    render() {
        let { name, number, email } = this.state

        return (
            <_ModalGetQuotes
                head={
                    <Fragment>
                        Please add details to compare, review <br />&amp; access all quotes for free.
                    </Fragment>
                }
                active={this.props.active}
                onClose={this.props.onClose}
            >
                <div className="note first">
                    Your details are private and we will only reach out to you when you send a request. View our <a>no spam policy</a>.
                </div>

                <div className={`container-input ${name.error && 'error'} ${name.value && 'active'}`}>
                    <input value={name.value} onBlur={this.validate('name')} onInput={e => this.handleInput(e, 'name')} />
                    <span>Please enter a valid Name</span>
                    <label>Your Name</label>
                </div>

                <div className={`container-input ${number.error && 'error'} ${number.value && 'active'}`}>
                    <input value={number.value} onBlur={this.validate('number')} onInput={e => this.handleInput(e, 'number')} />
                    <span>Please enter a valid, 10 digit Mobile Number</span>
                    <label>Mobile Number</label>
                </div>

                <div className={`container-input ${email.error && 'error'} ${email.value && 'active'}`}>
                    <input value={email.value} onBlur={this.validate('email')} onInput={e => this.handleInput(e, 'email')} />
                    <span>Please enter a valid Email ID</span>
                    <label>Email ID</label>
                </div>

                <Button className={`continue ${this.state.loading && 'loading'}`} purple continue label="View All Quotes" onClick={this.viewQuotes}>
                    <Spinner />
                </Button>

                <div className="note">
                    By clicking on ‘View All Quotes’, you accept our <a>Terms &amp; Conditions</a>. This might override your DNDC registration.
                </div>
            </_ModalGetQuotes>
        )
    }
}

class GetQuotes extends Component {
    state = { showModal: false }

    componentDidMount() {
        this.props.toggleContinue({
            stage: 'getquotes',
            enable: true
        })

        this.props.quotes.filters.insurers.list && 
        !this.props.quotes.filters.insurers.list[0] && 
        this.props.getInsurersList()

        //Always select the first combo of the first group & fetch quotes for it, if none present
        if (this.props.quotes.filters.members.groups.length > 0) {
            let groups = this.props.quotes.filters.members.groups
            this.props.toggleGroup(groups[0].id)

            this.props.toggleCombo({
                id: groups[0].id,
                combo: groups[0].combos[0].id,
                init: true
            })

            let defaultSelection = this.props.quotes.quotes[groups[0].id] && this.props.quotes.quotes[groups[0].id][groups[0].combos[0].id]
            if (!defaultSelection || defaultSelection.length == 0) {
                if (isEqual(this.props.diseases, this.props.quotes.filters.members.diseases)) {
                    this.getQuotes()
                }
            }
        }

        //If selected members changed or selected diseases changed

        //This condition will always be true the first time quotes are being loaded
        if (!isEqual(this.orderMembers(), this.props.quotes.filters.members.members)) {
            this.updateDiseases()
            this.updateParentsLocation()
            this.makeGroupsAndGetQuotes()
        } 
        else if (!isEqual(this.props.diseases, this.props.quotes.filters.members.diseases)) {
            this.updateDiseases()
            this.getQuotes()
        }

        //Update parents' location, check if changed
        if (!isEqual(this.props.parentsCity, this.props.quotes.filters.members.parentsLocation)) {
            this.updateParentsLocation()
            this.resetParentsQuotes()
        }
    }

    resetParentsQuotes = () => {
        let parents = this.props.quotes.filters.members.groups.find(group =>
            group.name == 'parents' || group.name == 'father' || group.name == 'mother'
        )
        parents && this.props.resetQuotes(parents.id)
    }

    updateParentsLocation = () => {
        this.props.editFilter({
            type: 'members',
            parentsLocation: this.props.parentsCity
        })
    }

    updateDiseases = () => {
        let { members, anySelected } = this.props.diseases
        this.props.editFilter({
            type: 'members',
            diseases: {
                anySelected,
                members
            }
        })
    }

    makeGroupsAndGetQuotes = () => {
        let groups = this.getMemberCombos(this.nameGroups(this.getGroups()))

        this.props.editFilter({
            type: 'members',
            members: this.orderMembers(),
            groups
        })

        this.props.toggleGroup(groups[0].id)

        this.props.toggleCombo({
            id: groups[0].id,
            combo: groups[0].combos[0].id,
            init: true
        })

        if (groups[1]) {
            this.props.toggleCombo({
                id: groups[1].id,
                combo: groups[1].combos[0].id,
                init: true
            })
        }

        this.getQuotes(groups)
    }

    getQuotes = (groups = this.props.quotes.filters.members.groups) => {
        let diseases = !this.props.diseases.anySelected ? this.props.diseases.anySelected : this.props.diseases.members

        this.props.getQuotes({
            group: groups[0].id,
            combo: groups[0].combos[0].id,
            groups: groups.map(group => group.id),
            request: JSON.stringify(generateQuotesRequest(false, groups[0].combos[0].members, diseases, this.getPincode(groups[0])))
        })

        // if (groups[1]) {
        //     this.props.getQuotes({
        //         group: groups[1].id,
        //         combo: groups[1].combos[0].id,
        //         groups: groups.map(group => group.id),
        //         request: JSON.stringify(generateQuotesRequest(false, groups[1].combos[0].members, diseases, this.getPincode(groups[1])))
        //     })
        // }
    }

    getPincode = selectedGroup => {
        let isParents = selectedGroup.name == 'parents' || selectedGroup.name == 'father' || selectedGroup.name == 'mother'
        if (isParents && this.props.parentsCity) return this.props.parentsCity[0]
        else return JSON.parse(sessionStorage.getItem('city')).pincode
    }

    orderMembers = (props = this.props) => {
        let members = [...values(omit(props.household, 'children')), ...props.household.children].filter(member => member.selected && member.age)
        let userPosition = members.findIndex(member => member.name == 'you')
        let user = members.splice(userPosition, 1)[0]
        members.unshift(user)
        return members
    }

    getGroups = () => {
        let members = this.orderMembers()
        let children = members.filter(member => member.name != 'father' && member.name != 'mother')
        let parents = members.filter(member => member.name == 'father' || member.name == 'mother')

        let groups = [{ members: children, type: 'children' }, { members: parents, type: 'parents' }].filter(group => group.members.length > 0)

        return groups
    }

    nameGroups = groups => {
        const getNames = group => {
            let adults = group.members.filter(member => member.type != 'child')
            let children = group.members.filter(member => member.type == 'child')
            let name = ''

            adults.length > 0 &&
                adults.forEach((adult, index) => {
                    if (index == 0) name += `${adult.name}`
                    if (index != 0 && index == adults.length - 1 && children.length == 0) name += ` & ${adult.name}`
                    else if (index != 0) name += `, ${adult.name}`
                })

            children.length > 1 && (name += ` & kids`)
            children.length == 1 && (name += ` & kid`)

            adults.filter(member => member.name == 'father' || member.name == 'mother').length == 2 && (name = `parents`)

            return name
        }

        return groups.map(group => {
            return { ...group, name: getNames(group), id: shortid.generate() }
        })
    }

    getMemberCombos = groups => {
        const getCombos = group => {
            let adults = group.members.filter(member => member.type != 'child')
            let children = group.members.filter(member => member.type == 'child')
            let combos = []

            let allAdults = adults.reduce(
                (all, adult, index) => {
                    adults.length > 1 && combos.push({ name: adult.name, members: [adult], id: shortid.generate() })
                    all.members.push(adult)
                    if (index == 0) all.name += `${adult.name}`
                    if (index != 0 && index == adults.length - 1 && children.length == 0) all.name += ` & ${adult.name}`
                    else if (index != 0) all.name += `, ${adult.name}`
                    return all
                },
                { name: '', members: [], id: shortid.generate() }
            )

            combos = [allAdults, ...combos]

            if (children.length > 0) {
                let withKids = combos.map(combo => {
                    return {
                        name: `${combo.name} & kid${children.length == 1 ? '' : 's'}`,
                        members: [...combo.members, ...children],
                        id: shortid.generate()
                    }
                })

                combos = [...withKids, ...combos.slice(1)]
            }

            return combos
        }

        return groups.map(group => {
            return { ...group, combos: getCombos(group) }
        })
    }

    getHeaderDesc = () => {
        const punctuate = (index, members) => {
            if (index == members.length - 1) return ''
            if (index == members.length - 2) return <span> &amp; </span>
            return ', '
        }

        let cities = {
            children: (sessionStorage.getItem('city') && JSON.parse(sessionStorage.getItem('city')).city) || '',
            parents: (this.props.parentsCity && this.props.parentsCity[2]) || ''
        }

        let text = this.getGroups().map((group, _index) => (
            <label className={_index == 0 ? 'active' : ''} key={group.type+_index}>
                {_index > 0 && <span className="breaker">&nbsp;&amp;&nbsp;</span>}
                {group.members.map((member, index) => (
                    <span key={member.name + index}>
                        {member.name}({member.age.value || '3 - 12 Mo.'})<label>{punctuate(index, group.members)}</label>
                    </span>
                ))}
                <span className='city'> in {cities[group.type]|| cities.children}</span>
            </label>
        ))

        return <Fragment>Insuring {text}.</Fragment>
    }

    toggleModal = () => {
        this.setState({ showModal: !this.state.showModal })
        // if (!this.props.quotes.user.name) {
        //     this.setState({ showModal: !this.state.showModal })
        // } else {
        //     this.props.history.push('/health/quotes')
        // }
    }

    formatSum = (sum, isMobile = false) => {
        if (sum >= 10000000) sum = `${sum / 10000000} Crore cover`
        else if (sum >= 100000) sum = `${sum / 100000} Lacs cover`
        else if (sum >= 1000) sum = `${sum / 1000} K cover`
        if (isMobile) {
            return (
                <label>
                    {sum.split(' ')[0]}
                    <br />
                    {sum.split(' ')[1]}
                </label>
            )
        }
        return {
            label: sum,
            value: this.props.sumInsured
        }
    }

    transformPremium = premium => {
        let years = Object.keys(premium)
        let labels = ['First Year', 'Second Year', 'Third Year']
        let selected_keys = ['firstYear', 'secondYear', 'thirdYear'];
        return years.map((year, index) => {
            if(year == 'selected') {
                return
            }
            let discounted = premium[year].discount && premium[year].discount.totalPremium
            //let _premium = parseInt(discounted || premium[year].totalPremium).toLocaleString('en-IN')
            let _premium = parseInt(Math.round(discounted || premium[year].totalPremium)).toLocaleString('en-IN')
            return {
                label: labels[index],
                value: _premium,
                selected: selected_keys[index]
            }
        })
    }

    render() {
        let { filters: { members: { selected: { group, combo } }}, quotes } = this.props.quotes
        let quotesList = quotes[group] && combo[group] && quotes[group][combo[group]]

        return (
            <_GetQuotes>
                <HeaderDesc quotes desc={this.getHeaderDesc()} 
                    head={`Top Recommendations${!this.props.isMobile ? ' for your requirements' : ''}`} 
                />

                {!quotesList && <h6 className="loader">Loading...</h6>}

                {quotesList && (
                    <Fragment>
                        <Quotes isGetQuotes onSelect={this.toggleModal} formatSum={this.formatSum}
                            isMobile={this.props.isMobile} transformPremium={this.transformPremium} 
                            products={window.innerWidth > 1021 ? quotesList.slice(0, 3) : quotesList.slice(0, 2)} 
                        />
                        
                        <div className="note">View all available options. Compare &amp; enroll easily online with no additional charges.</div>
                        
                        <Button className={`continue`} big continue label="View all Quotes" onClick={this.toggleModal} />
                    </Fragment>
                )}
                <ModalGetQuotes active={this.state.showModal} onClose={() => this.setState({ showModal: false })} 
                    history={this.props.history} initUser={this.props.initUser} 
                    userData={this.props.quotes.user}
                />
            </_GetQuotes>
        )
    }
}

const mapStateToProps = state => {
    let {
        household,
        details: { selectCity, selectDiseases },
        quotes
    } = state

    return {
        household,
        quotes,
        parentsCity: selectCity.selected && selectCity.selection,
        diseases: {
            anySelected: selectDiseases.selected,
            members: selectDiseases.selection
        }
    }
}

const mapDispatchToProps = dispatch => {
    return {
        initUser: user => dispatch(initUser(user)),
        toggleContinue: _continue => dispatch(toggleContinue(_continue)),
        getInsurersList: () => dispatch(getInsurersList()),
        editFilter: filter => dispatch(editFilter(filter)),
        toggleGroup: group => dispatch(toggleGroup(group)),
        toggleCombo: combo => dispatch(toggleCombo(combo)),
        getQuotes: req => dispatch(getQuotes(req)),
        resetQuotes: group => dispatch(resetQuotes(group))
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GetQuotes)
