import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import shortid from 'shortid'
import { values, omit, isEqual, isEmpty } from 'lodash'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { NavLink } from 'react-router-dom'
import { destroy } from 'redux-form'

import { addMember, editMember, addChild, editChild, removeChild } from '../actions/household'
import { addError, removeError, toggleContinue } from '../actions/errContinue'

import { stopBubbling } from '../utils/helpers'

import HeaderDesc from '../components/headerDesc'
import { RequirementsSection } from '../components/section'
import Checkbox from '../components/checkbox'
import Select from '../components/select'
import Button from '../components/button'

const _Household = RequirementsSection.extend``

const _Close = styled.div`
	width: 1.8rem;
	height: 1.8rem;
	border-radius: 8px;
	background-color: #ffffff;
	border: solid 1px rgba(255, 191, 191, 0.5);
	text-align:center;
	line-height:1.9rem;
	color:${props => props.theme.red};
	box-shadow: ${props => props.theme.shadow};
	cursor:pointer;
`

const _Member = styled.div`
	width:100%;
	position:relative;
	height:3.5rem;
	border: solid 1px #eaf2f5;
	background:#FFF;
	border-radius: 5px;
	margin: 0.8rem 0;
	user-select:none;
	cursor:pointer;

	.check {
		left:1.3rem;
	}

	h6 {
		display: inline-block;
		line-height: 3.5rem;
		text-transform: capitalize;
		margin-left: 4.3rem;
		font-family: medium;
		font-size: 1rem;
		color:${props => props.theme.tooltip};
	}

	.select {
		${props => props.theme.verticalCenter()};
		left: 50%;
		z-index: 1;
	}

	.close {
		${props => props.theme.verticalCenter()}
		right: 1.2rem;
		opacity:0;
		transition: all 100ms ease;
	}

	:hover {
		.close {
			opacity:1;
		}
	}

	@media (max-width: 1200px) {
		height:48px;

		h6 {
			font-size:14px;
			line-height:48px;
		}

		.select {
			height:48px;

			label {
				line-height:48px !important;
			}
		}
	}

	@media (max-width: 650px) {
		.close {
			opacity:1;
			color:${props => props.theme.copyLighter}
			border:none;
			box-shadow:none;
			font-size: 17px;
			right: 10px;
		}

		.check {
			left: 14px;
		}

		h6 {
			margin-left: 47px;
		}

		.select {
			.pointer {
				right: 0.5rem;
			}
		}
	}
`

const _MembersList = styled.div.attrs({ className: 'members-list' }) `
	height: ${props => `${props.children.length * 4.44}rem`}
	width:40rem;
	transition: height 300ms ease-out;
	margin:0 auto;
	margin-top:2rem;
	
	@media (max-width: 650px) {
		width:100%;
	}

	@media (max-width: 1200px) {
		height: ${props => `${props.children.length * 4.94}rem`}
	}
`

const AddMembers = styled.div`
	display:table;
	margin:0 auto;
	margin-top: 0.5rem;

	.select:nth-child(2) {
		margin-left:1rem;
	}
	
	@media (max-width: 1200px) {
		margin-top: 10px;
	}

	@media (max-width: 380px) {
		width:100%;

		.select {
			width: calc(50% - 0.5rem);
		}
	}
`

const Close = props => (
	<_Close className="close" onClick={props.onClick}>&#x2715;</_Close>
)

class Member extends Component {
	componentDidMount() {
		window.addEventListener('click', this.checkAge)
	}

	componentWillUnmount() {
		window.removeEventListener('click', this.checkAge)
	}

	checkAge = () => {
		if (this.props.selected && !this.props.age) {
			this.props.toggleCheck()
		}
	}

	handleClick = event => {
		stopBubbling(event)
		this.props.toggleCheck()
	}

	handleRemove = event => {
		stopBubbling(event)
		this.props.remove(this.props.member)
	}

	render() {
		return (
			<_Member className="member" onClick={this.handleClick}>
				<Checkbox checked={this.props.member.selected} toggleCheck={this.props.toggleCheck} />
				<h6>{this.props.member.name}</h6>
				<Select select placeholder="Select Age" open={this.props.member.selected && !this.props.member.age}
					options={this.props.ageGen(this.props.member)} handleSelect={this.props.selectAge} value={this.props.value}
					isMobile={this.props.isMobile} className={this.props.isMobile && !this.props.member.age && 'virgin'}
				/>
				{this.props.member.name != 'you' && this.props.member.name != 'spouse' && <Close onClick={this.handleRemove} />}
			</_Member>
		)
	}
}

class MembersList extends Component {

	getValue = member => {
		let { type, id } = member

		if (type == 'child') {
			let child = this.props.members.children.filter(child => child.id == id)[0]
			if (child) return child.selected && child.age
		}
		else {
			return this.props.members[type].selected && this.props.members[type].age
		}
	}

	render() {
		return (
			<TransitionGroup component={_MembersList}>
				{
					this.props.household.map(member => {
						if (member.init) {
							return (
								<CSSTransition key={member.type + member.id} classNames="slide" timeout={{ enter: 500, exit: 300 }}>
									<Member member={member} value={this.getValue(member)} remove={this.props.remove}
										ageGen={this.props.ageGen} selectAge={this.props.selectAge(member)}
										toggleCheck={this.props.toggleCheck(member)} isMobile={this.props.isMobile}
									/>
								</CSSTransition>
							)
						}
					})
				}
			</TransitionGroup>
		)
	}
}

class Household extends Component {

	componentDidMount() {
		this.checkErrors()
		if (!isEmpty(this.props.errContinue.errors)) {
			this.props.toggleContinue({
				stage: 'household',
				enable: false
			})

			this.props.toggleContinue({
				stage: 'details',
				enable: false
			})

			this.props.toggleContinue({
				stage: 'getquotes',
				enable: false
			})
		}
		window.addEventListener('click', this.unselectWithoutAge)
	}

	componentWillUnmount() {
		window.removeEventListener('click', this.unselectWithoutAge)
	}

	componentWillReceiveProps(_props) {
		if (!isEqual(this.props.household, _props.household)) {
			this.checkErrors(_props)
		}

		if (!isEqual(this.props.errContinue.errors, _props.errContinue.errors)) {
			this.props.toggleContinue({
				stage: 'household',
				enable: isEmpty(_props.errContinue.errors)
			})
		}
		if (!isEqual(_props.household,this.props.household)) {
			this.props.resetProposerForm()
			this.props.resetNomineeForm()
			this.props.resetMembersForm()
			this.props.resetAddressForm()
			this.props.resetMedicalHistoryForm()
		}
	}

	checkErrors = (props = this.props) => {
		let members = this.orderMembers(props)

		const getMembers = _members => members.filter(member => _members.includes(member.type) && member.selected && member.age)
			.map(member => parseInt(member.age.value))

		let coupleAgeMax = Math.max(...getMembers(['husband', 'wife']))
		let coupleAgeMin = Math.min(...getMembers(['husband', 'wife']))
		let parentsAgeMax = Math.max(...getMembers(['father', 'mother']))
		let parentsAgeMin = Math.min(...getMembers(['father', 'mother']))
		let childrenAgeMax = Math.max(...getMembers(['child']))
		let childrenAgeMin = Math.min(...getMembers(['child']))

		let is = {
			anySelected: coupleAgeMax > 0 || parentsAgeMax > 0,
			coupleAndParentsSelected: parentsAgeMax > 0 && coupleAgeMax > 0,
			coupleAndChildrenSelected: coupleAgeMax > 0 && childrenAgeMax > 0,
			childrenAndNoParent: getMembers(['child']).length > 0 && getMembers(['husband', 'wife']).length <= 0
		}

		let err = {
			noneSelected: {
				label: 'Please select atleast one member to insure',
				id: 'household_1'
			},
			parentsTooYoung: {
				label: 'Your parents must be atleast 18 years older than you and your spouse.',
				id: 'household_2'
			},
			childTooOld: {
				label: 'Your children must be atleast 18 years younger than you and your spouse.',
				id: 'household_3'
			},
			iWantDada: {
				label: 'Policy for children can only be bought along with a parent.',
				id: 'household_4',
				priority: true
			}
		}

		is.coupleAndParentsSelected && parentsAgeMin - coupleAgeMax < 18 ?
			props.addError(err.parentsTooYoung) : props.removeError(err.parentsTooYoung)

		is.coupleAndChildrenSelected && coupleAgeMin - childrenAgeMax < 18 ?
			props.addError(err.childTooOld) : props.removeError(err.childTooOld)

		is.childrenAndNoParent ? props.addError(err.iWantDada) : props.removeError(err.iWantDada)

		is.anySelected ? props.removeError(err.noneSelected) : props.addError(err.noneSelected)
	}

	unselectWithoutAge = (selectedMember, props = this.props) => {
		this.orderMembers(props).forEach(member => {
			if (member.selected && !member.age) {
				if (member.type != 'child' && (!selectedMember || (member.type != selectedMember.type))) {
					this.props.editMember({
						type: member.type,
						selected: false
					})
				}
				if (member.type == 'child' && (!selectedMember || (member.id != selectedMember.id))) {
					this.props.editChild({
						id: member.id,
						selected: false
					})
				}
			}
		})
	}

	orderMembers = (props = this.props) => {
		let members = [...values(omit(props.household, 'children')), ...props.household.children]
		let userPosition = members.findIndex(member => member.name == 'you')
		let user = members.splice(userPosition, 1)[0]
		members.unshift(user)
		return members
	}

	ageGen = member => {
		let { type } = member
		let ages = []

		const generateAges = (min, max) => {
			for (let x = min; x <= max; x++) {
				let age = {
					value: x,
					label: `${x} ${x == 1 ? 'year' : 'years'}`
				}
				ages.push(age)
			}
		}

		if (type != 'child') {
			if (type == 'husband' || type == 'wife') generateAges(18, 100)
			if (type == 'father' || type == 'mother') generateAges(36, 100)
		}
		else {
			generateAges(1, 25)
			ages.unshift({ value: 0, label: '3 - 12 mo.' })
		}

		return ages
	}

	selectAge = member => age => {
		let { type, id } = member

		if (type != 'child') {
			this.props.editMember({
				type,
				age,
				init: true,
				selected: true
			})
		} else {
			this.props.editChild({
				id,
				age,
				init: true,
				selected: true
			})
		}
	}

	childFromId = id => this.props.household.children.find(child => child.id == id)

	toggleCheck = member => () => {
		//Close all other open selects
		this.unselectWithoutAge(member)

		document.querySelectorAll('.select.add').forEach(select => {
			select.classList.remove('open')
			setTimeout(() => select.classList.remove('onTop'), 300)
		})

		//Toggle Select
		if (member.type != 'child') {
			this.props.editMember({
				type: member.type,
				selected: !this.props.household[member.type].selected
			})
		} else {
			this.props.editChild({
				id: member.id,
				selected: !this.childFromId(member.id).selected
			})
		}
	}

	addMember = kind => selection => {
		let type = selection.value.toLowerCase()

		if (kind == 'parent') {
			this.props.addMember({
				name: type,
				type,
				init: true
			})
		}

		if (kind == 'child') {
			this.props.addChild({
				id: shortid.generate(),
				name: type,
				type: kind,
				init: true,
				selected: false,
				gender: type == 'son' ? 'male' : 'female',
				age: undefined,
				counttype: kind + (parseInt(this.props.household.children.length + 1)),
			})
		}
	}

	removeMember = member => {
		this.unselectWithoutAge()

		if (member.type != 'child') {
			this.props.editMember({
				type: member.type,
				init: false,
				selected: false
			})
		} else {
			this.props.removeChild({
				id: member.id
			})
		}
	}

	checkValidation = (tProps) => {
		var checkValidation = tProps.errContinue.continue.household && isEmpty(tProps.errContinue.errors) ? true : false;
		return checkValidation;
	}

	render() {
		return (
			<_Household>
				<HeaderDesc head='Who needs to be insured?' 
					desc={!this.props.isMobile ? 'Let us know who in your household needs health insurance.' : 'Select members to insure.'} 
				/>
				<MembersList household={this.orderMembers()} selectAge={this.selectAge} remove={this.removeMember}
					members={this.props.household} toggleCheck={this.toggleCheck} ageGen={this.ageGen}
					isMobile={this.props.isMobile}
				/>
				<AddMembers>
					<Select add placeholder="Add Children" handleSelect={this.addMember('child')}
						options={[{ label: 'Son', value: 'Son' }, { label: 'Daughter', value: 'Daughter' }]}
						error='Max 4 children' disable={this.props.household.children.length >= 4}
						onClick={this.unselectWithoutAge} isMobile={this.props.isMobile}
					/>
					<Select add placeholder="Add Parents" handleSelect={this.addMember('parent')}
						options={[{ label: 'Father', value: 'Father' }, { label: 'Mother', value: 'Mother' }]}
						error='Both parents added' onClick={this.unselectWithoutAge} isMobile={this.props.isMobile}
						disable={this.props.household.father.init && this.props.household.mother.init}
					/>
				</AddMembers>
				{
					<NavLink to={`${this.checkValidation(this.props) ? `/health/details` : ``}`} onClick={ e => this.checkValidation(this.props) ? true : e.preventDefault() }>
						<Button className={`continue ${(!this.props.errContinue.continue.household || !isEmpty(this.props.errContinue.errors)) && 'disable'} ${this.props.errContinue.errors.length && 'error'}`} big continue label='Continue' error={!isEmpty(this.props.errContinue.errors) && this.props.errContinue.errors[0].label} />
					</NavLink>
				}
			</_Household>
		)
	}
}

const mapStateToProps = state => {
	let { household, errContinue } = state
	return { household, errContinue }
}

const mapDispatchToProps = dispatch => {
	return {
		addMember: member => dispatch(addMember(member)),
		editMember: member => dispatch(editMember(member)),
		addChild: child => dispatch(addChild(child)),
		editChild: child => dispatch(editChild(child)),
		removeChild: child => dispatch(removeChild(child)),
		addError: err => dispatch(addError(err)),
		removeError: err => dispatch(removeError(err)),
		toggleContinue: _continue => dispatch(toggleContinue(_continue)),
		resetProposerForm: () => dispatch(destroy('ProposerForm')),
		resetNomineeForm: () => dispatch(destroy('NomineeForm')),
		resetMembersForm: () => dispatch(destroy('MembersForm')),
		resetAddressForm: () => dispatch(destroy('AddressForm')),
		resetMedicalHistoryForm: () => dispatch(destroy('MedicalHistoryForm')),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Household)