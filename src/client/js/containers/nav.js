import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import Button from '../components/button'

import { toggleGroup } from '../actions/quotes'

import call from '../../images/call.png'
import share from '../../images/share.png'
import logo from '../../images/logo.png'

const _MembersSwitch = styled.div`
	${props => props.theme.absoluteCenter()}
	width:100%;
	height:100%;
	text-align:center;
	transition:all 400ms ease;
	transition-delay:100ms;

	>div {
		display:inline-block;
		height:100%;
		height:4.5rem;
		position:relative;
		
		span {
			display:inline-block;
			margin-right:1rem;
			position:relative;
			font-family:medium;
			line-height:4.5rem;
			color:${props => props.theme.copyLighter};
			cursor:pointer;
			transition:all 100ms ease;
			user-select:none;
			text-transform: capitalize;

			&.active {
				color:${props => props.theme.blue};
			}
		}

		label {
			position:absolute;
			width:100%;
			height:2px;
			background: ${props => props.theme.blue};
			left:0;
			bottom:0;
			opacity:1;
			transition:all 200ms ease;
			${props => props.theme.smoothen}
		}
	}

	&.hide {
		transition:all 200ms ease;
		transition-delay:0;
		margin-top: 5px;
		opacity:0;
		pointer-events:none;
	}
`

class MembersSwitch extends Component {
	
	state = { hovering: undefined, index: undefined, timeout: [] }

	componentDidMount() {
		this.setActiveIndex()
	}

	componentDidUpdate(_props,_state) {
		_state.index != this.state.index && this.state.index == undefined && this.setActiveIndex()
		_state != this.state && this.slide()
	}

	setActiveIndex = () => {
		let index = this.props.groups.map((group, index) => {
			if (group.id == this.props.selected) return index
			return undefined
		}).filter(index => index != undefined)[0]
		this.setState({ index })
	}

	slide = () => {
		if (this.state.index == undefined) return
		let hoveringOver = this.container.querySelectorAll('span')[this.state.index]
		this.slider.style.width = `${hoveringOver.clientWidth}px`

		if (this.state.index == 0) {
			this.slider.style.left = `0px`
		} else {
			let sibling = hoveringOver.previousSibling
			let left = sibling.offsetWidth
			let margin = parseInt(window.getComputedStyle(sibling, null).marginRight)
			this.slider.style.left = `${left+margin}px`
		}
	}

	handleHover = (id = undefined, index = undefined ) => {
		if (id) {
			this.setState({ hovering:id, index })
			clearTimeout(this.state.timeout)
		} else {
			this.setState({
				timeout: setTimeout(() => this.setState({ hovering: undefined, index: undefined }),500)
			})
		}
	}

	render() {
		return(
			<_MembersSwitch className={`members-switch ${this.props.hide && 'hide'}`}>
				<div ref={container => this.container = container} onMouseLeave={e => this.handleHover()}>
					{
						this.props.groups.map((group, index) => (
							<span className={`${this.props.selected == group.id && 'active'} ${this.state.hovering && 
								this.state.hovering != group.id && 'hovering'}`} 
								key={group.id} onClick={() => this.props.changeGroup(group.id)} 
								onMouseEnter={e => this.handleHover(group.id,index)} 
							>
								{group.name}
							</span>
						))
					}
					<label ref={slider => this.slider = slider}/>
				</div>
			</_MembersSwitch>
		)
	}
}

const _Nav = styled.nav`
	width:100%;
	height:4.5rem;
	border-bottom:solid 2px #f2f7f9;
	box-sizing:border-box;
	${props => props.theme.media({
		padding: [`0 ${props.theme.gutter.desktopL}`,`0 ${props.theme.gutter.desktop}`,`0 20px`,`0 10px`]
	})};
	position:fixed;
	top:0;
	left:0;
	z-index: 20;
	background:#FFF;

	.wrapper {
		height:100%;
	}

	.logo {
		font-family: logo;
		width:10rem;
		height:4.5rem;
		position:relative;
		float:left;
		transition: all 300ms ease;
		position:relative;
		z-index:2;

		a {
			display:block;
			width:100%;
			height:100%;
		}

		img {
			display:block;
			height:auto;
			width:100%;
			${props => props.theme.verticalCenter()}
			left:0;
		}
	}

	.call {
		float: right;
		position:relative;
		margin-right: 2.4rem;
		opacity:1;
		transition:all 300ms ease;
		transition-delay:100ms;
		position:relative;
		z-index:2;
		${props => props.theme.smoothen}
		
		label {
			line-height: 4.5rem;
			position: relative;
			cursor: pointer;
			color: ${props => props.theme.copyLight}
		}

		a {
			line-height: 4.5rem;
			color: ${props => props.theme.purple}
		}

		img {
			${props => props.theme.verticalCenter()}
			width:1.4rem;
			right:-2.4rem;
			top: 49%;
		}

		&.hide {
			transition:all 200ms ease;
			transition-delay:0;
			margin-top: -3px;
			opacity:0;
			pointer-events:none;
		}
	}

	button {
        background: ${props => props.theme.blue};
        color:#FFF;
        font-family:regular;
        font-size:13px;
        height:2rem;
        line-height: 1.9rem;
        width:5.5rem;
		position:absolute !important;
		top: 1.15rem;
		opacity:1;
		transition:all 400ms ease;
		transition-delay:100ms;

		&.modify {
			right:0;
		}

		&.share {
			right:6.5rem;
			background:#FFF;
			border:solid 1px #eaf2f5;
			color:${props => props.theme.blue}
			text-align: left;
			text-indent: 0.5rem;
			width: 6.5rem;
			font-size: 14px;
    		line-height: 14px;
			
			img {
				${props => props.theme.verticalCenter()}
				width:0.9rem;
				right:1rem;
			}
		}

		&.hide {
			transition:all 200ms ease;
			transition-delay:0;
			opacity:0;
			top: calc(1.15rem + 5px);
			pointer-events:none;
		}
	}

	@media (max-width: 1200px) {
		.call {
			a {
				font-size:14px;
			}
		}
	}

	@media (max-width: 650px) {
		button {
			&.share {
				width:7.5rem;
			}
		}
	}
`

class Nav extends Component {
	state = {
		width: window.innerWidth
	}

	componentDidMount() {
		window.addEventListener('resize', this.setWidth)
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.setWidth)
	}

	setWidth = () => this.setState({ width: window.innerWidth})

	render() {
		return (
			<_Nav>
				<div className="wrapper">
					<div className="logo">
						<a href="/">
							<img src={logo} />
						</a>
					</div>
					<div className={`call ${!(this.props.isRequirements || this.props.isPurchase) && 'hide'}`}>
						{ !this.props.isMobile && <label>Call us on&nbsp;&nbsp;</label>}
						<a href="tel:+91-7840040042" target="_blank">{!this.props.isMobile ? '+91-7840040042' : 'Call us'}</a>
						<img src={call} />
					</div>
					{
						this.props.members.groups.length > 0 && this.props.members.groups[0].id &&
						!this.props.isRequirements && this.state.width > 1021 && !this.props.isPurchase &&
						<MembersSwitch hide={this.props.history.location.pathname != '/health/quotes'} 
							groups={this.props.members.groups} changeGroup={this.props.toggleGroup}
							selected={this.props.members.selected.group}
						/>
					}
					<Button className={`share ${!this.props.isQuotes && 'hide'}`} label='Share'>
						<img src={share} />
					</Button>

					<Button className={`modify ${!this.props.isQuotes && 'hide'}`} label='Modify' onClick={() => window.location = '/'} />
				</div>
			</_Nav>
		)
	}
}

const mapStateToProps = state => {
	let { quotes: { filters: { members } }} = state
	return { members }
}

const mapDispatchToProps = dispatch => {
	return {
		toggleGroup: group => dispatch(toggleGroup(group))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav)