import React from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'
import { CSSTransition } from 'react-transition-group'

const StyledLink = styled(NavLink)`
    cursor: pointer;
    text-transform: uppercase;
    color: ${props => props.theme.copyLighter};
`

const Nav = styled.nav`
	${props => props.theme.fullWidth()}
	background:#fafcfc;
	line-height:3rem;
	height:3rem;
	position:relative;
	width: 100%;

	a {
		font-family:medium;
		font-size:0.9rem;
		
		&.left {
			float:left;
		}
	
		&.right {
			float:right;
		}
	
		&.center {
			${props => props.theme.absoluteCenter()}
			display: block;
			width: 5rem;
			text-align: center;
		}

		&.disable {
			pointer-events:none;
		}

		&.active {
			color:${props => props.theme.blue}
			pointer-events:all !important;
		}

		&.back {
			position:absolute;
			z-index:1;
			width:3rem;
			height:3rem;
			border-radius:50%;
			background:#FFF;
			left:0;
			top: 6.2rem;
			box-shadow:${props => props.theme.shadow};
			color:${props => props.theme.purple};
			text-align:center;
			font-size: 1.4rem;
			font-family:regular;
			line-height: 2.3rem;
			width: 2.3rem;
			height: 2.3rem;
		}
	}

	@media (max-width: 1200px) {
		a {
			font-size:12px;
		}
	}
`

const getPreviousStage = path => {
    let stages = ['household', 'details', 'getquotes']
    let index = stages.indexOf(path.split('/')[2])
    if (index > 0) return stages[index - 1]
}

const RequirementsNav = props => {
	let { household, details, getquotes } = props.continue
	
    return (
        <Nav className="requirements">
            <div className="wrapper">
                <StyledLink to="/health/household" exact className={`left ${!household && 'disable'}`}>
                    Household
                </StyledLink>
                <StyledLink to="/health/details" exact className={`center ${!details && 'disable'}`}>
                    Details
                </StyledLink>
                <StyledLink to="/health/getquotes" exact className={`right ${!getquotes && 'disable'}`}>
                    Get Quotes
                </StyledLink>
                {!props.isMobile && (
                    <CSSTransition in={getPreviousStage(props.location.pathname) ? true : false} classNames="slide-switch" timeout={{ enter: 500, exit: 300 }} unmountOnExit={true}>
                        <StyledLink to={`/health/${getPreviousStage(props.location.pathname)}`} className={`back`}>
                            &larr;
                        </StyledLink>
                    </CSSTransition>
                )}
            </div>
        </Nav>
    )
}

const mapStateToProps = state => state.errContinue

const mapDispatchToProps = dispatch => {
    return {}
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RequirementsNav)
