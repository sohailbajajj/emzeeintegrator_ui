import React, { Component, Fragemnt } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import { editCart, getQuoteDetails } from '../actions/quotes'

import { RequirementsSection } from '../components/section'
import Details from '../components/quotes/details'

const _Details = styled(Details)`
    @media (max-width: 950px) {
        transition: none !important;
        position:relative;
        padding-bottom: 0rem;
        height: calc(100% - 4rem);

        .underlay {
            transition: none !important;
            display:none;
        }

        .ps__rail-x {
            display:none !important;
        }

        .ps__rail-y {
            left: auto !important;
            right: 0 !important;
        }

        .container-modal {
            transition: none !important;
            border-top:0;

            .wrapper {
                height: calc(100% - 5.5rem);
            }

            .close {
                display:none;
            }

            .head  {
                border-bottom: solid 1px #f2f7f9;
                box-shadow:none;
                
                img {
                    width: 12rem;
                    right: 15px;
                }
            }

            .head h3, .container-details, .container-options {
                padding-left:20px;
                padding-right:20px;

                .menu {
                    padding-left:20px;
                    padding-right:20px;
                    margin-left: -20px;
                }

                .container-sum {
                    width:calc(100% + 40px);
                    margin-left: -20px;

                    .select {
                        margin-left: 20px;
                    }

                    label {
                        margin-right: 20px;
                    }
                }
            }

            .container-details {
                height:auto;
                padding-bottom: 5rem;

                .container-copy {
                    padding-bottom:3rem;
                }
            }

            .container-options {
                height:100%;

                .note {
                    padding-bottom: 3rem;
                }
            }

            .back {
                left:auto !important;
                right:0;
                top:0 !important;
                height:4.5rem;
                width:8rem;
                pointer-events: none !important;
                 
                &.active {
                    z-index:30;
                    pointer-events: all !important;
                }
            }
        }
    }

    @media (max-width: 430px) {
        .container-modal {
            .head img {
                width: 11rem;
            }
        }
    }

    @media (max-width: 400px) {

        .container-modal {
            .head img {
                right: 5px;
            }

            .head h3, .container-details, .container-options {
                padding-left:10px;
                padding-right:10px;

                .menu {
                    padding-left:10px;
                    padding-right:10px;
                    margin-left: -10px;
                }

                .container-sum {
                    width:calc(100% + 20px);
                    margin-left: -10px;

                    .select {
                        margin-left: 10px;
                    }

                    label {
                        margin-right: 10px;
                    }
                }
            }
        }
    }
`

const Section = styled(RequirementsSection)`
    height:100%;
    padding-top:0 !important;
    overflow: scroll;
    background: #FFF;

    >.wrapper {
        height:100%;
        display: block !important;
    }

    button.back {
        background: ${props => props.theme.blue};
        color:#FFF;
        font-family:regular;
        font-size:13px;
        height:2rem;
        line-height: 1.9rem;
        width:5.5rem;
		position:fixed !important;
        top: 1.15rem;
        right:10px;
        z-index:21;
	}

    @media (max-width: 950px) {
        width:calc(100% + 40px);
        margin-left:-20px;
        padding: 0 !important;

        button.back {
            right:20px;
        }
    }

    @media (max-width: 400px) {
        width:calc(100% + 20px);
        margin-left:-10px;
        
        button.back {
            right:10px;
        }
    }
`

const Internals = styled.div`
    height: calc(100% - 4rem);

    .head {
        position: relative;
        background:#FFF;
        margin-right: 36%;
        box-sizing: border-box;
        border-right:solid 1px #eaf2f5;

        h3 {
            font-size: 1.4rem;
            line-height: 2rem;
            font-family: medium;
            width: calc(100% - 8rem);
            padding: 1.6rem 1.5rem;
            padding-bottom: 1.8rem;
        }

        img {
            ${props => props.theme.verticalCenter()} width:8rem;
            right: 0;
        }
    }

    .wrapper {
        display:block !important;
        height: calc(100% + 4rem);
        overflow: scroll;
        -webkit-overflow-scrolling: touch;

        .wrapper-inner {
            display:block;
            height:calc(100% + 1px);

            .wrapper-positioning {
                position:relative;
                width:100%;
                height:1px;
            }
        }

        &.details {
            height: 100%;
        }
    }

    .container-details {
        background: #fff;
        float: left;
        width: 64%;
        padding: 0 1.5rem;
        box-sizing: border-box;
        border-right:solid 1px #eaf2f5;
        min-height: 100%;

        .container-sum {
            display:none;
            border-top: solid 1px #f2f7f9;
            height:56px;
            background: #FFF;
            width:calc(100% + 3rem);
            margin-left:-1.5rem;

            >label {
                float:left;
                margin-left:3rem;
                line-height:56px;
                font-size: 18px;
                color:${props => props.theme.blue}
                

                .icon-rupee {
                    height: 14px;
                    margin-right: 8px;

                    svg {
                        top: 55%;
                    }
                    
                    .icon {
                        stroke:${props => props.theme.blue};
                    }
                }
            }

            .select {
                height:56px;
                width: 12rem;
                margin-left:1.5rem;
                float:left;

                label {
                    line-height:56px !important;
                    font-size: 18px !important;
                    color:#232c37;
                }
            }
        }

        .menu {
            background: #fcfdfd;
            width: 100%;
            padding: 0 1.5rem;
            margin-left: -1.5rem;
            height: calc(4rem - 2px);
            box-shadow: 0 1px 3px 0 rgba(101, 116, 124, 0.1);
            border-top: solid 1px #f1f6f8;

            ul {
                float:left;
                list-style:none;

                li {
                    float:left;
                    line-height: 4rem;
                    margin-right:2rem;
                    color:${props => props.theme.copyLight}
                    position:relative;
                    cursor:pointer;

                    &.active {
                        color:${props => props.theme.blue} !important;
                        
                        &::after {
                            background-color:${props => props.theme.blue} !important;
                        }
                    }

                    &::after {
                        content:'';
                        width:100%;
                        height:2px;
                        background-color:transparent;
                        position:absolute;
                        left:0;
                        bottom:1rem;
                    }

                    &:hover {
                        color:${props => props.theme.blue}
                    }
                }
            }

            a {
                float:right;
                line-height:4rem;
                color:${props => props.theme.purple}
                position:relative;
                width:10rem;
                
                &::before {
                    content:'';
                    position:absolute;
                    top:0;
                    left: -1.5rem;
                    height:1.5rem;
                    width:1px;
                    background:#f1f6f8;
                    ${props => props.theme.verticalCenter()}
                }

                &::after {
                    content:'↓';
                    color:#FFF;
                    position:absolute;
                    ${props => props.theme.verticalCenter()}
                    background-color:${props => props.theme.purple}
                    width:1.4rem;
                    height:1.4rem;
                    line-height: 1.4rem;
                    font-size:0.9rem;
                    font-weight:900;
                    text-indent:0.3rem;
                    border-radius:50%;
                    right:0;
                }
            }
        }

        .container-copy {
            width:100%;
            float: left;
            position:relative;

            >div {
                width:100%;
                float: left;
                opacity:0;
                transition:all 300ms ease-out;
                top:3px;
                left:0;
                position:absolute;
                pointer-events:none;
                display:none;

                &.hospitals {
                    padding-top:1rem;
                }

                h3 {
                    color:${props => props.theme.tooltip}
                    font-size: 1.7rem;
                    margin-top: 2.5rem;
                    margin-bottom: 1.5rem;
                    float:left;
                }

                .container {
                    float: left;
                }

                .feature {
                    width:100%;
                    float:left;
                    margin-bottom:1rem;

                    p {
                        width:50%;
                        float:left;
                        line-height: 1.4rem;
                        box-sizing:border-box;

                        &.name {
                            color:${props => props.theme.copyLighter}
                            text-indent:1.5rem;
                            position:relative;

                            &::before {
                                content:'';
                                width:8px;
                                height:8px;
                                border-radius:50%;
                                background:#ff9595;
                                ${props => props.theme.verticalCenter()}
                                left:0;
                            }

                            &.available {
                                &::before {
                                    background:#0ace8e;
                                }
                            }
                        }

                        &.value {
                            color:${props => props.theme.tooltip}
                            padding-right:1rem;

                            &.not-available {
                                color:${props => props.theme.copyLighter}
                            }
                        }
                    }
                }

                p {
                    float:left;
                    color:${props => props.theme.copyLight}
                    width:90%;
                    line-height:1.4rem;

                    &.wp-value {
                        margin-bottom:1.5rem;
                    }
                }

                h6 {
                    float:left;
                    width:100%;
                    font-family:medium;
                    font-size:1rem;
                    margin-top:3rem;
                    margin-bottom:0.7rem;
                }

                .hospital {
                    border-bottom:solid 1px #f1f6f8;
                    width:100%;
                    float:left;
                    min-height:5rem;
                    position:relative;

                    span {
                        font-family:medium;
                        width: 20%;
                        max-width: 20%;
                        float:left;
                        font-size:0.95rem;
                        line-height:1.4rem;
                        color:${props => props.theme.tooltip}
                        margin: 1rem 0;
                    }

                    p {
                        width: 60%;
                        max-width: 60%;
                        float:right;
                        font-size:0.95rem;
                        line-height:1.4rem;
                        color:${props => props.theme.copyLight}
                        margin: 1rem 0;
                        margin-right: 13%;
                    }

                    a {
                        display:block;
                        width:1.2rem;
                        ${props => props.theme.verticalCenter()}
                        right:0;

                        img {
                            display:block;
                            width:100%;
                        }
                    }
                }

                &.hospitals {
                    h3 {
                        font-size: 1.3rem;
                        color:${props => props.theme.copyLighter}
                    }
                }

                &.active {
                    display:block;
                }

                &.activate {
                    position:relative;
                    top:0;
                    opacity:1;
                    transition:all 300ms ease-in;
                    pointer-events:all;
                }
            }
        }
    }

    .container-options {
        float: right;
        width: 36%;
        min-height: 100%;
        padding: 0 1.5rem;
        box-sizing: border-box;
        position:relative;

        .container {
            width:100%;
            background:#FFF;
            margin-bottom:1rem;
            border-radius: 6px;
            box-shadow: 0 1px 2px 0 rgba(101, 116, 124, 0.1);
            border: solid 1px #eaecef;
            min-height:4rem;
            box-sizing: border-box;

            >label {
                line-height:calc(4rem - 2px);
                font-size: 1.2rem;
                padding-left:1.2rem;
                color:${props => props.theme.copyLight}
            }

            >h6 {
                color:${props => props.theme.copyLighter}
                text-align:center;
                border-bottom:solid 1px #f1f6f8;
                font-family:medium;
                font-size: 0.8rem;
                line-height: 3rem;
                letter-spacing: 1px;
            }

            >b {
                line-height: calc(4rem - 2px);
                color:${props => props.theme.blue};
                font-size:1.2rem;
                float:right;
                margin-right:1.2rem;

                .icon-rupee {
                    height: 1rem;
                    
                    .icon {
                        stroke:${props => props.theme.blue};
                    }
                }
            }

            .select {
                float:right;
                height: calc(4rem - 2px);
                margin-right:1.2rem;
                width: 9rem;

                label {
                    line-height: calc(4rem - 2px) !important;
                    font-size: 1.2rem !important;
                    color:${props => props.theme.tooltip}
                }
            }

            .premium {
                border-bottom: solid 1px #f1f6f8;
                position:relative;
                width:100%;
                height:5rem;
                cursor:pointer;

                .duration {
                    font-family:medium;
                    line-height:4rem;
                    color:${props => props.theme.copyDark}
                    position:absolute;
                    left:4rem;
                    pointer-events:none;

                    span {
                        position:absolute;
                        left:0;
                        top:1.4rem;
                        color:#97a0aa !important;
                        font-size:0.85rem;
                        font-family:regular;
                    }
                }

                .value {
                    font-size:1.1rem;
                    line-height:4rem;
                    font-family:medium;
                    color:${props => props.theme.tooltip}
                    position:absolute;
                    right: 1.3rem;
                    pointer-events:none;

                    .icon-rupee {
                        height: 0.85rem;
                        
                        .icon {
                            stroke:${props => props.theme.tooltip};
                        }
                    }
                }

                .check {
                    left: 1.2rem;
                    pointer-events:none;
                }

                &.selected {
                    .duration {
                        color:${props => props.theme.blue}
                    }
                }

                &:last-child {
                    border:none;
                }
            }

            .addon {
                border-bottom: solid 1px #f1f6f8;
                position:relative;
                cursor:pointer;

                p {
                    font-size: 13px;
                    line-height: 19px;
                    color:${props => props.theme.tooltip}
                    width:calc(100% - 5.3rem);
                    padding: 1.2rem 0;
                    padding-left:4rem;
                    cursor:pointer;
                    user-select:none;

                    .icon-rupee {
                        height: 11px;
                        margin-left: 3px;
                        margin-right: 8px;

                        svg {
                            top: 62%;

                            .icon {
                                stroke: ${props => props.theme.tooltip}
                            }
                        }
                    }

                    label {
                        white-space:nowrap;
                    }
                }

                .check {
                    left: 1.2rem;
                    pointer-events:none;
                }

                &:last-child {
                    border:none;
                }

                &.isCompulsory {
                    cursor: default;
                    .checked {
                        background: ${props => props.theme.green}
                        cursor: default;
                    }

                    p {
                        cursor: default;
                    }
                }
            }
        }

        button {
            width:100%;
            height:4rem;
            line-height:calc(4rem - 2px);
            font-size: 1.4rem;
            box-shadow: 0 4px 6px 0 rgba(0, 255, 200, 0.2);

            .continue {
                line-height:calc(4rem - 2px);
            }
        }

        .note {
            color:${props => props.theme.copyLighter}
            font-size:0.85rem;
            line-height:1.4rem;
            text-align:center;
            margin-top:1rem;
            padding: 0 1rem;
        }

        .icon-spinner {
            width: 3rem;
            position:absolute;
            left:0;
            right:0;
            margin:0 auto;
            top: calc(39vh - 3rem);
            opacity:0;
            pointer-events:none;

            &.active {
                opacity:1;
            }
        }

        .wrapper-options {
            margin-top:10px;
            opacity:0;
            transition: all 300ms ease;

            &.active {
                margin-top:0;
                opacity:1;
            }
        }
    }

    button.continue {
        position:fixed;
        bottom:-10px;
        width:100%;
        left:0;
        margin:0;
        border-radius:0;
        max-width: 100% !important;
        height: 4rem;
        font-family: medium;
        font-size: 18px;
        line-height: 3.9rem;
        opacity:0;
        transition: all 300ms ease;
        pointer-events:none;
        display:none;

        &.active {
            bottom:0;
            opacity:1;
            pointer-events:all;
        }

    }

    button.backMobile {
        background: ${props => props.theme.blue};
        color:#FFF;
        font-family:regular;
        font-size:13px;
        height:2rem;
        line-height: 1.9rem;
        width:5.5rem;
		position:fixed !important;
        top: 1.15rem;
        right:10px;
        z-index:21;
        display:none;
    }
    
    @media (max-width:950px) {
        .head {
            margin-right:0;
            box-shadow: 0 1px 2px 0 rgba(101, 116, 124, 0.1);
            z-index: 1;

            img {
                right: 1.5rem;
            }
        }

        .container-details {
            width:100%;
            padding-bottom:3rem;

            .container-sum {
                display:block;
            }

            .menu {
                ul li, a {
                    font-size:13px;
                }
            }

            .container-copy {
                >div {

                    p {
                        font-size:13px;
                        line-height: 17px;
                    }

                    h6 {
                        font-size:15px;
                    }

                    .hospital {
                        span, p {
                            font-size:13px;
                            line-height: 17px;
                            max-width: 100%;
                        }

                        p {
                            width: 55%;
                        }

                        span {
                            width: 25%;
                        }
                    }
                }
            }
        }

        .container-options {
            width:100%;
            background:#fcfdfd;
            padding-top: 1.3rem;

            .note {
                margin-bottom: 3rem;
                font-size: 12px;
                line-height: 17px;
                padding: 0;
            }

            .container {
                >h6 {
                    font-size: 12px;
                    line-height: 38px;
                }

                .premium {
                    .duration, >span {
                        font-size:14px !important;

                        span {
                            font-size:11px;
                        }
                    }
                }
            }
        }

        .container-details, .container-options {
            min-height:0 !important;
            max-height:0;
            overflow:hidden;
            margin-top:10px;
            opacity:0;
            transition:opacity 300ms ease, margin-top 300ms ease, max-height 0s ease;
            position: relative;
            top: 0;

            &.active {
                max-height:9999px;
                overflow: visible;
                margin-top:0;
                opacity:1;
                transition:opacity 300ms ease, margin-top 300ms ease, max-height 1s ease;
            }
        }

        .container-details {
            margin-top:0;
            padding-bottom:0;

            &.active {
                padding-bottom:3rem;
            }
        }

        button.continue {
            display:block;
            z-index: 2;
        }

        button.backMobile {
            display:block;
            right:20px;
        }
    }

    @media (max-width:500px) {
        .head {
            h3 {
                width: calc(100% - 11rem);
            }
        }

        .container-details {
            
            .container-sum {
                border-bottom: solid 1px #f2f7f9;
                height: 48px !important;

                >label {
                    float:right;
                    margin-left:0;
                    margin-right:1.5rem;
                }

                .select {
                    height: 48px !important;
                    width: 10.5rem;

                    label {
                        font-size:16px !important;
                        line-height: 48px !important;
                    }
                }

                label {
                    font-size: 16px;

                    .icon-rupee {
                        height: 12px;
                    }
                }
            }

            .container-copy {
                >div {
                    .feature {
                        p.name {
                            width:100%;
                        }

                        p.value {
                            width:100%;
                            padding-left: 1.5rem;
                            box-sizing:border-box;
                            margin-top: 3px;
                        }
                    }

                    .hospital {
                        span, p {
                            width:100% !important;
                            margin-right:0 !important;
                            width: calc(100% - 3rem) !important;
                        }

                        span {
                            margin-bottom:0 !important;
                        }

                        p {
                            margin-top:3px !important;
                            float: left !important;
                        
                        }
                    }
                }
            }
        }

        .menu {
            margin-top:42px;
            position:relative;

            a {
                position:absolute !important;
                left:1.5rem;
                top: -47px;
                width: 10.5rem !important;
            }
        }
    }

    @media (max-width: 400px) {
        button.backMobile {
            right:10px;
        }
    }
`

class QuoteDetails extends Component {
    formatSum = (sum, isMobile = false) => {
        if (sum >= 10000000) sum = `${sum / 10000000} Crore cover`
        else if (sum >= 100000) sum = `${sum / 100000} Lacs cover`
        else if (sum >= 1000) sum = `${sum / 1000} K cover`
        if (isMobile) {
            return (
                <label>
                    {sum.split(' ')[0]}
                    <br />
                    {sum.split(' ')[1]}
                </label>
            )
        }
        return {
            label: sum,
            value: this.props.sumInsured
        }
    }

    transformPremium = premium => {
        let years = Object.keys(premium)
        let labels = ['First Year', 'Second Year', 'Third Year']
        let selected_keys = ['firstYear', 'secondYear', 'thirdYear'];
        return years.map((year, index) => {
            if(year == 'selected') {
                return
            }
            let discounted = premium[year].discount && premium[year].discount.totalPremium
            //let _premium = parseInt(discounted || premium[year].totalPremium).toLocaleString('en-IN')
            let _premium = parseInt(Math.round(discounted || premium[year].totalPremium)).toLocaleString('en-IN')
            return {
                label: labels[index],
                value: _premium,
                selected: selected_keys[index]
            }
        })
    }
    
    cartQuote = (cart = { product: undefined, quote: undefined, duration: undefined, sum: undefined, addons: [] }) => {
        this.props.editCart(cart)
        cart.product && this.getQuoteDetails(cart.product.productId)
    }
    
    getQuoteDetails = productId => {
		if (this.props.quoteDetails[productId]) return
		let { selected: {group , combo} } = this.props.filters.members
		let selectedCombo = combo[group]
		let payload  = this.getQuotes(group, selectedCombo, '0-999999999', true)
		this.props.getQuoteDetails({ productId, payload })
    }
    
    render() {
        return (
            <Section>
                <Internals className="container-internals">
                    <_Details className={'quote-details active activate'} active={this.props.cart.product}
                        product={this.props.cart.product} formatSum={this.formatSum}
                        quote={this.props.cart.quote} duration={this.props.cart.duration}
                        transformPremium={this.transformPremium} cart={this.props.cart}
                        selectDuration={duration => this.props.editCart({ duration })}
                        selectSum={sum => this.props.editCart({ sum })}
                        toggleAddon={addons => this.props.editCart({ addons })}
                        selectedSums={{}} cartQuote={this.cartQuote}
                        changeSum={() => ''} details={this.props.quoteDetails}
                        loadingDetails={!(this.props.cart.product && this.props.quoteDetails[this.props.cart.product.productId])}
                        isMobile={this.props.isMobile} onClose={() => ''}
                        history={this.props.history} renderMobile={true}
                    />
                </Internals>
            </Section>
        )
    }
}

const mapStateToProps = state => {
	let { quotes: { filters, quotes, user, cart, quoteDetails }, details } = state
	return { filters, quotes, user, details, cart, quoteDetails }
}

const mapDispatchToProps = dispatch => {
	return {
        getQuoteDetails: req => dispatch(getQuoteDetails(req)),
		editCart: quote => dispatch(editCart(quote))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(QuoteDetails)
