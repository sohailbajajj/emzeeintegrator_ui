import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { isEqual } from 'lodash'
import { generateQuotesRequest } from '../utils/helpers'
import axios from 'axios'
import { loadState } from '../utils/persist'

import { RequirementsSection } from '../components/section'
import Quotes from '../components/quotes/quotes'
import Filters from '../components/quotes/filters'
import Details from '../components/quotes/details'
import Compare from '../components/quotes/compare'
import Share from '../components/quotes/share'

import { initUser, editFilter, toggleInsurer, toggleCombo, getQuotes, editCart, getQuoteDetails, resetFilters, toggleGroup } from '../actions/quotes'
import { getForm }  from '../actions/buy'

const _AllQuotes = RequirementsSection.extend`
	padding-top:0 !important;
	
	.loader {
        text-align: center;
        margin: 8rem 0;
        font-size: 3rem;
        color: ${props => props.theme.blue};
    }
`

class AllQuotes extends Component {

	state = {
		compare: [],
		compareActive: false,
		selectedSums: {},
		share: {
			active: false,
			user: {
				selected: true
			},
			additional: {
				email: undefined,
				selected: false
			}
		}
	}

	componentWillMount() {
		let { selected: {group , combo} } = this.props.filters.members
		let selectedCombo = combo[group]
		
		// Call get quotes service with user data
		if (!this.props.user.init) {
			this.getQuotes(group, selectedCombo)
			this.props.initUser({ init: true })
		}
	}

	componentDidMount() {
		this.cartQuote()
		this.scrollToTop()
		let share = document.querySelector('button.share')
		share && share.addEventListener('mousedown', this.toggleShare)
	}

	componentDidUpdate(_props, _state) {
		let { selected: {group , combo} } = this.props.filters.members
		let selectedCombo = combo[group]

		// get quotes on changing members group or combo within a group
		if (!isEqual(_props.filters.members.selected, this.props.filters.members.selected)) {
			(!this.props.quotes[group] || !this.props.quotes[group][selectedCombo]) 
			&& this.getQuotes(group, selectedCombo)
		}

		if (!isEqual(this.props.filters.sum, _props.filters.sum)) {
			let { high, low } = this.props.filters.sum.selected.value
			this.getQuotes(group, selectedCombo, `${low}-${high}`)
		}

		if (!isEqual(this.state.compare, _state.compare)) {
			if (this.state.compare[0]) this.setState({ compareActive: true })
			else setTimeout(() => this.setState({ compareActive: false }), 300)
		}

		if (!isEqual(this.props.filters, _props.filters)) {
			this.setState({ compareActive: false })
			setTimeout(() => this.setState({ compare: [] }), 300)
		}
	}

	componentWillUnmount() {
		let share = document.querySelector('button.share')
		share && share.removeEventListener('mousedown', this.toggleShare)
	}

	toggleShare = () => {
		this.setState({
			share: {
				...this.state.share,
				active: !this.state.share.active
			}
		})
	}

	toggleShareSelect = (user, select) => {
		this.setState({
			share: {
				...this.state.share,
				[user]: {
					...this.state.share[user],
					selected: select || !this.state.share[user].selected
				}
			}
		})
	}

	toggleShareEmail = email => {
		this.setState({
			share: {
				...this.state.share,
				additional: {
					...this.state.share.additional,
					email,
					selected: email ? true : false
				}
			}
		})
	}

	shareQuotes = quote => async() => {
		this.setState({ share: { ...this.state.share, active: false }})
		
		let share = { ...this.state.share }
		delete share.active
		let emails = Object.keys(share).filter(user => this.state.share[user].selected)
			.map(user => user == 'user' ? this.props.user.email : this.state.share[user].email)

		let request = {
			"activityId": quote.trackingId || quote.activityId || 1001,
			"activityName": "shareQuote",
			"mailInfo": {
				"to": emails[0] || '',
				"cc": emails[1] || ''
			},
			"appState": loadState()
		}

		try {
			let resp = await axios.post('/api/share', request)
			console.log(resp.data)
		} catch (err) { console.log(err) }
		
	}

	getFilters = () => (
		<Filters editFilter={this.props.editFilter} filters={this.props.filters} 
			toggleInsurer={this.props.toggleInsurer} toggleCombo={this.props.toggleCombo}
			resetFilters={this.props.resetFilters} toggleGroup={this.props.toggleGroup}
		/>
	)

	getQuotes = (group, combo, sum, getRequest = false ) => {

		let selectedGroup = this.props.filters.members.groups.find(_group => _group.id == group)
		let selectedCombo = selectedGroup.combos.find(_combo => _combo.id == combo)
		
		let getPincode = () => {
			let { selected, selection } = this.props.details.selectCity
			let parentCity = selected && selection[0]
			let isParents = selectedGroup.name == 'parents' || selectedGroup.name == 'father' || selectedGroup.name == 'mother'
			if (isParents && parentCity) return parentCity
			else return JSON.parse(sessionStorage.getItem('city')).pincode
		}

		let getDiseases = () => {
			let { selected, selection } = this.props.details.selectDiseases
			return !selected ? selected : selection
		}
	
		let proposer = {
			gender: JSON.parse(sessionStorage.getItem('gender')),
			postalCode: getPincode(),
			firstname: this.props.user.name.split(' ')[0],
			lastname: this.props.user.name.split(' ').slice(1).reduce((name,_name) => name+= `${_name} `, '').trim() || '',
			mobileno: this.props.user.number,
			emailid: this.props.user.email
		}
	
		let request = {
			group, combo, sumchanged: sum && true,
			request: JSON.stringify(generateQuotesRequest(proposer, selectedCombo.members, getDiseases(), false, sum)), againInitialized: true,
		}

		if (getRequest) return JSON.parse(request.request)

		this.props.getQuotes(request)
	}

	filterQuotes = products => {
		if (!products[0]) return
		
		let { insurers, nocopay, roomrent, maternity, restore } = this.props.filters
		
		let filters = []
		
		if (insurers.selected[0]) {
			filters.push((quote, product) => insurers.selected.includes(product.insurerId)) 
		}

		if (nocopay.selected) {
			filters.push(quote => quote.searchFilter.noCopay)
		}

		if (roomrent.selected) {
			filters.push(quote => quote.searchFilter.noRoomRentLimit)
		}

		if (maternity.selected) {
			filters.push(quote => quote.searchFilter.maternityBenefit)
		}

		if (restore.selected) {
			filters.push(quote => quote.searchFilter.restoreBenefit)
		}

		if (filters[0]) {
			let applyFilters = (quotes, product) => quotes.filter(quote => filters.every(filter => filter(quote, product)))
			
			products =  products.map(product => ({ ...product, payloads: applyFilters(product.payloads, product)}))
				.filter(product => product.payloads[0])
		}

		return products
	}

	scrollToTop = () => {
		let docBody = document.body
		let focElem = document.documentElement
		setTimeout(() => docBody.scrollTop = focElem.scrollTop = 0, 300)
	}

	getQuoteDetails = productId => {
		if (this.props.quoteDetails[productId]) return
		let { selected: {group , combo} } = this.props.filters.members
		let selectedCombo = combo[group]
		let payload  = this.getQuotes(group, selectedCombo, '0-999999999', true)
		this.props.getQuoteDetails({ productId, payload })
	}

	cartQuote = (cart = { product: undefined, quote: undefined, duration: undefined, sum: undefined, addons: [] }) => {
		this.props.editCart(cart)
		
		if (cart.product) {
			this.getQuoteDetails(cart.product.productId)
			this.getForm(cart.product)
			window.innerWidth < 951 && this.props.history.push('/health/quotes/details')
		}
	}

	closeDetails = () => setTimeout(this.cartQuote, 500)

	formatSum = (sum, isMobile = false) => {
        if (sum >= 10000000) sum = `${sum / 10000000} Crore cover`
        else if (sum >= 100000) sum = `${sum / 100000} Lacs cover`
        else if (sum >= 1000) sum = `${sum / 1000} K cover`
        if (isMobile) {
            return (
                <label>
                    {sum.split(' ')[0]}
                    <br />
                    {sum.split(' ')[1]}
                </label>
            )
        }
        return {
            label: sum,
            value: this.props.sumInsured
        }
	}
	
	transformPremium = premium => {
        let years = Object.keys(premium)
		let labels = ['First Year', 'Second Year', 'Third Year']
		let selected_keys = ['firstYear', 'secondYear', 'thirdYear'];
        return years.map((year, index) => {
			if(year == 'selected') {
				return
			}
            let discounted = premium[year].discount && premium[year].discount.totalPremium
            //let _premium = parseInt(discounted || premium[year].totalPremium).toLocaleString('en-IN')
            let _premium = parseInt(Math.round(discounted || premium[year].totalPremium)).toLocaleString('en-IN')
            return {
                label: labels[index],
				value: _premium,
				selected: selected_keys[index]
            }
        })
	}
	
	toggleCompare = product => {
		if (!product) {
			this.setState({ compareActive: false })
			setTimeout(() => this.setState({ compare: [] }), 300)
			return
		}

		if (this.state.compare.find(_product => _product.productId == product.productId)) {
			this.setState({ compare: this.state.compare.filter(_product => _product.productId != product.productId)})
		} else {
			this.getQuoteDetails(product.productId)
			if (this.state.compare.length < 3) {
				this.setState({ compare: [...this.state.compare, product ]})
			}
		}
	}

	changeSum = (productId, quoteId) => {
		this.setState({ selectedSums: {
			...this.state.selectedSums,
			[`${productId}`]: quoteId
		} })
	}

	getForm = product => {
        
        let getLocation = () => {
            let { selected: { group }, groups, parentsLocation } = this.props.filters.members
        	let selectedGroup = groups.find(_group => _group.id == group)
			let isParents = selectedGroup.name == 'parents' || selectedGroup.name == 'father' || selectedGroup.name == 'mother'
			
			if (isParents && parentsLocation) {
				return {
					pincode: selection[0],
					city: selection[2],
					state: selection[1]
				}
			} else {
				return {
					pincode: JSON.parse(sessionStorage.getItem('city')).pincode,
					city: JSON.parse(sessionStorage.getItem('city')).city,
					state: JSON.parse(sessionStorage.getItem('city')).state
				}
			}
		}

        let { pincode, city, state } = getLocation()
        
        let request = { 
            verticalID: "1", 
            insurerID: product.insurerId, 
            productID: product.productId, 
            postalCode: pincode,
            cityName: city,
            stateName: state
		}
		
		this.props.getForm(request)
    }

	render() {
		let { members: { selected: { group, combo } } } = this.props.filters
		let quotesList = this.props.quotes[group] && combo[group] && this.props.quotes[group][combo[group]]
		let filteredQuotes = quotesList && this.filterQuotes(quotesList)

		return (
			<_AllQuotes nowrapper={this.getFilters()}>
				{!quotesList && <h6 className="loader">Loading...</h6>}
				{quotesList && !filteredQuotes[0] && <h6 className="loader">No Results..</h6>}
				{ 
					filteredQuotes && 
					<Quotes products={filteredQuotes} onSelect={this.cartQuote} 
						formatSum={this.formatSum} transformPremium={this.transformPremium}
						toggleCompare={this.toggleCompare} disableCompare={this.state.compare.length == 3}
						selectedSums={this.state.selectedSums} changeSum={this.changeSum}
						isMobile={this.props.isMobile} isGetQuotes={window.innerWidth < 1020}
						isQuotes={true}
					/> 
				}
				{
					window.innerWidth > 950 &&
					<Fragment>
						<Details active={this.props.cart.product} onClose={this.closeDetails}
							product={this.props.cart.product} formatSum={this.formatSum}
							quote={this.props.cart.quote} duration={this.props.cart.duration}
							transformPremium={this.transformPremium} cart={this.props.cart}
							selectDuration={duration => this.props.editCart({ duration })}
							selectSum={sum => this.props.editCart({ sum })}
							toggleAddon={addons => this.props.editCart({ addons })}
							selectedSums={this.state.selectedSums} cartQuote={this.cartQuote}
							changeSum={this.changeSum} details={this.props.quoteDetails}
							loadingDetails={!(this.props.cart.product && this.props.quoteDetails[this.props.cart.product.productId])}
							isMobile={this.props.isMobile}
						/>
						<Compare toggleCompare={this.toggleCompare} compare={this.state.compare} 
							active={this.state.compareActive} formatSum={this.formatSum}
							transformPremium={this.transformPremium} selectedSums={this.state.selectedSums}
							changeSum={this.changeSum} details={this.props.quoteDetails}
						/>
					</Fragment>
				}

				<Share share={this.state.share} userEmail={this.props.user.email} 
					onClose={() => this.setState({share: { ...this.state.share, active: false }})}
					toggleUser={this.toggleShareSelect} toggleEmail={this.toggleShareEmail}
					shareQuotes={this.shareQuotes(filteredQuotes && filteredQuotes[0])}
				/>
			</_AllQuotes>
		)
	}
}

const mapStateToProps = state => {
	//console.log(state)
	let { quotes: { filters, quotes, user, cart, quoteDetails }, details } = state
	return { filters, quotes, user, details, cart, quoteDetails }
}

const mapDispatchToProps = dispatch => {
	return {
		initUser: user => dispatch(initUser(user)),
		editFilter: filter => dispatch(editFilter(filter)),
		toggleInsurer: insurer => dispatch(toggleInsurer(insurer)),
		toggleCombo: combo => dispatch(toggleCombo(combo)),
		getQuotes: req => dispatch(getQuotes(req)),
		getQuoteDetails: req => dispatch(getQuoteDetails(req)),
		editCart: quote => dispatch(editCart(quote)),
		resetFilters: () => dispatch(resetFilters()),
		toggleGroup: group => dispatch(toggleGroup(group)),
		getForm: req => dispatch(getForm(req))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(AllQuotes)