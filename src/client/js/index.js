import React from 'react'
import ReactDOM from 'react-dom'

import '../styles/common.scss'
import '../styles/react.scss'
import '../styles/bootstrap-grid.css'
import App from './app'

ReactDOM.render(<App />, document.querySelector('.container-react'))