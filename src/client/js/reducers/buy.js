const INITIAL_STATE = {
    form: undefined
}

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
        case 'GET_FORM':
			return {
                ...state,
                form: action.form
            }
		default:
			return state
	}
}