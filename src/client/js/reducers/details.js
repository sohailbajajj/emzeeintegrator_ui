import { omit } from 'lodash'

const INITIAL_STATE = {
	selectCity: {
		label: 'Are your parents in a different city?',
		desc: 'This will help us make sure they get coverage in local hospitals',
		type: 'selectCity',
		init: false,
		cities: [],
		selected: false,
		selection: undefined
	},
	selectDiseases: {
		label: 'Does anyone to be insured have a known disease?',
		desc: 'This will help us make sure they get coverage in local hospitals',
		type: 'selectDiseases',
		diseases: ['Hypertension', 'High Cholesterol', 'Asthma', 'Thyroid Disorder', 'Diabetes', 'Heart Ailments'],
		init: true,
		selected: false,
		selection: {
			husband: [],
			wife: [],
			father: [],
			mother: [],
			children: {}
		}
	}
}

const toggleDisease = (disease, diseases) => {
	if (!diseases) return [disease] 
	if (!diseases.includes(disease)) return [...diseases, disease]
	else return diseases.filter(_disease => disease != _disease)
}

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case 'RESET_DETAILS':
			return INITIAL_STATE
		case 'EDIT_QUESTION':
			return {
				...state,
				[action.question.type]: {
					...state[action.question.type],
					...action.question
				}
			}
		case 'GET_CITIES':
			return {
				...state,
				selectCity: {
					...state.selectCity,
					cities: action.cities
				}
			}
		case 'TOGGLE_DISEASE':
			return {
				...state,
				selectDiseases: {
					...state.selectDiseases,
					selection: {
						...state.selectDiseases.selection,
						[action.member.type]: toggleDisease(action.member.disease, state.selectDiseases.selection[action.member.type])
					}
				}
			}
		case 'TOGGLE_DISEASE_FOR_CHILD':
			return {
				...state,
				selectDiseases: {
					...state.selectDiseases,
					selection: {
						...state.selectDiseases.selection,
						children: {
							...state.selectDiseases.selection.children,
							[action.child.id]: toggleDisease(action.child.disease, state.selectDiseases.selection.children[action.child.id])
						}
					}
				}
			}
		case 'REMOVE_DISEASED_CHILD':
			return {
				...state,
				selectDiseases: {
					...state.selectDiseases,
					selection: {
						...state.selectDiseases.selection,
						children: omit(state.selectDiseases.selection.children, action.child.id)
					}
				}
			}
		default:
			return state
	}
}