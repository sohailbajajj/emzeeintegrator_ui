const INITIAL_STATE = {
	errors: [], 
	continue: {
		household: false,
		details: false,
		getquotes: false
	}
}

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case 'ADD_ERROR':
			if (!state.errors.filter(err => err.id == action.error.id).length) {
				return {
					...state,
					errors: action.error.priority ? [ action.error , ...state.errors ] : [ ...state.errors, action.error ]
				}
			}
			return state
			break
		case 'REMOVE_ERROR':
			return {
				...state,
				errors: state.errors.filter(err => err.id != action.error.id)
			}
			break
		case 'TOGGLE_CONTINUE':
			return { 
				...state,
				continue: {
					...state.continue,
					[action.stage]: action.enable
				}
			}
			break
		default:
			return state
	}
}