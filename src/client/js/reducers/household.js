const INITIAL_STATE = {
	husband: {
		age: undefined,
		selected: false,
		name: undefined,
		type: 'husband',
		gender: 'male',
		init: false
	},
	wife: {
		age: undefined,
		selected: false,
		name: undefined,
		type: 'wife',
		gender: 'female',
		init: false
	},
	father: {
		age: undefined,
		selected: false,
		name: undefined,
		type: 'father',
		gender: 'male',
		init: false
	},
	mother: {
		age: undefined,
		selected: false,
		name: undefined,
		type: 'mother',
		gender: 'female',
		init: false
	},
	children: []
}

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case 'RESET_HOUSEHOLD':
			return INITIAL_STATE
		case 'ADD_MEMBER':
			return {
				...state,
				[action.member.type]: {
					...state[action.member.type],
					...action.member
				}
			}

		case 'EDIT_MEMBER':
			return {
				...state,
				[action.member.type]: {
					...state[action.member.type],
					...action.member
				}
			}

		case 'ADD_CHILD':
			return {
				...state,
				children: [...state.children, action.child]
			}

		case 'EDIT_CHILD':
			let position = state.children.findIndex(child => child.id == action.child.id)
			if (position > -1) {
				return {
					...state,
					children: state.children.map((child, index) => {
						if (index == position) return { ...state.children[position], ...action.child }
						return child
					})
				}
			} else return state

		case 'REMOVE_CHILD':
			return {
				...state,
				children: state.children.filter(child => child.id != action.child.id)
			}

		default:
			return state
	}
}