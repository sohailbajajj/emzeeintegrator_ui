import { combineReducers } from 'redux'
import household from './household'
import errContinue from './errContinue'
import details from './details'
import quotes from './quotes'
import buy from './buy'
import { reducer as reduxFormReducer } from 'redux-form';
import proposalForm from './proposalForm'

const rootReducer = combineReducers({ errContinue, household, details, quotes, buy, form: reduxFormReducer, proposalForm })

export default rootReducer
