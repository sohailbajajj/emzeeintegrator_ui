import {
	GET_PROPOSAL_FORM_INIT,
	GET_PROPOSAL_FORM_END,
	GET_VALIDATIONS,
	SUBMIT_PROPOSAL_FORM_INIT,
	SUBMIT_PROPOSAL_FORM_END,
	CREATE_PROPOSAL_FORM,
	UPDATE_PROPOSAL_FORM,
	SET_PG_URL,
	SET_CASCADING_OPTIONS,
	SET_DISCOUNTED_PREMIUM
} from '../actions/buy'

const INITIAL_STATE = {
	fields: undefined,
	validations: undefined,
	loading: false,
	error: false,
	submitResponse: undefined,
	createResponse: undefined,
	updateResponse: undefined,
	pgUrl: undefined,
	discountedPremium: undefined
}

export default function (state = INITIAL_STATE, action) {
	switch (action.type) {
		case GET_PROPOSAL_FORM_INIT:
			return {
				...state,
				loading: true,
				error: false,
				fields: undefined,
				submitResponse: undefined,
				createresponse: undefined,
				updateResponse: undefined,
				pgUrl: undefined,
				discountedPremium: undefined
			}
		case GET_PROPOSAL_FORM_END:
			return {
				...state,
				loading: false,
				error: action.error,
				fields: action.fields
			}
		case GET_VALIDATIONS:
			return {
				...state,
				validations: action.payload.data.proposerFormValidations.validation
			}
		case SUBMIT_PROPOSAL_FORM_INIT:
			return {
				...state,
				submitResponse: undefined,
				createResponse: undefined,
				updateResponse: undefined,
				pgUrl: undefined,
				discountedPremium: undefined
			}

		case SUBMIT_PROPOSAL_FORM_END:
			return {
				...state,
				submitResponse: action.response
			}

		case CREATE_PROPOSAL_FORM:
			console.log('This is the createProposal response -')
			console.log(action.payload)
			return {
				...state,
				createResponse: action.payload.data
			}

		case UPDATE_PROPOSAL_FORM:
			console.log('This is the updateProposal response -')
			console.log(action.payload)
			return {
				...state,
				updateResponse: action.payload.data
			}

		case SET_PG_URL:
			console.log(`This is the url - ${action.payload}`)
			return {
				...state,
				pgUrl: action.payload
			}

		case SET_CASCADING_OPTIONS:
			let { form, index, options } = action.payload
			let modifiedField = state.fields[form].slice(index, index + 1)[0]
			modifiedField.options = options

			return {
				...state,
				fields: {
					...state.fields,
					[form]: [
						...state.fields[form].slice(0, index),
						modifiedField,
						...state.fields[form].slice(index + 1),
					]
				}
			}

		case SET_DISCOUNTED_PREMIUM:
			return {
				...state,
				discountedPremium: action.payload
			}

		default:
			return state
	}
}