import { pick } from 'lodash'

const INITIAL_STATE = {
    user: {
        init: false,
        name: undefined,
        number: undefined,
        email: undefined,
        pincode: undefined,
        gender: undefined
    },
    filters: {
        sum: {
            selected: {
                label: '3 to 6 lacs',
                value: {
                    low: 300000,
                    high: 600000
                }
            }
        },
        insurers: {
            list: [],
            selected: []
        },
        nocopay: {
            selected: false
        },
        roomrent: {
            selected: false
        },
        maternity: {
            selected: false
        },
        restore: {
            selected: false
        },
        members: {
            selected: {
                group: undefined,
                combo: {}
            },
            groups: [],
            members: undefined,
            diseases: {
                members: undefined,
                anySelected: undefined
            },
            parentsLocation: undefined
        }
    },
    quotes: {},
    quoteDetails: {},
    cart: {
        product: undefined,
        quote: undefined,
        duration: undefined,
        sum: undefined,
        addons: []
    }
}

const toggleInsurers = (insurer, insurers) => {
    if (!insurers[0]) return [insurer]
    if (!insurers.includes(insurer)) return [...insurers, insurer]
    else return insurers.filter(_insurer => insurer != _insurer)
}

const getGroupQuotes = (quotes, quote, group, combo, groups) => {
    if (!quotes[group]) {
        return {
            [group]: {
                [combo]: [quote]
            }
        }
    }

    if (quotes[group] && !quotes[group][combo]) {
        return {
            [group]: {
                ...quotes[group],
                [combo]: [quote]
            }
        }
    }

    if (quotes[group] && quotes[group][combo]) {
        return {
            [group]: {
                ...quotes[group],
                [combo]: [...quotes[group][combo], quote]
            }
        }
    }
}

const getSelectedCombos = (members, group, combo) => {
    let groups = members.groups.map(group => group.id)
    let _combo = members.selected.combo
    _combo = pick(_combo, groups)
    return {
        ..._combo,
        [group]: combo
    }
}

const initQuotes = (state, action) => {
  if (action.groups) {
    return {
      ...pick(state.quotes, action.groups),
      [action.group]: {}
    }
  }

  if (action.sumchanged) return {}
  
  if (action.againInitialized) return {}

  return state.quotes
}

const toggleAddon = (addon, addons) => {
    if (!addon || (Array.isArray(addon) && !addon[0])) return []
    if (!addons[0]) return [ addon ]
    if (!addons.includes(addon)) return [ ...addons, addon ]
    else {
        if (addon.isCompulsory) return addons
        else return addons.filter(_addon => addon != _addon)
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'RESET_USER':
            return {
                ...state,
                user: {
                    ...state.user,
                    init: false,
                    name: undefined,
                    number: undefined,
                    email: undefined,
                }
            }
        case 'INIT_USER':
            return {
                ...state,
                user: {
                    ...state.user,
                    ...action.user
                }
            }
        case 'EDIT_FILTER':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    [action.filter.type]: {
                        ...state.filters[action.filter.type],
                        ...action.filter
                    }
                }
            }
        case 'RESET_FILTERS':
            return {
                ...state,
                filters: {
                    ...INITIAL_STATE.filters,
                    members: state.filters.members,
                    insurers: {
                        list: state.filters.insurers.list,
                        selected: []
                    }
                }
            }
        case 'GET_HEALTH_INSURERS_LIST':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    insurers: {
                        ...state.filters.insurers,
                        list: action.insurers
                    }
                }
            }
        case 'TOGGLE_INSURER':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    insurers: {
                        ...state.filters.insurers,
                        selected: toggleInsurers(action.insurer, state.filters.insurers.selected)
                    }
                }
            }
        case 'TOGGLE_GROUP':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    members: {
                        ...state.filters.members,
                        selected: {
                            ...state.filters.members.selected,
                            group: action.group
                        }
                    }
                }
            }
        case 'TOGGLE_COMBO':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    members: {
                        ...state.filters.members,
                        selected: {
                            ...state.filters.members.selected,
                            combo: getSelectedCombos(state.filters.members, action.group.id, action.group.combo)
                        }
                    }
                }
            }
        case 'RESET_QUOTES':
            if (!action.group) {
                return {
                    ...state,
                    quotes: {},
                    quoteDetails: {}
                }
            } else {
                return {
                    ...state,
                    quotes: {
                        ...state.quotes,
                        [action.group]: []
                    },
                    quoteDetails: {}
                }
            }
        case 'GET_QUOTES':
            return {
                ...state,
                quotes: initQuotes(state, action),
                quoteDetails: {}
            }
        case 'GOT_QUOTE':
            return {
                ...state,
                quotes: {
                    ...state.quotes,
                    ...getGroupQuotes(state.quotes, action.quote, action.group, action.combo)
                }
            }
        case 'EDIT_CART':
            return {
                ...state,
                cart: {
                    ...state.cart,
                    ...action.cart,
                    addons: toggleAddon(action.cart.addons, state.cart.addons)
                }
            }
        case 'GET_QUOTE_DETAILS':
            if (!action.details) return state
            
            return {
                ...state,
                quoteDetails: {
                    ...state.quoteDetails,
                    [action.details.productId]: action.details
                }
            }
        default:
            return state
    }
}
