import '../styles/static.scss'
import 'perfect-scrollbar/css/perfect-scrollbar.css'

import axios from 'axios'
import PS from 'perfect-scrollbar'
import { values } from 'lodash'
import Flickity from 'flickity'
import { enableScroll, disableScroll } from './utils/helpers'
import { notEmail, notMobile, notString } from './utils/validations'

window.requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})()

const scrollToY = (scrollTargetY, speed, easing) => {
    // scrollTargetY: the target scrollY property of the window
    // speed: time in pixels per second
    // easing: easing equation to use

    var scrollY = window.scrollY,
        scrollTargetY = scrollTargetY || 0,
        speed = speed || 2000,
        easing = easing || 'easeOutSine',
        currentTime = 0;

    // min time .1, max time .8 seconds
    var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));

    // easing equations from https://github.com/danro/easing-js/blob/master/easing.js
    var PI_D2 = Math.PI / 2,
        easingEquations = {
            easeOutSine: function (pos) {
                return Math.sin(pos * (Math.PI / 2));
            },
            easeInOutSine: function (pos) {
                return (-0.5 * (Math.cos(Math.PI * pos) - 1));
            },
            easeInOutQuint: function (pos) {
                if ((pos /= 0.5) < 1) {
                    return 0.5 * Math.pow(pos, 5);
                }
                return 0.5 * (Math.pow((pos - 2), 5) + 2);
            }
        };

    // add animation loop
    function tick() {
        currentTime += 1 / 60;

        var p = currentTime / time;
        var t = easingEquations[easing](p);

        if (p < 1) {
            requestAnimFrame(tick);

            window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
        } else {
            window.scrollTo(0, scrollTargetY);
        }
    }

    // call it once to get started
    tick();
}

const dropDown = {
    dds: document.querySelectorAll('.dropdown'),
    ps: [],
    selected: {},
    events() {
        this.dds.forEach(dd => {
            dd.addEventListener('mousedown', e => this.handleDdClick(e, dd))
            dd.addEventListener('touchstart', e => e.stopPropagation())
            this.handleItemSelect(dd)
            let input = dd.querySelector('input')
            if (input) {
                input.addEventListener('mousedown', e => e.stopPropagation())
                input.addEventListener('input', () => this.handleSearch(input.value, dd))
            }
            this.selected[dd.classList[1]] = undefined
        })
        ;['mousedown', 'touchstart'].forEach(evt => document.addEventListener(evt, () => this.closeAll()))
        this.psInit()
    },
    psInit() {
        this.dds.forEach(
            dd =>
                this.ps.push[
                    new PS(dd.querySelector('ul'), {
                        minScrollbarLength: 20
                    })
                ]
        )
    },
    psUpdate() {
        this.ps.forEach(ps => ps.update())
        this.dds.forEach(dd => (dd.querySelector('ul').scrollTop = 0))
    },
    handleSearch(value, dd) {
        if (value) {
            dd.querySelectorAll('ul li').forEach(li => {
                let length = value.length
                let match = li
                    .getAttribute('city')
                    .slice(0, length)
                    .toLowerCase()
                match === value.toLowerCase() ? li.classList.remove('hide') : li.classList.add('hide')
            })
            this.psUpdate()
        } else {
            dd.querySelectorAll('ul li').forEach(li => li.classList.remove('hide'))
            this.psUpdate()
        }
    },
    handleItemSelect(dd) {
        dd.addEventListener('mousedown', function(event) {
            if (event.target.tagName.toLowerCase() === 'li') {
                dd.querySelector('label').innerHTML = event.target.getAttribute('value') || `${event.target.getAttribute('city')} (${event.target.getAttribute('state')})`
                dd.classList.add('selected')
                dd.classList.remove('error')
                if (event.target.getAttribute('value')) {
					let value = event.target.getAttribute('value')
					dropDown.selected[dd.classList[1]] = value
					dropDown.isSelected(undefined, value)
                }
                if (event.target.getAttribute('city')) {
					let city = {
                        city: event.target.getAttribute('city'),
                        state: event.target.getAttribute('state'),
                        pincode: event.target.getAttribute('pincode')
                    }
					dropDown.selected[dd.classList[1]] = city
					dropDown.isSelected(city , undefined)
                }
			}
		})
    },
    handleDdClick(e, dd) {
        e.stopPropagation()
        dd.classList.remove('error')
        dd.classList.toggle('open')
        setTimeout(() => dd.querySelector('input') && (dd.querySelector('input').value = ''), 100)
        setTimeout(() => dd.querySelectorAll('ul li').forEach(li => li.classList.remove('hide')), 300)
        // Close other dds when one dd is opened
        this.dds.forEach(_dd => _dd != dd && _dd.classList.remove('open'))
    },
    closeAll() {
        this.dds.forEach(dd => dd.classList.remove('open'))
    },
    addDynamicData(type, dd, data) {
        let existing = Array.from(dd.querySelectorAll('li')).map(li => {
            li.classList.add('default')
            return li.getAttribute('city').toLowerCase()
        })

        let dynamicList = document.createDocumentFragment()
        //Search not working properly on backspacing keyword
        if (type == 'cities') {
            data.forEach(city => {
                if (!existing.includes(city[2].toLowerCase())) {
                    let li = document.createElement('li')
                    li.setAttribute('city', city[2])
                    li.setAttribute('state', city[1])
                    li.setAttribute('pincode', city[0])
                    let text = document.createElement('span')
                    text.innerHTML = `${city[2]} (${city[1]})`
                    li.appendChild(text)
                    li.className = 'dynamic'
                    dynamicList.appendChild(li)
                }
            })
        }
        dd.querySelector('ul').appendChild(dynamicList)
        this.psUpdate()
    },
    isSelected(city, gender) {
		
		let dropdowns = values(this.dds)
        let ddCity = dropdowns.filter(dd => dd.className.includes('city'))
		let ddGender = dropdowns.filter(dd => dd.className.includes('gender'))

        if (city) {
			ddCity.forEach(dd => {
				dd.querySelector('label').innerHTML = `${city.city} (${city.state})`
				dd.classList.add('selected')
				dropDown.selected['city'] = {
					city: city.city,
					state: city.state,
					pincode: city.pincode
				}
			})
        }

        if (gender) {
			ddGender.forEach(dd => {
				dd.querySelector('label').innerHTML = gender
				dd.classList.add('selected')
				dropDown.selected['gender'] = gender
			})
        }
    },
    init() {

        if (!this.dds[0]) return
		this.events()
		let city = JSON.parse(sessionStorage.getItem('city'))
		let gender = JSON.parse(sessionStorage.getItem('gender'))
        this.isSelected(city, gender)
    }
}

const formOps = (form = document.querySelector('form')) => {
        let gender =  form.querySelector('.gender')
        let city = form.querySelector('.city')
        let name = form.querySelector('form .name input')
		let email = form.querySelector('form .email input')
		let phone = form.querySelector('form .phone input')
        let submit = form.querySelector('button')
        let dds = form.querySelectorAll('.dropdown')
        let modal = document.querySelector('section.modal')
        let closeModal = modal && modal.querySelector('.close')
		
		let handleSubmit = e => {
            e.preventDefault()
			!isInvalid() && submitForm()
        }
		
		let isInvalid = () => {
            let areDddsInvalid =  Array.from(dds).map(dd => {
					!dd.classList.contains('selected') && dd.classList.add('error')
					return dd.classList.contains('selected')
				}).includes(false)

			if (email && phone && name) {
				let isEmailInvalid = email.value && !notEmail(email.value) ? false : true
				if (isEmailInvalid) email.parentElement.classList.add('error')
				else email.parentElement.classList.remove('error')

				let isPhoneInvalid = phone.value && !notMobile(phone.value) ? false : true
				if (isPhoneInvalid) phone.parentElement.classList.add('error')
                else phone.parentElement.classList.remove('error')
                
                let isNameInvalid = name.value && !notString(name.value) ? false : true
				if (isNameInvalid) name.parentElement.classList.add('error')
				else name.parentElement.classList.remove('error')
					
				return (
                    areDddsInvalid || isEmailInvalid || isPhoneInvalid || isNameInvalid ? true : false
                )
			}

			return areDddsInvalid
        }
		
		let submitForm = () => {
            if (dropDown.selected['gender'] && dropDown.selected['city']) {
                sessionStorage.setItem('gender', JSON.stringify(dropDown.selected['gender']))
			    sessionStorage.setItem('city', JSON.stringify(dropDown.selected['city']))
            }
			if (email && phone && name) {
				sessionStorage.setItem('email', email.value)
                sessionStorage.setItem('number', phone.value)
                sessionStorage.setItem('name', name.value)
                modal.classList.add('active')
                disableScroll()
                
			} else {
                window.location = '/health'
            }
		}
		
        let events = () => {
            form.addEventListener('submit', e => handleSubmit(e))
            closeModal && closeModal.addEventListener('mousedown', () => {
                if (modal) {
                    modal.classList.remove('active')
                    enableScroll()
		            setTimeout(() => enableScroll(), 1000)
                }
            })
            document.addEventListener('keydown', e => {
                if (e.which == 27 && modal) {
                    modal.classList.remove('active')
                    enableScroll()
		            setTimeout(() => enableScroll(), 1000)
                }
            })
		}
		
        let fetchCityData = async() => {
            let cities = (await axios.get('/api/getCities?cityName=')).data
            return cities
		}

		let isSelected = (_email, _number, _name) => {
			if (_email && email) email.value = _email
            if (_number && phone ) phone.value = _number
            if (_name && name ) name.value = _name
		}
		
        let init = async() => {
            events()
            let cities = await fetchCityData()
			city && dropDown.addDynamicData('cities', city, cities)
			let email = sessionStorage.getItem('email')
            let number = sessionStorage.getItem('number')
            let name = sessionStorage.getItem('name')
			isSelected(email, number, name)
		}
		
		init()
}

const detectScroll = {
    doc: document.documentElement || document.body || document.body.parentNode,
    nav: document.querySelector('nav'),
    events() {
        window.addEventListener('scroll', () => this.toggleNav())
    },
    toggleNav() {
		
        let nav = this.nav.classList
        if (parseInt(this.doc.scrollTop || window.pageYOffset) > 0) nav.add('scrolled')
        else nav.remove('scrolled')
    },
    init() {
        this.events()
    }
}

const carousel = {
    carousels: document.querySelectorAll('.carousel'),
    initCarousels: [],
    options: {
        cellSelector: '.cell',
        contain: true,
        initialIndex: 0,
        accessibility: true,
        groupCells: true,
        wrapAround: true,
        adaptiveHeight: true
	},
	handleContentLength(carousel) {
		if ( window.innerWidth < 1100) {
			values(carousel.querySelectorAll('p')).forEach(p => {
				if (p.innerHTML.split(' ').length > 35) {
					p.innerHTML = `${p.innerHTML.split(' ').slice(0,35).join(' ')}...`
				}
			})
		}
	},
    init() {
		this.carousels.forEach(carousel => {
			this.initCarousels.push(new Flickity(carousel, this.options))
			this.handleContentLength(carousel)
		})
    },
    update() {
        this.initCarousels.forEach(carousel => carousel.resize())
    }
}

let insurers = {
    nodes: {
        main: document.querySelector('section .insurers'),
        logos: values(document.querySelectorAll('.insurers .logo'))
    },
    methods: {
        init() {
            insurers.methods.load()
        },
        load() {
            insurers.nodes.logos.forEach((logo, index) => {
                let overlay = require(`../images/insurers/download-copy-${index}.png`)
                logo.querySelector('.overlay').style.backgroundImage = `url(${overlay})`
            })
        }
    },
    init() {
        if (this.nodes.main) {
            this.methods.init()
        }
    }
}

let navMobile = {
	opener: document.querySelector('nav .hamburger'),
	closer: document.querySelector('nav .close'),
	menu: document.querySelector('nav .nav-mobile'),
	open() {
		this.menu.classList.add('active')
		disableScroll()
	},
	close() {
		this.menu.classList.remove('active')
		enableScroll()
		setTimeout(() => enableScroll(), 1000)
	},
	init() {
		this.opener.addEventListener('mousedown', () => this.open())
		this.closer.addEventListener('mousedown', () => this.close())
		document.addEventListener('keydown', e => e.which == 27 && this.close())
	}
}

document.querySelector('.toTop') && document.querySelector('.toTop').addEventListener('mousedown', () => scrollToY(0, 2000))

dropDown.init()
values(document.querySelectorAll('form')).forEach(form => formOps(form))
detectScroll.init()
carousel.init()
setTimeout(() => carousel.update(), 2000)
insurers.init()
navMobile.init()
