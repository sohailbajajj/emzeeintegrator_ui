import { throttle } from 'lodash'

export const stopBubbling = event => {
    if (event) {
        event.preventDefault && event.preventDefault()
        event.stopPropagation && event.stopPropagation()
        event.nativeEvent && event.nativeEvent.stopImmediatePropagation && event.nativeEvent.stopImmediatePropagation()
    }
}

let _disableScroll = event => {
    stopBubbling(event)
    let doc = document.documentElement || document.body || document.body.parentNode
	let body = document.querySelector('body')
	let html = document.querySelector('html')

    let scroll = doc.scrollTop || window.pageYOffset
    let top = body.style.top

    body.style.top = `-${parseInt(body.style.top) || parseInt(scroll)}px`
	body.style.position = 'fixed'
	html.style.position = 'fixed'
}

export const disableScroll = throttle(_disableScroll, 100)

export const enableScroll = event => {
    let doc = document.documentElement || document.body || document.body.parentNode
	let body = document.querySelector('body')
	let html = document.querySelector('html')

	body.style.removeProperty('position')
	html.style.removeProperty('position')
    body.style.top && (doc.scrollTop = parseInt(Math.abs(parseInt(body.style.top))))
	body.style.removeProperty('top')
}

export const generateQuotesRequest = (proposer, members, diseases, pincode, sum) => {

    const getRelation = member => {
		let { type, name } = member

		if (name == 'you') return 'proposer'
		if (type != 'child') return type
		if (type == 'child') return name
	}
	
	const getDiseases = member => {
		const diseaseMap = _diseases => {
			_diseases = _diseases.map(_disease => _disease.toLowerCase())
			return {
				hyperTension: _diseases.includes('hypertension'),
				highCholestral: _diseases.includes('high cholesterol'),
				asthma: _diseases.includes('asthma'),
				thyroidDisorder: _diseases.includes('thyroid disorder'),
				diabetes: _diseases.includes('diabetes'),
				heartAilments: _diseases.includes('heart ailments')
			}
		}

		if (!diseases) return { isPEDExist: false }
		else {
			let { name, type, id } = member
			let _diseases = undefined
			if (type != 'child') _diseases = diseases[type]
			else _diseases = diseases.children[id] || []
			if (_diseases.length == 0) return { isPEDExist: false }
			return {
				isPEDExist: true,
				diseasesinfo: diseaseMap(_diseases)
			}
		}
	}

    const getInsuredMember = member => {
        return {
			gender: member.gender,
			age: member.age.value,
			relation: getRelation(member),
			...getDiseases(member)
        }
	}
	
	const getProposer = () => {
		if (!proposer) {
			return {
				gender: JSON.parse(sessionStorage.getItem('gender')),
				postalCode: pincode,
				firstname: "getQuotes",
				lastname: "getQuotes",
				mobileno: 9999999999,
				emailid: "getQuotes@getQuotes.getQuotes"
			}
		} else return proposer
	}

    const init = () => {
        return {
			proposer: getProposer(),
			insured: [ ...members.map(member => getInsuredMember(member)) ],
			verticalId: 1,
			sumInsuredRange: sum || "300000-600000"
		}
    }

    return init()
}
