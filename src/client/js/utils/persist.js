export const loadState = () => {
	try {
		const serializeState = sessionStorage.getItem('state')
		if (serializeState === null) {
			return {}
		}
		return JSON.parse(serializeState)
	}
	catch (err) {
		return undefined
	}
}

export const saveState = (state) => {
	try {
		const serializeState = JSON.stringify(state)
		sessionStorage.setItem('state', serializeState)
	}
	catch (err) { }
}