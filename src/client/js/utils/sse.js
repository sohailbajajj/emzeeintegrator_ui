import { SSE } from 'sse.js'
import { gotQuote } from '../actions/quotes'

const connect = (store, request, group, combo) => {

    let source = new SSE('/api/gqs',
        {
            headers: { 'Content-Type': 'application/json' },
            'withCredentials': true,
            payload: request,
            method: 'POST'
        }
    )

    source.onmessage = event => handleQuote(store, JSON.parse(event.data), group, combo)
    source.onerror = error => console.log(error)
    source.stream()
}

const handleQuote = (store, quote, group, combo) => {
	try {
		quote.insurer && store.dispatch(gotQuote({ quote, group, combo }))
	}
	catch (error) {
		console.log(error)
	}
}

export default store => next => action => {
    let { request, group, combo, type } = action
    type == 'GET_QUOTES' && connect(store, request, group, combo)
    return next(action)
}