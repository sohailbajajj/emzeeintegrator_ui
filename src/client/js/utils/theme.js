import facepaint from 'facepaint'

const media = facepaint([
  '@media(min-width: 1401px)',
  '@media(max-width: 1400px)',
  '@media(max-width: 1200px)',
  '@media(max-width: 400px)'
], { literal: true })

export const theme = {
  blue: '#5267ff',
  green: '#00ffc8',
  purple: '#262f96',
  copyDark: '#252736',
  copyLight: '#65747c',
  copyLighter: '#96a9b3',
  border: '#d9e6ec',
  disable: '#f9fbfc',
  shadow: '0px 0px 12px -4px rgba(134, 134, 134, 0.75)',
  ddHover: '#f6f7ff',
  tooltip: '#1b272f',
  red: '#ff5a5a',
  absoluteCenter,
  verticalCenter,
  gutter: {
	  desktopL: '10rem',
	  desktop: '5rem'
  },
  fullWidth,
  requirementsBg: '#fcfdfd',
  media,
  applyGutter,
  smoothen: ``
}

function absoluteCenter() {
  return `
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		left:0;
		right:0;
		margin:0 auto;
	`
}

function verticalCenter() {
  return `
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
	`
}

function applyGutter() {
  return media({
    width: [`calc(100% - 20rem)`,`calc(100% - 10rem)`,`calc(100% - 20px)`,`calc(100% - 10px)`],
    margin: `0 auto`
  })
}

function fullWidth() {
	return media({
		width: `100%`,
		padding: [`0 ${this.gutter.desktopL}`,`0 ${this.gutter.desktop}`,`0 20px`,`0 10px`],
		marginLeft: [`-${this.gutter.desktopL}`,`-${this.gutter.desktop}`,`-20px`,`-10px`]
	})
}
