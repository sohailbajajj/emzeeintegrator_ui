
export const notMobile = value => value && /^[789]\d{9}$/.test(value) ? false : 'Invalid mobile number'

export const notEmail = value => value && /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
  	false : 'Invalid email address'

export const notString = value => value && /^[a-zA-Z()]+$/.test(value) ? false : 'Must only have alphabets'

export const notName = value => value && /^[a-zA-Z][a-zA-Z ]+$/.test(value) ? false : 'Must be a string'