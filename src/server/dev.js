import express from 'express'
import path from 'path'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import webpackConfig from '../../webpack.dev.config.js'
import proxy from 'http-proxy-middleware'


const app = express()

const config = {
	src: path.join(__dirname, '../'),
	port: process.env.PORT || 9000
}

const webpackCompiler = webpack(webpackConfig)

app.use(webpackDevMiddleware(webpackCompiler, {
	publicPath: webpackConfig.output.publicPath,
	hot: true,
	historyApiFallback: true,
	quiet: false,
	stats: {
		colors: true,
		warnings: false,
		minimal: true,
		chunks: false,
		assets: false,
		modules: false
	}
}))

app.use(webpackHotMiddleware(webpackCompiler, {
	heartbeat: 10 * 1000,
	log: console.log,
}))

app.use('*', proxy({
	target: `http://localhost:9001`,
	changeOrigin: true,
	timeout: 0
}))

app.listen(config.port, (err) => {
	if (err) {
		return console.error(err);
	}
	console.log(`Webpack server running on port ${config.port}.`)
})