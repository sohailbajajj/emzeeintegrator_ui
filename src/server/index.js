import express from 'express'
import path from 'path'
import fs from 'fs'
import handlebars from 'express-handlebars'
import robots from 'express-robots'
import compression from 'compression'
import helmet from 'helmet'
import axios from 'axios'

import ApiRoutes from './routes/api'

const app = express()

// ** Config ** 

const config = {
	dev: process.env.NODE_ENV == ('dev' || 'development'),
	src: path.join(__dirname, '../'),
	manifest: path.join(__dirname, '../../dist/manifest.json'),
	port: process.env.PORT || 9001,
	api: {
		dev: 'http://192.168.1.38:8093',
		production: 'http://localhost:8092',
		testing: 'http://testing.zipcover.in:8094'
	}
}

const manifest = fs.existsSync(config.manifest) ? require(config.manifest) : undefined

const hbs = handlebars.create({
	extname: '.hbs',
	layoutsDir: path.join(config.src, 'client', 'views', 'layouts'),
	defaultLayout: 'main',
	partialsDir: path.join(config.src, 'client', 'views', 'partials'),
	helpers: {
		parseJSON: (data, options) => options.fn(JSON.parse(data)),
		parseCities: function() {
			return `<li city=${this[2]} state=${this[1]} pincode=${this[0]}><span>${this[2]} (${this[1]})</span></li>`
		} 
	}
})

app.set('views', path.join(config.src, 'client', 'views'))

app.engine('hbs', hbs.engine)

app.set('view engine', 'hbs')

app.use(robots(path.join(config.src, 'config', 'robots.txt')))

app.get('/sitemap.xml', (req, res) => res.sendFile(path.join(config.src, 'config', 'sitemap.xml')))

app.use('/assets', compression())

app.use('/assets', express.static(path.join(config.src,'../dist')))

app.use(helmet())

// ** Config **

app.use('/api', ApiRoutes(config.api[process.env.NODE_ENV]))

app.get('/', (req, res) => {
	res.render('index', { 
		script: config.dev ? 'static.js' : manifest && manifest['static.js'], 
		styles: config.dev ? undefined : 'static'
	})
})

app.get('/landing', (req, res) => {
	res.render('landing', { 
		script: config.dev ? 'static.js' : manifest && manifest['static.js'], 
		styles: config.dev ? undefined : 'static'
	})
})
app.get('/insurance-health', (req, res) => {
	res.render('health', { 
		script: config.dev ? 'static.js' : manifest && manifest['static.js'], 
		styles: config.dev ? undefined : 'static'
	})
})

app.get('/health', (req, res) => {
	res.render('app', { 
		script: config.dev ? 'bundle.js' : manifest && manifest['bundle.js'],
		styles: config.dev ? undefined : 'bundle',
		app: true
	})
})

app.get('/health/*', async(req, res) => {
	
	let appState = undefined
	
	if (req.query.appStateId) {
		try {
			appState = (await axios.get(`${config.api[process.env.NODE_ENV]}/emzee/api/findRequest?appStateId=${req.query.appStateId}`)).data
		} catch(err) { 
			appState = undefined 
		}
	}

	res.render('app', { 
		script: config.dev ? 'bundle.js' : manifest && manifest['bundle.js'],
		styles: config.dev ? undefined : 'bundle',
		appState: appState ? JSON.stringify(appState) : undefined,
		app: true 
	})
})

app.listen(config.port, () => {
	console.log(`Server running on port ${config.port}.`)
})
	
