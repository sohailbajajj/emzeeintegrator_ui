import express from 'express'
import proxy from 'http-proxy-middleware'

let Router = express.Router()

const Routes = apiURL => Router
	.use('/getCities', proxy({
		target: `${apiURL}/emzee/api/getLocationData`,
		changeOrigin: true,
		pathRewrite: {
			'^/api/getCities': '/'
		},
		timeout: 0
	}))

	.use('/ghil', proxy({
        target: `${apiURL}/emzee/h1/gil`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/ghil': ''
        },
        timeout: 0
	}))
	
	.use('/gqs', proxy({
        target: `${apiURL}/emzee/getQuote`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/gqs': '/'
        },
        timeout: 0
    }))

    .use('/gqd', proxy({
        target: `${apiURL}/emzee/getQuoteDetail`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/gqd': '/'
        },
        timeout: 0
    }))

    .use('/gpf', proxy({
        target: `${apiURL}/emzee/health/getProposalFormElements`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/gpf': '/'
        },
        timeout: 0
    }))

    .use('/findLogo', proxy({
        target: `${apiURL}/emzee/api/findLogo`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/findLogo': '/'
        },
        timeout: 0
    }))

    .use('/findDoc', proxy({
        target: `${apiURL}/emzee/api/findDoc`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/findDoc': '/'
        },
        timeout: 0
    }))

    .use('/share', proxy({
        target: `${apiURL}/emzee/api/shareActivity`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/share': '/'
        },
        timeout: 0
    }))
    .use('/spf', proxy({
        target: `${apiURL}/emzee/getHealthProposal`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/spf': '/'
        },
        timeout: 0
    }))
    .use('/getLocation', proxy({
        target: `${apiURL}/emzee/api/getStarLocation`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/getLocation': '/'
        },
        timeout: 0
    }))
    .use('/getPaymentToken', proxy({
        target: `${apiURL}/emzee/getPaymentToken`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/getPaymentToken': '/'
        },
        timeout: 0
    }))
    .use('/obbc', proxy({
        target: `${apiURL}/emzee/onBuyButtonClick`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/obbc': '/'
        },
        timeout: 0
    }))
    .use('/paymentSuccess', proxy({
        target: `${apiURL}/emzee/paymentSuccess`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/paymentSuccess': '/'
        },
        timeout: 0
    }))
    /*.use('/up', proxy({
        target: `${apiURL}/emzee/api/updateProposal`,
        changeOrigin: true,
        pathRewrite: {
            '^/api/up': '/'
        },
        timeout: 0
    }))*/

export default Routes