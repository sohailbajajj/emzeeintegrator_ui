import webpack from 'webpack'
import autoprefixer from 'autoprefixer'

//Suppress deprication warnings
process.noDeprecation = true

module.exports = {
	mode: 'development',
	devtool: 'eval-source-map',
	entry: {
		bundle: [
			'webpack-hot-middleware/client?reload=true',
			'babel-polyfill',
			'./src/client/js/index.js'
		],
		static: [
            'webpack-hot-middleware/client?reload=true',
            'babel-polyfill',
            './src/client/js/static.js'
        ]
	},
	output: {
		path: '/',
		publicPath: '/assets/',
		filename: '[name].js'
	},
	resolve: {
		extensions: ['.js', '.jsx']
	},
	resolveLoader: {
		moduleExtensions: ['-loader']
	},
	watch: true,
	module: {
		rules: [{
				test: [/\.(jsx|js)$/],
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: {
					cacheDirectory: true,
					plugins: ['react-hot-loader/babel'],
				}
			},
			{
				test: /\.(scss|css)$/,
				use: [{
						loader: "style-loader",
						options: {
							sourceMap: true
						}
					},
					{
						loader: "css-loader",
						options: {
							sourceMap: true
						}
					},
					{
						loader: "sass-loader",
						options: {
							sourceMap: true,
							includePaths: ['src/client/styles']
						}
					}
				]
			},
			{
				test: /\.(jpeg|jpg|png|gif|svg)$/,
				use: [{
					loader: 'file-loader?name=[name].[hash].[ext]',
				}]
			},
			{
				test: /\.(woff|woff2|ttf|eot|otf)$/,
				loaders: ['url-loader?limit=10000']
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
		}),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin()
	]
}