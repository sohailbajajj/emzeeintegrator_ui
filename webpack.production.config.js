import webpack from 'webpack'
import path from 'path'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import autoprefixer from 'autoprefixer'
import CompressionPlugin from 'compression-webpack-plugin'
import WebpackMd5Hash from 'webpack-md5-hash'
import ManifestPlugin from 'webpack-manifest-plugin'
import ImageminPlugin from 'imagemin-webpack-plugin'
import UglifyJSPlugin from 'uglifyjs-webpack-plugin'

module.exports = {
    target: 'web',
    mode: 'production',
    entry: {
        static: ['babel-polyfill', './src/client/js/static.js'],
        bundle: ['babel-polyfill', './src/client/js/index.js']
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: '',
        filename: '[name].[chunkhash].js',
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    resolveLoader: {
        moduleExtensions: ['-loader']
    },
    module: {
        rules: [{
                test: [/\.(jsx|js)$/],
                exclude: /node_modules/,
                loaders: ['babel-loader'],
            },
            {
                test: /\.(scss|css)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                            loader: "css-loader",
                            options: {
                                minimize: true
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                includePaths: ['src/client/styles']
                            }
                        }
                    ]
                })
            },
            {
                test: /\.(jpeg|jpg|png|gif|svg)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[hash].[ext]',
                        publicPath: '/assets/'
                    }
                }]
            },
            {
                test: /\.(woff|woff2|ttf|eot|otf)$/,
                loaders: ['url-loader?limit=10000']
            }
        ]
    },
    optimization: {
        minimizer: [
            new UglifyJSPlugin({
                uglifyOptions: {
                    mangle: true,
                    comments: false, // remove comments
                    compress: {
                        unused: true,
                        dead_code: true, // big one--strip code that will never execute
                        drop_debugger: true,
                        conditionals: true,
                        evaluate: true,
                        drop_console: false, // strips console statements
                        sequences: true,
                        booleans: true,
                    }
                }
            })
        ]
    },
    plugins: [
        new WebpackMd5Hash(),
        new ManifestPlugin(),
        new webpack.DefinePlugin({ // <-- key to reducing React's size
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new ExtractTextPlugin({
            filename: '[name].css'
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
        new CompressionPlugin({
            asset: "[path].gz[query]",
            algorithm: "gzip",
            test: /\.(jsx|js|css)$/,
            threshold: 10240,
            minRatio: 0.8
        }),
        new ImageminPlugin({
            pngquant: {
                quality: '95-100'
            },
            optipng: {
                optimizationLevel: 9
            }
        })
    ]
}